\documentclass[letterpaper,11pt]{article}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\newtheorem{thm}{Theorem}
\newtheorem{cor}[thm]{Corollary}
\newtheorem{lemma}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\theoremstyle{definition}
\newtheorem{defn}[thm]{Definition} 
\newtheorem{convention}[thm]{Convention}
\newtheorem{ex}[thm]{Example}
\newtheorem{exercise}[thm]{Exercise}
\newtheorem{notation}[thm]{Notation}
\newtheorem*{summary}{Summary}
\newtheorem{rmk}[thm]{Remark}
\numberwithin{equation}{thm}

\begin{document}

\LARGE
\textsc{Math} $117$\\ 
\Large
Lecture\\
\large
September $17$, $2014$
\normalsize

\vspace{\baselineskip}

\section*{Slope}

Recall that the \emph{slope} of the line passing through two distinct points $(x_1,y_1)$ and $(x_2,y_2)$ is $m = \frac{y_2-y_1}{x_2-x_1}$.
In particular, it is defined when $x_1\neq x_2$, i.e. when the line is not vertical.

\begin{exercise}
Find the slopes of the lines passing through the following pairs of points:
\begin{enumerate}
\item $(-2,4)$, $(2,-6)$;
\item $(-3,8)$, $(5,8)$;
\item $(-4,-10)$, $(-4,10)$.
\end{enumerate}
\end{exercise}
\begin{proof}[Solution]\
\begin{enumerate}
\item $m=\frac{4-(-6)}{-2-2} = \frac{10}{-4} = -\frac{5}{2}$
\item $m = \frac{8-8}{5-(-3)} = \frac{0}{8} = 0$
\item $m = \frac{10-(-10)}{-4-(-4)} = \frac{20}{0} = \mbox{undefined}$ \qedhere
\end{enumerate}
\end{proof}

\begin{ex}
The slope of the line $L$ given by the function $f(x) = ax+b$, $a,b\in\mathbf{R}$, is $a$. 
Indeed, let $x_1\neq x_2$.
Then we find
\[
m = \frac{f(x_2)-f(x_1)}{x_2-x_1}
= \frac{ax_2+b-(ax_1+b)}{x_2-x_1} 
= \frac{a(x_2-x_1)+(b-b)}{x_2-x_1}
= \frac{a(x_2-x_1)}{x_2-x_1}
= a.
\]
\end{ex}

\begin{exercise}
Find the slope of the line with equation $2x-5y=10$.
\end{exercise}
\begin{proof}[Solution]
We have 
\begin{align*}
2x-5y &= 10 \\
-5y &= -2x+10 \\
y &= \frac{2}{5}x-2
\end{align*}
so, by the previous example, $\frac{2}{5}$ is the slope of the line.
\end{proof}

\begin{exercise}
Graph the line with slope $\frac{4}{3}$ passing through $(-2,-3)$.
\end{exercise}

Note that the slope is the average rate of change in $y$ per unit change in $x$.

\begin{ex}
There were $66.7$ million television subscribers in the USA in $2001$ and $62.9$ million in $2009$. 
Assuming a linear relationship between time and the number of television subscribers in the USA, the average rate of change in the number of subscribers per year is 
\[
\frac{62.9 - 66.7}{2009-2001} = -0.475.
\]
\end{ex}


\section*{Equations of lines}

Consider a line $L$ passing through a point $(x_1,y_1)$ with slope $m$.
If $(x,y)$ is any other point on $L$, then we have
\begin{align*}
\frac{y-y_1}{x-x_1} &= m\\
y-y_1 &= m(x-x_1).
\end{align*}
Here we are tacitly relying on the observation that the slope $m$ is defined, so $L$ is not vertical and hence $x\neq x_1$, i.e. $x-x_1\neq0$.

\begin{defn}
The \emph{point-slope form} of the equation of the line with slope $m$ passing through the point $(x_1,y_1)$ is $\boxed{y-y_1 = m(x-x_1).}$ 
\end{defn}

Observe that this form of the equation allows us to read off the slope $m$ of the line and the point $(x_1,y_1)$ directly.
Also, note that this is indeed the equation of the line we started with because any two lines with the same slope and passing through $(x_1,y_1)$ are equal.

\begin{exercise}
Give an equation of the line passing through $(3,-5)$ with slope $-2$.
\end{exercise}
\begin{proof}[Solution]
In point-slope form, this line is given by $y-(-5) = (-2)(x-3)$ or $y+5 = -2(x-3)$ or $y= -2(x-3)-5$ or $y=-2x+1$.
\end{proof}

\begin{exercise}
Give an equation of the line passing through $(-4,3)$ and $(5,-1)$ in standard form $Ax+By=C$.
\end{exercise}
\begin{proof}[Solution]
First, we compute the slope:
\[
m = \frac{3-(-1)}{-4-5} = -\frac{4}{9}.
\]
Choosing the first point $(x_1,y_1) = (-4,3)$, the point-slope form of the equation is
$ y-3 = -\frac{4}{9}(x-(-4))$, which gives us 
$ \frac{4}{9}x+y = \frac{11}{9}$ or, multiplying through by $9$, $4x+9y = 11$.
\end{proof}

In the special case where $(x_1,y_1) = (0,b)$, the point-slope form gives us $y-y_1=y-b$ and $m(x-x_1) = m(x-0) = mx$, so $y-y_1 = m(x-x_1)$ gives us $y-b = mx$, hence $y = mx+b$.

\begin{defn}
The \emph{slope-intercept form} of the equation of the line with slope $m$ and $y$-intercept $b$ is $\boxed{y = mx+b.}$
\end{defn}

This form of the equation allows us to read off the slope and the point at which the line intersects the $y$-axis directly.

\begin{exercise}
Find the slope and $y$-intercept of the line with equation $3x-4y = 12$.
\end{exercise}
\begin{proof}[Solution]
We could either compute the $y$-intercept and slope directly or, somewhat more efficiently, just rewrite the equation in point-slope form:
\begin{align*}
3x-4y &= 12  \\
-4y &= -3x +12 \\
y &= \frac{3}{4}x-3,
\end{align*}
which tells us that the $y$-intercept is $\boxed{b=-3}$ and the slope is $\boxed{m=\frac{3}{4}.}$
\end{proof}

\begin{exercise}
Find the slope-intercept form of the equation for the line passing through $(-2,4)$ and $(2,2)$, then graph it.
\end{exercise}
\begin{proof}[Solution]
First, compute the slope:
\[
m = \frac{4-2}{-2-2} = \frac{2}{-4} = -\frac{1}{2}.
\]
Now, the slope-intercept form of the equation is
\[
y = -\frac{1}{2}x +b
\]
where $b$ is the $y$-intercept, which we can determine by remarking that $(-2,4)$ is on the line so we have
\[
4 = -\frac{1}{2} \cdot (-2) +b \quad\Rightarrow\quad 
3=b.
\]
So the slope-intercept form of the equation is $\boxed{y = -\frac{1}{2}x+3.}$
\end{proof}

\section*{Vertical, horizontal, parallel and perpendicular lines}

\textbf{Facts:}
\begin{enumerate}
\item The equation of a vertical line passing through $(a,b)$ is $x=a$.
\item The equation of a horizontal line passing through $(a,b)$ is $y=b$.
\item Two nonvertical lines are parallel if and only if they have the same slope.
\item Let $L_1$ and $L_2$ be two nonvertical lines with slopes $m_1$ and $m_2$, respectively. Then $L_1$ is perpendicular to $L_2$ if and only if $m_1m_2 = -1$, i.e. if and only if $m_1 = -\frac{1}{m_2}$.
\end{enumerate}

\begin{exercise}
Find the slope-intercept forms of the equations for the following lines:
\begin{enumerate}
\item the line passing through $(2,-4)$ parallel to the line $3x-2y=5$;
\item the line passing through $(2,-4)$ perpendicular to the line $3x-2y=5$.
\end{enumerate}
\end{exercise}
\begin{proof}
[Solution]\
\begin{enumerate}
\item First, rewrite $3x-2y=5$ in slope-intercept form to find $y=\frac{3}{2}x-\frac{5}{2}$.
The slope of this line, and hence that of the line we're looking for, is $\frac{3}{2}$.
Since the line passes through $(2,-4)$ with slope $\frac{3}{2}$, to determine the $y$-intercept $b$, we have 
\[
-4 = \frac{3}{2}\cdot2+b \quad\Rightarrow\quad b = -7,
\]
so the slope-intercept form of the equation is $\boxed{y = \frac{3}{2}x-7.}$
\item 
\end{enumerate}
\end{proof}

\end{document}
