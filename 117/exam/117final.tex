\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, polynom, sectsty}
\usepackage[margin=1in]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newtheorem{exercise}{Exercise}

%\runningheader{}{}{\oddeven{Name: \underline{\hspace{3in}}}{}}
\renewcommand{\thequestion}{\textbf{\arabic{question}}}
\renewcommand{\thepartno}{\thequestion\textbf{\alph{partno}}}
\renewcommand{\thechoice}{\alph{choice}}
\renewcommand{\choicelabel}{(\thechoice)}
\CorrectChoiceEmphasis{\color{red}\bfseries}

\begin{document}

%\printanswers

\Large
\textsc{Math} $117$
\hfill
\normalsize Name:\enspace\hrulefill 
\\
Final exam
\hfill
USC ID number:\enspace\hrulefill
\\
December $10$, $2014$\\
\mbox{}
\hfill
Signature:\enspace\hrulefill

\begin{center}
\fbox{\fbox{\parbox{5.5in}{
\begin{itemize}
\item Fill in your name, USC ID number and signature above
\item You have $120$ minutes to complete the exam
\item Circle your final answers
\item Show all work for full credit
\item To receive maximal partial credit, write legibly and intelligibly, using plain English to explain your thought process
\item You may detach the last sheet to use as scrap paper and you may request more scrap paper as needed
\item Textbooks, notes, calculators and electronic devices are not permitted
\end{itemize}
}}}
\end{center}
\addpoints

\begin{center}
\gradetable
\end{center}

\clearpage

\begin{questions}

\question[20]
Find the solution set of the following inequality:
\[
\left|\frac{x+1}{x+5}\right| \geq 1
\]

\begin{solution}[5in]
Solving the abolute value inequality is equivalent to finding the values of $x$ such that $\frac{x+1}{x+5} \geq 1$ or $\frac{x+1}{x+5}\leq -1$.
\[
\frac{x+1}{x+5} \geq 1 
\Rightarrow
\frac{x+1-(x+5)}{x+5} \geq 0 
\Rightarrow
\frac{-4}{x+5} \geq 0
\]
So the solutions in this case are in $(-\infty,-5)$.
\[
\frac{x+1}{x+5} \leq -1
\Rightarrow
\frac{x+1+(x+5)}{x+5} \leq 0 
\Rightarrow
\frac{2x+6}{x+5} \leq 0
\]
To solve the last inequality, we note that $x+5=0$ implies $x=-5$ and $2x+6=0$ implies $x=-3$, so we have the table

\begin{center}
\begin{tabular}{c|c|c}
\text{interval} & $x$ & $\frac{2x+6}{x+5}$ \\
\hline
$(-\infty,-5)$ & $-6$ & $>0$ \\
$(-5,-3]$ & $-4$ & $\leq0$ \\
$[-3,\infty)$ & $0$ & $>0$ 
\end{tabular}
\end{center}
Thus, the solutions in this case are $(-5,-3]$.
Combining the two cases, the solution of the original inequality are {\color{red}$(-\infty,-5)\cup(-5,-3]$}.
\end{solution}

\question
Consider two distinct points $P$ and $R$ in the $xy$-plane.
Let $Q$ be the midpoint of the line segment joining $P$ and $R$ and let $L$ be the line passing through $P$ and $R$.
The coordinates of $Q$ are $(-1,2)$ and those of $R$ are $(3,6)$.
\begin{parts}
\part[5] What is the distance $d(P,R)$ from $P$ to $R$?

\begin{solution}[2in]
\[
d(P,R) 
=
2d(Q,R) 
= 
2\sqrt{(-1-3)^2+(2-6)^2}
=
2\sqrt{16+16}
=
2\sqrt{32}
=
{\color{red}8\sqrt{2}}.
\]
\end{solution}

\clearpage

\part[5] What are the coordinates of $P$?

\begin{solution}[2in]
Let $(x,y)$ denote the coordinates of $P$.
Since $Q$ is the midpoint between $P$ and $R$, we have
$
(-1,2) 
=
\Big(\frac{x+3}{2},\frac{y+6}{2}\Big),
$
so
{\color{red}$P=(-5,-2)$}.
\end{solution}

\part[5] Give an equation of the line perpendicular to $L$ containing $R$. 

\begin{solution}[2in]
Since $L$ contains $Q$ and $R$, the slope of $L$ is $m=\frac{6-2}{3-(-1)}=1$.
The slope of any line perpendicular to $L$ is therefore $-\frac{1}{1}=-1$.
If such a perpendicular line contains $R$, then the point-slope form of its equation is {\color{red} $y-6=-(x-3)$}.
\end{solution}

\end{parts}

\question[10]
Determine the center and radius of the circle described by the following equation:
\[
3x^2+3y^2+12x-18y -1=152
\]

\begin{solution}[3in]
\begin{align*}
3x^2+3y^2+12x-18y 
&=
153 \\
x^2+y^2+4x-6y 
&=
51 \\
\Big(x^2+4x+\big(\frac{4}{2}\big)^2\Big) + \Big(y^2-6y+\big(\frac{-6}{2}\big)^2\Big)
&=
51+\big(\frac{4}{2}\big)^2+\big(\frac{-6}{2}\big)^2\\
(x+2)^2 + (y-3)^2
&=
64
\end{align*}
Hence, the {\color{red}center is $(-2,3)$ and the radius is $\sqrt{64}=8$}.
\end{solution}

\clearpage

\question
Consider the function $f(x) = 2^{x+3}-1$.
\begin{parts}
\part[5] Sketch the graph of $f$, identifying any asymptotes and $x$- and $y$-intercepts.

\begin{solution}[4in]
The function $f$ is obtained from $g(x) = 2^x$ by horizontal translation to the left by $3$ and vertical translation down by $1$, i.e. $f(x)=g(x+3)-1$.
The function $g$ has a horizontal asymptote at $y=0$, so $f$ has a {\color{red}horizontal asymptote at $y=-1$}.
Moreover, $f$ has the {\color{red}$y$-intercept $f(0)=2^{0+3}-1=7$}.
Solving $0=f(x)$, we have
\[
0=2^{x+3}-1 
\Rightarrow
1=2^{x+3}
\Rightarrow
\log_2(1) =x+3 
\Rightarrow
0 = x+3 
\Rightarrow
x=-3,
\]
so the {\color{red}$x$-intercept is $-3$}.

\begin{center}
\includegraphics[scale=.4]{117finalgraphic1.png}
\end{center}
\end{solution}

\part[5] Show that $f$ is one-to-one.
[You may use the graph from part (i) for this.]

\begin{solution}[2in]
Either note that the graph from (i) passes the horizontal line test or prove that $f$ is one-to-one algebraically as follows:
\begin{align*}
2^{a+3}-1
&=
2^{b+3}-1 \\
2^{a+3} 
&=
2^{b+3} \\
a+3 
&=
b+3 \\
a
&=
b
\end{align*}
\end{solution}

\part[5] Find the inverse function $f^{-1}$.

\begin{solution}[2in]
\begin{align*}
x
&=
2^{y+3}-1 \\
x+1
&= 
2^{y+3} \\
\log_2(x+1)
&=
y+3 \\
\log_2(x+1)-3
&=
y
\end{align*}
so we have {\color{red}$f^{-1}(x)=\log_2(x+1)-3$}. 
\end{solution}

\end{parts}

\question
Consider the function $f(x) = (x-1)(x^4+2x^3-8x^2-10x+15)$.
\begin{parts}
\part[10] Show that $g(x) = x^4+2x^3-8x^2-10x+15$ has no real roots less than $-5$ or greater than $3$.

\begin{solution}[3.9in]
By the Zero Factor Property, it suffices to show that the factor $x^4+2x^3-8x^2-10x+15$ has no such roots.
By the Boundedness Theorem, it suffices to consider the synthetic divisions:
\[
\polyhornerscheme[x=-5]{x^4+2x^3-8x^2-10x+15}
\quad
\polyhornerscheme[x=3]{x^4+2x^3-8x^2-10x+15}
\]
Since the signs in the bottom row of the first synthetic division alternate and those in the bottom row of the second are all positive, we can conclude.
\end{solution}

\part[10] Factor $f(x) = (x-1)(x^4+2x^3-8x^2-10x+15)$. [Hint: The \emph{rational} roots of $f$ are $1$ and $-3$.]

\begin{solution}[3.9in]
We check how many times we can factor $x-1$ and $x+3$ out of $f$.
\[
\polyhornerscheme[x=1]{x^4+2x^3-8x^2-10x+15}
\]
This shows that $f(x) = (x-1)^2(x^3+3x^2-5x-15)$.
\[
\polyhornerscheme[x=-3]{x^3+3x^2-5x-15}
\]
This gives $f(x) = (x-1)^2(x+3)(x^2-5)$ and we factor $x^2-5=(x+\sqrt{5})(x-\sqrt{5})$.
Thus, $f(x)=(x-1)^2(x+3)(x+\sqrt{5})(x-\sqrt{5})$ and the roots of $f$ are {\color{red} $1$, $-3$, $\pm\sqrt{5}$}.
\end{solution}

\clearpage

\part[5] Sketch the graph of $f(x) = (x-1)(x^4+2x^3-8x^2-10x+15)$.

\begin{solution}[3in]
\begin{center}
\includegraphics[scale=.4]{117finalgraphic2.png}
\end{center}
\end{solution}

\end{parts}

\question
Consider the function $f(x) = \frac{3x(x+2)}{x^2-16}$.
\begin{parts}
\part[4] Find all vertical asymptotes of $f$.

\begin{solution}[.75in]
The denominator $x^2-16$ factors as $(x+4)(x-4)$, so the {\color{red}vertical asymptotes are $x=4$ and $x=-4$}.
\end{solution}

\part[4] Find all nonvertical asymptotes of $f$.

\begin{solution}[.75in]
The numerator and denominator are of the same degree with respective leading coefficients $3$ and $1$, so the {\color{red}nonvertical asymptote is $y=3$}.
\end{solution}

\part[4] Find all $x$-intercepts of the graph of $f$.

\begin{solution}[.75in]
The $x$-intercepts are the solutions of $0=3x(x+2)$, that is, {\color{red}$x=0,\ -2$}.
\end{solution}

\part[3] Find all $y$-intercepts of the graph of $f$.

\begin{solution}[.75in]
The $y$-intercept is given by {\color{red}$f(0)=0$}.
\end{solution}

\part[5] Find all points of intersection of the graph of $f$ with its asymptotes.

\begin{solution}[1in]
There is no intersection with the vertical asymptotes.
The intersections with the horizontal asymptote are the solutions of $3=f(x)$:
\[
3=\frac{3x(x+2)}{x^2-16}
\Rightarrow
3(x^2-16) = 3(x^2+2x)
\Rightarrow
-16 = 2x
\Rightarrow
{\color{red} x=-8}.
\]
\end{solution}

\clearpage

\part[5] Sketch the graph of $f$.

\begin{solution}[3in]
\begin{center}
\includegraphics[scale=.4]{117finalgraphic3.png}
\end{center}
\end{solution}

\end{parts}

\question
Simplify the following expressions:
\begin{parts}
\part[5] $\big(\log_5(3^3)\big)\big(\log_3(5^2)\big)$

\begin{solution}[2.5in]
\[
\log_5(3^3)\log_3(5^2)
=
\big(3\log_5(3)\big)\big(2\log_3(5)\big)
=
6\frac{\log_3(3)}{\log_3(5)}\log_3(5)
=
6\log_3(3)
=
{\color{red}6}
\]
\end{solution}

\part[5] $4^{\log_4(5)}-2\log_3(3^7)$

\begin{solution}[2.5in]
\[
4^{\log_4(5)}-2\log_3(3^7)
=
5-2(7)
=
{\color{red}-9}
\]
\end{solution}

\part[5] $\log_6\big(\frac{63}{2}\big) +3\log_6(2) - \log_6(7)$

\begin{solution}[2in]
\[
\log_6\big(\frac{63}{2}\big) +3\log_6(2) - \log_6(7)
=
\log_6\Big(\frac{63}{2}\cdot\frac{2^3}{7}\Big)
=
\log_6(9\cdot4)
=
\log_6(36)
=
{\color{red}2}
\]
\end{solution}

\end{parts}

\question
Solve the following exponential and logarithmic problems:
\begin{parts}
\part[5] If $x^3=\log_{27}\big(\frac{1}{81}\big)$, what are the possible values of $x$? Simplify your result as far as possible.)

\begin{solution}[3in]
\[
x^3
=
\log_{27}(3^{-4})
\Rightarrow
27^{x^3}
=
3^{-4}
\Rightarrow
3^{3x^3}
=
3^{-4}
\Rightarrow
3x^3
=
-4
\Rightarrow
x
=
{\color{red}\sqrt[3]{\frac{-4}{3}}}
\]
\end{solution}

\part[5] If $5(2^{x+3}) = 5^{2x+1}$, what are the possible values of $x$?

\begin{solution}[2.5in]
\[
5(2^{x+3}) = 5^{2x+1}
\Rightarrow
2^{x+3}
=
5^{2x}
\Rightarrow
x+3
=
\log_2(5^{2x})
\Rightarrow
x+3
=
2x\log_2(5)
\Rightarrow
{\color{red}x
=
\frac{-3}{1-2\log_2(5)}}
\]
\end{solution}

\clearpage

\part[5] If $\log_2(5-3x) = \log_2(3-x)+\log_2(3+x)$, what are the possible values of $x$?

\begin{solution}[3in]
\[
\log_2(5-3x)
=
\log_2((3-x)(3+x))
\Rightarrow
5-3x
=
9-x^2
\Rightarrow
x^2-3x-4 
=
0
\]
The possible solutions are $x=-1$ and $x=4$, \emph{but} $x=4$ is not in the domain, so {\color{red}$x=-1$}.
\end{solution}


\part[5] A $\$12,000$ principal was deposited into an account with an annual interest rate of $7\%$ compounded continuously.
No further deposits or withdrawals have been made on the account, which is currently worth $\$132,000$.
How long ago was the principal deposited?

\begin{solution}[1.5in]
\[
132000
=
12000e^{0.07t}
\Rightarrow
11
=
e^{0.07t}
\Rightarrow
{\color{red}
t
=
\frac{\ln(11)}{0.07}}
\]
\end{solution}

\end{parts}

\question
Consider the following matrices:
\[
A
=
\begin{bmatrix}
2 & 0 & -5 \\
1 & -3 & 1 \\
-3 & 2 & 0
\end{bmatrix}
\qquad
B
=
\begin{bmatrix}
7 & 0 \\
-1 & 1 \\
3 & -2
\end{bmatrix}
\qquad
C 
=
\begin{bmatrix}
2 & -1 \\
1 & 0 \\
1 & -9
\end{bmatrix}
\]
\begin{parts}
\part[2] Determine which of $A+B$, $B+C$ and $C+A$ exist and compute those that do. 

\begin{solution}[2in]
Ony $B+C$ is defined:
\[
B+C 
=
{\color{red}
\begin{bmatrix}
9 & -1 \\
0 & 1 \\
4 & -11
\end{bmatrix}
}
\]
\end{solution}

\clearpage

\part[3] Determine which of $AB$, $BC$ and $CA$ exist and compute those that do. 

\begin{solution}[2in]
Only $AB$ is defined:
\[
AB
=
{\color{red}
\begin{bmatrix}
-1 & 10 \\
13 & -5 \\
-23 & 2
\end{bmatrix}
}
\]
\end{solution}

\end{parts}

\question[15]
Solve the following system of equations:
\[
\begin{cases}
x -2z = 5 \\
3x+y-3z=11 \\
2y+4z = -6 
\end{cases}
\]

\begin{solution}[5in]
\begin{align*}
&
\begin{bmatrix}
1 & 0 & -2 & 5 \\
3 & 1 & -3 & 11 \\
0 & 2 & 4 & -6
\end{bmatrix}
\xrightarrow{R_2-3R_1}
\begin{bmatrix}
1 & 0 & -2 & 5 \\
0 & 1 & 3 & -4 \\
0 & 2 & 4 & -6
\end{bmatrix}
\xrightarrow{R_3-2R_2}
\begin{bmatrix}
1 & 0 & -2 & 5 \\
0 & 1 & 3 & -4 \\
0 & 0 & -2 & 2 
\end{bmatrix}
\xrightarrow{\frac{-1}{2}R_3}
\begin{bmatrix}
1 & 0 & -2 & 5 \\
0 & 1 & 3 & -4 \\
0 & 0 & 1 & -1 
\end{bmatrix}
\\
&
\xrightarrow{R_2-3R_3}
\begin{bmatrix}
1 & 0 & -2 & 5 \\
0 & 1 & 0 & -1 \\
0 & 0 & 1 & -1 
\end{bmatrix}
\xrightarrow{R_1+2R_3}
\begin{bmatrix}
1 & 0 & 0 & 3 \\
0 & 1 & 0 & -1 \\
0 & 0 & 1 & -1 
\end{bmatrix}
\Rightarrow
{\color{red}
\begin{bmatrix}
x \\
y \\
z
\end{bmatrix}
=
\begin{bmatrix}
3 \\
-1 \\
-1
\end{bmatrix}
}
\end{align*}
\end{solution}

\question
Consider the matrix
\[
A
=
\begin{bmatrix}
0 & 5 & -1 & 2 \\
1 & -2 & 0 & 0 \\
-1 & 7 & 0 & -1 \\
4 & -2 & 3 & 0
\end{bmatrix}
\]
\begin{parts}
\part[10] 
Compute the determinants of the following submatrices:
\[
M_{21} 
= 
\begin{vmatrix}
5 & -1 & 2 \\
7 & 0 & -1 \\
-2 & 3  & 0
\end{vmatrix}
\quad
\text{and}
\quad
M_{22}
=
\begin{vmatrix}
0 & -1 & 2 \\
-1 & 0 & -1 \\
4 & 3 & 0
\end{vmatrix}
\]

\begin{solution}[3in]
\[
M_{21}
=
(-1)^{1+2}(-1)((7)(0)-(-1)(-2)) + 0 + (-1)^{3+2}(3)((5)(-1)-(2)(7))
=
{\color{red}
55
}
\]
\[
M_{22}
=
0 + (-1)^{1+2}(-1)((-1)(0)-(-1)(4)) + (-1)^{1+3}(2)((-1)(3)-(0)(4))
=
{\color{red}
-2
}
\]
\end{solution}

\part[5] Using the results of (i), find the determinant of $A$.
[You may express $|A|$ in terms of the unknowns $M_{21}$ and $M_{22}$ for the full 5 points without solving part (i) beforehand.]

\begin{solution}[3in]
\[
|A|
=
{\color{red}
(-1)^{2+1}(1)M_{21} + (-1)^{2+2}(-2)M_{22}
=
-55 -2(-2)
=
-51
}
\]
\end{solution}

\end{parts}

\question
Consider the following matrix:
\[
A
=
\begin{bmatrix}
1 & 0 & 4 \\
0 & 1 & 5 \\
-1 & 2 & -1
\end{bmatrix}
\]
\begin{parts}
\part[15] If it exists, compute the inverse $A^{-1}$ of the matrix $A$.

\begin{solution}[5in]
\begin{align*}
&\begin{bmatrix}
1 & 0 & 4 & 1 & 0 & 0 \\
0 & 1 & 5 & 0 & 1 & 0 \\
-1 & 2 & -1 & 0 & 0 & 1 
\end{bmatrix}
\xrightarrow{R_3+R_1}
\begin{bmatrix}
1 & 0 & 4 & 1 & 0 & 0 \\
0 & 1 & 5 & 0 & 1 & 0 \\
0 & 2 & 3 & 1 & 0 & 1 
\end{bmatrix}
\xrightarrow{R_3-2R_2}
\begin{bmatrix}
1 & 0 & 4 & 1 & 0 & 0 \\
0 & 1 & 5 & 0 & 1 & 0 \\
0 & 0 & -7 & 1 & -2 & 1 
\end{bmatrix}
\\
&
\xrightarrow{\frac{-1}{7}R_3}
\begin{bmatrix}
1 & 0 & 4 & 1 & 0 & 0 \\
0 & 1 & 5 & 0 & 1 & 0 \\
0 & 0 & 1 & \frac{-1}{7} & \frac{2}{7} & \frac{-1}{7} 
\end{bmatrix}
\xrightarrow{R_2-5R_3}
\begin{bmatrix}
1 & 0 & 4 & 1 & 0 & 0 \\
0 & 1 & 0 & \frac{5}{7} & \frac{-3}{7} & \frac{5}{7} \\
0 & 0 & 1 & \frac{-1}{7} & \frac{2}{7} & \frac{-1}{7} 
\end{bmatrix}
\\
&
\xrightarrow{R_1-4R_3}
\begin{bmatrix}
1 & 0 & 0 & \frac{11}{7} & \frac{-8}{7} & \frac{4}{7} \\
0 & 1 & 0 & \frac{5}{7} & \frac{-3}{7} & \frac{5}{7} \\
0 & 0 & 1 & \frac{-1}{7} & \frac{2}{7} & \frac{-1}{7} 
\end{bmatrix}
\Rightarrow
A^{-1} 
=
{\color{red}
\frac{1}{7}
\begin{bmatrix}
11 & -8 & 4 \\
5 & -3 & 5 \\
-1 & 2 & -1
\end{bmatrix}
}
\end{align*}

\end{solution}

\part[5] Using the results of part (i), solve the following system of equations:
\[
\begin{cases}
x + 4z = 9 \\
y+5z = -8 \\
-x+2y-z = -5 
\end{cases}
\]

\begin{solution}[1.5in]
\[
\begin{bmatrix}
x \\
y \\
z
\end{bmatrix}
=
\frac{1}{7}
\begin{bmatrix}
11 & -8 & 4 \\
5 & -3 & 5 \\
-1 & 2 & -1
\end{bmatrix}
\begin{bmatrix}
9 \\
-8 \\
-5
\end{bmatrix}
=
\frac{1}{7}
\begin{bmatrix}
99+64-20 \\
45+24-25 \\
-9-16+5
\end{bmatrix}
=
{\color{red}
\frac{1}{7}
\begin{bmatrix}
143 \\
44 \\
-20
\end{bmatrix}
}
\]
\end{solution}

\end{parts}

%\clearpage

\vspace*{\fill}

%\clearpage

\vspace*{\fill}

%\clearpage

\vspace*{\fill}

%\clearpage

\vspace*{\fill}

\end{questions}


\end{document}
