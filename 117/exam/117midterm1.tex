\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[margin=1in]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newtheorem{exercise}{Exercise}
%\newtheorem{question}{Question}

\runningheader{}{}{\oddeven{Name: \underline{\hspace{3in}}}{}}
\renewcommand{\thepartno}{\roman{partno}}
\renewcommand{\thechoice}{\alph{choice}}
\renewcommand{\choicelabel}{(\thechoice)}
\CorrectChoiceEmphasis{\color{red}\bfseries}

\begin{document}

\printanswers

\Large
\textsc{Math} $117$
\hfill
\normalsize Name:\enspace\hrulefill 
\\
Midterm $1$ 
\hfill
USC ID number:\enspace\hrulefill
\\
October $1$, $2014$
\hfill
Discussion section (circle one): 11am, 12pm, 1pm

\begin{center}
\fbox{\fbox{\parbox{5.5in}{
You have $50$ minutes to complete the exam.
Write your final answers in the blanks provided.
Show all your work in Part II.
You may detach the last page to use as scrap paper or extra space for you work.
The use of calculators or notes is not permitted.
Point values are indicative and subject to change.
\hfill A}}}
\end{center}

\addpoints

%\begin{center}
%\gradetable[h]
%\end{center}

\section*{About the grades}

The grades for this midterm were curved using the following formula:
$
C = 100\sqrt{\frac{R}{100}}+8,
$
where $C$ denotes the curved grade and $R$ the raw score. As an exercise, you should be able to graph $C$ as a function of $R$ and guess why this formula might be appropriate for curving grades on a test.

\section*{Part I: Multiple choice (20 points total)}
\textbf{Instructions:} Circle the correct answer(s).
This portion of the exam aims to test your understanding of the essential concepts.
In some cases, it would be prudent to rule out the incorrect responses by thinking critically about the meaning of the problem, rather than solving the problem directly.

\begin{questions}

\question[20]

\

\begin{parts}

\part
What is the number of real solutions of the equation 
$
31x^2 - 611x + 8 = 0
$?

\begin{oneparchoices}
\choice 0
\choice 1
\CorrectChoice 2
\choice 3
\choice infintely many
\end{oneparchoices}

\begin{solution}
The discriminant of the quadratic is $(-611)^2-4(31)(8)$. Since $611^2$ is clearly much larger than $4(31)(8)$ we can conclude \emph{without computing any further} that the discriminant is positive, so there must be two distinct real roots.
\end{solution}

\part
What is the solution set of the inequality $|21x-483|<147$?
\begin{choices}
\CorrectChoice $(16,30)$
\choice $(-\infty,16)\cup(30,\infty)$
\choice $[16,30]$
\choice $(-\infty,16]\cup[30,\infty)$
\end{choices}

\begin{solution}\

\textit{Algebraic method:}
This inequality can be rewritten in the form $-147 < 21x-483 < 147$ or $(483-187)/21 < x < (147+483)/21$, so the solution has to be an open interval. We don't need to compute any further because there is only one open interval to choose from: $(16,30)$.

\textit{Graphical method:}
The graph of $y= |21x-483|$ is a ``V'' shape and the solutions are the $x$-coordinates of the points on this graph strictly below the horizontal line $y=147$, so it has to be an open interval and $(16,30)$ is the only choice matching this description.

\textit{Brute force method:}
If you really just refuse to think about the problem (rarely a wise approach, but anything goes in multiple choice), just test values from each of the possible solutions sets and eliminate the choices that don't satisfy the given relation. Don't forget to test the endpoints $16$ and $30$!
Actually, you only really need to test two values: for example, we could test $x=20$, see that it works, so we can rule out (b) and (d), and when we test $x=30$ and see that it fails, we can rule out (c), so by process of elimination, (a) has to be the answer.
\end{solution}

\part
Let $a$, $c$ and $d$ be real numbers such that $a<0$ and $c>d$.
What is the solution set of the inequality $ax^2+c \geq d$?
\begin{choices}
\choice $\varnothing$, i.e. there are no solutions
\CorrectChoice $\big[-\sqrt{(d-c)/a},\sqrt{(d-c)/a}\big]$
\choice $\big(-\infty, -\sqrt{(d-c)/a}\big] \cup \big[\sqrt{(d-c)/a},\infty\big)$
\choice $\big(-\sqrt{(d-c)/a},\sqrt{(d-c)/a}\big)$
\choice $\big(-\infty, -\sqrt{(d-c)/a}\big) \cup \big(\sqrt{(d-c)/a},\infty\big)$
\choice $(-\infty,\infty)$
\end{choices}

\begin{solution}
Start by rewriting the inequality in the form $ax^2+(c-d) \geq 0$.
The quadratic formula tells us that the solutions of $ax^2+(c-d)=0$ are
\[
x = \frac{-0\pm \sqrt{0^2-4a(c-d)}}{2a} = \pm\frac{\sqrt{-4a(c-d)}}{\sqrt{4a^2}} = \pm\sqrt{\frac{d-c}{a}}
\]
(or you could have guessed this by looking at the choices given).
Note that we assumed $a<0$ and that $c>d$ implies $d-c<0$, so $\frac{d-c}{a}>0$ and so both of these roots are real.
We now have two options:

\textit{Graphical method:}
The graph of $y=ax^2+(c-d)$ is a parabola, \emph{facing downwards since $a<0$}.
Since the roots are the intersection points of this parabola and the $x$-axis and the inequality is nonstrict, (b) is the only reasonable choice.

\textit{Algebraic method:}
Since the inequality is nonstrict, we consider the intervals $\big(-\infty,-\sqrt{\frac{d-c}{a}}\big]$, $\big[-\sqrt{\frac{d-c}{a}},\sqrt{\frac{d-c}{a}}\big]$ and $\big[\sqrt{\frac{d-c}{a}},\infty\big)$.
We only need to test one interval for quadratic inequalities, so let test the middle one at $x=0$: $a(0)^2+c = c\geq d$ is \emph{true}, since we assumed from the beginning that $c>d$.
So the answer must be $(b)$.

\textit{Plug and chug method:}
Skipping the first step altogether where we found the roots $\pm\sqrt{\frac{d-c}{a}}$, just choose values for $a$, $c$ and $d$ satisfying the conditions $a<0$, $c<d$, e.g. $a=-1$, $c=1$, $d=0$ and solve the problem just like any other of the good old quadratic inequalities that we know and love.
\end{solution}

\part
Let $f$ be a function whose domain is the interval $[1,3]$ such that $f(1)=9$, $f(2)=5$ and $f(3)=1$.
Is $f$ a decreasing function on $[1,3]$?

\begin{oneparchoices}
\choice Yes
\choice No
\CorrectChoice Maybe, more information is required
\end{oneparchoices}

\begin{solution}
All we can say for sure is that $f$ gives us a decreasing function on the domain $\{1,2,3\}$.
We can't rule out the possibility that $f(1) = 9 < f(1.5)=1245$.
\end{solution}

\part
Does the point given in rectangular coordinates in the $xy$-plane by $(12,0)$ lie on the line given by the equation $3y=21x+36$?

\begin{oneparchoices}
\choice Yes
\CorrectChoice No
\choice Maybe, more information is required
\end{oneparchoices}

\begin{solution}
\

\emph{Graphical method:}
Dividing by $3$, we have $y=7x+12$, which is in slope-intercept form.
This tells us that the slope is positive and the line intersects the $y$-axis at $(0,12)$, so there's no way such a line can hit the $x$-axis to the right of the origin.

\emph{Algebraic method:}
Plug the point $(12,0)$ into the equation and find $3(0) = 21(12)+36$, which is not true, so the line can't contain this point.
\end{solution}

\part
Which of the following points given in rectangular coordinates in the $xy$-plane lie on the circle described by the equation $(x-4)^2+(y+5)^2 = 81$? 
Circle all that apply.
\begin{choices}
\CorrectChoice $(13,-5)$
\choice $(4,14)$
\choice $(-4,76)$
\choice None of the above
\end{choices}

\begin{solution}
This is the equation of a line centered at $(4,-5)$ with radius $\sqrt{81} = 9$. Be careful with the signs!
The $(13,-5)$ is the only point a distance of $9$ from $(4,-5)$, so the only choice that lies on the circle.

Alternatively, just plug each of the three points into the equation and test its validity.
\end{solution}

\part
Which of the following lines are perpendicular to the line described by the equation $2y=-6x+8$?
\begin{choices}
\choice $y=-6x+14$
\choice $y=-\frac{1}{3}x+4$
\CorrectChoice $y=\frac{1}{3}x$
\choice $y=3x-\frac{1}{8}$
\choice $y=\frac{1}{6}x-8$
\end{choices}

\begin{solution}
The product of the slopes of two perpendicular lines is $-1$.
The given line has slope-intercept form $y=-3x+4$, so it's slope is $-3$, meaning that the slope of any perpendicular line is $\frac{1}{3}$, and (c) is the only choice.
\end{solution}

\part
What is the domain of the function given by the expression $f(x) = \frac{1}{x^2-2}$?
\begin{choices}
\choice $(-\infty,2)\cup (2,\infty)$
\choice $(-\infty,-2)\cup (-2,2)\cup (2,\infty)$
\choice $(-\infty,\sqrt{2}) \cup (\sqrt{2},\infty)$
\CorrectChoice $(-\infty,-\sqrt{2}) \cup (-\sqrt{2},\sqrt{2})\cup (\sqrt{2},\infty)$
\end{choices}

\begin{solution}
The expression $\frac{1}{x^2-2}$ is well-defined except at roots of the denominator $x^2-2= (x+\sqrt{2})(x-\sqrt{2})$, so the domain is all real numbers except $\pm\sqrt{2}$, which is given in interval notation by (d).
\end{solution}

\part
What is the range of the function given by the expression $f(x) = \frac{1}{x}$?
\begin{choices}
\choice $(-\infty,\infty)$
\choice $(0,\infty)$
\choice $(-\infty,0)$
\CorrectChoice $(-\infty,0)\cup (0,\infty)$
\end{choices}

\begin{solution}
\

\emph{The ``attend lecture'' method:}
Recall the fact that was mentioned more than once in lecture that the range of $f(x)=\frac{1}{x}$ is $(-\infty,0)\cup (0,\infty)$.

\emph{Algebraic method:}
Just compute directly which real numbers $y$ can be written in the form $y=\frac{1}{x}$ for some nonzero real number $x$: if $y=\frac{1}{x}$, then $xy=1$ and so $y\neq0$ and $x=\frac{1}{y}$ is a nonzero real number such that $f(x)=y$. So the only condition is that $y\neq0$.

\emph{Graphical method:}
Sketch the graph of $f$, which was also done more than once in lecture, and look for horizontal lines that don't intersect this graph: the only one is $y=0$.
\end{solution}

\part
Let $f$ be a function whose graph lies in Quadrant II of the $xy$-plane, i.e. each ordered pair $(x,y)$ on the graph of $f$ satisfies the conditions $x\leq0$ and $y\geq0$.
Circle each of the quadrants that could contain points on the graph of the function $g(x)= 3f(x+70)+2$.
\begin{choices}
\choice Quadrant I: $\{(x,y) \mid x\geq0,\, y\geq0\}$
\CorrectChoice Quadrant II: $\{(x,y) \mid x\leq 0,\, y\geq0\}$
\choice Quadrant III: $\{(x,y) \mid x\leq 0,\, y\leq0\}$
\choice Quadrant IV: $\{(x,y) \mid x\geq0,\, y\leq0\}$
\choice More information is required
\end{choices}

\begin{solution}
Note that the graph of $g$ is obtained from that of $f$ by translating horizontally to the left by $70$, stretching vertically by a factor of $3$ and translating vertically upwards by $2$. Each of these operations sends points in Quadrant II to points in Quadrant II, so (b) is the answer.
\end{solution}

\end{parts}

\section*{Part II: Long answer (80 points total)}

\question
Deduce the quadratic formula for the general quadratic equation $ax^2+bx+c=0$ by completing the square.

\begin{parts}

\part[2]
Quadratic formula:

\begin{solution}
{\color{red} $x = \frac{-b\pm \sqrt{b^2-4ac}}{2a}$}
\end{solution}

\part[11]
Derivation of the quadratic formula:

\begin{solution}
As directed, we're just going to show that the quadratic formula is valid by completing the square:
\begin{align*}
ax^2+bx+c &= 0 && \\
x^2 + \frac{b}{a}x + \frac{c}{a} &= 0 && \text{divide by $a$} \\
x^2 + \frac{b}{a}x &= -\frac{c}{a} && \text{subtract $\frac{c}{a}$} \\
x^2 + \frac{b}{a}x + \Big(\frac{b}{2a}\Big)^2 &= \Big(\frac{b}{2a}\Big)^2- \frac{c}{a} && \text{add the square of half of the coefficient of $x$} \\
\Big(x+\frac{b}{2a}\Big)^2 &= \frac{b^2-4ac}{4a^2} && \text{factor the left hand side and simplify the right} \\
x+\frac{b}{2a} &= \pm\sqrt{\frac{b^2-4ac}{4a^2}} && \text{apply the square root property} \\
x &= -\frac{b}{2a} \pm \sqrt{\frac{b^2-4ac}{4a^2}} && \text{subtract $\frac{b}{2a}$} \\
x &= \frac{-b\pm\sqrt{b^2-4ac}}{2a} && \text{simplify}
\end{align*}
\end{solution}

\end{parts}

\question[13]
Find an equation of the circle with center $(-3,4)$ passing through the origin.

\begin{solution}
The center-radius form of the equation of a circle centered and $(h,k)$ or radius $r$ is $(x-h)^2+(y-k)^2 = r^2$.
Since we know $(h,k)=(-3,4)$, it only remains to find $r^2$.
Since the circle passes through the origin $(0,0)$, we can use the distance formula to find the radius:
\[r^2=\mathrm{d}((-3,4),(0,0))^2
=\big(\sqrt{(-3-0)^2+(4-0)^2}\big)^2 = 9+16 =25.
\]
Hence, our circle is described by the equation {\color{red}$(x-(-3))^2+(y-4)^2=25$} or {\color{red}$(x+3)^2+(y-4)^2=25$}.
\end{solution}

\question
\begin{parts}

\part[10]
Solve the following inequality: $\frac{1}{x+2}-\frac{1}{x-2} \geq 1$.

\begin{solution}
Follow our general method for solving rational inequalities:
rewrite the inequality in standard form $\frac{f(x)}{g(x)} \geq 0$ for some $f$ and $g$; find the zeroes of $f$ and $g$; test values from each interval bounded by these zeroes:
\begin{align*}
\frac{x-2}{(x+2)(x-2)}-\frac{x+2}{(x-2)(x+2)} - 1 &\geq 0 && \text{make denominators comparable and subtract $1$}\\
\frac{x-2-(x+2)-(x-2)(x+2)}{(x-2)(x+2)} &\geq 0 && \text{rewrite $1$ as $\frac{(x-2)(x+2)}{(x-2)(x+2)}$ and combine} \\
\frac{-x^2}{(x-2)(x+2)} &\geq 0 && \text{simplify} 
\end{align*}
This is now in standard form.
The only zero of the numerator $-x^2$ is $x=0$ and the zeroes of the denominator $(x-2)(x+2)$ are $x=\pm2$.
Since the inequality is nonstrict, we consider the intervals in the following table:
\begin{center}
\begin{tabular}{c|c|c|c}
Interval & $x$ & $\frac{1}{x+2}-\frac{1}{x-2}$  & Test result \\
\hline
 $(-\infty,-2)$ & -3 & $-1-(-\frac{1}{5})$  &  not a solution  \\
$(-2,0]$ & -1 & $1-(-\frac{1}{3})$  & solution \\
$[0,2)$ & 1 & $\frac{1}{3}-(-1)$  &  solution\\
$(2,\infty)$ & 3 & $\frac{1}{5} - 1$  & not a solution 
\end{tabular}
\end{center}
We conclude that the solution set is {\color{red}$(-2,0]\cup[0,2) = (-2,2)$}.
\end{solution}

\part[3]
Solve the following inequality: $\left|\frac{1}{x+2}-\frac{1}{x-2}\right| \geq 1$.

\begin{solution}
We already know when $\frac{1}{x+2}-\frac{1}{x-2} \geq 1$, so we only need the solutions of $\frac{1}{x+2}-\frac{1}{x-2} \leq -1$.
We have
\begin{align*}
\frac{x-2-(x+2)+(x-2)(x+2)}{(x-2)(x+2)} &\leq 0 \\
\frac{-4+x^2-4}{(x-2)(x+2)} &\leq 0 \\
\frac{(x+\sqrt{8})(x-\sqrt{8})}{(x+2)(x-2)} &\leq 0
\end{align*}
\begin{center}
\begin{tabular}{c|c|c|c}
Interval & $x$ & $\left|\frac{1}{x+2}-\frac{1}{x-2}\right|$ & Test result \\
\hline
$(-\infty,-\sqrt{8}]$ & -3 & $\left|-1-(-\frac{1}{5})\right|$  & not a solution  \\
$[-\sqrt{8},-2)$ & -2.5  & $\left|-2-(-\frac{1}{4.5})\right|$ & solution \\
$(-2,2)$ & -1 & $\left|1-(-\frac{1}{3})\right|$  & solution \\
$(2,\sqrt{8}]$ & 2.5 & $\left|\frac{1}{4.5}-2\right|$ & solution\\
$[\sqrt{8},\infty)$ & 3 & $\left|\frac{1}{5}-1\right|$  & not a solution 
\end{tabular}
\end{center}
so the solution set is {\color{red}$[-\sqrt{8},-2)\cup(-2,2)\cup(2,\sqrt{8}]$}.

\end{solution}

\end{parts}

\question[13]
Give an equation of the line through the point $(2,6)$ perpendicular to the line described by the equation $x+2y=1$ and find its $y$-intercept.

\begin{solution}
Rewrite $x+2y=1$ as $y=-\frac{1}{2}x+\frac{1}{2}$, which describes a line of slope $-\frac{1}{2}$. Any line perpendicular to it has slope $-\frac{1}{-(1/2)}=2$. The line of slope $2$ through $(2,6)$ is given in point-slope form by {\color{red}$y-6 = 2(x-2)$}.
The $y$-intercept, i.e. the point on the graph of the form $(0,b)$, is thus $b=2(0-2)+6={\color{red}2}$.
\end{solution}

\question[15]
Let $L$ be the line segment with endpoints $P$ and $Q$ and suppose $P$ is given in rectangular coordinates by $(12,11)$ and the midpoint $M$ of $L$ is given by $(-5,7)$.
Give the following:
\begin{enumerate}
\item[(i)] the coordinates of $Q$;
\item[(ii)] the length of $L$; and
\item[(iii)] an equation for the line containing $L$.
\end{enumerate}

\begin{solution}
Let $Q=(x,y)$.
Since $M$ is the midpoint, by the midpoint formula, we must have
$-5 = \frac{12+x}{2}$, so $x = -22$, and $7 = \frac{11+y}{2}$, so $y= 3$. Thus, {\color{red} $Q = (-22,3)$}.
Since $M$ is the midpoint, the length of $L$ is twice the distance from $P$ to $M$, i.e. it's
\[
2\mathrm{d}((12,11),(-5,7))
=
2\sqrt{(12-(-5))^2+(11-7)^2}
=
{\color{red}2\sqrt{17^2+4}
=
2\sqrt{305}}.
\]
Finally, any line containing $L$ passes through $P$ and $M$, so it's slope must be
\[
m = \frac{11-7}{12-(-5)} = \frac{4}{17}
\]
and we can therefore describe it by the point-slope form {\color{red}$y-11 = \frac{4}{17}(x-12)$}.
\end{solution}

\question
Sketch the graphs of the following functions and explain in words why the graph is accurate:

\begin{parts}

\part[6]
$f(x) = 3\sqrt{x} -2$

\begin{solution}
\

\begin{minipage}{.3\textwidth}
\includegraphics[scale=.3]{117midterm1graphic7a.png}
\end{minipage}
\hfill
\begin{minipage}{.5\textwidth}
The red line is the graph of $f$ and the green line is that of $g(x) = \sqrt{x}$. The graph is accurate since $f$ is obtained from $g$ by stretching vertically by $3$ and translating vertically downwards by $2$.
\end{minipage}
\end{solution}

\part[7]
$f(x) = -3(x-2)^2+1$

\begin{solution}
\

\begin{minipage}{.3\textwidth}
\includegraphics[scale=.3]{117midterm1graphic7b.png}
\end{minipage}
\hfill
\begin{minipage}{.5\textwidth}
The red line is the graph of $f$ and the green line is that of $g(x) = x^2$. The graph is accurate since $f$ is obtained from $g$ by translating horizontally to the right by $2$, reflecting across the $x$-axis, vertically stretching by a factor of $3$ and translating vertically upwards by $1$.
\end{minipage}
\end{solution}
\end{parts}

\end{questions}
\end{document}

\clearpage

Extra space (clearly indicate problem numbers in order to receive credit for work shown):

\clearpage

Extra space (clearly indicate problem numbers in order to receive credit for work shown):



