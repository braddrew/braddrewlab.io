\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, polynom, sectsty}
\usepackage[margin=1in]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newtheorem{exercise}{Exercise}
%\newtheorem{question}{Question}

\runningheader{}{}{\oddeven{Name: \underline{\hspace{3in}}}{}}
\renewcommand{\thepartno}{\roman{partno}}
\renewcommand{\thechoice}{\alph{choice}}
\renewcommand{\choicelabel}{(\thechoice)}
\CorrectChoiceEmphasis{\color{red}\bfseries}

\begin{document}

%\printanswers

\Large
\textsc{Math} $117$
\normalsize
\hfill
Practice Midterm $2$

\begin{center}
\fbox{\fbox{\parbox{5.5in}{
WARNING: All the materical covered in lecture is fair game, regardless of what questions appear below. 
This practice test is a supplement to and not a substitute for studying lecture notes or working through practice problems from each of the relevant sections of the text book.
You have $50$ minutes to complete the exam.
%Write your final answers in the blanks provided.
The use of calculators or notes is not permitted.
Point values are indicative and subject to change.
}}}
\end{center}

\addpoints

%\begin{center}
%\gradetable[h]
%\end{center}

\begin{questions}

\question[20]

Consider the rational function $f(x) = \frac{2(x^3-x^2+4x-4)}{(x-2)(x^2-9)}$.

\begin{parts}
\item Give the domain of $f$ and find all of its asymptotes.

\begin{solution} First, we factor the numerator and denominator to be sure that they have no common divisors.

By the Rational Zeros Theorem, the possible rational roots of $x^3-x^2+4x-4$ are $\pm1$, $\pm2$, $\pm4$, $\pm\frac{1}{2}$, $\pm\frac{1}{4}$.
Taking $x=1$, we have $(1)^3-(1)^2+4(1)-4=0$, so $x-1$ is a factor and the synthetic division
\[
\polyhornerscheme[x=1]{x^3-x^2+4x-4}
\]
tells us that $x^3-x^2+4x-4 = (x-1)(x^2+4)$.
The remaining roots are the imaginary numbers $\pm2\sqrt{-1}$, so $1$ is the only real root.
The denominator factors as $(x-2)(x^2-9)=(x-2)(x-3)(x+3)$.
Thus, the numerator and denominator have no common roots.

This factorization tells us that the denominator vanishes when $x=2$ or $x=\pm3$, so the domain of $f$ is {\color{red} $(-\infty,-3)\cup (-3,2) \cup (2,3)\cup (3,\infty)$} and the vertical asymptotes are {\color{red} $x=2$, $x=-3$ and $x=3$}.

The numerator and denominator are of the same degree, with leading coefficients $2$ and $1$, respectively, so the nonvertical asymptote is horizontal, determined by the quotient $\frac{2}{1}$ of the leading coefficients of the numerator and denominator.
Thus, the horizontal asymptote is {\color{red} $y=2$}.
\end{solution}

\item Give the coordinates of the points of intersection of the graph of $f$ with the $x$-axis, the $y$-axis and each of its asymptotes. [Fun fact: $\sqrt{257} \approx 16.0$.]

\begin{solution}
The $y$-intercept is $(0,f(0)) = {\color{red} (0,-8/18)=(0,-4/9)}$.

The $x$-intercepts are the solutions of $0 = 2(x^3-x^2+4x-4)$ or, by the factorization found in part (i), $0=2(x-1)(x^2+4)$, so the only real solution is {\color{red}$x=1$}.

The points of intersection of the graph of $f$ with the horizontal asymptote $y=2$ are the solutions of 
\begin{align*}
2 
&=
\frac{2(x^3-x^2+4x-4)}{(x-2)(x^2-9)} \\
2(x-2)(x^2-9) 
&=
2(x^3-x^2+4x-4) \\
x^3-2x^2-9x+18 
&= 
x^3-x^2+4x-4 \\
0
&=
x^2+13x-22 \\
{\color{red}x}
& 
{\color{red} = \frac{-13\pm\sqrt{257}}{2}}\\
x
&\approx \frac{-13\pm 16.0}{2} = \frac{3.0}{2}, \frac{-29.0}{2}= 1.5, -14.5
\end{align*}
\end{solution}

\item Given that $f(-20)\approx 1.97$ and $f(2.5)\approx -22.36$, use the results of part (ii) and what you know about asymptotes to sketch the graph of $f$.
[Hint: You will need to perform one more computation to determine the nature of the graph for $x>3$.]

\begin{solution}
The given data, along with the asymptotes found in part (i) and the intercepts found in part (ii) allow us to sketch the graph in on the interval $(-\infty,3)$.
As suggested by the hint, to understand the behavior for $x>3$, we compute 
\[
f(4) = \frac{2((4)^3-(4)^2+4(4)-4)}{(4-2)((4)^2-9)} = \frac{60}{7} >2.
\]
Since $f(-20)<2$, the graph approaches the horizontal asymptote $y=2$ from below as $x$ approaches $-\infty$ and it crosses the asymptote at $x\approx -14.5$, after which $f(x)$ approaches $\infty$ as $x$ approaches $-3$ from the left.
The $x$- and $y$-intercepts tell us that $f(x)$ approaches $-\infty$ as $x$ approaches $-3$ from the right and $f(x)$ approaches $\infty$ as $x$ approaches $2$ from the left.
Since $f(2.5)\approx -22.36$ and the graph doesn't cross the $x$-axis on $(2,3)$, $f(x)$ approaches $-\infty$ both as $x$ approaches $2$ from the right and $3$ from the left.
Finally, as $f(4) >2$ and the graph doesn't cross the horizontal asymptote $y=2$ on $(3,\infty)$, $f(x)$ approaches $\infty$ as $x$ approaches $3$ from the right and $f(x)$ approaches $2$ from above as $x$ approaches $\infty$.
In summary, the graph looks like the following:
\begin{center}
\includegraphics[scale=.3]{117midterm2practicegraphic1.png}
\end{center}
\end{solution}

\end{parts}

\question[16]
Consider the functions $f(x) = \frac{-3x+12}{x-6}$ and $g(x)= \frac{6x+12}{x+3}$.

\begin{parts}
\item Show that $f$ is one-to-one.

\begin{solution}
The domain of $f$ is $(-\infty,6)\cup(6,\infty)$.
Suppose $a$ and $b$ are two emelents of the domain of $f$ such that $f(a)=f(b)$.
Then we have
\begin{align*}
\frac{-3a+12}{a-6}
&=
\frac{-3b+12}{b-6} \\
(-3a+12)(b-6) 
&=
(-3b+12)(a-6) \\
-3ab+18a+12b-72
&=
-3ab+18b+12a-72 \\
6a
&=
6b \\
a
&=b
\end{align*}
so $f$ is one-to-one.
\end{solution}

\item Show that $g$ is the inverse function of $f$.

\begin{solution}
We must show that $f(g(x))=x$ for each $x$ in the domain of $g$ and $g(f(x))=x$ for each $x$ in the domain of $f$.
We have
\[
f(g(x)) 
=
\frac{-3\frac{6x+12}{x+3}+12}{\frac{6x+12}{x+3}-6}
=
\frac{-3(6x+12)+12(x+3)}{(6x+12)-6(x+3)}
=
\frac{-6x}{-6}
=
x
\]
\[
g(f(x))
=
\frac{6\frac{-3x+12}{x-6}+12}{\frac{-3x+12}{x-6}+3}
=
\frac{6(-3x+12)+12(x-6)}{(-3x+12)+3(x-6)}
=
\frac{-6x}{-6}
=
x
\]
as desired.
\end{solution}

\end{parts}

\question[16]
Solve the equation $\log_{10}(x^2+10x-39)-\frac{\log_2(x-3)}{\log_2(10)}=\log_{10}(10)$ for $x$.

\begin{solution}
Let's use the common logarithm notation $\log = \log_{10}$.
First, we want to put all the logarithms in the same base.
By the Change-of-Base Theorem, $\frac{\log_2(x-3)}{\log_2(10)} = \log_{10}(x-3)$, so we have
\begin{align*}
\log(x^2+10x-39)-\log(x-3)
&=
\log(10) \\
\log\left(\frac{x^2+10x-39}{x-3}\right)
&=
\log(10) \\
\frac{x^2+10x-39}{x-3}
&=
10 \\
\frac{x^2+10x-39 -10(x-3)}{x-3}
&=
0 \\
\frac{x^2-9}{x-3}
&=
0 \\
x-3 
&=
0 \\
x
&= -3
\end{align*}
Taking $x=-3$ in our original equation, we find that $\log(x^2+10x-39)$ and $\log(x-3)$ are both undefined as $(-3)^2+10(-3)-39<0$ and $-3-3<0$, whereas the domain of $\log$ is $(0,\infty)$.
Thus, {\color{red} the solution set is empty}.
\end{solution}

\question[16]
A principal of $\$34,567,890.12$ is deposited in a bank account at an annual interest rate of $5\%$ compounded continuously.
Assuming no money is deposited or withdrawn from the account, how many years will it take for the the balance on the account to equal $4$ times the principal amount?
Give an exact answer.

\begin{solution}
Let $P=34,567,890.12$ and $r=0.05$.
The formula for the compound amount is $A(t) = Pe^{rt}$.
So we would like to solve the equation
\[
4P = Pe^{rt} 
\Rightarrow 
4 = e^{rt} 
\Rightarrow
\ln(4) = \ln(e^{rt}) = rt
\Rightarrow 
t = \frac{\ln(4)}{r} = {\color{red} \frac{\ln(4)}{0.05}}.
\]
\end{solution}

\question[16]
Solve the following system of equations:
\[
\begin{cases}
3x-3z = -9 \\
5y+5z = 45 \\
2x+2z = 14
\end{cases}
\]

\begin{solution}
We can solve the system by performing Gauss-Jordan elimination on the augmented matrix:
\begin{align*}
&\begin{bmatrix}
3 & 0 & -3 & -9 \\
0 & 5 & 5 & 45 \\
2 & 0 & 2 & 14
\end{bmatrix} 
\xrightarrow{\frac{1}{3}{R_1}}
\begin{bmatrix}
1 & 0 & -1 & -3 \\
0 & 5 & 5 & 45 \\
2 & 0 & 2 & 14 
\end{bmatrix} 
\xrightarrow{\frac{1}{5}R_2}
\begin{bmatrix}
1 & 0 & -1 & -3 \\
0 & 1 & 1 & 9 \\
2 & 0 & 2 & 14 
\end{bmatrix}
\xrightarrow{\frac{1}{2}R_3}
\begin{bmatrix}
1 & 0 & -1 & -3 \\
0 & 1 & 1 & 9 \\
1 & 0 & 1 & 7
\end{bmatrix} \\
&\xrightarrow{R_3-R_1}
\begin{bmatrix}
1 & 0 & -1 & -3 \\
0 & 1 & 1 & 9 \\
0 & 0 & 2 & 10
\end{bmatrix}
\xrightarrow{\frac{1}{2}R_3}
\begin{bmatrix}
1 & 0 & -1 & -3 \\
0 & 1 & 1 & 9 \\
0 & 0 & 1 & 5
\end{bmatrix}
\xrightarrow{R_2-R_3}
\begin{bmatrix}
1 & 0 & -1 & -3 \\
0 & 1 & 0 & 4 \\
0 & 0 & 1 & 5 
\end{bmatrix}
\xrightarrow{R_1+R_3}
\begin{bmatrix}
1 & 0 & 0 & 2 \\
0 & 1 & 0 & 4 \\
0 & 0 & 1 & 5
\end{bmatrix}
\end{align*}
The last matrix is in reduced row echelon form and it tells us that the system is equivalent to 
\[
\begin{cases}
x = 2 \\
y = 4 \\
z = 5
\end{cases}
\]
so the solution set is {\color{red}$\{(2,4,5)\}$}.

Alternatively, one could solve this system by substitution.
Beware that, even though substitution works here, you are expected to be able to perform Gauss-Jordan elimination.
\end{solution}

\question[16]
Consider the following matrices:
\[
A 
=
\begin{bmatrix}
2 & -1 & 4 \\
3 & 0 & 5
\end{bmatrix},
\quad
B
=
\begin{bmatrix}
-1 & 2 & 0 \\
0 & 3 & 2 \\
0 & 1 & 4
\end{bmatrix},
\quad
C
=
\begin{bmatrix}
2 & -1 & 2 \\
0 & 2 & 1 \\
3 & 0 & -1
\end{bmatrix}.
\]

\begin{parts}
\item Compute the matrix sums $A+B$ and $B+C$.

\begin{solution}
Note that $A$ is a $2\times 3$ matrix, whereas $B$ and $C$ are both $3\times 3$ matrices.
Thus, $A+B$ is {\color{red}undefined}.
As for $B+C$, we have
\[
\begin{bmatrix}
-1+2 & 2+(-1) & 0 +2 \\
0+0 & 3+2 & 2+1 \\
0+3 & 1+0 & 4+(-1)
\end{bmatrix}
=
{\color{red}
\begin{bmatrix}
1 & 1 & 2 \\
0 & 5 & 3 \\
3 & 1 & 3
\end{bmatrix}
}
\]
\end{solution}

\item Compute the matrix products $AB$ and $CA$. 

\begin{solution}
Recall that the products $MN$ of two matrices $M$ and $N$ is defined if and only if $M$ has the same number of columns as $N$ has rows.
Hence, the product $CA$ is undefined: $C$ has $3$ columns, whereas $A$ has only $2$ rows.
On the other hand, $A$ has $3$ columns and $B$ has $3$ rows, so $AB$ is defined and is computed as follows:
\begin{align*}
&\begin{bmatrix}
(2)(-1)+(-1)(0)+(4)(0) & (2)(2)+(-1)(3)+(4)(1) & (2)(0)+(-1)(2)+(4)(4) \\
(3)(-1)+(0)(0)+(5)(0) & (3)(2)+(0)(3)+(5)(1) & (3)(0)+(0)(2)+(5)(4)
\end{bmatrix} \\
&=
{\color{red}
\begin{bmatrix}
-2 & 5 & 14 \\
-3 & 11 & 20
\end{bmatrix}
}
\end{align*}
\end{solution}
\end{parts}
\end{questions}


\end{document}
