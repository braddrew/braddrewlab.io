\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, polynom, sectsty}
\usepackage[margin=1in]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newtheorem{exercise}{Exercise}
%\newtheorem{question}{Question}

\runningheader{}{}{\oddeven{Name: \underline{\hspace{3in}}}{}}
\renewcommand{\thepartno}{\roman{partno}}
\renewcommand{\thechoice}{\alph{choice}}
\renewcommand{\choicelabel}{(\thechoice)}
\CorrectChoiceEmphasis{\color{red}\bfseries}

\begin{document}

\printanswers

\Large
\textsc{Math} $117$
\normalsize
\hfill
Practice Midterm $2$

\begin{center}
\fbox{\fbox{\parbox{5.5in}{
WARNING: All the materical covered in lecture is fair game, regardless of what questions appear below. 
This practice test is a supplement to and not a substitute for studying lecture notes or working through practice problems from each of the relevant sections of the text book.
You have $50$ minutes to complete the exam.
%Write your final answers in the blanks provided.
The use of calculators or notes is not permitted.
Point values are indicative and subject to change.
}}}
\end{center}

\addpoints

%\begin{center}
%\gradetable[h]
%\end{center}

\begin{questions}

\question[20]

Consider the rational function $f(x) = \frac{x^2-10x+25}{x^3-x^2-9x+9}$.

\begin{parts}
\item Give the domain of $f$ and find all of its asymptotes.

\begin{solution}
Start by factoring the numerator and denominator to make sure that they have no common factors. 
The numerator factors as $x^2-10x+25 = (x-5)^2$.
As for the denominator, note that $(1)^3-(1)^2-9(1)+9 = 0$, so $x-1$ is a factor the synthetic division
\[
\polyhornerscheme[x=1]{x^3-x^2-9x+9}
\] 
tells us that $x^3-x^2-9x+9 = (x-1)(x^2-9) = (x-1)(x+3)(x-3)$, so the numerator and denominator have no common factors.

We also deduce that the domain of $f$ is {\color{red}$(-\infty,-3)\cup(-3,1)\cup(1,3)\cup(3,\infty)$} and the vertical asymptotes are {\color{red}$x=1$, $x=3$ and $x=-3$}.
The numerator is of degree $2$, while the denominator is of degree $3$, so there is a horizontal asymptote at {\color{red}$y=0$}.
\end{solution}

\item Give the coordinates of the points of intersection of the graph of $f$ with the $x$-axis, the $y$-axis and each of its asymptotes. 

\begin{solution}
The $y$-intercept is at $f(0) = {\color{red} 25/9}$.

The $x$-intercepts are the solutions of $0 = x^2-10x+25$ or, by the factorization found in part (i), $0=(x-5)^2$, so the only solution is {\color{red}$x=5$} (and it's a root of multiplicity $2$).

The $x$-axis is the horizontal asymptote, so, again the only intersection is at $(0,5)$.
\end{solution}

\item Use the results of part (ii) and what you know about asymptotes to sketch the graph of $f$.

\begin{solution}
The asymptotes divide the $xy$-plane into several regions.
Let's make a table to help us understand which regions contain the graph of $f$.
The vertical asymptotes and the $x$-intercept break the $x$-axis into the following intervals: $(-\infty,-3)$, $(-3,1)$, $(x,3)$, $(3,5)$ and $(5,\infty)$, so we have the following table:
\[
\begin{array}{c|c|c}
\text{interval} & x & f(x) = \frac{(x-5)^2}{(x-1)(x+3)(x-3)}\\
\hline
(-\infty,-3) & -4 & \frac{(-4-5)^2}{(-4-1)(-4+3)(-4-3)} <0 \\
\hline
(-3,1) & 0 & \frac{25}{9} >0 \\
\hline
(1,3) & 2 & \frac{(2-5)^2}{(2-1)(2+3)(2-3)} <0 \\
\hline
(3,5) & 4 & \frac{(4-5)^2}{(4-1)(4+3)(4-3)} >0 \\
\hline
(5,\infty) & 6 & \frac{(6-5)^2}{(6-1)(6+3)(6-3)} >0
\end{array}
\]

\begin{center}
\includegraphics[scale=.3]{117midterm2practice2graphic1.png}
\end{center}

Note in particular that the graph touches the $x$-axis at $x=5$ but \emph{does not} cross it, since $f(6)>0$.
\end{solution}

\end{parts}

\question[16]
Consider the function $f(x) = 4e^{3x^5-1}+2$.

\begin{parts}
\item Show that $f$ is one-to-one.

\begin{solution}
The domain of $f$ is $\mathbf{R}=(-\infty,\infty)$.
Suppose $a$ and $b$ are two emelents of the domain of $f$, i.e. two real numbers, such that $f(a)=f(b)$.
Then we have
\begin{align*}
4e^{3a^5-1}+2 
&=
4e^{3b^5-1}+2 \\
4e^{3a^5-1}
&= 
4e^{3b^5-1} \\
e^{3a^5-1}
&=
e^{3b^5-1} \\
\ln(e^{3a^5-1})
&=
\ln(e^{3b^5-1}) \\
3a^5-1
&=
3b^5-1 \\
3a^5
&=
3b^5 \\
a^5 
&=
b^5 \\
\sqrt[5]{a^5}
&=
\sqrt[5]{b^5} \\
a
&=
b
\end{align*}
so $f$ is one-to-one.
\end{solution}

\item Find the inverse function $f^{-1}$ of $f$ and give its domain.

\begin{solution}
We must solve $x = 4e^{3y^5-1}+2$ for $y$.
\begin{align*}
x
&=
4e^{3y^5-1}+2 \\
x-2
&= 
4e^{3y^5-1} \\
\frac{x-2}{4}
&=
e^{3y^5-1} \\
\ln\left(\frac{x-2}{4}\right)
&= 
\ln(e^{3y^5-1}) \\
\ln\left(\frac{x-2}{4}\right)
&=
3y^5-1 \\
\ln\left(\frac{x-2}{4}\right)+1
&=
3y^5 \\
\sqrt[5]{\frac{1}{3}\left(\ln\left(\frac{x-2}{4}\right)+1\right)}
&=
y
\end{align*}
Thus, {\color{red} $f^{-1}(x) = \sqrt[5]{\frac{1}{3}\left(\ln\left(\frac{x-2}{4}\right)+1\right)}$} is the inverse function of $f$.
The domain is the set of real numbers $x$ such that $\frac{x-2}{4} >0$, i.e. {\color{red} $(2,\infty)$}.
\end{solution}

\end{parts}

\question[16]
Simplify the following expressions:

\begin{parts}
\item $2\log_5(x)-3\log_5(2x)+\log_5(y)$.

\begin{solution}
\begin{align*}
2&\log_5(x)-3\log_5(2x)+\log_5(y) \\
&=
\log_5(x^2)-\log_5((2x)^3)+\log_5(y)
&&
\text{exponent rule} \\
&=
\log_5\left(\frac{x^2}{(2x)^3}\right)+\log_5(y) 
&&
\text{quotient rule} \\
&=
\log_5\left(\frac{x^2y}{(2x)^3}\right)
&&
\text{product rule} \\
&=
\log_5\left(\frac{x^2y}{8x^3}\right) \\
&=
{\color{red}\log_5\left(\frac{y}{8x}\right)}
\end{align*}
\end{solution}

\item $\left(3^{\log_2(2^3)}\right)\left(\log_2(3^{\log_3(4)})\right)$

\begin{solution}
\begin{align*}
\left(3^{\log_2(2^3)}\right)&\left(\log_2(3^{\log_3(4)})\right) \\
&=
(3^3)(\log_2(4))
&&
\text{since }\log_a(a^x)=x=a^{\log_a(x)} \\
&=
27\log_2(2^2) \\
&=
27\cdot 2 \\
&=
{\color{red}54}
\end{align*}
\end{solution}

\item $\frac{\log_3(e^{561})}{\log_3(e)}$

\begin{solution}
By the Change-of-Base Theorem, $\frac{\log_3(e^{561})}{\log_3(e)} = \log_e(e^{561}) = \ln(e^{561})={\color{red}561}$.
\end{solution}

\end{parts}

\question[16]
Solve the following equations for $x$.

\begin{parts}
\item $\log_4(x)-\log_4(x-1) = \frac{1}{2}$ 

\begin{solution}
\begin{align*}
\log_4(x)-\log_4(x-1)
&=
\frac{1}{2} \\
\log_4\left(\frac{x}{x-1}\right)
&=
\frac{1}{2}
&&
\text{quotient rule} \\
4^{\log_4\left(\frac{x}{x-1}\right)}
&=
4^{\frac{1}{2}} \\
\frac{x}{x-1}
&=
\sqrt{4} \\
x
&=
2(x-1) \\
x
&=
2
\end{align*}
Note that taking $x=2$, $\log_4(x)$ and $\log_4(x-1)$ are both defined, so {\color{red} $x=2$} is the solution.
\end{solution}

\item $e^{2x}-12 = e^x$

\begin{solution}
The equation is equivalent to $e^{2x}-e^x-12 =0$.
Letting $u=e^x$, we have $u^2-u-12=0$.
This quadratic factors as $(u+3)(u-4)=0$, so $u=-3$ or $u=4$.
Thus, $e^x=u=-3$ or $e^x=u=4$, whence $x = \ln(e^x) = \ln(-3)$ or $x=\ln(e^x)=\ln(4)$.
However, $\ln(-3)$ is undefined, so the only solution is {\color{red} $x=\ln(4)$}.
\end{solution}
\end{parts}

\question[16]
Solve the following system of equations:
\[
\begin{cases}
x+y+2z=3 \\
3x+y+4z = 1 \\
3x+3y+7z = 1
\end{cases}
\]

\begin{solution}
We can solve the system by performing Gauss-Jordan elimination on the augmented matrix:
\begin{align*}
&\begin{bmatrix}
1 & 1 & 2 & 3 \\
3 & 1 & 4 & 1 \\
3 & 3 & 7 & 1
\end{bmatrix} 
\xrightarrow{R_2-3R_1}
\begin{bmatrix}
1 & 1 & 2 & 3 \\
0 & -2 & -2 & -8 \\
3 & 3 & 7 & 1
\end{bmatrix} 
\xrightarrow{R_3-3R_1}
\begin{bmatrix}
1 & 1 & 2 & 3 \\
0 & -2 & -2 & -8 \\
0 & 0 & 1 & -8
\end{bmatrix} \\
&\xrightarrow{\frac{-1}{2}R_2}
\begin{bmatrix}
1 & 1 & 2 & 3 \\
0 & 1 & 1 & 4 \\
0 & 0 & 1 & -8
\end{bmatrix} 
\xrightarrow{R_2-R_3}
\begin{bmatrix}
1 & 1 & 2 & 3 \\
0 & 1 & 0 & 12 \\
0 & 0 & 1 & -8
\end{bmatrix}
\xrightarrow{R_1-2R_3}
\begin{bmatrix}
1 & 1 & 0 & 19 \\
0 & 1 & 0 & 12 \\
0 & 0 & 1 & -8
\end{bmatrix} \\
&\xrightarrow{R_1-R_2}
\begin{bmatrix}
1 & 0 & 0 & 7 \\
0 & 1 & 0 & 12 \\
0 & 0 & 1 & -8
\end{bmatrix}
\end{align*}
The last matrix is in reduced row echelon form and it tells us that the system is equivalent to the system $\{x = 7, y = 12, z = -8\}$ so the solution set is {\color{red}$\{(7,12,-8)\}$}.
\end{solution}

\question[16]
Consider the following matrices:
\[
A 
=
\begin{bmatrix}
2 & -1  \\
3 & 0 \\
4 & 5
\end{bmatrix},
\quad
B
=
\begin{bmatrix}
-1 & 0 & 2 \\
3 & 0 & 2 \\
4 & 1 & 0
\end{bmatrix},
\quad
C
=
\begin{bmatrix}
2 & -1 & 2 \\
0 & 2 & 1 \\
3 & 0 & -1
\end{bmatrix}.
\]

\begin{parts}
\item Compute the matrix sums $A+B$ and $B+C$.

\begin{solution}
Note that $A$ is a $3\times 2$ matrix, whereas $B$ and $C$ are both $3\times 3$ matrices.
Thus, $A+B$ is {\color{red}undefined}.
As for $B+C$, we have
\[
\begin{bmatrix}
(-1)+2 & 0 + (-1) & 2 +2 \\
3+0 & 0+2 & 2+1 \\
4+3 & 1+0 & 0+(-1)
\end{bmatrix}
=
{\color{red}
\begin{bmatrix}
1 & -1 & 4 \\
3 & 2 & 3 \\
7 & 1 & -1
\end{bmatrix}
}
\]
\end{solution}

\item Compute the matrix products $AB$ and $CA$. 

\begin{solution}
Recall that the products $MN$ of two matrices $M$ and $N$ is defined if and only if $M$ has the same number of columns as $N$ has rows.
Hence, the product $AB$ is undefined: $A$ has $2$ columns, whereas $B$ has $3$ rows.
On the other hand, $C$ has $3$ columns and $A$ has $3$ rows, so $CA$ is defined and is computed as follows:
\begin{align*}
\begin{bmatrix}
2\cdot2+(-1)3+2\cdot4 & 2(-1)+(-1)0+2\cdot5 \\
0\cdot2+2\cdot3+1\cdot4 & 0(-1)+2\cdot0+1\cdot5 \\
3\cdot2+0\cdot3+(-1)4 & 3(-1)+0\cdot0+(-1)5
\end{bmatrix} 
&=
{\color{red}
\begin{bmatrix}
9 & 8 \\
10 & 5 \\
2 & -8
\end{bmatrix}
}
\end{align*}
\end{solution}
\end{parts}
\end{questions}


\end{document}
