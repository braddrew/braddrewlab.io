\documentclass[letterpaper,11pt]{article}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}

\begin{document}

\noindent
\LARGE
\textsc{Math} $117$\\ 
\Large
Discussion $1$ \\
\large
August $26$, $2014$
\normalsize

\vspace{\baselineskip}

\noindent
All references are to \textit{College Algebra \& Trigonometry}, fifth edition, by Lial, Hornsby, Schneider and Daniels.

This sheet most likely contains more exercises than we will have time for in discussion sections this week.
If you aren't comfortable with the exercises covered in class, please work through the surplus exercises on your own and ask for help if you need it: you will get a lot more out of this class if you make the effort to become comfortable with these kinds of algebraic manipulations.

\vspace{.5\baselineskip}

\noindent
\textbf{Review of operations on sets and algebra} 

\noindent
The following exercises are drawn from the chapter review exercises starting on page $73$.

\vspace{.5\baselineskip}

\noindent
Let $S$ and $T$ be two sets.
Three of the most important operations we perform on $S$ and $T$ are the following:
forming their \emph{intersection} $S\cap T = \{x \mid x\in S\mbox{ and }x\in T\}$; forming their \emph{union} $S\cup T = \{x \mid x\in S\mbox{ or }x\in T\}$; and forming the \emph{difference} $T-S = \{x \mid x\in T\mbox{ and } x\not\in S\}$.
Note that $T-S \neq S-T$ in general.


\vspace{.5\baselineskip}

\noindent
Let $B = \{2,4,6,8\}$, $C = \{1,3,5,7\}$, $D = \{1,2,3\}$ and $E = \{3,7\}$.
Specify each of the following sets by listing their elements:
\begin{enumerate}
\item[15.] $B\cap E$
\item[16.] $C\cup E$
\item[17.] $D\cap \varnothing$
\item[18.] $B\cup \varnothing$
\item[A.] $(B - D) \cup E$
\item[B.] $B-(D\cup E)$
\end{enumerate}

\noindent
Let $K = \{-12,-6,-0.9,-\sqrt{7},-\sqrt{4},0,\frac{1}{8},\frac{\pi}{4},6,\sqrt{11}\}$.
List all elements of $K$ that belong to the following sets:
\begin{enumerate}
\item[23.] $\mathbf{Z}$
\item[24.] $\mathbf{Q}$
\end{enumerate}

\noindent
Choose all words from the following list that apply to each of $25$-$28$: natural number, whole number, integer, rational number, irrational number, real number.
\begin{enumerate}
\item[25.] $\frac{4\pi}{5}$
\item[26.] $\frac{\pi}{0}$
\item[27.] $0$
\item[28.] $-\sqrt{36}$.
\end{enumerate}

\noindent
Perform the following divisions:
\begin{enumerate}
\item[59.] $\frac{30m^3-9m^2+22m+5}{5m+1}$
\item[62.] $\frac{5m^3-7m^2+14}{m^2-2}$
\end{enumerate}

\noindent
Factor the following expression:
\begin{enumerate}
\item[75.] $(3x-4)^2+(x-5)(2)(3x-4)(3)$ 
\end{enumerate} 

\noindent
\textbf{Linear equations}

\noindent
The following exercises are drawn from the exercises for section 1.1 starting on page $84$.


\noindent
For which values of $x$ do the following equations hold:
\begin{enumerate}
\item[20.] $\frac{1}{15}(2x+5) = \frac{x+2}{9}$
\item[29.] $4(2x+7) = 2x +22 +3(2x+2)$
\item[32.] $-8(x+5) = -8x-5(x+8)$
\item[36.] $-6(2x+1) -3(x-4) = -15x+1$
\item[58.] $-x = (5x+3)(3k+1)$
\item[60.] Levada Qualls borrows \$$30,900$ from her bank to open a florist shop. 
She agrees to repay the money in $18$ months with simple annual interest of $5.5\%$.
\begin{enumerate}
\item[(a)] How much must she pay the bank in $18$ months?
\item[(b)] How much of the amount in part (a) is interest?
\end{enumerate}
\end{enumerate}

\noindent
\textbf{Complex numbers}

\noindent
The following exercises are drawn from the exercises for section 1.3 starting on page $102$.

\noindent
Identify the following numbers as real, complex, imaginary or nonreal complex:
\begin{enumerate}
\item[12.] $-6-2i$
\item[13.] $\pi$
\item[16.] $\sqrt{-36}$
\end{enumerate}

\noindent
Write each of the following expressions in standard form $a+bi$:
\begin{enumerate}
\item[25.] $\sqrt{-13}\cdot\sqrt{-13}$
\item[30.] $\frac{\sqrt{-70}}{\sqrt{-7}}$
\item[37.] $\frac{-6-\sqrt{-24}}{2}$
\item[52.] $(-2+3i)(4-2i)$
\item[71.] $\frac{2-i}{2+i}$
\end{enumerate}

\noindent
\textbf{Quadratic equations}

\noindent
The following exercises are drawn from the exercises for section 1.4 starting on page $112$.

\noindent
Solve the following equations using the suggested method:
\begin{enumerate}
\item[13.] $x^2-5x+6 = 0$; zero-factor property
\item[21.] $4x^2-4x+1 = 0$; zero-factor property
\item[29.] $x^2 = -81$; square root property
\item[33.] $(x+5)^2 = -3$; square root property
\item[38.] $x^2-7x+12 = 0$; completing the square
\item[44.] $3x^2+2x = 5$; completing the square
\item[58.] $-6x^2 = 3x+2$; quadratic formula
\item[67.] $x^3-8 = 0$; factoring and quadratic formula
\end{enumerate}

\noindent
Evaluate the discriminant of the following quadratic equations and use it to predict the number of distinct solutions and whether they are rational, irrational or nonreal complex:
\begin{enumerate}
\item[84.] $x^2+4x+4 = 0$ 
\item[87.] $4x^2 = -6x+3$
\end{enumerate} 
\end{document}

