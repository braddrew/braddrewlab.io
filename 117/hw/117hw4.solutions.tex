\documentclass[letterpaper,11pt]{article}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, etoolbox, hyperref, microtype, mathtools, polynom, sectsty}
\usepackage{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newtheorem{exercise}{Exercise}

\begin{document}

\LARGE
\textsc{Math} $117$\\ 
\Large
Homework $4$ Solutions \\
\normalsize

\vspace{\baselineskip}

\begin{exercise}
\label{1}
Sketch the graph of the function $f(x) = -\frac{1}{3}(x+1)^3-3$ and determine the intervals of its domain on which it is increasing or decreasing.
\end{exercise}
\begin{proof}
[Solution]
We can recover the graph of $f$ from that of the basic function $g(x) = x^3$ as follows: replacing $x$ by $x+1$ leads to a translation to the left by $1$; multiplying by $-\frac{1}{3}$ reflects the graph across the $x$-axis and shrinks the graph vertically; finally, adding $-3$ translates the graph downwards by $3$ units. 
The end result is the following:

\begin{center}
\includegraphics[scale=.3]{117hw4graphic1.png}
\end{center}

The same reasoning and the fact that $g(x)=x^3$ is increasing on $(-\infty,\infty)$, we find that $f$ is decreasing on $(-\infty,\infty)$ and nowhere is it increasing.
\end{proof}

\begin{exercise}
\label{2}
Factor the function $f(x) = x^4+3x^3-3x^2-11x-6$ as a product of linear polynomials and graph it.
\end{exercise}
\begin{proof}
By the rational zeros theorem, if $f$ admits any rational zeros, then they are contained in the following set:
$
\{
\pm1, \pm2, \pm3, \pm6
\}
$.
Let's try $-1$ first:
\[
\polyhornerscheme[x=-1]{x^4+3x^3-3x^2-11x-6}
\]
Fortunately, the remainder is $0$, so $x+1$ is a factor and the quotient is $q(x)=x^3+2x^2-5x-6$.
Notice that $q(-1)=(-1)^3+2(-1)^2-5(-1)-6=0$, so $x+1$ is also a factor of $q(x)$ and the computation
\[
\polyhornerscheme[x=-1]{x^3+2x^2-5x-6}
\]
shows that $f(x)/(x+1)^2=q(x)/(x+1)=x^2+x-6$.
This is a garden-variety quadratic polynomial and either by observation or by the quadratic formula, we find that $x^2+x-6=(x-2)(x+3)$, so \fbox{$f(x) = (x+1)^2(x-2)(x+3)$.}

This factorization tells us that the $x$-intercepts of $y=f(x)$ are $x=-3,-1,2$.
Since $f$ is of degree $4$, which is even, and the leading coefficient is $1$, which is positive, we know that $f(x) \to \infty$ as $|x| \to \infty$.
Also, $y=f(x)$ has a $y$-intercept at $f(0)=-6$.
This tells us that the graph is below the $x$-axis between the $x$-intercepts at $x=-1$ and $x=2$ and above the $x$-axis to the left of $x=-3$ and to the right of $x=2$.
It remains to consider the interval $(-3,-1)$, so we test the value $x=-2$ and find that $f(-2)=(-2)^4+3(-2)^3-3(-2)^2-11(-2)-6 = 16-24-12+22-6 = -4$, so the graph is below the $x$-axis from $x=-3$ to $x=-1$.

Piecing this all together, we find the graph:
\begin{center}
\includegraphics[scale=.3]{117hw4graphic2.png}
\end{center}
\end{proof}

\begin{exercise}
\label{3}
\
\begin{enumerate}
\item Describe the end behavior of the function $f(x) = 3+2x- 10x^4 -5x^2$.
\item Does the function $f(x) = x^4+x^3-6x^2-20x-16$  have a real root between $3.7$ and $3.8$? Justify your response.
\end{enumerate}
\end{exercise}
\begin{proof}
[Solution]
(1) The leading term of $f$ is $-10x^4$, so $f$ is of degree $4$ and its leading coefficient is $-10$.
Since the degree is even and the leading coefficient is negative, as $|x|\to\infty$, we know that $f(x) \to -\infty$.

(2) This looks like an intermediate value theorem question, but when we compute $f(3.7)$ and $f(3.8)$, we find that they are both positive \emph{and so we cannot deduce anything from the intermediate value theorem}.
But if we use synthetic division to compute $f(3.7)$, then we can use the boundedness theorem to answer the question:
\[
\begin{array}{r|rrrrr}
& 1 & 1 & -6 & -20 & -16 \\
3.7 & & 3.7 & 17.39 & 42.143 & 81.9291 \\ \cline{2-6}
\multicolumn{2}{r}{1} & 4.7 & 11.39 & 22.143 & 65.9291
\end{array}
\]
Since $3.7>0$ and all the entries in the bottom row are positive, the boundedness theorem tells us that there is root of $f$ greater than $3.7$ and, in particular, no root between $3.7$ and $3.8$.
\end{proof}

\begin{exercise}
\label{4}
Does the function $f(x) = 3x^4+2x^3-4x^2+x-1$ have a real root less than $-2$? Justify your response.
\end{exercise}
\begin{proof}
Let's consider the boundedness theorem.
Specifically, since the leading coefficient $3$ is positive and $-2$ is negative, it suffices to consider the synthetic division by $x+2$ and see whether the signs in the bottom row alternate.
\[
\polyhornerscheme[x=-2]{3x^4+2x^3-4x^2+x-1}
\]
Indeed, the signs to alternate, so the boundedness theorem tells us that there is no zero less than $-2$.
\end{proof}

\end{document}
