\documentclass[letterpaper,11pt]{article}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, etoolbox, hyperref, microtype, mathtools, polynom, sectsty}
\usepackage{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newcounter{thm}
\newtheorem{exercise}[thm]{Exercise}

\begin{document}

\LARGE
\textsc{Math} $117$\\ 
\Large
Homework $5$ Solutions\\
\normalsize

\vspace{\baselineskip}

\begin{exercise}[20 points]
\label{1}
Use synthetic division to decide whether the given number $k$ is a root of the polynomial function $f$.
If $k$ is not a zero of $f$, give the value of $f(k)$.
\begin{enumerate}
\item $f(x) = 3x^4+13x^3-10x+8$; $k=-\frac{4}{3}$
\item $f(x) = 16^4+3x^2-2$; $k=\frac{1}{2}$
\end{enumerate}
\end{exercise}
\begin{proof}
[Solution]
(1) We perform synthetic division by $x-(-4/3)$:
\[
\begin{array}{r|rrrrr}
& 3 & 13 & 0 & -10 & 8 \\
-\frac{4}{3} & & -4 & -12 & 16 & -8 \\ \cline{2-6}
\multicolumn{2}{r}{3} & 9 & -12 & 6 & 0
\end{array}
\]
The remainder is $0$, so $-\frac{4}{3}$ is indeed a root of $f$.

(2) Again, we perform synthetic division, this time by $x-(1/2)$:
\[
\begin{array}{r|rrrrr}
& 16 & 0 & 3 & 0 & -2 \\
\frac{1}{2} & & 8 & 4 & 3.5 & 1.75 \\ \cline{2-6}
\multicolumn{2}{r}{16} & 8 & 7 & 3.5 & -0.25 
\end{array}
\]
This time, the remainder is nonzero, so $\frac{1}{2}$ is not a root of $f$. 
The remainder theorem tells us that $f(1/2)$ equals the remainder of this division, so \fbox{$f(1/2)=-\frac{1}{4}$.}
\end{proof}

\begin{exercise}[20 points]
\label{2}
Consider the polynomial function $f(x) = x^3+5x^2+2x-8$.
\begin{enumerate}
\item List all possible rational zeros of $f$.
\item List all actual rational zeros of $f$ and factor $f$ as a product of linear functions.
\end{enumerate}
\end{exercise}
\begin{proof}
[Solution]
(1) By the rational zeros theorem, the possible rational zeros are \fbox{$\{\pm1, \pm2, \pm4, \pm8\}$,} since the leading coefficient is $1$, the constant coefficient is $-8$ and the positive integer factors of $8$ are $1$, $2$, $4$ and $8$.

(2) Just looking at the coefficients of $f$, it's easy to spot that $x=1$ is a root.
Synthetically dividing by the factor $x-1$, we find
\[
\polyhornerscheme[x=1]{x^3+5x^2+2x-8}
\]
so it remains to factor the quadratic $x^2+6x+8$.
By inspection or by the quadratic formula, we see that $x^2+6x+8 = (x+2)(x+4)$, so the rational zeros of $f$ are \fbox{$\{1,-2,-4\}$} and \fbox{$f(x) = (x-1)(x+2)(x+4)$}.
\end{proof}

\begin{exercise}[30 points]
\label{3}
Give equations for all vertical, horizontal and oblique asymptotes of the following functions and then graph the functions and their asymptotes:
\begin{enumerate}
\item $f(x) = \frac{x^2-7x+10}{x^2+9}$
\item $g(x) = \frac{2x^2+3}{x-4}$
\item $h(x) = \frac{x}{4-x^2}$
\end{enumerate}
\end{exercise}
\begin{proof}
[Solution]
(1) Note that $x^2-7x+10 = (x-2)(x-5)$ and $x^2+9$ has no real roots.
In particular, the numerator and denominator have no common factor and so $f$ is in lowest terms. 
The function $x^2+9$ is never zero, so there are no vertical asymptotes.
Since the numerator and denominator of $f$ have the same degree, there is a horizontal asymptote at $y=\frac{1}{1}=1$.

There is a $y$-intercept at $f(0)=\frac{10}{9}$.
The $x$-intercepts are the solutions of 
\begin{align*}
0&= \frac{x^2-7x+10}{x^2+9} \\
0 &= x^2-7x+10 \\
0 &= (x-2)(x-5)
\end{align*}
i.e. $x=2$ and $x=5$.
The graph intersects the horizontal asymptote at the solutions of 
\begin{align*}
1 &= \frac{x^2-7x+10}{x^2+9} \\
x^2+9 &= x^2-7x+10 \\
9 &= -7x+10 \\
x &=\frac{1}{7} 
\end{align*}
Piecing this information together and thinking about what it means to have a function intersecting a horizontal asymptote at one point, we find that the graph must look like this:
\begin{center}
\includegraphics[scale=.3]{117hw5graphic3.png}
\end{center}

(2) The rational expression is in lowest terms.
The denominator has a root at $x=4$, so there is a vertical asymptote at $x=4$.
The degree of the numerator is one greater than that of the denominator, so there is an oblique asymptote, which we compute as follows:
\[
\polyhornerscheme[x=4]{2x^2+3}
\]
We disregard the remainder $35$ and find that the oblique asymptote is $y=2x+8$.

The $y$-intercept is $g(0)=-\frac{3}{4}$.
The $x$-intercepts are the solutions of 
\begin{align*}
0 &= 2x^2+3 \\
x^2 &= -\frac{3}{2}
\end{align*}
but there is no real number $x$ whose square is negative, so there are no $x$-intercepts.
The intersections with the oblique asymptote are the solutions of 
\begin{align*}
2x+8 & = \frac{2x^2+3}{x-4} \\
(2x+8)(x-4) &= 2x^2+3 \\
2x^2-32 &= 2x^2+3 \\
-32 &= 3
\end{align*}
This is false, so there are no intersections with $y=2x+8$.
The $y$-intercept and the fact that there is no intersection with the oblique asymptote tell us the shape of the graph to the left of $x=4$.
We also see that as $x\to \infty$, $f(x)\to \infty$, so we also know the shape of the graph to the right of $x=4$, and it must look like this:
\begin{center}
\includegraphics[scale=.3]{117hw5graphic32.png}
\end{center}

(3) Again, the rational expression is in lowest terms.
The denominator factors as $-(x^2-4) = -(x+2)(x-2)$, so we have vertical asymptotes at $x=2$ and $x=-2$.
The degree of the denominator is greater than that of the numerator, so there is a horizontal asymptote at $y=0$.

The $y$-intercept is $h(0)=0$.
The $x$-intercepts are the solutions of $0 = \frac{x}{4-x^2}$, i.e. $x=0$.
Since $h(-1)= -\frac{1}{3}<0$,  $h(1) = \frac{1}{3}>0$ and there's only one intersection with the $x$-axis (at $x=0$), we can guess the shape of the graph between $x=-2$ and $x=2$.
Thinking about the sign of $h(x)$ as $x\to \infty$ and $x\to-\infty$, we deduce that the graph must look like this:
\begin{center}
\includegraphics[scale=.3]{117hw5graphic33.png}
\end{center}
\end{proof}

\begin{exercise}[30 points]
\label{4}
Consider $f(x) = \frac{-1}{x+1}$, $g(x) = \frac{1-x}{x}$ and $h(x) = \frac{-3x+12}{x-6}$.
\begin{enumerate}
\item Determine whether $f$ and $g$ are inverses, where $f(x) = \frac{-1}{x+1}$ and $g(x) = \frac{1-x}{x}$.
\item Give the domain and range of $h$ and determine whether $h$ is one-to-one and, if so, write an equation for its inverse function in the form $y=h^{-1}(x)$.
\item Give the domain and range of $h^{-1}$ and graph $h$ and $h^{-1}$ on the same axes.
\end{enumerate}
\end{exercise}
\begin{proof}
[Solution]
(1) Since
\[
f(g(x)) 
=
f\left(\frac{1-x}{x}\right)
=
\frac{-1}{\frac{1-x}{x}+1}
=
\frac{-1}{\frac{(1-x)+x}{x}}
=
\frac{-x}{(1-x)+x}
=
-x
\neq x
\]
$f$ and $g$ cannot be inverse functions.

(2) The domain of $h$ is $(-\infty,6) \cup (6,\infty)$.
The range of $h$ is the set of real numbers $y$ such that $y$ can be written in the form $y=h(x)$ for some $x$ in the domain of $h$.
We have
\begin{align*}
y &= \frac{-3x+12}{x-6} \\
y(x-6) &= -3x+12 \\
yx-6y &= -3x+12 \\
x(y+3) -6y &= 12 \\
x &= \frac{6y+12}{y+3}
\end{align*}
for each $y\neq -3$, the range is all real numbers except $-3$, i.e. $(-\infty,-3)\cup (-3,\infty)$.
To check that $h$ is one-to-one, suppose $h(a)=h(b)$:
\begin{align*}
\frac{-3a+12}{a-6} &= \frac{-3b+12}{b-6} \\
(-3a+12)(b-6) &= (-3b+12)(a-6) \\
-3ab+18a+12b-72 &= -3ab+18b+12a-72 \\
6a &= 6b \\
a &=b
\end{align*}
so $h$ is indeed one-to-one.
We already found the inverse of $h$ when we computed its range: \fbox{$h^{-1}(x) = \frac{6x+12}{x+3}$.}

(3) The domain of $h^{-1}$ is the range of $h$, i.e. $(-\infty,-3)\cup(-3,\infty)$.
The range of $h^{-1}$ is the domain of $h$, i.e. $(-\infty,6)\cup(6,\infty)$.
The graph of $h$ is in red and that of $h^{-1}$ in blue:
\begin{center}
\includegraphics[scale=.3]{117hw5graphic4.png}
\end{center}
\end{proof}

\end{document}
