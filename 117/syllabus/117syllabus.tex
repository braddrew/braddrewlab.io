\documentclass[letterpaper,11pt]{article}
\usepackage{etoolbox, hyperref, microtype, wrapfig}
\usepackage[margin=1in]{geometry}
\usepackage[sfmath,frenchstyle]{kpfonts}
\usepackage[default]{cantarell}
\usepackage[T1]{fontenc}
\setlength{\parindent}{0pt}

\begin{document}

\noindent
\LARGE
\textbf{\textsc{Math} $\mathbf{117}$ Fall $\mathbf{2014}$
\\
Introduction to mathematics for business and economics}

\vspace{.5\baselineskip}

\normalsize
\large
\textbf{Instructor:} Brad Drew (\href{mailto:bdrew@usc.edu}{\texttt{bdrew@usc.edu}}) \\
\normalsize
\textbf{Office:} KAP 406E \\
\textbf{Lecture:} MWF 11am in ZHS 352 \\
\textbf{Office Hours:} W 3:30pm-5:00pm, F 1:30pm-3:00pm in KAP 406E or by appointment

\vspace{.5\baselineskip}

\large
\textbf{Teaching Assistant:} Shuang Li (\href{mailto:lishuang@usc.edu}{\texttt{lishuang@usc.edu}}) \\
\normalsize
\textbf{Office:} KAP 424F \\
\textbf{Discussions:} TTh 11am, 12pm, 1pm in KAP 138 \\
\textbf{Office Hours:} TBD

\vspace{.5\baselineskip}

\large
\textbf{Course description} \\
\normalsize
This course constitutes a study of the notions of function and equality, with particular attention devoted to several special classes of functions that play fundamental roles in quantitative analyses in many of the natural and social sciences: polynomial, rational, exponential and logarithmic functions.
We will also study matrix arithmetic and apply it to the solution of systems of linear equations and further topics as time permits.

\vspace{.5\baselineskip} 

\large 
\textbf{Prerequisites:} 
\normalsize
\textsc{Math} $040$x or math placement exam

\vspace{.5\baselineskip}

\large
\textbf{Textbook:}
\normalsize
\textit{College Algebra \& Trigonometry}, fifth edition, Lial, Hornsby, Schneider, Daniels

\vspace{.5\baselineskip}

\large
\textbf{Grading policies} 
\normalsize

\textbf{Quizzes and midterms:}
There will be 20-minute quizzes during the Thursday discussion sections on September 11 and October 23 and 50-minute midterms during the Wednesday lectures on October 1 and November 12.

\textbf{Homework:}
Homework assignments will be collected for grading in discussion section most Tuesdays of weeks in which neither a quiz nor a midterm is scheduled.

\textbf{Final exam:}
The final exam will take place on Wednesday, December 10 from 11am to 1pm.

\textbf{Final grades:} 
Each quiz will carry the same weight as each homework assignment and the two lowest scores among the combined quiz and homework grades will be dropped.
Homework will not be accepted after the designated deadline and make-up quizzes will not be allowed.
Without a valid excuse, each missed homework or quiz will figure into the final grade calculation as a $0\%$.
Final grades will be calculated as follows:
\begin{center}
\begin{tabular}{l}
Quizzes and homeworks: $25\%$ \\
Midterm I: $20\%$ \\
Midterm II: $20\%$ \\
Final exam: $35\%$ \\
\end{tabular}
\end{center}

%\vspace{.5\baselineskip}

\large
\textbf{Collaboration} \\
\normalsize
Students are encouraged to discuss course material and homework problems with one another. 
However, students should write up solutions individually and acknowledge collaborators by name on collected assignments.
Collaboration is not permitted during quizzes or exams and students are expected to uphold the University's standards of academic integrity recalled below. 

\vspace{.5\baselineskip}

\large
\textbf{Technology and resources} \\
\normalsize
Neither electronic nor written resources will be permitted during quizzes or exams.
Students are furthermore discouraged from relying on calculators or computer programs while studying for this course and preparing homework assignments.

\vspace{.5\baselineskip} 

\large
\textbf{Course website:}
\normalsize
\url{http://www-bcf.usc.edu/~bdrew/117/}\\
Course information and materials will also be hosted at \url{https://blackboard.usc.edu/}.


\vspace{.5\baselineskip}

\large
\textbf{Course schedule (subject to change)} 
\normalsize
\begin{center}
\begin{tabular}{|l|l|l|l|}
\hline
\S\S & \textbf{Topics} & \textbf{Dates} \\
\hline
1.4, 1.7, 1.8 & Equations and inequalities & 8/25-9/5 \\
\hline
& Quiz I & 9/11 \\
\hline
2.1-2.7 & Graphs and functions & 9/8-9/26 \\
\hline
& Midterm I & 10/1 \\
\hline
3.4, 3.5 & Polynomial and rational functions & 9/29-10/8 \\
\hline
4.1-4.6 & Inverse functions, exponentials and logarithms & 10/10-10/24 \\
\hline
& Quiz II & 10/23 \\
\hline 
9.1-9.8 & Systems of equations and inequalities & 10/27-11/19 \\
\hline 
& Midterm II & 11/12 \\
\hline
11.1-11.7 & Sequences, induction, counting & 11/21-12/5 \\
\hline
& Final exam & 12/10 \\
\hline
\end{tabular}
\end{center}

\vspace{.5\baselineskip}

\large
\textbf{Office hours and the Math Center} \\
\normalsize
Students are strongly encouraged to ask questions, to attend office hours and to make use of the Math Center (KAP 263, \url{https://dornsife.usc.edu/mathcenter}) for help.

\vspace{.5\baselineskip}

\large
\textbf{Statement for students with disabilities} \\
\normalsize
Any student requesting academic accomodations based on a disability is required to register with Disability Services and Programs (DSP) each semester.
A letter of verification for approved accomodations can be obtained from DSP.
Please be sure the letter is delivered to me (or to Shuang Li) as early in the semester as possible.
DSP is located in STU 301 and is open 8:30am-5:00pm, Monday through Friday. Website and contact information for DSP: \\
\url{http://sait.usc.edu/academicsupport/centerprograms/dsp/home_index.html},\\ 
213-740-0776 (phone), 213-740-6948 (TDD only), 213-7408216 (fax), \href{mailto:ability@usc.edu}{ability@usc.edu}.

\vspace{.5\baselineskip}

\large
\textbf{Statement on academic integrity} \\
\normalsize
USC seeks to maintain an optimal learning environment.
General principles of academic honesty include the concept of respect for the intellectual property of others, the expectation that individual work will be submitted unless otherwise allowedby an instructor, and the obligations both to protect one's own academic work from misuse by others as well as to avoid useing another's work as one's own.
All students are expected to understand and abide by these principles.
\emph{SCampus}, the Student Guidebook, (\url{www.usc.edu/scampus} or \url{http://scampus.usc.edu}) contains the University Student Conduct Code (see University Governance, Section 11.00), while the recommended sanctions are located in Appendix A.

\vspace{.5\baselineskip}
 

\textbf{Disclaimer:} This syllabus is not a contract and the instructor reserves the right to make modifications to it over the course of the semester.
\end{document}

