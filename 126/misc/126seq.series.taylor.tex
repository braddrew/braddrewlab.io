\documentclass[letterpaper, 11pt]{article}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, enumitem, etoolbox, hyperref, microtype, mathtools}
\usepackage[margin=2cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[mathcal]{eucal}
\usepackage[frenchstyle, largesmallcaps]{kpfonts}
%\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newtheorem{ex}{Example}

\renewcommand{\d}[1]{\mathrm{d} #1}

\begin{document}

\noindent
\LARGE
\textsc{Math} $126$ 
\hfill
\large
March $10$, $2017$ \\
\normalsize

\section*{Convergence of sequences}
\noindent 
We have the following tests for determining whether a \emph{sequence} $\{a_n\}_{n \geq 0}$ is convergent:

\begin{itemize}
\item If $f$ is a function such that $f(n) = a_n$ for each $n \geq 0$ and $\displaystyle \lim_{x \to \infty} f(x) = L$, then $\displaystyle \lim_{n \to \infty} a_n = L$.
\item \textbf{Squeeze Theorem}: If there are two sequences $\{b_n\}_{n \geq 0}$ and $\{c_n\}_{n \geq 0}$ such that $b_n \leq a_n \leq c_n$ and such that $\displaystyle \lim_{n \to \infty} b_n = L = \lim_{n \to \infty} c_n$, then $\displaystyle \lim_{n \to \infty} a_n = L$.
\item \textbf{Monotone Convergence Theorem}: If $\{a_n\}_{n \geq 0}$ is (eventually) monotonic and (eventually) bounded, then it is convergent.
\end{itemize}

\noindent
The following properties of limits allow us to perform many computations.
If $\{a_n\}_{n \geq 0}$ and $\{b_n\}_{n \geq 0}$ are convergent sequences, and $c$ is constant, then:
\begin{itemize}
\item $\displaystyle \lim_{n \to \infty} (a_n + b_n) = \lim_{n \to \infty} a_n + \lim_{n \to \infty} b_n$
\item $\displaystyle \lim_{n \to \infty} ca_n = c \lim_{n \to \infty} a_n$
\item $\displaystyle \lim_{n \to \infty} (a_nb_n) = \left(\lim_{n \to \infty} a_n\right)\left(\lim_{n \to \infty} b_n\right)$
\item $\displaystyle \lim_{n \to \infty} \frac{a_n}{b_n} = \frac{\displaystyle \lim_{n \to \infty} a_n}{\displaystyle \lim_{n \to \infty} b_n}$ \quad provided that $\displaystyle \lim_{n \to \infty} b_n \neq 0$
\item $\displaystyle \lim_{n \to \infty} a_n^p = \left(\lim_{n \to \infty} a_n\right)^p$ \quad provided that $a_n \geq 0$ and $p >0$
\end{itemize}


\clearpage

\section*{Convergence of series}

\subsection*{Tests for convergence}

\noindent
Consider a series $\sum_{n =0}^{\infty} a_n$.

\newtheorem*{divtest}{Divergence test}
\begin{divtest}
If the \emph{sequence} $\{a_n\}_{n \geq 0}$ does not converge to zero, then the \emph{series} $\sum_{n = 0}^{\infty} a_n$ diverges.
\end{divtest}

\newtheorem*{geomseries}{Geometric series}
\begin{geomseries}
The geometric series
$
\sum_{n = 0}^{\infty} ar^n = a + ar + ar^2+ \cdots
$
is convergent if and only if $|r| < 1$, and in that case its sum is
$
\sum_{n = 0}^{\infty} ar^n = \frac{a}{1-r}.
$
\end{geomseries}

\newtheorem*{inttest}{Integral test}
\begin{inttest}
If $f$ is a continuous, (eventually) positive, (eventually) decreasing function defined on $[1, \infty)$ and $a_n = f(n)$ for each integer $n \geq 1$, then $\sum_{n = 1}^{\infty} a_n$ is convergent if and only if the improper integral $\int_1^{\infty} f(x) \d{x}$ is convergent.
\end{inttest}

\newtheorem*{ptest}{$p$-series test}
\begin{ptest}
The series $\sum_{n =1}^{\infty} \frac{1}{n^p}$ is convergent if and only if $p>1$. 
\end{ptest}

\newtheorem*{comptest}{Comparison test}
\begin{comptest}
Suppose that $0 \leq a_n \leq b_n$ for each $n \geq 0$.
\begin{itemize}[itemsep=0ex]
\item If $\sum_{n = 0}^{\infty} b_n$ is convergent, then $\sum_{n = 0}^{\infty} a_n$ is convergent.
\item If $\sum_{n = 0}^{\infty} a_n$ is divergent, then $\sum_{n = 0}^{\infty} b_n$ is divergent.
\end{itemize}
\end{comptest}

\newtheorem*{limcomptest}{Limit comparison test}
\begin{limcomptest}
If $0 \leq a_n$ and $0 \leq b_n$ for each $n \geq 0$, and if 
\[
\lim_{n \to \infty} \frac{a_n}{b_n} = c
\]
with $0 < c < \infty$, then $\sum_{n = 0}^{\infty} a_n$ is convergent if and only if $\sum_{n = 0}^{\infty} b_n$ is convergent.
\end{limcomptest}

\newtheorem*{altseriestest}{Alternating series test}
\begin{altseriestest}
If $0 \leq a_{n+1} \leq a_n$ for each $n \geq 0$ and $\displaystyle \lim_{n \to \infty} a_n = 0$, then series $\sum_{n = 0}^{\infty} (-1)^na_n$ is convergent.
\end{altseriestest}

\newtheorem*{abscvgtest}{Absolute convergence test}
\begin{abscvgtest}
If $\sum_{n = 0}^{\infty} |a_n|$ is convergent, then so is $\sum_{n = 0}^{\infty} a_n$.
\end{abscvgtest}

\newtheorem*{ratiotest}{Ratio test}
\begin{ratiotest}
Let $L = \displaystyle \lim_{n \to \infty} \left|\frac{a_{n+1}}{a_n}\right|$.
\begin{itemize}[itemsep=0ex]
\item If $L < 1$, then $\sum_{n = 0}^{\infty}$ is absolutely convergent.
\item If $L > 1$, then $\sum_{n = 0}^{\infty}$ is divegent.
\item If $L = 1$, then the test is inconclusive.
\end{itemize}
\end{ratiotest}

\newtheorem*{roottest}{Root test}
\begin{roottest}
Let $L = \displaystyle \lim_{n \to \infty} \sqrt[n]{|a_n|}$.
\begin{itemize}[itemsep=0ex]
\item If $L < 1$, then $\sum_{n = 0}^{\infty}$ is absolutely convergent.
\item If $L > 1$, then $\sum_{n = 0}^{\infty}$ is divegent.
\item If $L = 1$, then the test is inconclusive.
\end{itemize}
\end{roottest}

\subsection*{Properties of series}

\noindent
If $\sum_{n = 0}^{\infty} a_n$ and $\sum_{n = 0}^{\infty} b_n$ are convergent series and $c$ is a constant, then the series $\sum_{n = 0}^{\infty} ca_n$ and $\sum_{n = 0}^{\infty} (a_n + b_n)$ are both convergent, and we have
\[
\sum_{n = 0}^{\infty} ca_n = c\sum_{n = 0}^{\infty} a_n
\quad
\text{and}
\quad
\sum_{n = 0}^{\infty} (a_n + b_n) = \sum_{n = 0}^{\infty} a_n + \sum_{n = 0}^{\infty} b_n.
\]



\clearpage

\section*{Taylor series}

\newtheorem*{intcvg}{Interval of convergence}
\begin{intcvg}
Consider a power series $s(x) = \sum_{n = 0}^{\infty} c_n(x-a)^n$ centered at $a$.
Exactly one of the following statements is true:
\begin{itemize}[itemsep=0ex]
\item the series $s(x)$ converges only for $x = a$;
\item the series $s(x)$ converges for all values of $x$; or
\item there exists $R > 0$ such that the series $s(x)$ converges if $|x - a| < R$ and diverges if $|x - a| > R$.
\end{itemize}
In the first case, the radius of convergence is $0$ and the interval of convergence is $x = a$.
In the second case, the radius of convergence is $\infty$ and the interval of convergence is $(-\infty, \infty)$.
In the third case, the radius of convergence is $R$, and the interval of convergence is
\[
(a-R, a+R),
\quad
(a-R, a+R],
\quad
[a-R, a+r),
\quad
\text{or}
\quad
[a-R, a+R].
\]
In order to determine each one, check whether $s(a-R)$ and $s(a+R)$ converge by hand.
\end{intcvg}

\newtheorem*{findingradius}{Determining the radius of convergence}
\begin{findingradius}
In order to determine the radius of convergence of the power series $\sum_{n = 0}^{\infty} c_n(x-a)^n$, you will usually want to apply the \emph{Ratio Test} or the \emph{Root Test}.
\end{findingradius}

\newtheorem*{taylor}{Taylor series}
\begin{taylor}
Let $a$ be a constant, $f$ be a function whose $n$th derivative $f^{(n)}(a)$ at $a$ exists for each $n \geq 0$.
The \emph{Taylor series of $f$ at $a$} is the power series
\[
T(x) = \sum_{n =0}^{\infty} \frac{f^{(n)}(a)}{n!}(x-a)^n = f(a) + \frac{f'(a)}{1!}(x-a) + \frac{f''(a)}{2!}(x-a)^2 + \frac{f^{(3)}(a)}{3!}(x-a)^3 + \cdots.
\]
The \emph{Taylor polynomial of degree $k$ at $a$} is the polynomial
\[
T_k(x) = \sum_{n = 0}^k \frac{f^{(n)}(a)}{n!}(x-a)^n = f(a) + \frac{f'(a)}{1!}(x-a) + \frac{f''(a)}{2!}(x-a)^2 + \cdots + \frac{f^{(k)}(a)}{k!}(x-a)^k.
\]
\end{taylor}

\newtheorem*{taylorsformula}{Taylor's formula}
\begin{taylorsformula}
If the derivatives $(k+1)$st derivative $f^{(k+1)}(x)$ exists and is continuous for $|x-a|< R$, then there exists some $z$ strictly between $x$ and $a$ such that
\[
R_k(x) \coloneqq f(x) - T_k(x) = \frac{f^{(k+1)}(z)}{(k+1)!}(x-a)^{k+1}.
\]
\end{taylorsformula}

\newtheorem*{taylorapprox}{Error in Taylor approximation}
\begin{taylorapprox}
Taylor's formula gives us a means of determining how far the $k$th Taylor approximation $T_k(x)$ is from the actual value $f(x)$ in the worst case scenario.
Typically, things work like this: if $f$ satisfies the hypotheses of Taylor's formula, and 
\[
\left|\frac{f^{(k+1)}(z)}{(k+1)!}(x-a)^{k+1}\right| \leq \varepsilon
\]
for each $z$ between $x$ and $a$, then $|f(x) - T_k(x)| \leq \varepsilon$.
\end{taylorapprox}

\clearpage

\newtheorem*{importantex}{Important Taylor series}
\begin{importantex}
Here are some important Taylor series centered at $a = 0$:
\begin{itemize}[itemsep=0ex]
\item $\displaystyle \frac{1}{1-x} = \sum_{n = 0}^{\infty} x^n$ 
\item $\displaystyle \mathrm{e}^x = \sum_{n = 0}^{\infty} \frac{x^n}{n!}$
\item $\displaystyle \sin(x) = \sum_{n = 0}^{\infty} (-1)^n \frac{x^{2n+1}}{(2n+1)!}$
\item $\displaystyle \cos(x) = \sum_{n = 0}^{\infty} (-1)^n \frac{x^{2n}}{(2n)!}$
\item $\displaystyle \arctan(x) = \sum_{n = 0}^{\infty} (-1)^n \frac{x^{2n+1}}{2n+1}$
\item $\displaystyle (1+x)^k = \sum_{n = 0}^{\infty} \binom{k}{n}x^n = \sum_{n = 0}^{\infty} \frac{k(k-1)\cdots (k-n+1)}{n!}x^n$
\end{itemize}
\end{importantex}

\end{document}
