\documentclass[letterpaper, 11pt]{article}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, enumitem, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[margin=2cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[mathcal]{eucal}
\usepackage[frenchstyle, largesmallcaps]{kpfonts}
%\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newtheorem{ex}{Example}

\renewcommand{\d}[1]{\mathrm{d} #1}

\begin{document}

\pagestyle{empty}

\noindent
\LARGE
\textsc{Math} $126$ 
\hfill
\large
March $9$, $2017$ \\
\normalsize

\begin{center}
\Large
\textsc{Taylor series}
\normalsize
\end{center}

\newtheorem*{intcvg}{Interval of convergence}
\begin{intcvg}
Consider a power series $s(x) = \sum_{n = 0}^{\infty} c_n(x-a)^n$ centered at $a$.
Exactly one of the following statements is true:
\begin{itemize}[itemsep=0ex]
\item the series $s(x)$ converges only for $x = a$;
\item the series $s(x)$ converges for all values of $x$; or
\item there exists $R > 0$ such that the series $s(x)$ converges if $|x - a| < R$ and diverges if $|x - a| > R$.
\end{itemize}
In the first case, the radius of convergence is $0$ and the interval of convergence is $x = a$.
In the second case, the radius of convergence is $\infty$ and the interval of convergence is $(-\infty, \infty)$.
In the third case, the radius of convergence is $R$, and the interval of convergence is
\[
(a-R, a+R),
\quad
(a-R, a+R],
\quad
[a-R, a+R),
\quad
\text{or}
\quad
[a-R, a+R].
\]
In order to determine each one, check whether $s(a-R)$ and $s(a+R)$ converge by hand.
\end{intcvg}

\newtheorem*{findingradius}{Determining the radius of convergence}
\begin{findingradius}
In order to determine the radius of convergence of the power series $\sum_{n = 0}^{\infty} c_n(x-a)^n$, you will usually want to apply the \emph{Ratio Test} or the \emph{Root Test}.
\end{findingradius}

\newtheorem*{taylor}{Taylor series}
\begin{taylor}
Let $a$ be a constant, and $f$ a function whose $n$th derivative $f^{(n)}(a)$ at $a$ exists for each $n \geq 0$.
The \emph{Taylor series of $f$ at $a$} is the power series
\[
T(x) = \sum_{n =0}^{\infty} \frac{f^{(n)}(a)}{n!}(x-a)^n = f(a) + \frac{f'(a)}{1!}(x-a) + \frac{f''(a)}{2!}(x-a)^2 + \frac{f^{(3)}(a)}{3!}(x-a)^3 + \cdots.
\]
The \emph{Taylor polynomial of degree $k$ at $a$} is the polynomial
\[
T_k(x) = \sum_{n = 0}^k \frac{f^{(n)}(a)}{n!}(x-a)^n = f(a) + \frac{f'(a)}{1!}(x-a) + \frac{f''(a)}{2!}(x-a)^2 + \cdots + \frac{f^{(k)}(a)}{k!}(x-a)^k.
\]
\end{taylor}

\newtheorem*{taylorsformula}{Taylor's formula}
\begin{taylorsformula}
If the derivatives $(k+1)$st derivative $f^{(k+1)}(x)$ exists and is continuous for $|x-a|< R$, then there exists some $z$ strictly between $x$ and $a$ such that
\[
R_k(x) \coloneqq f(x) - T_k(x) = \frac{f^{(k+1)}(z)}{(k+1)!}(x-a)^{k+1}.
\]
\end{taylorsformula}

\newtheorem*{taylorapprox}{Error in Taylor approximation}
\begin{taylorapprox}
Taylor's formula gives us a means of determining how far the $k$th Taylor approximation $T_k(x)$ is from the actual value $f(x)$ in the worst case scenario.
Typically, things work like this: if $f$ satisfies the hypotheses of Taylor's formula, and 
\[
\left|\frac{f^{(k+1)}(z)}{(k+1)!}(x-a)^{k+1}\right| \leq \varepsilon
\]
for each $z$ between $x$ and $a$, then $|f(x) - T_k(x)| \leq \varepsilon$.
\end{taylorapprox}

\clearpage

\newtheorem*{importantex}{Important Taylor series}
\begin{importantex}
Here are some important Taylor series centered at $a = 0$:
\begin{itemize}[itemsep=0ex]
\item $\displaystyle \frac{1}{1-x} = \sum_{n = 0}^{\infty} x^n$ 
\item $\displaystyle \mathrm{e}^x = \sum_{n = 0}^{\infty} \frac{x^n}{n!}$
\item $\displaystyle \sin(x) = \sum_{n = 0}^{\infty} (-1)^n \frac{x^{2n+1}}{(2n+1)!}$
\item $\displaystyle \cos(x) = \sum_{n = 0}^{\infty} (-1)^n \frac{x^{2n}}{(2n)!}$
\item $\displaystyle \arctan(x) = \sum_{n = 0}^{\infty} (-1)^n \frac{x^{2n+1}}{2n+1}$
\item $\displaystyle (1+x)^r = \sum_{n = 0}^{\infty} \binom{r}{n}x^n = \sum_{n = 0}^{\infty} \frac{r(r-1)\cdots (r-n+1)}{n!}x^n$
\end{itemize}
\end{importantex}
\end{document}
