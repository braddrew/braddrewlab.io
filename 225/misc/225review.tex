\documentclass[letterpaper, 10pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, enumitem, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[margin=2cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[mathcal]{eucal}
\usepackage[frenchstyle, largesmallcaps]{kpfonts}
%\setlength{\parindent}{0pt}

\allsectionsfont{\centering\fontseries{m}\scshape}

\theoremstyle{plain}
\newtheorem{thm}{Theorem}
\newtheorem{lemma}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{cor}[thm]{Corollary}
\theoremstyle{definition}
\newtheorem{defn}[thm]{Definition}
\newtheorem{rmk}[thm]{Remark}
\newtheorem{problem}{Problem}
\newtheorem*{ex}{Example}

\renewcommand{\thequestion}{\textsc{Problem} \arabic{question}}
\renewcommand{\thepartno}{\alph{partno}}
\renewcommand{\solutiontitle}{\textsc{Solution:}\enspace}

\firstpagefooter{}{}{}
\runningfooter{}{}{}

\newcommand{\im}{\operatorname{im}}

\begin{document}

\noindent
\LARGE
\textsc{Math} $225$ 
\hfill
\large
April $22$, $2016$ \\
\normalsize

\begin{center}
\Large
\textsc{Review}
\normalsize
\end{center}

\section*{Linear systems of equations}

\begin{enumerate}[topsep=0ex, itemsep=0ex, leftmargin=*]
\item
We began by introducing the notion of an $m \times n$ matrix and the operations of matrix addition and multiplication.
We then defined a linear system of $m$ equations in $n$ variables and observed that such systems are equivalent to matrix equations of the form $A\mathbf{x} = \mathbf{b}$.
\item
After defining elementary row operations, we were able to solve all such systems by row reduction.
In particular, we showed that the ranks of the matrix of coefficients $A$ and the augmented matrix $[A|\mathbf{b}]$ determine the number of solutions of the system:
\begin{enumerate}[topsep=0ex, itemsep=0ex]
\item
if $\operatorname{rk}(A) < \operatorname{rk}(A|\mathbf{b})$, then there are no solutions;
\item
if $\operatorname{rk}(A) = \operatorname{rk}(A|\mathbf{b}) = n$, where $n$ is the number of variables in the system, then there is a unique solution; and
\item
if $\operatorname{rk}(A) = \operatorname{rk}(A|\mathbf{b}) <n$, then there are infinitely many solutions.
\end{enumerate}
\item
Once we were able to solve linear systems, we were in particular able to compute the inverse $A^{-1}$, if it exists, of the square matrix $A$ by row reducing the augmented matrix $[A|I_n]$.
Our understanding of solution sets of linear systems told us that the $n \times n$ matrix $A$ is invertible precisely when $\operatorname{rk}(A) = n$.
\item
For smaller square matrices, we found another method for determining whether $A$ is invertible, namely, computing its determinant $\det(A)$.
Specifically, we said that $A$ was invertible if and only if $\det(A) \neq 0$.
We also learned a couple ways to compute $\det(A)$:
by row-reducing $A$ and keeping track of how each elementary row operation affects the determinant,
and by cofactor expansion.
We also mentioned some properties that can be very useful in simplifying these computations:
$\det(AB) = \det(A)\det(B)$;
$\det(A^T) = \det(A)$; and
$\det(A^{-1}) = \det(A)^{-1}$.
\end{enumerate}

\section*{Vector spaces and linear maps}

\begin{enumerate}[topsep=0ex, itemsep=0ex, leftmargin=*]
\item
We defined a vector space to be a set (of vectors) equipped with the additional structure of scalar multiplication and vector addition, satisfying the same properties as the set of $m \times n$ matrices with respect to matrix addtition and scalar multiplication.
Two of the most important examples are $\mathbf{R}^n$, the vector space of column vectors of length $n$, and $\ker(A)$, the kernel of an $m \times n$ matrix.
\item
We defined subspaces as nonempty subsets of vector spaces that contain the zero vector and are stable under vector addition and scalar multiplication.
For example, if $A$ is an $m \times n$ matrix, then $\ker(A)$ is a subspace of $\mathbf{R}^n$.
Subspaces are vector spaces in their own right.
\item
We then discussed the span of a set of vectors, defined by $\operatorname{span}\{\mathbf{v}_1, \ldots, \mathbf{v}_n\} = \{\sum_{i = 1}^na_i\mathbf{v}_i \mid a_i \in \mathbf{R}\}$, and the condition that such a set be linear dependent, i.e., that there exist scalars $c_1, \ldots, c_n$ not all zero such that $\sum_{i=1}^nc_i\mathbf{v}_i = \mathbf{0}$.
\item
We then defined a basis of a vector space $V$ to be a linearly independent subset that spans $V$.
Any two bases of $V$ have the same number of elements, and we refer to this number as the dimension $\dim(V)$ of $V$.
If $\dim(V) = n$, then any set $S= \{\mathbf{v}_1, \ldots, \mathbf{v}_n\}$ of exactly $n$ vectors in $V$ is a basis if and only if $\operatorname{span}(S) = V$ if and only if $S$ is linearly independent.
\item
If we choose an ordering of the elements of a basis $B$ of $V$, then we can make sense of the coordinate vector $[\mathbf{v}]_V$ of any $\mathbf{v} \in V$ with respect to the ordered basis $V$.
Given two ordered bases $B$ and $B'$ of $V$, we can also define a change of basis matrix $P_{B' \leftarrow B}$ with the property that $[\mathbf{v}]_{B'} = P_{B' \leftarrow B} [\mathbf{v}]_B$.
\item
We defined a linear map $f: V \to W$ between two vector spaces to be a function that preserves vector addition and scalar multiplication.
For example, any $m \times n$ matrix $A$ defines a linear map $\mathbf{R}^n \to \mathbf{R}^m$ sending $\mathbf{x}$ to $A \mathbf{x}$.
More generally, if we choose ordered bases $B$ and $C$ of $V$ and $W$, respectively, then every linear map $f$ is determined by a matrix $[f]^C_B$ relative to these ordered bases: $[f(\mathbf{v})]_C = [f]^C_B[\mathbf{v}]_B$.
\item
Each linear map $f:V \to W$ has a kernel, $\ker(f) \subseteq V$, consisting of the vectors that $f$ sends to $\mathbf{0}$ in $W$, and an image, $\operatorname{im}(f) \subseteq W$, consisting of the outputs of $f$.
These are both subspaces and the rank-nullity theorem tells us that 
$
\dim(\ker(f)) + \dim(\operatorname{im}(f)) = \dim(V).
$
Computing the kernel of $f$ usually involves row reduction, whereas computing the image often involves taking the span of the columns in a matrix representing $f$.
\item
We defined the linear map $f$ to be one-to-one if $\ker(f) = \{\mathbf{0}\}$ and onto if $\operatorname{im}(f) =W$.
\end{enumerate}

\section*{Eigenstuff}

\begin{enumerate}[topsep=0ex, itemsep=0ex, leftmargin=*]
\item
An eigenvalue of the $n \times n$ matrix $A$ is a scalar $\lambda$ such that there exists $\mathbf{0} \neq \mathbf{v} \in \mathbf{R}^n$ such that $A\mathbf{v} = \lambda\mathbf{v}$.
In this situation, we said that $\mathbf{v}$ was an eigenvector for $A$.
\item
In order to find the eigenvalues of $A$, we find the roots of its characteristic polynomial $p(t) = \det(A-tI_n)$.
If $\lambda$ is an eigenvalue of $A$, then we can find corresponding eigenvectors by computing $\ker(A-\lambda I_n)$.
The dimension of $\ker(A-\lambda I_n)$ is the geometric multiplicity of $\lambda$, and its algebraic multiplicity is its multiplicity as a root of $p(t)$.
We said that the geometric multiplicity was never greater than the algebraic multiplicity.
\end{enumerate}

\section*{Linear differential equations}

\begin{enumerate}[topsep=0ex, itemsep=0ex, leftmargin=*]
\item
Linear differential equations of order $n$ look like
$
y^{(n)} + a_{n-1}y^{(n-1)} + \cdots + a_1y'+ a_0y = F,
$
where $a_i$ and $F$ are some functions of $x$ and $y$ is an unknown function of $x$.
If $F(x)=0$ for each $x$, then we say that the linear differential equation is homogeneous.
\item
We solved the first-order equation $y' + py = q$ by multiplying by the integrating factor and integrating.
\item
We solved homogeneous linear differential equations by finding the roots of the auxiliary polynomial $P(r)$:
for each root $\alpha$ of multiplicity $m$, $\mathrm{e}^{\alpha x}$, $x\mathrm{e}^{\alpha x}$, \ldots, $x^{m-1}\mathrm{e}^{\alpha x}$ were $m$ linearly independent possibly complex solutions.
\item
In order to prove that these solutions were linearly independent, we checked that their Wronskian was nonzero.
There are at most $n$ linearly independent solutions to our order $n$ linear differential equation, so once we have found any $n$ linearly independent solutions $y_1, \ldots, y_n$, the general solution is $c_1y_1+ \cdots +c_ny_n$.
In order to find real-valued solutions, we used Euler's formula $\mathrm{e}^{a+bi} = \mathrm{e}^a(\cos(b) + i\sin(b))$ to find the real and imaginary parts of each complex-valued solution.
\item
In order to solve inhomogeneous linear differential equations, we used the method of undetermined coefficients, making an appropriate guess for the particular solution $y_p$ involving some undetermined coefficients, computing the first $n$ derivatives of $y_p$ and solving for the coefficients by plugging everything into the original differential equation.
\item
For inhomogeneous equations of the form $Ly = F_1 + F_2$, we found particular solutions by finding particular solutions $y_{p_1}$ for $Ly = F_1$ and $y_{p_2}$ for $Ly = F_2$ and then adding them: $y_p = y_{p_1} + y_{p_2}$.
Once we have any particular solution, the general solution is $y = y_c + y_p$, where $y_c$ is the general solution to the complementary homogeneous equation.
\end{enumerate}

\section*{First-order linear systems}

\begin{enumerate}[topsep=0ex, itemsep=0ex, leftmargin=*]
\item
Finally, we studied the solutions of constant-coefficient homogeneous first-order linear systems of differential equations, given by equations of the form $\mathbf{x}' = A\mathbf{x}$ with $A$ an $n \times n$ matrix of complex or real numbers.
\item
We showed that solutions to $\mathbf{x}' = A\mathbf{x}$ were determined by eigenvectors of $A$.
In particular, to solve this system, we found $n$ linearly independent, possibly complex, eigenvectors $\mathbf{v}_1,\ldots,\mathbf{v}_n$ for $A$, with corresponding eignevalues $\lambda_1,\ldots,\lambda_n$, and observed that the general solution was $\mathbf{z}(t) = \sum_{i=1}^nc_1\mathrm{e}^{\lambda_it}\mathbf{v}_i$.
\item
If $A$ was a real matrix, we noted that some of its eigenvalues may nevertheless be complex, but they come in complex-conjugate pairs.
We used this to find real-valued solutions:
whenever $\mathbf{v}$ is a complex eigenvector of $A$ with eigenvalue $\lambda = a+bi$ with $b \neq 0$, the complex solution $\mathbf{z}(t) = \mathrm{e}^{\lambda t}\mathbf{v}$ induces two real-valued solutions:
$\mathbf{x}_1(t) = \operatorname{Re}(\mathbf{z}(t))$ and $\mathbf{x}_2(t) = \operatorname{Im}(\mathbf{z}(t))$.
\end{enumerate}

\end{document}
