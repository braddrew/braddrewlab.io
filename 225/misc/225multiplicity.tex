\documentclass[letterpaper, 11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, enumitem, etoolbox, hyperref, microtype, mathtools}
\usepackage[margin=2cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[mathcal]{eucal}
\usepackage[frenchstyle, largesmallcaps]{kpfonts}
%\setlength{\parindent}{0pt}

\theoremstyle{plain}
\newtheorem{thm}{Theorem}
\theoremstyle{definition}
\newtheorem*{defn}{Definition}
\newtheorem{problem}{Problem}
\newtheorem{ex}{Example}

\renewcommand{\thequestion}{\textsc{Problem} \arabic{question}}
\renewcommand{\thepartno}{\alph{partno}}
\renewcommand{\solutiontitle}{\textsc{Solution:}\enspace}

\firstpagefooter{}{}{}
\runningfooter{}{}{}

\newcommand{\im}{\operatorname{im}}

\begin{document}

\noindent
\LARGE
\textsc{Math} $225$ 
\hfill
\large
April $4$, $2016$ \\
\normalsize

\begin{center}
\Large
\textsc{Multiplicities of eigenvalues}
\normalsize
\end{center}

\begin{thm}
If $\lambda \in \mathbf{C}$ is an eigenvalue of $M_{n \times n}(\mathbf{C})$ with corresponding eigenspace $E_{\lambda}$, algebraic multiplicity $m$ and geometric multiplicity $d = \dim(E_{\lambda})$, then $1 \leq d \leq m$.
\end{thm}

\begin{proof}
Since $\lambda$ is an eigenvalue, $E_{\lambda} \coloneqq \ker(A-\lambda I_n) \neq \{\mathbf{0}\}$, so $1 \leq \dim(E_{\lambda}) = d$.
Choose an ordered basis $B = \langle \mathbf{v}_1, \ldots, \mathbf{v}_d\rangle$ for $E_{\lambda}$ and extend it to an ordered basis $B' = \langle \mathbf{v}_1, \ldots, \mathbf{v}_d, \mathbf{w}_{d+1}, \ldots, \mathbf{w}_n\rangle$ for $\mathbf{C}^n$.
Let $f: \mathbf{C}^n \to \mathbf{C}^n$ denote the linear map given by $A$, so that $[f]^E_E = A$, where $E$ denotes the standard basis of $\mathbf{C}^n$.
Then consider $[f]^{B'}_{B'} = P_{B' \leftarrow E}AP_{E\leftarrow B'}$.
Since 
\[
[f]^{B'}_{B'}[\mathbf{v}_i]_{B'} 
= 
[f(\mathbf{v}_i)]_{B'} 
= 
[A\mathbf{v}_i]_{B'}
=
[\lambda\mathbf{v}_i]_{B'}
=
\lambda[\mathbf{v}_i]_{B'}
=
\lambda\mathbf{e}_i,
\]
we find that
\[
[f]^{B'}_{B'}
=
\left[
\begin{array}{ccc|ccc}
& & & & &  \\
& \lambda I_{d} & & & C & \\
& & & & &  \\
\hline
& & & & & \\
& 0 & & & D & \\
& & & & &
\end{array}
\right]
\]
where $C$ is a $d \times (n-d)$ matrix, $D$ an $(n-d) \times (n-d)$ matrix and $0$ is the $(n-d) \times d$ matrix all of whose entries are zero.

Computing the characteristic polynomial of $[f]^{B'}_{B'}$ by cofactor expansion, we find $(\lambda - t)^d\det(D)$.
By Lemma $27.3$, the characteristic polynomial of $[f]^{B'}_{B'}$ equals the characteristic polynomial of $A$, so the characteristic polynomial $p(t)$ of $A$ is $(\lambda-t)^d\det(D)$.
Since $(\lambda-t)^d$ divides $p(t)$, $(t-\lambda)^d = (-1)^d(t-\lambda)$ divides $p(t)$, so $(t-\lambda)^d$ divides $p(t)$.
By definition, $m$ is the \emph{largest} integer such that $(t-\lambda)$ that divides $p(t)$.
Since $d$ is another integer such that $(t-\lambda)^d$ divides $p(t)$ and $m$ is the largest such integer, we must have $d \leq m$.
\end{proof}

\begin{thm}
Let $\lambda_1, \ldots, \lambda_k \in \mathbf{C}$ be distinct eigenvalues of $A \in M_{n \times n}(\mathbf{C})$, i.e., $\lambda_i \neq \lambda_j$ for each $i \neq j$, and, for each $i$, let $\mathbf{v}_i \in \mathbf{C}^n$ be an eigenvector of $A$ with eigenvalue $\lambda_i$.
Then $\{\mathbf{v}_1, \ldots, \mathbf{v}_k\}$ is linearly independent.
\end{thm}

\begin{proof}
If $k \leq 1$, then the claim is evident.
We proceed by induction on $k$.
Suppose that $k > 1$ and that each set of $k-1$ eigenvectors of $A$ with $k-1$ distinct corresponding eigenvalues is linearly independent.
Suppose furthermore that 
\begin{equation}
\label{2.1}
\mathbf{0}
=
a_1\mathbf{v}_1 + \cdots + a_k \mathbf{v}_k.
\end{equation}
Multiplying by $\lambda_1$ and by $A$, we find
\begin{align*}
\lambda_1(a_1\mathbf{v}_1 + a_2\mathbf{v}_2 + \cdots a_k\mathbf{v}_k)
=
\mathbf{0}
&=
A(a_1\mathbf{v}_1 + \cdots + a_k\mathbf{v}_k)
\\
\lambda_1a_1\mathbf{v}_1 + \lambda_1a_2\mathbf{v}_2 + \cdots \lambda_1 a_k\mathbf{v}_k
=
\mathbf{0}
&=
\lambda_1a_1\mathbf{v}_1 + \cdots + \lambda_ka_k\mathbf{v}_k.
\end{align*}
Subtracting the left-hand side from the right-hand side, we have 
\[
\mathbf{0} 
=
(\lambda_1 - \lambda_1)a_1\mathbf{v}_1 + (\lambda_2-\lambda_1)a_2\mathbf{v}_2 + \cdots + (\lambda_k-\lambda_1)a_k\mathbf{v}_k
=
(\lambda_2-\lambda_1)a_2\mathbf{v}_2 + \cdots + (\lambda_k-\lambda_1)a_k\mathbf{v}_k.
\]
By the inductive hypothesis, $\{\mathbf{v}_2, \ldots, \mathbf{v}_k\}$ is linearly independent, so $(\lambda_i-\lambda_1)a_i = 0$ for each $2 \leq i \leq k$.
Since $\lambda_i - \lambda_1 \neq 0$ for $2 \leq i \leq k$, we find $a_2 = a_3 = \cdots = a_k = 0$.
Plugging this into \eqref{2.1}, we have $a_1\mathbf{v}_1 = 0$, so $a_1 = 0$ also.
This proves the claim.
\end{proof}

\end{document}
