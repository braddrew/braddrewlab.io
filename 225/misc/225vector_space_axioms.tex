\documentclass[letterpaper, 11pt]{article}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, enumitem, etoolbox, hyperref, microtype, mathtools}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[frenchstyle, largesmallcaps]{kpfonts}

\theoremstyle{plain}
\newtheorem*{thm}{Theorem}
\newtheorem{lemma}{Lemma}

\theoremstyle{remark}
\newtheorem*{defn}{Definition}
\newtheorem*{example}{Example}

\begin{document}

\LARGE
\textsc{Math} $225$ 
\hfill
\large
February $10$, $2016$ \\
\normalsize

\begin{center}
\Large
\textsc{Axioms for vector spaces}
\normalsize
\end{center}

\noindent
Let $\mathbf{F}$ denote either $\mathbf{R}$ or $\mathbf{C}$.
A \emph{vector space over $\mathbf{F}$} is a set $V$ equipped with rules for scalar multiplication and vector addition that satisfies the following axioms:

\begin{enumerate}
\item[(V1)]
The set $V$ is \emph{closed under vector addition}: for each $\mathbf{v},\ \mathbf{w} \in V$, their sum $\mathbf{v}+\mathbf{w}$ belongs to $V$.
\item[(V2)]
\emph{Vector addition is commutative}: for each $\mathbf{v},\ \mathbf{w} \in V$, we have $\mathbf{v} + \mathbf{w} = \mathbf{w} + \mathbf{v}$.
\item[(V3)]
\emph{Vector addition is associative}: for each $\mathbf{u},\ \mathbf{v},\ \mathbf{w} \in V$, we have $\mathbf{u} + (\mathbf{v} + \mathbf{w}) = (\mathbf{u} + \mathbf{v}) + \mathbf{w}$.
\item[(V4)]
There exists a vector $\mathbf{0} \in V$ such that, for each $\mathbf{v} \in V$, we have $\mathbf{v} + \mathbf{0} = \mathbf{v}$.
We call such a vector $\mathbf{0}$ a \emph{zero vector}.
\item[(V5)]
For each $\mathbf{v} \in V$, there exists $\mathbf{w} \in V$ such that $\mathbf{v} + \mathbf{w} = \mathbf{0}$.
We call such a vector $\mathbf{w}$ an \emph{additive inverse} of $\mathbf{v}$ and write $\mathbf{w} = -\mathbf{v}$.
\item[(V6)]
The set $V$ is \emph{closed under scalar multiplication}: for each $a \in \mathbf{F}$ and $\mathbf{v} \in V$, their product $a \cdot \mathbf{v}$ belongs to $V$.
\item[(V7)]
\emph{Scalar addition is distributive} over scalar multiplication: for each $a,\ b \in \mathbf{F}$ and $\mathbf{v} \in V$, we have $(a+b)\cdot \mathbf{v} = a\cdot \mathbf{v} + b\cdot \mathbf{v}$.
\item[(V8)]
\emph{Vector addition is distributive} over scalar multiplication: for each $a \in \mathbf{F}$ and $\mathbf{v},\ \mathbf{w} \in V$, we have $a\cdot(\mathbf{v} + \mathbf{w}) = a\cdot \mathbf{v} + a\cdot \mathbf{w}$.
\item[(V9)]
\emph{Scalar multiplication is associative}: for each $a,\ b \in \mathbf{F}$ and $\mathbf{v} \in V$, we have $(ab)\cdot \mathbf{v} = a\cdot (b\cdot \mathbf{v})$.
\item[(V10)]
The scalar $1 \in \mathbf{F}$ is a \emph{unit for scalar multipliction}: for each $\mathbf{v} \in V$, we have $1\cdot \mathbf{v} = \mathbf{v}$.
\end{enumerate}

\begin{example}
Let $\mathbf{F} = \mathbf{R}$ and let $V = \mathbf{R}^2$ denote the set of all column vectors of length $2$ with entries in $\mathbf{R}$.
Define scalar multiplication on $\mathbf{R}^2$ by
\[
a\cdot \begin{bmatrix}x \\ y\end{bmatrix}
:=
\begin{bmatrix} ax \\ ay \end{bmatrix}
\]
for each $a \in \mathbf{R}$ and each $(x,y) \in \mathbf{R}^2$.
Define vector addition in $\mathbf{R}^2$ by 
\[
\begin{bmatrix}x \\ y\end{bmatrix}
+
\begin{bmatrix}z \\ w\end{bmatrix}
:=
\begin{bmatrix}x+z \\ y+w\end{bmatrix}
\]
for each $(x,y),\ (z,w) \in \mathbf{R}^2$.
These definitions make $\mathbf{R}^2$ into a vector space over $\mathbf{R}$.
Indeed, we can check each of the axioms as follows:
\begin{enumerate}
\item[(V1)]
if $(x,y),\ (z,y) \in \mathbf{R}^2$, then $(x+z, y+w) \in \mathbf{R}^2$, so $\mathbf{R}^2$ is closed under vector addition;
\item[(V2)]
$(x,y) + (z,w) = (x+z,y+w) = (z+x,w+y) = (z,w) + (x,y)$, so vector addition is commutative;
\item[(V3)]
\begin{align*}
(x,y) + ((z,w) + (t,u)) 
&= (x,y) + (z+t, w+u)
\\
&=
(x+(z+t),y+(w+u))
\\
&=
((x+z)+t,(y+w)+u)
\\
&=
(x+z,y+w) + (t,u)
\\
&=
((x,y) + (z,w)) + (t,u)
\end{align*}
so vector addition is associative;
\item[(V4)]
$(x,y) + (0,0) = (x,y)$ for each $(x,y) \in \mathbf{R}^2$, so $\mathbf{0} := (0,0) \in \mathbf{R}^2$ is the required zero vector;
\item[(V5)]
$(x,y) + (-x,-y) = (x-x,y-y) = (0,0) = \mathbf{0}$, so $(x,y)$ admits the additive inverse $(-x,-y) \in \mathbf{R}^2$;
\item[(V6)]
$a\cdot (x,y) = (ax,ay) \in \mathbf{R}^2$, so $\mathbf{R}^2$ is closed under scalar multiplication;
\item[(V7)]
\begin{align*}
(a+b)\cdot (x,y) 
&= 
((a+b)x,(a+b)y)
\\
&= 
(ax+bx,ay+by) 
\\
&=
(ax,ay) + (bx,by)\\
&=
a\cdot(x,y) + b\cdot(x,y),
\end{align*}
so scalar addition distributes over scalar multiplication;
\item[(V8)]
\begin{align*}
a\cdot ((x,y) + (z,w)) 
&= 
a\cdot (x+z,y+w)
\\
&=
(a(x+z),a(y+w))
\\
&=
(ax+az,ay+aw)
\\
&=
(ax,ay) + (az,aw)
\\
&=
a\cdot(x,y) + a\cdot(z,w),
\end{align*}
so vector addition distributes over scalar multiplication;
\item[(V9)]
$(ab)\cdot(x,y) = ((ab)x,(ab)y)
=
(a(bx),a(by))
=
a\cdot(bx,by)
=
a\cdot(b\cdot(x,y))$,
so scalar multiplication is associative;
\item[(V10)]
$1\cdot(x,y) = (1x,1y) = (x,y)$, so $1$ is a unit for scalar multiplication.
\end{enumerate}
\end{example}

\end{document}
