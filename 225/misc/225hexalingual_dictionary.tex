\documentclass[letterpaper, 11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, enumitem, etoolbox, hyperref, microtype, mathtools, pdflscape, tabularx}
\usepackage[margin=1cm, top=2cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[mathcal]{eucal}
\usepackage[frenchstyle, largesmallcaps]{kpfonts}
\setlength{\parindent}{0pt}

\theoremstyle{plain}
\newtheorem*{thm}{Theorem}
\theoremstyle{definition}
\newtheorem*{defn}{Definition}
\newtheorem{problem}{Problem}
\newtheorem{ex}{Example}

\renewcommand{\thequestion}{\textsc{Problem} \arabic{question}}
\renewcommand{\thepartno}{\alph{partno}}
\renewcommand{\solutiontitle}{\textsc{Solution:}\enspace}

\firstpagefooter{}{}{}
\runningfooter{}{}{}

\newcommand{\im}{\operatorname{im}}
\newcommand{\rk}{\operatorname{rk}}
\newcommand{\linspan}{\operatorname{span}}

\begin{document}

\begin{landscape}

\noindent
\LARGE
\textsc{Math} $225$ 
\hfill
\large
March $23$, $2016$ \\
\normalsize

If you have been haunted by a sense of \emph{d\'ej\`a vu} for much of the semester, it's probably for a good reason.
The following table illustrates how the seemingly large number of different definitions and processes we've studied are different incarnations of the basic ones we saw in the first couple weeks of the semester when we studied linear systems.

\vspace{\baselineskip}

\centering

\def\tabularxcolumn#1{m{#1}}
\begin{tabularx}{9.5in}{|>{\raggedright\hsize=1in}X|>{\raggedright\hsize=1.5in}X|>{\raggedright\hsize=1.2in}X|>{\raggedright\hsize=1.7in}X|>{\raggedright\hsize=1.2in}X|>{\raggedright}X|}
\hline
\textbf{Linear system of $m$ equations in $n$ variables}
&
\textbf{Matrix $A \in M_{m\times n}(\mathbf{R})$ and $\mathbf{b} \in \mathbf{R}^m$}
&
\textbf{List of vectors $S = \{\mathbf{v}_1, \ldots, \mathbf{v}_n\}$ in a vector space $V$ and $\mathbf{v} \in V$}
&
\textbf{Coordinate vectors $S = \{[\mathbf{v}_1]_B, \ldots, [\mathbf{v}_k]_B\}$, $[\mathbf{v}]_B$ relative to an ordered basis $B$}
&
\textbf{Linear map $f:V \to W$}
&
\textbf{Matrix $[f]^C_B$ of a linear map $f: V \to W$ relative to ordered bases $B$ and $C$}
\tabularnewline
\hline
the linear system
&
augmented matrix $[A|\mathbf{b}]$
&
vector equation $\sum_{i=1}^na_i\mathbf{v}_i = \mathbf{v}$ in a vector space $V$
&
vector equation $\sum_{i=1}^ka_i[\mathbf{v}_i]_B = [\mathbf{v}]_B$ in $\mathbf{R}^n$
or augmented matrix $\left[\begin{array}{ccc|c} [\mathbf{v}_1]_B & \cdots & [\mathbf{v}_k]_B & [\mathbf{v}]_B\end{array}\right]$
&
vector equation $f(\mathbf{v}) = \mathbf{w}$ 
&
vector equation $[f]^C_B[\mathbf{v}]_B = [\mathbf{w}]_C$ or augmented matrix $\left[\begin{array}{c|c}[f]^C_B & [\mathbf{w}]_C\end{array}\right]$ 
\tabularnewline
\hline
solving the system
&
row reduction
&
solving the equation
&
solving the equation
&
showing $\mathbf{w} \in \im(f)$
&
row reduction
\tabularnewline
\hline
solution set
&
(R)REF
&
$\{(a_1, \ldots, a_n) \mid \sum_{i=1}^na_i\mathbf{v}_i = \mathbf{v} \}$
&
$\{(a_1, \ldots, a_n) \mid \sum_{i=1}^ka_i[\mathbf{v}_i]_B = [\mathbf{v}]_B \}$
or (R)REF
&
$\{\mathbf{v} \in V \mid f(\mathbf{v}) = \mathbf{w}\}$
&
$\{\mathbf{v} \in V \mid [f]^C_B[\mathbf{v}]_B = [\mathbf{w}]_C\}$
\tabularnewline
\hline
empty solution set
&
$\rk(A) < \rk(A|\mathbf{b})$
&
$\mathbf{v} \not \in \linspan(S)$
&
$[\mathbf{v}]_B \not \in \linspan(S)$
&
$\mathbf{w} \not \in \im(f)$
&
$\rk([f]^C_B) < \rk([f]^C_B|[\mathbf{w}]_C)$
\tabularnewline
\hline
infinite solution set
&
$\rk(A) = \rk(A|\mathbf{b}) < n$
(when $m = n$, $A$ is not invertible and $\det(A) = 0$)
&
$\mathbf{v} \in \linspan(S)$ and $S$ is linearly dependent
&
$[\mathbf{v}]_B \in \linspan(S)$ and $S$ is linearly dependent
&
$\mathbf{w} \in \im(f)$ and $\ker(f) \neq \{\mathbf{0}\}$, i.e., $f$ is not one-one
&
$\rk([f]^C_B) < \rk([f]^C_B|[\mathbf{w}]_C) < \dim(V)$
\tabularnewline
\hline
unique solution
&
$\rk(A) = \rk(A|\mathbf{b}) = n$ (when $m=n$, $A$ is invertible, $\det(A) \neq0$)
&
$\mathbf{v} \in \linspan(S)$ and $S$ is linearly independent
&
$[\mathbf{v}]_B \in \linspan(S)$ and $S$ is linearly independent
&
$\mathbf{w} \in \im(f)$ and $\ker(f) = \{\mathbf{0}\}$, i.e., $f$ is one-one
&
$\rk([f]^C_B) = \rk([f]^C_B | [\mathbf{w}]_C) = \dim(V)$
\tabularnewline
\hline
free variable
&
nonpivot column
&
redundant vector in $S$
&
redundant vector in $S$
&
N/A
&
nonpivot column
\tabularnewline
\hline
bound variable
&
pivot column
&
irredundant vector in $S$
&
irredundant vector in $S$
&
N/A
&
pivot column
\tabularnewline
\hline
\end{tabularx}

\end{landscape}
\end{document}
