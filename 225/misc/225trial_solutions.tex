\documentclass[letterpaper, 11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, enumitem, etoolbox, hyperref, microtype, mathtools}
\usepackage[margin=2cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[mathcal]{eucal}
\usepackage[frenchstyle, largesmallcaps]{kpfonts}
%\setlength{\parindent}{0pt}

\theoremstyle{plain}
\newtheorem{thm}{Theorem}[section]
\numberwithin{thm}{section}
\newtheorem{lemma}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{cor}[thm]{Corollary}
\theoremstyle{definition}
\newtheorem*{defn}{Definition}
\newtheorem{rmk}[thm]{Remark}
\newtheorem{problem}{Problem}
\newtheorem*{ex}{Example}

\renewcommand{\thequestion}{\textsc{Problem} \arabic{question}}
\renewcommand{\thepartno}{\alph{partno}}
\renewcommand{\solutiontitle}{\textsc{Solution:}\enspace}

\firstpagefooter{}{}{}
\runningfooter{}{}{}

\newcommand{\im}{\operatorname{im}}

\begin{document}

\noindent
\LARGE
\textsc{Math} $225$ 
\hfill
\large
April $15$, $2016$ \\
\normalsize

\begin{center}
\Large
\textsc{Trial solutions}
\normalsize
\end{center}

\noindent
Consider the inhomogeneous linear differential equation
\begin{equation*}
y^{(n)} + a_{n-1}y^{(n-1)} + \cdots + a_0y = F(x).
\end{equation*}
The following table indicates which trial solutions one should consider when searching for a particular solution, as determined by the form of the function $F(x)$ and its relation to the auxiliary polynomial of the complementary homogeneous differential equation.

\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
$F(x)$ & $\alpha$ & $P(\alpha)$ & $y_p$
\\
\hline
$cx^k$ & $\alpha = 0$ & $P(\alpha) \neq 0$ & $A_0 + A_1x + \cdots + A_kx^k$
\\
\hline
$cx^k$& $\alpha = 0$ & $P(\alpha) = 0$ with mult. $m$ & $x^m(A_0 + A_1x + \cdots + A_kx^k)$ 
\\
\hline
$cx^k\mathrm{e}^{\alpha x}$ & $\alpha \in \mathbf{C}$ & $P(\alpha) \neq 0$ & $\mathrm{e}^{\alpha x}(A_0 + \cdots + A_kx^k)$
\\
\hline
$cx^k\mathrm{e}^{\alpha x}$ & $\alpha \in \mathbf{C}$ & $P(\alpha) = 0$ with mult. $m$ & $x^m \mathrm{e}^{\alpha x}(A_0 + \cdots + A_kx^k)$
\\
\hline
$cx^k\mathrm{e}^{a x}\cos(bx)$ & $\alpha = a+bi$ & $P(\alpha) \neq 0$ & $\mathrm{e}^{ax}(\cos(bx)(A_0+ \cdots +A_kx^k) + \sin(bx)(B_0 + \cdots + B_kx^k))$
\\
\hline
$cx^k\mathrm{e}^{ax}\cos(bx)$ & $\alpha = a+bi$ & $P(\alpha) = 0$ with mult. $m$ & $x^m\mathrm{e}^{ax}(\cos(bx)(A_0+ \cdots+A_kx^k) + \sin(bx)(B_0+ \cdots+B_kx^k))$
\\
\hline
$cx^k\mathrm{e}^{a x}\sin(bx)$ & $\alpha = a+bi$ & $P(\alpha) \neq 0$ & $\mathrm{e}^{ax}(\cos(bx)(A_0+ \cdots +A_kx^k) + \sin(bx)(B_0 + \cdots + B_kx^k))$
\\
\hline
$cx^k\mathrm{e}^{ax}\sin(bx)$ & $\alpha = a+bi$ & $P(\alpha) = 0$ with mult. $m$ & $x^m\mathrm{e}^{ax}(\cos(bx)(A_0+ \cdots+A_kx^k) + \sin(bx)(B_0+ \cdots+B_kx^k))$
\\
\hline
\end{tabular}
\end{center}

\begin{ex}
Consider the linear differential equation
\[
y'-2y = x\cos(3x).
\]
In the notation of the above table, we have $F(x) = x\cos(3x)$, $c = 1$, $k = 1$, $a = 0$, $b = 3$, $\alpha = 0 + 3i = 3i$, and $P(r) = r-2$.
In particular, $P(3i) \neq 0$.
We therefore choose the trial solution 
\[
y_p 
= 
\mathrm{e}^{0x}(\cos(3x)(A_0+A_1x) + \sin(3x)(B_0 + B_1x)) 
= 
\cos(3x)(A_0+A_1x) + \sin(3x)(B_0 + B_1x).
\]
We find $y_p' = \cos(3x)(A_1 + 3(B_0+B_1x)) + \sin(3x)(B_1-3(A_0+A_1x))$, so we have
\begin{align*}
x\cos(3x)
&=
y'_p - 2y_p 
\\
&=
\cos(3x)(A_1 + 3(B_0+B_1x)) + \sin(3x)(B_1-3(A_0+A_1x))
-2(\cos(3x)(A_0+A_1x) + \sin(3x)(B_0 + B_1x))
\\
&=
\cos(3x)(-2A_0+A_1+3B_0 +(-2A_1+3B_1)x)
+
\sin(3x)(-3A_0-2B_0+B_1+(-3A_1-2B_1)x)
\end{align*}
Since $\sin(3x)$, $\cos(3x)$, $x\sin(3x)$ and $x\cos(3x)$ are linearly independent, we obtain a linear system of equations:
\[
\left\{
\begin{array}{ccl}
0 & = & -2 A_0 + A_1 + 3 B_0
\\
1 & = & -2A_1+3B_1
\\ 
0 & = & -3A_0-2B_0+B_1 
\\
0 & = & -3A_1 - 2B_1
\end{array}
\right.
\]
Solving this system, we find $A_0 = \frac{5}{169}$, $A_1 = \frac{-2}{13}$, $B_0 = \frac{12}{169}$, $B_1 = \frac{3}{13}$.
We therefore have a particular solution 
\[
y_p 
= 
\left(\frac{5}{169} - \frac{2}{13}x\right)\cos(3x) + \left(\frac{2}{169} + \frac{3}{13}x\right)\sin(3x).
\]
The general solution for the complementary equation $y' - 2y = 0$ is $y_c = c_1\mathrm{e}^{2x}$, so the general solution for our inhomogeneous equation is 
\[
y
=
c_1\mathrm{e}^{2x} + \left(\frac{5}{169} - \frac{2}{13}x\right)\cos(3x) + \left(\frac{2}{169} + \frac{3}{13}x\right)\sin(3x).
\]
\end{ex}

\begin{ex}
Consider the linear differential equation
\[
y'' -6y' +9y = 5x^2 \mathrm{e}^{3x}.
\]
We have $F(x) = 5x^2\mathrm{e}^{3x}$, $c = 5$, $k=2$, $\alpha = 3$ and $P(r) = r^2-6r+9 = (r-3)^2$.
Thus, $P(3) = 0$ with multiplicity $2$.
So we choose the trial solution 
\[
y_p 
=
x^2\mathrm{e}^{3x}(A_0 + A_1x + A_2x^2)
=
\mathrm{e}^{3x}(A_0x^2 + A_1x^3+ A_2x^4).
\]
We find 
\begin{align*}
y_p'
&=
\mathrm{e}^{3x}(2A_0x + (3A_0+3A_1)x^2 + (3A_1+4A_2)x^3 + 3A_2x^4)
\\
y_p''
&=
\mathrm{e}^{3x}((2A_0+6A_1) + (12A_0+6A_1)x + (9A_0+18A_1+12A_2)x^2 + (9A_1+24A_2)x^3 + (9A_2x^4))
\end{align*}
and a somewhat lengthy check shows that 
\[
y_p'' - 6y_p' + 0y_p
=
\mathrm{e}^{3x}((2A_0+6A1)  + 6A_1x + 12A_2x^2).
\]
Setting this equal to $F(x) = 5x^2\mathrm{e}^{3x}$, we obtain a linear system
\[
\left\{
\begin{array}{ccl}
0 & = & 2A_0 + 6A_1 \\
0 & = & 6A_1 \\
5 & = & 12 A_2
\end{array}
\right.
\]
so $A_0 = 0$, $A_1 = 0$ and $A_2 = \frac{5}{12}$.
Our particular solution is therefore $y_p = \frac{5}{12}x^4\mathrm{e}^{3x}$.
Our complementary solution is $y_c = c_1\mathrm{e}^{3x} + c_2x \mathrm{e}^{3x}$, so the general solution is
\[
y
=
y_c+y_p
=
c_1\mathrm{e}^{3x} + c_2x \mathrm{e}^{3x} + \frac{5}{12}x^4\mathrm{e}^{3x}.
\]
\end{ex}

\end{document}

