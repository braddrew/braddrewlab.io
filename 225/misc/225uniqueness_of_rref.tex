\documentclass[letterpaper,11pt]{article}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[frenchstyle, largesmallcaps, light]{kpfonts}
\usepackage[T1]{fontenc}

\theoremstyle{plain}
\newtheorem*{thm}{Theorem}

\begin{document}

%\printanswers

\LARGE
\textsc{Math} $225$ 
\hfill
\large
January $28$, $2016$ \\
\normalsize

\begin{center}
\Large
\textsc{Uniqueness of reduced row echelon form}
\normalsize
\end{center}

\begin{thm}
Suppose $A$, $B$ and $C$ are $m \times n$ matrices, $A \sim B$ and $A \sim C$, and $B$ and $C$ are in reduced row echelon form. Then $B = C$.
\end{thm}

\begin{proof}
We proceed by induction on $n$, the number of columns in $A$.

\emph{Base case:} Suppose $n =1$.
If $A = \mathbf{0}$ is the zero matrix, then $B = \mathbf{0} = C$: no nonzero matrix is row equivalent to the zero matrix.
If $A \neq \mathbf{0}$, then $B$ and $C$ are both equal to $\begin{bmatrix}1 & 0 & \cdots & 0 \end{bmatrix}^T$.
Indeed, this is the only nonzero $m \times 1$ matrix in reduced row echelon form.
In either case, $B = C$.

\emph{Inductive hypothesis:} Let $n \geq 2$ and assume the claim is true for matrices with at most $n-1$ columns.

\emph{Inductive step:} We wish to show that the claim is true for matrices with $n$ columns. 
Let $A'$ (resp. $B'$, resp. $C'$) denote the $m \times (n-1)$ submatrix of $A$ (resp. $B$, resp. $C$) given by the first $n-1$ columns.
Since $B$ and $C$ are both in reduced row echelon form, so are $B'$ and $C'$.
To convince yourself of this, first write down any matrix in reduced row echelon form.
Then delete the last column and check that the result is still in reduced row echelon form.
Now explain why this works in general, i.e., explain to yourself why deleting the last column of a matrix in reduced row echelon form results in a new matrix that still satisfies the conditions defining reduced row echelon form.

Similarly, convince yourself that $A' \sim B'$ and $A' \sim C'$.
To do this, observe that the same elementary row operations that take us from $A$ to $B$ (resp. from $A$ to $C$) also take us from $A'$ to $B'$ (resp. $A'$ to $C'$).
Since $A'$, $B'$ and $C'$ are $m \times (n-1)$, $B'$ and $C'$ are in reduced row echelon form and $A' \sim B'$ and $A' \sim C'$, the inductive hypothesis implies that $B' = C'$.
In other words, by the inductive hypothesis, if $B \neq C$, the only place they can disagree is in the $n$th column, as they are equal in their first $n-1$ columns.
So we only need to show that their $n$th columns are also equal.

Suppose for contradiction that the $n$th columns of $B$ and $C$ are not equal, so that $b_{kn} \neq c_{kn}$ for some $1 \leq k \leq m$.
Consider the homogeneous linear systems $A \mathbf{x} = \mathbf{0}$, $B \mathbf{x} = \mathbf{0}$ and $C \mathbf{x} = \mathbf{0}$.
Since $A$, $B$ and $C$ are row equivalent, these systems all have the same solution sets.
Also, homogeneous linear systems are always consistent: the zero vector $\mathbf{x} = \mathbf{0}$ is always a solution.
So consider an arbitrary solution $\mathbf{x} = \begin{bmatrix} x_1 & \cdots & x_n\end{bmatrix}^T$ of these systems.
Then we have
\begin{align*}
b_{k1}x_1 + \cdots + b_{k,n-1}x_{n-1} + b_{kn}x_n &= 0 \\
c_{k1}x_1 + \cdots + c_{k,n-1}x_{n-1} + c_{kn}x_n &= 0
\end{align*}
Subtracting the second equation from the first and recalling that $b_{ij} = c_{ij}$ for each $1 \leq i \leq m$ and $1 \leq j \leq n-1$, because $B$ and $C$ are equal except in the last column, we find $(b_{kn} - c_{kn})x_n = 0$.
Since $b_{kn} \neq c_{kn}$ by hypothesis, we have $b_{kn} - c_{kn} \neq 0$, which means that $x_n = 0$.
In particular, $x_n$ \emph{cannot} be a free variable for our linear systems $B\mathbf{x} = \mathbf{0}$ and $C \mathbf{x} = \mathbf{0}$: if it were a free variable, then it could take on any value whatsoever, but we have just shown that it is bound to take only the value $x_n = 0$.

Since $x_n$ is not a free variable, it must correspond to pivot columns of the matrices $B$ and $C$, which are in reduced row echelon form.
This means that the $n$th columns of $B$ and $C$ both contain leading coefficients.
These leading coefficients must both be contained in the first row below the lowest nonzero row of $B' = C'$ and they must both equal $1$.
Moreover, all other entries in this $n$th column must be zeros.
This shows that the $n$th columns of $B$ and $C$ are in fact equal.
This contradicts our hypothesis that these $n$th columns were not equal, thus completing the proof by contradiction.
\end{proof}

\end{document}
