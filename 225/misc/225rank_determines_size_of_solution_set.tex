\documentclass[letterpaper,11pt]{article}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, enumitem, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[frenchstyle, largesmallcaps, light]{kpfonts}

\theoremstyle{plain}
\newtheorem*{thm}{Theorem}

\begin{document}

%\printanswers

\LARGE
\textsc{Math} $225$ 
\hfill
\large
January $28$, $2016$ \\
\normalsize

\begin{center}
\Large
\textsc{Rank determines the size of the solution set}
\normalsize
\end{center}

\begin{thm}
Let $A$ be an $m \times n$ matrix, $\mathbf{b}$ an $n \times 1$ matrix and let $A' \coloneqq \begin{bmatrix} A & \mathbf{b} \end{bmatrix}$ denote the corresponding augmented matrix.
\begin{enumerate}[label=$(\roman*)$]
\item
If $\operatorname{rk}(A) < \operatorname{rk}(A')$, then $A\mathbf{x} = \mathbf{b}$ is inconsistent.
\item
If $\operatorname{rk}(A) = \operatorname{rk}(A') = n$, then $A \mathbf{x} = \mathbf{b}$ admits a unique solution.
\item
If $\operatorname{rk}(A) = \operatorname{rk}(A') < n$, then $A \mathbf{x} = \mathbf{b}$ admits infinitely many solutions.
\end{enumerate}
\end{thm}

\begin{proof}
Let us first prove $(i)$.
Suppose $\operatorname{rk}(A) < \operatorname{rk}(A)$.
Let $B'$ be an $m \times (n+1)$ matrix row equivalent to $A'$ in reduced row echelon form and let $B$ be the submatrix given by the first $m \times n$ columns.
Then $B$ is the unique reduced row echelon form matrix which is row equivalent to $A$.
Indeed, since it's given by the first $n$ columns of $B'$, it must be in reduced row echelon form, and the exact same elementary row operations that transform $A'$ into $B'$ transform $A$ into $B$.

To say that $\operatorname{rk}(A)$ is strictly less than $\operatorname{rk}(A')$ is to say that $B'$ more nonzero rows that $B$.
Since the only difference between $B$ and $B'$ is the extra column of $B'$, this can only happen if the last column of $B'$ is a pivot column.
The row containing the leading coefficient of this last column of $B'$ looks like $\begin{bmatrix} 0 & \cdots & 0 & 1\end{bmatrix}$, which corresponds to the equation $0x_1 + \cdots + 0x_n = 1$, which has no solution.
Since $B'$ is the augmented matrix of an inconsistent linear system, $A' \sim B'$ and row equivalent matrices correspond to linear systems with the same solution sets, $A\mathbf{x} = \mathbf{b}$ is therefore inconsistent.

Let us now prove $(ii)$.
Suppose $\operatorname{rk}(A) = \operatorname{rk}(A') = n$. 
Let $B$ and $B'$ be our reduced row echelon forms as above.
By hypothesis, the first $n$ rows of $B$ each contain a leading coefficient, so every column of $B$ is a pivot column.
This means that the first $n$ rows of $B$ are the identity matrix $I_n$ and, for $1 \leq i \leq n$, the $i$th row of $B'$ corresponds to the equation $x_i = b_{i,n+1}$, where $b_{i,n+1}$ is the $(i,n+1)$-entry of $B'$.
So there is one and only one solution: $\mathbf{x} = (b_{1,n+1}, b_{2,n+1}, \ldots, b_{n,n+1})$.

Finally, let us prove $(iii)$.
Suppose $\operatorname{rk}(A) = \operatorname{rk}(A') < n$, let $B$ and $B'$ be as above and, for brevity, let $r \coloneqq \operatorname{rk}(A)$.
Note that $B$ contains $n-r$ nonpivot columns.
Writing $B = \begin{bmatrix}\mathbf{b}_1 & \cdots & \mathbf{b}_n\end{bmatrix}$, let's say that $\mathbf{b}_{j_1}$, $\mathbf{b}_{j_2}$, \ldots, $\mathbf{b}_{j_{n-r}}$ are the nonpivot columns. 
Since $B'$ is in reduced row echelon form, each nonzero row of $B'$ corresponds to an equation of the form
\[
x_i + b_{i,j_k}x_{j_k} + b_{i,j_{k+1}}x_{j_{k+1}} + \cdots + b_{i,j_r}x_{j_r} = b_{i,n+1},
\]
where the leading coefficient of this row is contained in the $i$th column and $\mathbf{b}_{j_k}$ is the first nonpivot column to the right of the $i$th column.
Notice that this equation involves only one bound variable, $x_i$, and possibly several free variables, $x_{j_k}$, $x_{j_{k+1}}$, \ldots, $x_{j_r}$.

Choose arbitrary values for these free variables: $x_{j_k} = c_{j_k}$, $x_{j_{k+1}} = c_{j_{k+1}}$, \ldots, $x_{j_r} = c_{j_r}$.
Then we have no choice but to define 
\[
x_i \coloneqq -b_{i,j_k}c_{j_k} - b_{i,j_{k+1}}c_{j_{k+1}} - \cdots - b_{i,j_r}c_{j_r} +  b_{i,n+1}.
\]
In so doing, we see that our choices of values for the free variables leads to a determined value for each bound variable.
So any choice of values for the free variables leads to a solution of our system, and any \emph{different} choice of for the free variables leads to a \emph{different} solution.
There are infinitely many possibilities to choose from for the free variables, hence infinitely many solutions.
\end{proof}
\end{document}
