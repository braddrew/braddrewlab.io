\documentclass[letterpaper, 11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, enumitem, etoolbox, hyperref, microtype, mathtools}
\usepackage[margin=2cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[mathcal]{eucal}
\usepackage[frenchstyle, largesmallcaps]{kpfonts}
%\setlength{\parindent}{0pt}

\theoremstyle{plain}
\newtheorem*{thm}{Theorem}
\theoremstyle{definition}
\newtheorem*{defn}{Definition}
\newtheorem{problem}{Problem}
\newtheorem{ex}{Example}

\renewcommand{\thequestion}{\textsc{Problem} \arabic{question}}
\renewcommand{\thepartno}{\alph{partno}}
\renewcommand{\solutiontitle}{\textsc{Solution:}\enspace}

\firstpagefooter{}{}{}
\runningfooter{}{}{}

\newcommand{\im}{\operatorname{im}}

\begin{document}

\noindent
\LARGE
\textsc{Math} $225$ 
\hfill
\large
March $23$, $2016$ \\
\normalsize

\begin{center}
\Large
\textsc{Eigenvalue problems}
\normalsize
\end{center}

\begin{defn}
\label{1}
Let $A \in M_{n \times n}(\mathbf{C})$ and $\lambda \in \mathbf{C}$.
\begin{enumerate}[topsep=.25\baselineskip, itemsep=0ex, label=\textup{($\roman*$)}]
\item
We say $\lambda$ is an \emph{eigenvalue of $A$} if the linear system $A \mathbf{x} = \lambda \mathbf{x}$, or, equivalently, $(A - \lambda I_n)\mathbf{x} = \mathbf{0}$, admits a nontrivial solution $\mathbf{x}$.
\item
An \emph{eigenvector of $A$ with eigenvalue $\lambda$} is a \textbf{\emph{nonzero}} vector $\mathbf{x} \in \mathbf{C}^n$ such that $A \mathbf{x} = \lambda \mathbf{x}$.
\end{enumerate}
\end{defn}

Geometrically, the equation $A\mathbf{x} = \lambda \mathbf{x}$ tells us that the linear map given by $A$ stretches $\mathbf{x}$ out by a factor of $\lambda$, but does not rotate it.
 
Consider the matrix
$
A
=
\begin{bmatrix}
1 & -1 \\
2 & 4
\end{bmatrix}
$.
Since $A\mathbf{e}_1 = (1,2)$, $A$ stretches vectors on the $x$-axis and sends them to the line $y = 2x$.
Similarly, $A\mathbf{e}_2 = (-1,4)$, so $A$ stretches vectors on the $y$-axis by a different factor and sends them to the line $y = -4x$.

Does $A$ preserve any lines through the origin?
If we knew $A$ had an eigenvalue $\lambda \in \mathbf{R}$, then we could just solve the system $A \mathbf{x} = \lambda \mathbf{x}$ by row reduction; any nontrivial solution would give us a line fixed by $A$.
It turns out that $2$ and $3$ are both eigenvalues of $A$ in this example.
To find the eigenvectors corresponding to $2$, we compute the kernel of the matrix
\[
A-2I_2
=
\begin{bmatrix}
1 - 2  & -1 \\
2 & 4 -2
\end{bmatrix},
\]
which turns out to be $\{(a,-a) \mid a \in \mathbf{R}\}$, so each \emph{nonzero} vector of the form $(a,-a)$, e.g., $(1,-1)$, is an eigenvector of $A$ with eigenvalue $2$.
Indeed, 
\[
\begin{bmatrix}
1 & -1 \\
2 & 4
\end{bmatrix}
\begin{bmatrix}
1 \\
-1
\end{bmatrix}
=
\begin{bmatrix}
2 \\
-2
\end{bmatrix}
=
2
\begin{bmatrix}
1 \\
-1
\end{bmatrix}.
\]
Similarly, to find eigenvectors for $3$, we compute the kernel of $A-3I_2$ to find $\{(a-2a)\mid a \in \mathbf{R}\}$, so $(1,-2)$, in particular, is an eigenvector of $A$ with eigenvalue $3$, as we can check:
\[
\begin{bmatrix}
1 & -1 \\
2 & 4
\end{bmatrix}
\begin{bmatrix}
1 \\
-2
\end{bmatrix}
=
\begin{bmatrix}
3 \\
-6
\end{bmatrix}
=
3
\begin{bmatrix}
1 \\
-2
\end{bmatrix}.
\]
Thus, if we know what the eigenvalues are, finding eigenvectors is easy.

So how do we find eigenvalues?
The key observation is that the following are \emph{equivalent}: 
\begin{enumerate}[topsep=.25\baselineskip, itemsep=0ex, label=\textup{($\roman*$)}]
\item
the scalar $\lambda$ is an eigenvalue of $A$;
\item
the linear system $A\mathbf{x} = \lambda \mathbf{x}$ admits a nontrivial solution;
\item
the homogeneous system $(A-\lambda I_n)\mathbf{x} = \mathbf{0}$ admits a nontrivial solution;
\item
the $n \times n$ matrix $A-\lambda I_n$ is \emph{not} invertible;
\item
$\det(A-\lambda I_n) = 0$.
\end{enumerate}
So the problem of finding the eigenvalues of $A$ is precisely that of identifying the values of $\lambda$ that make the expression $\det(A-\lambda I_n)$ zero.
For large matrices, this can be a formidable chore: finding the determinant alone is painful, and, even if we can compute the determinant, it will still remain to find its roots.
For small matrices, however, this is manageable.
Returning to the example above, we have
\[
\det(A-\lambda I_2) 
=
\begin{vmatrix}
1 - \lambda & -1 \\
2 & 4 - \lambda
\end{vmatrix}
=
(1-\lambda)(4-\lambda) + 2
=
\lambda^2 - 5\lambda + 6
=
(\lambda - 2)(\lambda - 3),
\]
which equals zero exactly when $\lambda = 2,3$.
So $2$ and $3$ are indeed eigenvalues of $A$, as the prophets foretold.

\begin{defn}
Let $A$ be an $n \times n$ matrix.
The \emph{characteristic polynomial $p(t)$ of $A$} is the polynomial $p(t) \coloneqq \det(A-tI_n)$ in the variable $t$.
By cofactor expansion and induction on $n$, one can check that $p(t)$ really is a polynomial in $t$ and that it has degree $n$.
\end{defn}

The above discussion actually contains a proof of the following theorem.

\begin{thm}
Let $A$ be an $n \times n$ matrix.
\begin{enumerate}[topsep=.25\baselineskip, itemsep=0ex, label=\textup{($\roman*$)}]
\item
The eigenvalues of $A$ are precisely the roots of the characteristic polynomial $p(t)$, i.e., the values $t = \lambda$ such that $p(\lambda) = 0$.
\item
If $\lambda$ is an eigenvalue of $A$, then the eigenvectors $\mathbf{x}$ of $A$ with eigenvalue $\lambda$ are precisely the nontrivial solutions of $(A-\lambda I_n)\mathbf{x} = \mathbf{0}$.
\end{enumerate}
\end{thm}

A few words of caution:
if $A \in M_{n \times n}(\mathbf{R})$, then its characteristic polynomial $p(t)$ has real coefficients, but not all polynomials with real coefficients have real roots.
For example, $x^2+1$ has no real roots.
Nevertheless, the fundamental theorem of algebra states that every degree $n$ polynomial in the variable $t$ with real or complex coefficients has $n$ \emph{complex} roots (possibly with repetition).
So, when dealing with eigenvalue problems, we are often led to work with complex vectors, even if $A$ has real entries.

Consider the matrix 
\[
A
=
\begin{bmatrix}
1 & -3 & 3 \\
3 & -5 & 3 \\
6 & -6 & 4
\end{bmatrix}.
\]
We now have the skills required to find the eigenvalues of $A$ and corresponding eigenvectors.
Begin with the eigenvalues: we need the roots of the characteristic polynomial:
\begin{align*}
p(t) 
= 
\det(A-tI_n)
&=
\begin{vmatrix}
1 -t & -3 & 3 \\
3 & -5 -t & 3 \\
6 & -6 & 4 - t
\end{vmatrix}
=
(1- t)
\begin{vmatrix}
-5-t & 3 \\
-6 & 4-t
\end{vmatrix}
-
(-3)
\begin{vmatrix}
3 & 3 \\
6 & 4-t
\end{vmatrix}
+
3
\begin{vmatrix}
3 & -5 -t \\
6 & -6
\end{vmatrix}
\\
&=
(1-t)((-5-t)(4-t) + 18)
+
3(3(4-t) - 18)
+
3(-18-6(-5-t))
%\\
%&=
%(1-t)(t^2+t - 20 + 18)
%+
%3(12-3t-18)
%+
%3(-18+30+6t)
%\\
%&=
%(-t^3 -t^2+2t+t^2+t-2)
%+
%(-9t-18)
%+
%(18t + 36)
%\\
=
-t^3 + 12t + 16.
\end{align*}
Optimists that we are, we hope that the eigenvalues are integers, in which case they must divide the constant term $16$ of $p(t)$, so they must be one of $\pm1, \pm2, \pm4, \pm8, \pm16$.
Computing, we find $p(1) \neq 0$, $p(-1) \neq 0$, $p(2) \neq 0$, $p(-2) = 0$, so $-2$ is an eigenvalue.
It follows that $(t-(-2))$ divides $p(t)$.
Indeed, by polynomial division, we find $p(t) = (t+2)(-t^2+2t+8)$ and $-t^2+2t+8$ factors as $-(t+2)(t-4)$.
All told, $p(t) = -(t+2)^2(t-4)$, so the eigenvalues of $A$ are $-2$ and $4$.

It remains to find corresponding eigenvectors.
For $\lambda = 4$, we need the kernel of 
\[
A-4I_3
=
\begin{bmatrix}
1-4 & -3 & 3 \\
3 & -5 -4 & 3 \\
6 & -6 & 4 - 4
\end{bmatrix}
\longrightarrow
\begin{bmatrix}
1 & 0 & -1/2 \\
0 & 1 & -1/2 \\
0 & 0 & 0 
\end{bmatrix},
\]
which is spanned by $(1,1,2)$.
So $(1,1,2)$ is an eigenvector with eigenvalue $4$.

For $\lambda = -2$, we have
\[
A-(-2)I_3
=
\begin{bmatrix}
1-(-2) & -3 & 3 \\
3 & -5 -(-2) & 3 \\
6 & -6 & 4 -(-2)
\end{bmatrix}
\longrightarrow
\begin{bmatrix}
1 & -1 & 1 \\
0 & 0 & 0 \\
0 & 0 & 0 
\end{bmatrix}
\]
whose kernel is two-dimensional, spanned by $(1,1,0)$ and $(-1,0,1)$.
So both $(1,1,0)$ and $(-1,0,1)$ are eigenvectors with eigenvalue $-2$, as is any nonzero linear combination of these two eigenvectors.

\begin{questions}
\question
Let $A$ be an $n \times n$ matrix.
Prove that $A$ is invertible if and only if $0$ is \emph{not} an eigenvalue of $A$.
\end{questions}
\end{document}
