\documentclass[letterpaper, 11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, enumitem, etoolbox, hyperref, microtype, mathtools, sectsty, thmtools}
\usepackage[margin=2cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[mathcal]{eucal}
\usepackage[frenchstyle, largesmallcaps]{kpfonts}
%\setlength{\parindent}{0pt}

\allsectionsfont{\fontseries{m}\scshape}

\declaretheoremstyle[spaceabove=6pt, spacebelow=6pt, headfont=\scshape, notefont=\normalfont, bodyfont=\itshape, postheadspace=.5em]{thm}
\declaretheoremstyle[spaceabove=6pt, spacebelow=6pt, headfont=\scshape, notefont=\normalfont, bodyfont=\normalfont, postheadspace=.5em]{defn}

\theoremstyle{thm}
\newtheorem{thm}{Theorem}[section]
\numberwithin{thm}{section}
\newtheorem{lemma}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{cor}[thm]{Corollary}
\theoremstyle{defn}
\newtheorem*{defn}{Definition}
\newtheorem{rmk}[thm]{Remark}
\newtheorem{problem}{Problem}
\newtheorem{ex}[thm]{Example}
\numberwithin{equation}{thm}

\renewcommand{\thequestion}{\textsc{Problem} \arabic{question}}
\renewcommand{\thepartno}{\alph{partno}}
\renewcommand{\solutiontitle}{\textsc{Solution:}\enspace}

\firstpagefooter{}{}{}
\runningfooter{}{}{}

\newcommand{\im}{\operatorname{im}}

\begin{document}

\noindent
\LARGE
\textsc{Math} $225$ 
\hfill
\large
April $11$, $2016$ \\
\normalsize

\begin{center}
\Large
\textsc{Linear differential equations}
\normalsize
\end{center}

\section{Essential definitions}

\begin{defn}
Let $I \subseteq \mathbf{R}$ be an interval.
\begin{enumerate}
[topsep=.5ex, itemsep=0ex, label=$(\roman*)$]
\item
Let $n \in \mathbf{Z}_{\geq0}$.
The set $\mathcal{C}^n(I)$ of \emph{$n$-times continuously differentiable functions $f: I \to \mathbf{C}$} is the set of functions $f: I \to \mathbf{C}$ such that the first $n$ derivatives $f', f'', f^{(3)}, \ldots, f^{(n)}$ of $f$ exist on $I$ and are continuous on $I$.
For example, polynomial functions, $\sin(x)$, $\cos(x)$ and $\mathrm{e}^x$ all belong to $\mathcal{C}^n(I)$, as do any linear combinations of these functions and any composites thereof.
In particular, $\mathcal{C}^n(I)$ is a $\mathbf{C}$-vector space (of infinite dimension).
\item
Let $n \in \mathbf{Z}_{>0}$.
The \emph{differentiation operator $D: \mathcal{C}^n(I) \to \mathcal{C}^{n-1}(I)$} is the function given by $D(f) \coloneqq f'$.
Note that $D$ is a linear map: $D(a(f+g)) = (a(f+g))' = af'+ag' = aD(f) + aD(g)$.
\item
A \emph{linear differential operator $L: \mathcal{C}^n(I) \to \mathcal{C}^0(I)$ of order $n$ on $I$} is a linear combination of powers of the differentiation operator of the form $L = D^n + a_{n-1}D^{n-1} + \cdots + a_1D + a_0$, with $a_0, a_1, \ldots, a_{n-1} \in \mathcal{C}^0(I)$.
Such a linear combination is a linear map $\mathcal{C}^n(I) \to \mathcal{C}^0(I)$ and sends $f \in \mathcal{C}^n(I)$ to 
\begin{align*}
Lf
=
(D^n + a_{n-1}D^{n-1} + \cdots + a_1D + a_0)f
&=
D^n(f) + a_{n-1}D^{n-1}(f) + \cdots + a_1D(f) + a_0f
\\
&=
f^{(n)} + a_{n-1}f^{(n-1)} + \cdots + a_1f' + a_0f.
\end{align*}
For example, $L = \cos(x)D^3 - x^2D + \tan(x)$ is a linear differential operator of order $3$ on $I = (-\pi/2, \pi/2)$. 
\item
A \emph{linear differential equation} is an equation of the form $Ly = F$, where $L$ is a linear differential operator of order $n$ on $I$, $F \in \mathcal{C}^0(I)$ and $y$ is an unknown function.
If $F(x) = 0$ for each $x \in I$, then we say that the linear differential equation is \emph{homogeneous}.
For example, $\cos(x)y''' -x^2y' + \tan(x)y = -2\sin(3x)$ is a linear differential equation of order $3$ on $I = (-\pi/2, \pi/2)$.
\item
An \emph{initial value problem of order $n$ on $I$} is a linear differential equation $Ly = F$ of order $n$ with \emph{initial conditions} $y(x_0) = c_0$, $y'(x_0) = c_1$, \ldots, $y^{(n-1)}(x_0) = c_{n-1}$ for some $x_0 \in I$ and some constants $c_0, \ldots, c_{n-1} \in \mathbf{C}$.
For example, $\cos(x)y''-x^2y'+\tan(x)y = -2\sin(3x)$, $y(0) = 3$, $y'(0) = -7$, $y''(0) = 1$ is an initial value problem or order $3$ on $I = (-\pi/2, \pi/2)$.
\item
If $f_1, \ldots, f_n \in \mathcal{C}^{n-1}(I)$ and $x_0 \in I$, then their \emph{Wronskian $W[f_1,\ldots,f_n](x_0)$} is the determinant of the $n \times n$ matrix $[f^{(i)}_j(x_0)]$.
For example, if $f_1(x) = \cos(x)$ and $f_2(x) = \sin(x)$, then
\[
W[f_1, f_2](x)
=
\begin{vmatrix}
\cos(x_0) & \sin(x_0) \\
-\sin(x_0) & \cos(x_0)
\end{vmatrix}
=
\cos^2(x_0) + \sin^2(x_0) = 1.
\]
\end{enumerate}
\end{defn}

\section{First-order linear differential equations}

Consider a linear differential equation of the form $y' + p(x)y = q(x)$ with $p,q \in \mathcal{C}^0(I)$.
To solve this differential equation, we multiply both sides by the \emph{integrating factor $\mathrm{e}^{\int p(x) \mathrm{d}x}$}:
\begin{align*}
y' + p(x)y
&=
q(x)
\\
\mathrm{e}^{\int p(x) \mathrm{d}x}(y' + p(x) y)
&=
\mathrm{e}^{\int p(x) \mathrm{d}x}q(x)
\\
\mathrm{e}^{\int p(x) \mathrm{d}x}y' + p(x)\mathrm{e}^{\int p(x) \mathrm{d}x}y
&=
\mathrm{e}^{\int p(x) \mathrm{d}x}q(x)
\\
\frac{\mathrm{d}}{\mathrm{d}x}\left[\mathrm{e}^{\int p(x) \mathrm{d}x}y\right]
&=
\mathrm{e}^{\int p(x) \mathrm{d}x}q(x)
&&
\textup{product rule for differentiation}
\\
\int\frac{\mathrm{d}}{\mathrm{d}x}\left[\mathrm{e}^{\int p(x) \mathrm{d}x}y\right] \mathrm{d}x
&=
\int \mathrm{e}^{\int p(x) \mathrm{d}x}q(x) \mathrm{d}x
\\
\mathrm{e}^{\int p(x) \mathrm{d}x}y 
&=
\int \mathrm{e}^{\int p(x) \mathrm{d}x} q(x) \mathrm{d}x
\\
y
&=
\fbox{$\mathrm{e}^{-\int p(x) \mathrm{d}x}\left(\int \mathrm{e}^{\int p(x) \mathrm{d}x} q(x) \mathrm{d}x +C\right).$}
\end{align*}

For example, if we consider the first-order linear differential equation $y' + \tan(x) y = \cos^2(x)$, then the integrating factor is $\mathrm{e}^{\int \tan(x)\mathrm{d}x} = \mathrm{e}^{-\ln(\cos(x))} = \mathrm{e}^{\ln(\sec(x))} = \sec(x)$ and we have
\[
y 
=
\frac{1}{\sec(x)} \left(\int \sec(x)\cos^2(x)\mathrm{d}x + C\right)
=
\frac{1}{\sec(x)} \left(\int \cos(x)\mathrm{d}x + C \right)
=
\frac{1}{\sec(x)} (\sin(x) + C)
=
\cos(x) (\sin(x) + C),
\]
where $C$ is a constant of integration.

In fact, these are the \emph{only} possible solutions.
We'll return to the inhomogeneous case, in which $q \neq 0$, later.
For now, let's consider the homogeneous case $q = 0$.

\begin{lemma}
\label{1.1}
Let $I$ be an interval, $x_0 \in I$ and $p \in \mathcal{C}^0(I)$.
The first-order initial value problem on the interval $I$ given by the homogeneous linear differential equation $y' + py = 0$ and the initial condition $y(x_0) = 0$ admits only the zero solution $y(x) = 0$ for each $x \in I$.
\end{lemma}

\begin{proof}
Suppose $y$ is a solution to this initial value problem.
Then we have
\[
\frac{\mathrm{d}}{\mathrm{d}t}\left[\mathrm{e}^{\int p\mathrm{d}t}y\right]
=
\mathrm{e}^{\int p\mathrm{d}t}(y'+py)
=
0.
\]
As its derivative is always zero, the function $\mathrm{e}^{\int p\mathrm{d}t}y$ must be constant.
However, since $y(x_0) = 0$, when we evaluate $\mathrm{e}^{\int p \mathrm{d}t}y$ at $x_0$, we get $0$.
Since it's a constant function, it must take the value $0$ at each $x \in I$.
Since $\mathrm{e}^{\int p\mathrm{d}t}$ is never $0$, we must have $y(x) = 0$ for each $x \in I$.
\end{proof}

\begin{prop}
\label{1.2}
Let $I$ be an interval and $p \in \mathcal{C}^0(I)$.
Every solution to the first-order homogeneous linear differential equation $y' + py = 0$ is of the form $y = C\mathrm{e}^{-\int p\mathrm{d}x}$ for some $C \in \mathcal{C}$.
\end{prop}

\begin{proof}
As we say above, $y_0 = \mathrm{e}^{-\int p\mathrm{d}t}$ is a solution to this differential equation.
Let $y_1$ be another solution, $x_0 \in I$ and $c = y_1(x_0)$.
We can choose $C \in \mathbf{C}$ such that $Cy_0(x_0) = c_0$.
Then $y_1 - Cy_0$ is also a solution to the same differential equation: this is easy enough to check directly, or else we can defer to \ref{1} for proof.	
Moreover, $(y_1 - Cy_0)(x_0) = y_1(x_0) - Cy_0(x_0) = C_0 - c_0 = 0$.
So $y_1 - Cy_0$ is in fact a solution to the initial value problem $y' + py = 0$, $y(x_0) = 0$.
By \ref{1.1}, $0 = y_1 - Cy_0$, so $y_1 = Cy_0$, as required.
\end{proof}

\section{Higher-order homogeneous linear differential equations}

Throughout this section, we consider an interval $I \subseteq \mathbf{R}$ and the homogeneous linear differential equation
\begin{equation}
\label{*}
Ly = y^{(n)} + a_{n-1}y^{(n-1)} + \cdots + a_0y = 0
\end{equation}
with $a_i \in \mathcal{C}^0(I)$ for $0 \leq i \leq n-1$ and $L = D^n + a_{n -1}D^{n-1} + \cdots +a_1D + a_0$.

We will use what we know about vector spaces to study the solution sets of homogeneous linear differential equations.
Here is a brief summary of the process:
\begin{enumerate}
[topsep=.5ex, itemsep=0ex, label=$(\roman*)$]
\item
we begin by noticing that the solution set of a homogeneous linear differential equation is a vector space (\ref{1});
\item
next, we use Wronskians to characterize linear independence for solutions of homogeneous linear differential equations (\ref{2}, \ref{3}, \ref{4});
\item
we then show that initial value problems have unique solutions, first when the initial conditions are homogeneous (\ref{5}), then in general (\ref{6});
\item
combining our characterization of linear independence with the uniqueness of solutions to initial value problems, we can conclude that the set of solutions of a homogeneous linear differential equation of order $n$ is of dimension $\leq n$ (\ref{8}).
\end{enumerate}

The proofs of some of these results are more involved---and of a different nature---than those we have become accustomed to in this course, but a little patience and a willingness to recall some basic calculus are all you need to follow the arguments.
Of course, you won't need to memorize these proofs for this course, but skepticism is essential to good science and this document will, I hope, convince you that these facts are not inscrutable mysteries requiring you suspend your disbelief.

\begin{lemma}
\label{1}
The set of solutions to the homogeneous linear differential equation \textup{\eqref{*}} is a vector space.
\end{lemma}

\begin{proof}
A function $y \in \mathcal{C}^n(I)$ is a solution to \eqref{*} if and only if $Ly=0$, i.e., if and only if $y \in \ker(L)$.
The kernel of any linear map is a subspace, hence a vector space.
\end{proof}

\begin{thm}[Abel]
\label{2}
Let $y_1, \ldots, y_n$ be solutions to the homogeneous linear differential equation \textup{\eqref{*}} on $I$.
Then for each $x_0, x \in I$, we have
\[
W[y_1, \ldots, y_n](x) 
=
W[y_1, \ldots, y_n](x_0)\mathrm{e}^{-\int_{x_0}^x a_{n-1}(t)\mathrm{d}t},
\]
i.e., the value of the Wronskian at $x$ is determined by its value at $x_0$ and the integral of the coefficient function $a_{n-1}$ from $t=x_0$ to $t=x$.
\end{thm}

\begin{proof}
We give the proof for $n=2$.
Let $W = W[y_1, y_2]$.
Then 
\[
W' 
= 
\frac{\mathrm{d}}{\mathrm{d}x}[y_1y_2' - y_2y_1']
=
(y_1y_2'' + y_1'y_2') - (y_2'y_1' + y_2y_1'')
=
y_1y_2'' - y_2y_1''.
\]
Recall that, as $n=2$, our differential equation \eqref{*} looks like $y'' = -(a_1y' +a_0y)$.
As $y_1$ and $y_2$ are both solutions for this differential equation, we have $y''_i = -(a_1y_i'+a_0y_i)$ for $i =1,2$.
Plugging this into $W' = y_1y_2'' - y_2y_1''$, we find
\begin{align*}
W'
&=
y_1y_2'' - y_2y_1''
\\
&=
y_1(-(a_1y_2' + a_0y_2)) - y_2(-(a_1y_1' + a_0y_1))
\\
&=
-a_1y_1y_2' - a_0y_1y_2 + a_1y_2y_1' + a_0y_1y_2
\\\
&=
-a_1(y_1y_2' - y_2y_1')
\\
&=
-a_1W
\\
\frac{W'}{W}
&=
-a_1
\\
\int_{x_0}^x\frac{W'(t)}{W(t)}\mathrm{d}t
&=
-\int_{x_0}^xa_1(t)\mathrm{d}t
\\
\int_{t=x_0}^{t=x}\frac{\mathrm{d}W}{W}
&=
-\int_{x_0}^xa_1(t)\mathrm{d}t
&&
\textup{substitution}
\\
\left[\ln(W(t))\right]^{t=x}_{t=x_0}
&
-\int_{x_0}^xa_1(t)\mathrm{d}t
\\
\ln(W(x)) - \ln(W(x_0))
&=
-\int_{x_0}^xa_1(t)\mathrm{d}t
\\
\ln\left(\frac{W(x)}{W(x_0)}\right)
&=
-\int_{x_0}^xa_1(t)\mathrm{d}t
\\
\frac{W(x)}{W(x_0)}
&=
\mathrm{e}^{-\int_{x_0}^xa_1(t)\mathrm{d}t}
\\
W(x)
&=
W(x_0)\mathrm{e}^{-\int_{x_0}^xa_1(t)\mathrm{d}t}
\end{align*}
as desired.
\end{proof}

\begin{prop}
\label{3}
Let $f_1, \ldots, f_n \in \mathcal{C}^{n-1}(I)$.
Then $W[f_1, \ldots, f_n](x) = 0$ for each $x \in I$ if and only if $\{f_1, \ldots, f_n\}$ is linearly dependent in $\mathcal{C}^{n-1}(I)$.
\end{prop}

\begin{proof}
Suppose $c_1f_1 + \cdots + c_nf_n = 0$ for some $c_1, \ldots, c_n \in \mathbf{C}$.
Then we have
\begin{align*}
c_1f_1(x) + \cdots + c_nf_n(x) &= 0 \\
c_1f_1'(x) + \cdots + c_nf_n'(x) &= 0 \\
&\vdots \\
c_1f_1^{(n-1)}(x) + \cdots + c_nf_n^{(n-1)}(x) &= 0
\end{align*}
for each $x \in I$.
In other words, if $A(x)$ denotes the $n \times n$ matrix
\[
A(x)
=
\begin{bmatrix}
f_1(x) & \cdots & f_n(x) \\
f_1'(x) & \cdots & f_n'(x) \\
\vdots & \ddots & \vdots \\
f_1^{(n-1)}(x) & \cdots & f_n^{(n-1)}(x)
\end{bmatrix},
\]
then $A(x)\begin{bmatrix}c_1 \\ \vdots \\ c_n\end{bmatrix} = \mathbf{0}$ for each $x \in I$.
However, $\det(A(x)) = W[f_1, \ldots, f_n](x)$, so $W[f_1, \ldots, f_n](x) = 0$ if and only if this linear system admits a nontrivial solution $(c_1, \ldots, c_n)$, which in turn is true if and only if $\{f_1, \ldots, f_n\}$ is linearly dependent.
\end{proof}

\begin{cor}
\label{4}
Let $y_1, \ldots, y_n$ be solutions on $I$ to \textup{\eqref{*}}.
The following are equivalent:
\begin{enumerate}
[topsep=.5ex, itemsep=0ex, label=$(\roman*)$]
\item
$W[y_1,\ldots, y_n](x_0) = 0$ for some $x_0 \in I$;
\item
$W[y_1, \ldots, y_n](x) = 0$ for each $x \in I$;
\item
$\{y_1, \ldots, y_n\}$ is linearly dependent in $\mathcal{C}^n(I)$.
\end{enumerate}
\end{cor}

\begin{proof}
Conditions $(ii)$ and $(iii)$ are equivalent by \ref{3}.
Also, $(ii)$ clearly implies $(i)$.
Suppose $(i)$ is true.
By \ref{2}, we have $W[y_1,\ldots,y_n](x) = W[y_1, \ldots, y_n](x_0)\mathrm{e}^{-\int a_1(t) \mathrm{d}t} = 0\cdot\mathrm{e}^{-\int a_1(t) \mathrm{d}t} = 0$ for each $x \in I$, so $(ii)$ follows.
\end{proof}

\begin{lemma}
\label{5}
Let $x_0 \in I$.
Suppose $y$ is a solution to the initial value problem given by \textup{\eqref{*}} and the initial conditions $y(x_0) = 0$, $y'(x_0) = 0$, \ldots, $y^{(n-1)}(x_0) = 0$.
Then $y(x) = 0$ for each $x \in I$, i.e., the \emph{only} solution to this initial value problem is the zero function $y(x) = 0$ for each $x \in I$.
\end{lemma}

\begin{proof}
The $n=1$ was proved in \ref{1.1}.
We give the proof for $n = 2$ under the additional hypothesis that $y$ is a \emph{real-valued} solution.
In that case, $y$ satisfies the differential equation $y'' + a_1y' + a_0y = 0$ or, equivalently, $y'' = -(a_1y'+a_0y)$ and the initial conditions $y(x_0) = y'(x_0) = 0$.
Consider the function $z = y^2 + (y')^2$.
As a sum of squares of real numbers, $z(t) \geq0$ for each $t \in I$ and $z(x_0) = y(x_0)^2 + y'(x_0)^2 = 0$.
Differentiating $z$, we find
\begin{align*}
z'
=
2yy' + 2y'y''
&=
2yy' + 2y'(-(a_1y'+a_0y))
\\
&=
2yy' -2a_1(y')^2 - 2a_0yy'
\\
&=
-2a_1(y')^2 + 2(1-a_0)yy'.
\end{align*}
Recall the following basic inequalities for real numbers $a, b\in \mathbf{R}$: $|a+b| \leq |a| + |b|$ and $a^2 + b^2 \geq 2|ab| \geq |ab|$.
The latter follows from the fact that $a^2-2|ab|+b^2 = (|a|-|b|)^2 \geq 0$.
Applying these inequalities, we find
\begin{align*}
z'
=
-2a_1(y')^2 + 2(1-a_0)yy'
&\leq
2|a_1|(y')^2 + 2|1-a_0||yy'|
\\
&\leq
2|a_1|(y')^2 + 2(1+|a_0|)|yy'|
\\
&\leq
2|a_1|(y')^2 + 2(1+|a_0|)(y^2 + (y')^2)
\\
&=
2|a_1|(y')^2 + 2(1+|a_0|)y^2 + 2(1+|a_0|)(y')^2
\\
&=
2(1+|a_0|)y^2 + 2(1+|a_0|+|a_1|)(y')^2.
\end{align*}
Choosing $K \geq 2\max\{1+|a_0(t)|+|a_1(t)| \mid t \in I\}$, we have $K \geq 2(1+|a_0(t)|)$ and $K \geq 2(1+|a_0(t)| + |a_1(t)|)$ for each $t \in I$, which implies that $z'(t) \leq Ky(t)^2 + Ky'(t)^2 = Kz(t)$ for each $t \in I$.

We claim that $z(t) = 0$ for each $t \in I$.
If not, there exists $t_0 \in I$ such that $z(t_0) >0$.
If $t_0 > x_0$, then 
\[
\frac{\mathrm{d}}{\mathrm{d}t}[\mathrm{e}^{-Kt}z]
=
\mathrm{e}^{-Kt}(z' - Kz) 
\leq 0
\]
by our previous observation that $z'(t) \leq Kz(t)$.
As a function whose first derivative is nonpositive at each point in $I$, $\mathrm{e}^{-Kt}z$ is a nonincreasing function, so we must have $\mathrm{e}^{-Kt_0}z(t_0) \leq \mathrm{e}^{-Kx_0}z(x_0) = 0$.
As we assumed that $z(t_0) > 0$, this is a contradiction, so $z(t) = 0$ for each $t \in I$ with $t \geq x_0$.
The same proof works if we assume $z(t_0) > 0$ for some $t_0 < x_0$ in $I$.
We can now complete the proof by observing that $0 = z(t) = y(t)^2 + y'(t)^2 \geq y(t)^2 \geq 0$ for each $y \in I$, so $y(t)^2 = 0$ and hence $y(t) = 0$ for each $t \in I$, as required.
\end{proof}

\begin{cor}
\label{6}
Let $c_0, \ldots, c_{n-1} \in \mathbf{C}$ and $x_0 \in I$.
The initial value problem given by \textup{\eqref{*}} and the initial conditions $y(x_0) = c_0$, \ldots, $y^{(n-1)}(x_0) = c_{n-1}$ admits at most one solution, i.e., any two solutions $y_1$ and $y_2$ of this initial value problem are equal.
\end{cor}

\begin{proof}
The $n=1$ case follows from \ref{1.2}.
We give the proof for $n = 2$.
Let $y_1$ and $y_2$ be two solutions for the initial value problem.
We have
\[
W[y_1, y_2](x_0)
=
\begin{vmatrix}
y_1(x_0) & y_2(x_0) \\
y_1'(x_0) & y_2'(x_0)
\end{vmatrix}
=
\begin{vmatrix}
c_0 & c_0 \\
c_1 & c_1
\end{vmatrix}
=
0
\]
for each $x \in I$.
By \ref{4}, $\{y_1, y_2\}$ is therefore linearly dependent.
Without loss of generality, we may assume that $y_2 = \alpha y_1$ for some $\alpha \in \mathbf{C}$.
If $c_0 \neq 0$, then $y_2(x_0) = c_0 = y_1(x_0) = \alpha y_2(x_0)$, hence $\alpha = 1$.
Similarly, if $c_1 \neq 0$, then $y_2'(x_0) = c_1 = y_1'(x_0) = \alpha y_1'(x_0)$ and so $\alpha = 1$.
If $c_0 = c_1 = 0$, then \ref{5} implies that $y_1 = 0 = y_2$.
\end{proof}

\begin{rmk}
\label{7}
Lemma \ref{5} was a big pain to prove, but we only need it to prove \ref{6}, and, as the proof of \ref{6} shows, if we're given an initial value problem with an initial condition of the form $y^{(k)}(x_0) = c_k$ with $c_k \neq 0$ for some $0 \leq k \leq n-1$, we don't need \ref{5} at all: the proof of the uniqueness is elemenary in that case.
\end{rmk}

\begin{thm}
\label{8}
The set of solutions of \textup{\eqref{*}} is of dimension $\leq n$.
\end{thm}

\begin{proof}
The $n = 1$ case was proved in \ref{1.2}.
We give the proof for $n = 2$.
Suppose $y_1, y_2, y_3$ are solutions to \eqref{*} on $I$.
We claim that $\{y_1, y_2, y_3\}$ is linearly dependent.
Choose $x_0 \in I$ and let $y_i(x_0) = c_i$, $y_i'(x_0) = d_i$ for each $i = 1,2,3$.
As a set of three vectors in a two-dimensional vector space, $\{(c_1,d_1), (c_2,d_2), (c_3, d_3)\}$ is linearly dependent.
Suppose without loss of generality that $(c_3,d_3) = \alpha(c_1,d_1) + \beta(c_2,d_2)$ for some $\alpha, \beta \in \mathbf{C}$.
Then 
\begin{align*}
L(\alpha y_1 + \beta y_2)
&=
\alpha Ly_1 + \beta L y_2
=
0
\\
(\alpha y_1 + \beta y_2)(x_0)
&=
\alpha y_1(x_0) + \beta y_2(x_0)
=
\alpha c_1 + \beta c_2
=
c_3
\\
(\alpha y'_1 + \beta y'_2)(x_0)
&=
\alpha y'_1(x_0) + \beta y'_2(x_0)
=
\alpha d_1 + \beta d_2
=
d_3,
\end{align*}
so $\alpha y_1 + \beta y_2$ and $y_3$ are both solutions to the same initial value problem.
By \ref{6}, they must be equal $\alpha y_1 + \beta y_2 = y_3$, and so $\{y_1, y_2, y_3\}$ is linearly dependent.
\end{proof}

\section{Constant-coefficient homogeneous linear differential equations}

Consider the linear differential equation
\begin{equation}
\label{**}
Ly= y^{(n)} + a_{n-1} y^{(n-1)} + \cdots + a_1y' + a_0y = 0
\end{equation} 
with $a_i \in \mathbf{R}$ for $0 \leq i < n$ and $L = D^n + a_{n -1}D^{n-1} + \cdots +a_1D + a_0$.
In this section, we will construct $n$ linearly independent solutions for this equation, first by considering complex-valued solutions (\ref{11}), then by restricting to real-valued solutions (\ref{13}).
By \ref{8}, these linearly independent solutions will form a basis for the vector space of all solutions (\ref{1}).

\begin{defn}
\label{9}
The \emph{auxiliary polynomial} of \textup{\eqref{**}} is $P(r) = r^n + a_{n-1}r^{n-1} + \cdots + a_1r + a_0$.
In other words, its the polynomial obtained by replacing $y^{(k)}$ by $r^k$ in $Ly$.
\end{defn}

\begin{prop}
\label{10}
Suppose $\alpha \in \mathbf{C}$ is a root of the auxiliary polynomial $P(r)$ of \textup{\eqref{**}} with multiplicity $m$, i.e., suppose $m \geq 1$, $(r-\alpha)^m$ divides the polynomial $P(r)$ and $(r-\alpha)^{m+1}$ does not divide $P(r)$.
Then $y = x^k\mathrm{e}^{\alpha x}$ is a solution of \textup{\eqref{**}} for each $1 \leq k < m$.
\end{prop}

\begin{proof}
Let's start will some elementary cases to illustrate what's going on.
If \eqref{**} is of the form $y' - \alpha y = 0$, then $P(r) = r-\alpha$, so $\alpha$ is a root of multiplicity $1$, so we claim $y = \mathrm{e}^{\alpha x}$ is a solution of \eqref{**}.
Indeed, $y' = \alpha \mathrm{e}^{\alpha x}$, so $y' - \alpha x = \alpha \mathrm{e}^{\alpha x} - \alpha \mathrm{e}^{\alpha x} = 0$.

Next suppose \eqref{**} is of the form $y'' - (\alpha+\beta)y' + \alpha \beta y= 0$, with $\alpha \neq \beta$.
Then $P(r) = r^2-(\alpha + \beta)r + \alpha\beta$, so $\alpha$ is again a root of multiplicity $1$, so we claim $y = \mathrm{e}^{\alpha x}$ is a solution.
We can rewrite \eqref{**} as $(D-\beta)(D-\alpha)y = 0$, and we saw in the previous case that $(D-\alpha)y = 0$, so $(D-\beta)(D-\alpha)y = 0$ as well and $y$ is a solution.

Next, suppose \eqref{**} is of the form $y'' - 2\alpha y' + \alpha^2y = 0$.
In this case $\alpha$ is a root of $P(r)$ of multiplicity $2$, so we claim that $y_1 = \mathrm{e}^{\alpha x}$ and $y_2 = x\mathrm{e}^{\alpha x}$ are both solutions.
As in the previous case, \eqref{**} is given by $Ly = (D-\alpha)(D-\alpha)y = 0$, so we again find that $(D-\alpha)(D-\alpha)y_1 = 0$.
As for $y_2$, we have
\[
(D-\alpha)(D-\alpha)x\mathrm{e}^{\alpha x}
=
(D-\alpha)(Dx\mathrm{e}^{\alpha x} - \alpha x\mathrm{e}^{\alpha x})
=
(D-\alpha)(\mathrm{e}^{\alpha x}(1+\alpha x) - \alpha x \mathrm{e}^{\alpha x})
=
(D-\alpha)(\mathrm{e}^{\alpha x}
=
0.
\]

In general, we use the same strategy and induct on $m$:
if $\alpha$ is a root of $P(r)$ of multiplicity $m$, we can write $P(r) = Q(r)(r-\alpha)^m$ for some polynomial $Q(r)$ and therefore $Ly = L'(D-\alpha)^m$, where $L'$ is the linear differential operator whose auxiliary polynomial is $L'$, and so 
\[
Lx^k\mathrm{e}^{\alpha x}
=
L'(D-\alpha)^mx^k\mathrm{e}^{\alpha x} 
=
L'(D-\alpha)^{m-1}(Dx^k\mathrm{e}^{\alpha x} - \alpha x^k\mathrm{e}^{\alpha x})
=
L'(D-\alpha)^{m-1}kx^{k-1}\mathrm{e}^{\alpha x}
=
0
\]
for each $1 \leq k < m$.
\end{proof}

\begin{cor}
\label{11}
The vector space of complex solutions to the homogeneous linear differential equation \textup{\eqref{**}} of order $n$ is of dimension $n$.
\end{cor}

\begin{proof}
By \ref{8}, it has dimension at most $n$.
By the fundamental theorem of algebra, the auxiliary polynomial $P(r)$ factors as a product of distinct linear polynomials: 
\begin{equation}
\label{11.1}
P(r)
=
(r-\alpha_1)^{m_1} (r-\alpha_+2)^{m_2} \cdots (P-\alpha_{\ell})^{m_{\ell}}
\end{equation}
with $\alpha_i \in \mathbf{C}$, $\alpha_i \neq \alpha_j$ for each $i \neq j$ and  $m_i \in \mathbf{Z}_{>0}$.
Applying \ref{11}, we find that the following functions are all solutions to \eqref{**}:
\begin{align*}
&\mathrm{e}^{\alpha_1 x}, x\mathrm{e}^{\alpha_1 x}, \ldots, x^{m_1-1}\mathrm{e}^{\alpha_1 x}, \\
&\mathrm{e}^{\alpha_2 x}, x\mathrm{e}^{\alpha_2 x}, \ldots, x^{m_2-1}\mathrm{e}^{\alpha_2 x}, \\
&\qquad\qquad\cdots, \\
&\mathrm{e}^{\alpha_{\ell} x}, x\mathrm{e}^{\alpha_{\ell} x}, \ldots, x^{m_{\ell}-1}\mathrm{e}^{\alpha_{\ell} x}.
\end{align*}
Since $P(r)$ is of degree $n$, \eqref{11.1} implies that $n = m_1 + m_2 + \cdots +m_{\ell}$, so we have $n$ distinct solutions.
Plugging them into a gigantic Wronskian shows that they all linearly independent.
\end{proof}

\begin{ex}
\label{12}
Consider the homogeneous linear differential equation $(D+2)^2(D^2+25)y = 0$.
By \ref{11}, to find the general solution, it suffices to factor the auxiliary polynomial $P(r) = (r+2)^2(r^2+25) = (r+2)^2(r-5i)(r+5i)$.
Its roots are $-2$, $\pm5i$, and $-2$ has multiplicity $2$.
So the general complex solution is 
\[
\boxed{
y
=
c_1\mathrm{e}^{-2x} + c_2x\mathrm{e}^{-2x} + c_3\mathrm{e}^{5ix} + c_4 \mathrm{e}^{-5ix}
}
\]
\end{ex}

\begin{prop}
\label{13}
If $\alpha = a+bi$ is a root of the auxiliary polynomial $P(r)$ of \textup{\eqref{**}} of multiplicity $m$, with $a, b\in \mathbf{R}$, $b \neq 0$, then $x^k\mathrm{e}^{a x}\cos(bx)$ and $x^k\mathrm{e}^{ax}\sin(bx)$ are both linearly independent solutions of \textup{\eqref{**}}.
\end{prop}

\begin{proof}
Recall that, if $F(x)$ is a polynomial with real coefficients and $\alpha = a+bi$ is a complex root of $F(x)$, then its complex conjugate $\overline{\alpha} = a-bi$ is also a root of $F(x)$.
In particular, $\alpha = a-bi$ is also a root of the auxiliary polynomial with the same multiplicity.
By \ref{11}, $z_1 = x^k\mathrm{e}^{\alpha x}$ and $z_2 = x^k\mathrm{e}^{\overline{\alpha} x}$ are both complex solutions to \eqref{**}.

Recall Euler's formula: $\mathrm{e}^{a+bi}=\mathrm{e}^a(\cos(b) + i\sin(b))$.
In particular, it implies that
\[
\overline{z_1}
=
\overline{x^k\mathrm{e}^{(a+bi)x}}
=
\overline{x^k\mathrm{e}^{ax}(\cos(bx) + i\sin(bx))}
=
x^k\mathrm{e}^{ax}(\cos(bx) - i \sin(bx))
=
z_2.
\]
Since the solutions to \eqref{**} form a vector space, 
\[
x^k\mathrm{e}^{a x}\cos(bx)
=
\frac{1}{2}(z_1 + z_2)
=
\frac{1}{2}(z_1 + \overline{z}_1)
=
\operatorname{Re}(z_1)
\quad
\textup{and}
\quad
x^k\mathrm{e}^{ax} \sin(bx)
=
\frac{1}{2i}(z_1-z_2)
=
\frac{1}{2i}(z_1-\overline{z_1})
=
\operatorname{Im}(z_1)
\]
are both solutions.
They are clearly real-valued, and we can check that their Wronskian is nonzero, so they are independent.
\end{proof}

\begin{ex}
\label{14}
Returning $(D+2)^2(D^2+25)y = 0$ as in \ref{12}, we find that the general real-valued solution is
\[
y
=
c_1\mathrm{e}^{-2x} +c_2x\mathrm{e}^{-2x} + c_3\operatorname{Re}(\mathrm{e}^{5ix}) + c_4 \operatorname{Im}(\mathrm{e}^{5ix})
=
\boxed{c_1\mathrm{e}^{-2x} + c_2\mathrm{e}^{-2x} + c_3\cos(5x) + c_4 \sin(5x)}
\]
\end{ex}

\section{Inhomogeneous equations}

\noindent
Consider the inhomogeneous linear differential equation
\begin{equation*}
y^{(n)} + a_{n-1}y^{(n-1)} + \cdots + a_0y = F(x).
\end{equation*}
The following table indicates which trial solutions one should consider when searching for a particular solution, as determined by the form of the function $F(x)$ and its relation to the auxiliary polynomial of the complementary homogeneous differential equation.

\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
$F(x)$ & $\alpha$ & $P(\alpha)$ & $y_p$
\\
\hline
$cx^k$ & $\alpha = 0$ & $P(\alpha) \neq 0$ & $A_0 + A_1x + \cdots + A_kx^k$
\\
\hline
$cx^k$& $\alpha = 0$ & $P(\alpha) = 0$ with mult. $m$ & $x^m(A_0 + A_1x + \cdots + A_kx^k)$ 
\\
\hline
$cx^k\mathrm{e}^{\alpha x}$ & $\alpha \in \mathbf{C}$ & $P(\alpha) \neq 0$ & $\mathrm{e}^{\alpha x}(A_0 + \cdots + A_kx^k)$
\\
\hline
$cx^k\mathrm{e}^{\alpha x}$ & $\alpha \in \mathbf{C}$ & $P(\alpha) = 0$ with mult. $m$ & $x^m \mathrm{e}^{\alpha x}(A_0 + \cdots + A_kx^k)$
\\
\hline
$cx^k\mathrm{e}^{a x}\cos(bx)$ & $\alpha = a+bi$ & $P(\alpha) \neq 0$ & $\mathrm{e}^{ax}(\cos(bx)(A_0+ \cdots +A_kx^k) + \sin(bx)(B_0 + \cdots + B_kx^k))$
\\
\hline
$cx^k\mathrm{e}^{ax}\cos(bx)$ & $\alpha = a+bi$ & $P(\alpha) = 0$ with mult. $m$ & $x^m\mathrm{e}^{ax}(\cos(bx)(A_0+ \cdots+A_kx^k) + \sin(bx)(B_0+ \cdots+B_kx^k))$
\\
\hline
$cx^k\mathrm{e}^{a x}\sin(bx)$ & $\alpha = a+bi$ & $P(\alpha) \neq 0$ & $\mathrm{e}^{ax}(\cos(bx)(A_0+ \cdots +A_kx^k) + \sin(bx)(B_0 + \cdots + B_kx^k))$
\\
\hline
$cx^k\mathrm{e}^{ax}\sin(bx)$ & $\alpha = a+bi$ & $P(\alpha) = 0$ with mult. $m$ & $x^m\mathrm{e}^{ax}(\cos(bx)(A_0+ \cdots+A_kx^k) + \sin(bx)(B_0+ \cdots+B_kx^k))$
\\
\hline
\end{tabular}
\end{center}

\begin{ex}
Consider the linear differential equation
\[
y'-2y = x\cos(3x).
\]
In the notation of the above table, we have $F(x) = x\cos(3x)$, $c = 1$, $k = 1$, $a = 0$, $b = 3$, $\alpha = 0 + 3i = 3i$, and $P(r) = r-2$.
In particular, $P(3i) \neq 0$.
We therefore choose the trial solution 
\[
y_p 
= 
\mathrm{e}^{0x}(\cos(3x)(A_0+A_1x) + \sin(3x)(B_0 + B_1x)) 
= 
\cos(3x)(A_0+A_1x) + \sin(3x)(B_0 + B_1x).
\]
We find $y_p' = \cos(3x)(A_1 + 3(B_0+B_1x)) + \sin(3x)(B_1-3(A_0+A_1x))$, so we have
\begin{align*}
x\cos(3x)
&=
y'_p - 2y_p 
\\
&=
\cos(3x)(A_1 + 3(B_0+B_1x)) + \sin(3x)(B_1-3(A_0+A_1x))
-2(\cos(3x)(A_0+A_1x) + \sin(3x)(B_0 + B_1x))
\\
&=
\cos(3x)(-2A_0+A_1+3B_0 +(-2A_1+3B_1)x)
+
\sin(3x)(-3A_0-2B_0+B_1+(-3A_1-2B_1)x)
\end{align*}
Since $\sin(3x)$, $\cos(3x)$, $x\sin(3x)$ and $x\cos(3x)$ are linearly independent, we obtain a linear system of equations:
\[
\left\{
\begin{array}{ccl}
0 & = & -2 A_0 + A_1 + 3 B_0
\\
1 & = & -2A_1+3B_1
\\ 
0 & = & -3A_0-2B_0+B_1 
\\
0 & = & -3A_1 - 2B_1
\end{array}
\right.
\]
Solving this system, we find $A_0 = \frac{5}{169}$, $A_1 = \frac{-2}{13}$, $B_0 = \frac{12}{169}$, $B_1 = \frac{3}{13}$.
We therefore have a particular solution 
\[
y_p 
= 
\left(\frac{5}{169} - \frac{2}{13}x\right)\cos(3x) + \left(\frac{2}{169} + \frac{3}{13}x\right)\sin(3x).
\]
The general solution for the complementary equation $y' - 2y = 0$ is $y_c = c_1\mathrm{e}^{2x}$, so the general solution for our inhomogeneous equation is 
\[
y
=
c_1\mathrm{e}^{2x} + \left(\frac{5}{169} - \frac{2}{13}x\right)\cos(3x) + \left(\frac{2}{169} + \frac{3}{13}x\right)\sin(3x).
\]
\end{ex}

\begin{ex}
Consider the linear differential equation
\[
y'' -6y' +9y = 5x^2 \mathrm{e}^{3x}.
\]
We have $F(x) = 5x^2\mathrm{e}^{3x}$, $c = 5$, $k=2$,$\alpha = 3$ and $P(r) = r^2-6r+9 = (r-3)^2$.
Thus, $P(3) = 0$ with multiplicity $2$.
So we choose the trial solution 
\[
y_p 
=
x^2\mathrm{e}^{3x}(A_0 + A_1x + A_2x^2)
=
\mathrm{e}^{3x}(A_0x^2 + A_1x^3+ A_2x^4).
\]
We find 
\begin{align*}
y_p'
&=
\mathrm{e}^{3x}(2A_0x + (3A_0+3A_1)x^2 + (3A_1+4A_2)x^3 + 3A_2x^4)
\\
y_p''
&=
\mathrm{e}^{3x}((2A_0+6A_1) + (12A_0+6A_1)x + (9A_0+18A_1+12A_2)x^2 + (9A_1+24A_2)x^3 + (9A_2x^4))
\end{align*}
and a somewhat lengthy check shows that 
\[
y_p'' - 6y_p' + 0y_p
=
\mathrm{e}^{3x}((2A_0+6A1)  + 6A_1x + 12A_2x^2).
\]
Setting this equal to $F(x) = 5x^2\mathrm{e}^{3x}$, we obtain a linear system
\[
\left\{
\begin{array}{ccl}
0 & = & 2A_0 + 6A_1 \\
0 & = & 6A_1 \\
5 & = & 12 A_2
\end{array}
\right.
\]
so $A_0 = 0$, $A_1 = 0$ and $A_2 = \frac{5}{12}$.
Our particular solution is therefore $y_p = \frac{5}{12}x^4\mathrm{e}^{3x}$.
Our complementary solution is $y_c = c_1\mathrm{e}^{3x} + c_2x \mathrm{e}^{3x}$, so the general solution is
\[
y
=
y_c+y_p
=
c_1\mathrm{e}^{3x} + c_2x \mathrm{e}^{3x} + \frac{5}{12}x^4\mathrm{e}^{3x}.
\]
\end{ex}


\end{document}
