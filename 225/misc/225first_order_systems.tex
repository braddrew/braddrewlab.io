\documentclass[letterpaper, 11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, enumitem, etoolbox, hyperref, microtype, mathtools}
\usepackage[margin=2cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[mathcal]{eucal}
\usepackage[frenchstyle, largesmallcaps]{kpfonts}
%\setlength{\parindent}{0pt}

\theoremstyle{plain}
\newtheorem{thm}{Theorem}
\newtheorem{lemma}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{cor}[thm]{Corollary}
\theoremstyle{definition}
\newtheorem{defn}[thm]{Definition}
\newtheorem{rmk}[thm]{Remark}
\newtheorem{problem}{Problem}
\newtheorem*{ex}{Example}

\renewcommand{\thequestion}{\textsc{Problem} \arabic{question}}
\renewcommand{\thepartno}{\alph{partno}}
\renewcommand{\solutiontitle}{\textsc{Solution:}\enspace}

\firstpagefooter{}{}{}
\runningfooter{}{}{}

\newcommand{\im}{\operatorname{im}}

\begin{document}

\noindent
\LARGE
\textsc{Math} $225$ 
\hfill
\large
April $20$, $2016$ \\
\normalsize

\begin{center}
\Large
\textsc{First-order linear systems}
\normalsize
\end{center}

\begin{thm}
\label{1}
Consider an $n \times n$ matrix $A$ whose entries are continuous complex-valued (resp. real-valued) functions on the interval $I$, a point $t_0 \in I$ and a complex (resp. real) column vector $\mathbf{x}_0$ of length $n$.
The initial value problem $\mathbf{x}' = A\mathbf{x}$, $\mathbf{x}(t_0) = \mathbf{x}_0$ admits at most one solution on $I$.
\end{thm}

\begin{proof}
The proof of this theorem is quite similar to that of Corollary 29.5.
\end{proof}

\begin{thm}
\label{2}
Consider an $n \times n$ matrix $A$ whose entries are continuous complex-valued (resp. real-valued) functions on the interval $I$.
The solution set of the homogeneous first-order linear system $\mathbf{x}' = A \mathbf{x}$ is a complex (resp. real) vector space of dimension $n$.
\end{thm}

\begin{proof}
This theorem follows from \ref{1} in almost the same way that Corollary 30.1 followed from Corollary 29.5.
\end{proof}

\begin{defn}
\label{3}
Let $\mathbf{x}_1(t), \ldots, \mathbf{x}_n(t)$ be column vectors of complex-valued or real-valued functions on an interval $I$.
Their \emph{Wronskian} $W[\mathbf{x}_1, \ldots, \mathbf{x}_n](t)$  at $t \in I$ is the determinant
\[
W[\mathbf{x}_1, \ldots, \mathbf{x}_n](t)
=
\det\left(\begin{bmatrix}\mathbf{x}_1(t) & \mathbf{x}_2(t) & \cdots & \mathbf{x}_n(t)\end{bmatrix}\right).
\]
For example, if $\mathbf{x}_1(t) = (\cos(3t), \sin(2t))$, $\mathbf{x}_2(t) = (t^3, \mathrm{e}^t)$, then
\[
W[\mathbf{x}_1,\mathbf{x}_2](t)
=
\begin{vmatrix}
\cos(3t) & t^3 \\
\sin(2t) & \mathrm{e}^t
\end{vmatrix}
=
\mathrm{e}^t\cos(3t) - t^3\sin(2t).
\]
\end{defn}

\begin{prop}
\label{4}
Let $A$ be an $n \times n$ matrix whose entries are complex-valued or real-valued function on the interval $I$, and let $\mathbf{x}_1, \ldots, \mathbf{x}_n$ be solutions to the homogeneous first-order linear system $\mathbf{x}' = A\mathbf{x}$.
Then the following are equivalent:
\begin{enumerate}[topsep=0ex, itemsep=0ex, label=$(\roman*)$]
\item $W[\mathbf{x}_1, \ldots, \mathbf{x}_n](t_0) \neq 0$ for some $t_0 \in I$;
\item $W[\mathbf{x}_1, \ldots, \mathbf{x}_n](t) \neq 0$ for each $t \in I$;
\item the set $\{\mathbf{x}_1, \ldots, \mathbf{x}_n\}$ is linearly independent.
\end{enumerate}
\end{prop}

\begin{proof}
The proof is almost the same as that of Corollary 29.4. 
\end{proof}

\begin{center}
\textsc{Complex solutions for constant-coefficient systems}
\end{center}

\noindent
Let $A \in M_{n \times n}(\mathbf{C})$ and suppose $A$ admits $n$ linearly independent eigenvectors $\mathbf{v}_1, \ldots, \mathbf{v}_n$ with corresponding eigenvalues $\lambda_1, \ldots, \lambda_n$.
Then the general solution of the first-order linear system $\mathbf{z}' = A\mathbf{z}$ is 
\[
\fbox{$\mathbf{z}(t) 
=
c_1\mathrm{e}^{\lambda_1t}\mathbf{v}_1 + \cdots + c_n\mathrm{e}^{\lambda_nt}\mathbf{v}_n$
}
\]
with $c_1, \ldots, c_n \in \mathbf{C}$.

\begin{ex}
Let $A = \begin{bmatrix} 3 & -1 \\ 0 & 2 \end{bmatrix}$.
It has eigenvalues $2$ and $3$, with corresponding eigenvectors $(1,1)$ and $(1,0)$, respectively.
This implies that $\mathbf{x}_1(t) = \mathrm{e}^{2t}\begin{bmatrix}1 \\1\end{bmatrix}$ and $\mathbf{x}_2(t) = \mathrm{e}^{3t}\begin{bmatrix}1 \\0\end{bmatrix}$ are solutions to the homogeneous first-order system $\mathbf{x}' = A\mathbf{x}$.
We have
\[
W[\mathbf{x}_1, \mathbf{x}_2](t)
=
\begin{vmatrix}
\mathrm{e}^{2t} & \mathrm{e}^{3t} \\
\mathrm{e}^{2t} & 0
\end{vmatrix}
=
-\mathrm{e}^{5t}
\neq0,
\]
so $\mathbf{x}_1$ and $\mathbf{x}_2$ are linearly independent by \ref{4}.
By \ref{2}, the vector space of solutions to this system is of dimension $2$, so $\{\mathbf{x}_1, \mathbf{x}_2\}$ is a basis for it, and the general solution is therefore $\mathbf{x}(t) = c_1\mathbf{x}_1(t) + c_2\mathbf{x}_2(t)$.
\end{ex}

\clearpage

\begin{center}
\textsc{Real solutions for constant-coefficient systems}
\end{center}

\noindent
Let $A \in M_{n \times n}(\mathbf{R})$ and let $\mathbf{z}$ be a complex eigenvector of $A$ with eigenvalue $\lambda = a+bi$, where $a,b \in \mathbf{R}$. 
We can write $\mathbf{z} = \mathbf{v} + i\mathbf{w}$ with $\mathbf{v}$ and $\mathbf{w}$ \emph{real} vectors by writing each component of $\mathbf{z}$ as the sum of its real and imaginary parts, e.g. $\mathbf{z} = (2+3i, 7-\pi i) = (2,7) + i(3,-\pi)$, so $\mathbf{v} = (2,7)$ and $\mathbf{w} = (3,-\pi)$.
The complex conjugate $\overline{\mathbf{z}} = \mathbf{v} - i\mathbf{w}$ is also an eigenvector of $A$, with eigenvalue $\overline{\lambda} = a-bi$.
Then $\mathbf{z}_1(t) = \mathrm{e}^{\lambda t}\mathbf{z}$ and $\mathbf{z}_2(t) = \mathrm{e}^{\overline{\lambda}t}\overline{\mathbf{z}}$ are two complex solutions to $\mathbf{x}' = A\mathbf{x}$.
We want to use them to find \emph{real} solutions.

We have
\begin{align*}
\mathrm{e}^{(a\pm bi)t}(\mathbf{v} \pm i\mathbf{w})
&=
\mathrm{e}^{at}(\cos(bt) \pm i\sin(bt))(\mathbf{v} \pm i\mathbf{w})
\\
&=
\mathrm{e}^{at}(\cos(bt)\mathbf{v} \pm i\cos(bt)\mathbf{w} \pm i\sin(bt)\mathbf{v} - \sin(bt)\mathbf{w})
\\
&=
\mathrm{e}^{at}((\cos(bt)\mathbf{v} - \sin(bt)\mathbf{w}) \pm i(\sin(bt)\mathbf{v} + \cos(bt)\mathbf{w}))
\end{align*}
which shows that $\mathbf{z}_2(t) = \overline{\mathbf{z}_1(t)}$.

Since the solution set to the homogeneous first-order system $\mathbf{x}' = A\mathbf{x}$ is a vector space, we have the solutions
\begin{align*}
\mathbf{x}_1(t)
&\coloneqq
\frac{1}{2}(\mathbf{z}_1(t) - \mathbf{z}_2(t))
=
\boxed{\mathrm{e}^{at}(\cos(bt)\mathbf{v} - \sin(bt)\mathbf{w})}
\\
\mathbf{x}_2(t)
&\coloneqq
\frac{1}{2i}(\mathbf{z}_1(t) + \mathbf{z}_2(t))
=
\boxed{\mathrm{e}^{at}(\sin(bt)\mathbf{v} + \cos(bt)\mathbf{w})}
\end{align*}
both of which are real-valued.
Notice that $\mathbf{x}_1(t)$ is nothing but the real part of $\mathbf{z}_1(t)$: 
\[
\boxed{\mathbf{x}_1(t) = \operatorname{Re}(\mathbf{z}_1(t))}
\]
and $\mathbf{x}_2(t)$ is nothing but the imaginary part of $\mathbf{z}_1(t)$:
\[
\boxed{\mathbf{x}_2(t) = \operatorname{Im}(\mathbf{z}_1(t))}
\]
Computing their Wronskian $W[\mathbf{x}_1, \mathbf{x}_2](t)$, we find that they are linearly independent if $b \neq 0$.
So the complex-conjugate pair $\mathbf{z}_1, \mathbf{z}_2$ provides us with two linearly independent real solutions $\mathbf{x}_1$ and $\mathbf{x}_2$.
In particular, \emph{if $A$ admits $n$ linearly independent complex eigenvectors, then $\mathbf{x}' = A\mathbf{x}$ admits $n$ linearly independent real-valued solutions constructed by the above method.}

\begin{ex}
Let $A = \begin{bmatrix} 2 & 1 \\ -2 & 0\end{bmatrix}$.
It has eigenvalues $\lambda = 1 \pm i$ with corresponding complex eigenvectors $\mathbf{z} = (1+i,-2)$ and $\overline{\mathbf{z}} = (1-i,-2)$.
We therefore have complex solutions $\mathbf{z}_1(t) = \mathrm{e}^{(1+i)t}\mathbf{z}$ and $\mathbf{z}_2(t) = \mathrm{e}^{(1-i)t}\overline{\mathbf{z}}$ to $\mathbf{x}' = A\mathbf{x}$.
Writing $\mathbf{z}$ as the sum of its real and imaginary parts $\mathbf{z} = (1,-2) + i(1,0)$, we find that the real-valued solutions are given by 
\begin{align*}
\mathbf{x}_1(t)
&=
\operatorname{Re}(\mathbf{z}_1(t))
=
\mathrm{e}^t\left(\cos(t)\begin{bmatrix} 1 \\ -2\end{bmatrix} - \sin(t) \begin{bmatrix} 1 \\0\end{bmatrix}\right)
=
\mathrm{e}^t\begin{bmatrix} \cos(t) - \sin(t) \\ -2\cos(t)\end{bmatrix}
\\
\mathbf{x}_2(t)
&=
\operatorname{Im}(\mathbf{z}_1(t))
=
\mathrm{e}^t\left(\sin(t)\begin{bmatrix} 1 \\ -2\end{bmatrix} + \cos(t) \begin{bmatrix} 1 \\0\end{bmatrix}\right)
=
\mathrm{e}^t\begin{bmatrix} \sin(t) + \cos(t) \\ -2\sin(t) \end{bmatrix}
\end{align*}
\end{ex}

\end{document}

