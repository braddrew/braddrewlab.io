\documentclass[letterpaper,11pt]{article}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle, largesmallcaps, light]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\allsectionsfont{\fontseries{m}\scshape}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0.25\baselineskip}

\theoremstyle{remark}
\newcounter{thm}
%\numberwithin{thm}{section}
\newtheorem{problem}[thm]{Problem}
\newtheorem{definition}[thm]{\textsc{Definition}}
\newtheorem{example}[thm]{\textsc{Example}}
\newtheorem{exercise}[thm]{\textsc{Exercise}}

\theoremstyle{plain}
\newtheorem{fact}[thm]{\fontseries{m}\textsc{Fact}}

\newcommand{\dd}[1]{\ensuremath{\mathrm{d}#1}}
\newcommand{\ddt}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}t}}}
\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}
\newcommand{\ddu}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}u}}}
\newcommand{\DDh}[1]{\ensuremath{\frac{\partial#1}{\partial h}}}
\newcommand{\DDr}[1]{\ensuremath{\frac{\partial#1}{\partial r}}}
\newcommand{\DDx}[1]{\ensuremath{\frac{\partial#1}{\partial x}}}
\newcommand{\DDy}[1]{\ensuremath{\frac{\partial#1}{\partial y}}}
\newcommand{\DDxx}[1]{\ensuremath{\frac{\partial^2#1}{\partial x^2}}}
\newcommand{\DDyy}[1]{\ensuremath{\frac{\partial^2#1}{\partial y^2}}}
\newcommand{\DDxy}[1]{\ensuremath{\frac{\partial^2#1}{\partial x\partial y}}}
\newcommand{\DDyx}[1]{\ensuremath{\frac{\partial^2#1}{\partial y\partial x}}}
\newcommand{\dddx}[1]{\ensuremath{\frac{\mathrm{d}^2#1}{\mathrm{d}x^2}}}
\newcommand{\defn}[1]{\textbf{\emph{#1}}}
\newcommand{\im}{\operatorname{im}}

\begin{document}

%\printanswers

\LARGE
\textsc{Math} $225$ 
\hfill
\large
January $13$, $2016$ \\
\normalsize

\begin{center}
\Large
\textsc{Basic definitions pertaining to and examples of matrices}
\normalsize
\end{center}

\textsc{Matrix transpose:}
If $A$ is an $m \times n$ matrix, then its \emph{transpose} $A^T$ is the $n \times m$ matrix whose $ij$th entry is the $ji$th entry of $A$: $[A^T]_{ij} = [a_{ji}]$.
Note that $(A^T)^T = A$.
For example, the following matrices are transposes of one another:
\[
\begin{bmatrix}
3 & 1 \\
2 & x \\
-1 & 0
\end{bmatrix}
\qquad\text{and}\qquad
\begin{bmatrix}
3 & 2 & -1 \\
1 & x & 0
\end{bmatrix}.
\]

\textsc{Square matrix:}
A matrix of size $n \times n$ for some $n$, i.e., a matrix with as many rows as columns, is said to be \emph{square}.
For example,
\begin{equation}
\label{1}
\begin{bmatrix}
1 & -1 & 2 \\
3 & -5 & 8 \\
12 & 4 & 34
\end{bmatrix}
\end{equation}
is a $3 \times 3$ square matrix.
The transpose of a square matrix is another square matrix of the same size.

\textsc{Diagonal:}
If $A$ is an $n \times n$ square matrix, then its \emph{diagonal} is the set of entries of the form $a_{ii}$ for $1 \leq i \leq n$.
For example, $1$, $-5$ and $34$ are the entries of the diagonal of the square matrix \eqref{1}.
We say that $A$ is a \emph{diagonal matrix} if $a_{ij} = 0$ for each $i \neq j$, i.e., if its only nonzero entries are on the diagonal.

\textsc{Trace:}
The \emph{trace} $\operatorname{tr}(A)$ of an $n \times n$ square matrix $A$ is the sum of its diagonal entries: $\operatorname{tr}(A) := \sum_{i=1}^na_{ii}$.
For example, $30 = 1 + (-5) + 34$ is the trace of the square matrix \eqref{1}.

\textsc{Upper (lower) triangular matrix:}
An $n \times n$ square matrix $A$ is said to be \emph{upper triangular} (resp., \emph{lower triangular}) if $a_{ij} = 0$ for each $i>j$ (resp., $i<j$).
For example,
\[
A
=
\begin{bmatrix}
3 & -2 & 7 \\
0 & 5 & -2 \\
0 & 0 & 11
\end{bmatrix}
\]
is upper triangular and $A^T$ is lower triangular, whereas
\[
\begin{bmatrix}
0 & 1 \\
-1 & 0
\end{bmatrix}
\]
is neither upper nor lower triangular.

\textsc{Symmetric matrix:}
An $n \times n$ matrix $A$ is \emph{symmetric} if $A = A^T$.
For example,
\begin{equation}
\label{2}
\begin{bmatrix}
1 & 0 & -2 \\
0 & -1 & 3 \\
-2 & 3 & -4
\end{bmatrix}
\end{equation}
is symmetric, but
\begin{equation}
\label{3}
\begin{bmatrix}
1 & 0 & -2 \\
0 & -1 & 3 \\
2 & -3 & -4
\end{bmatrix}
\end{equation}
is not.
All diagonal matrices are symmetric.

\textsc{Antisymmetric matrix:}
An $n \times n$ matrix $A$ is \emph{antisymmetric} if $A = -A^T$.
For example,
\[
\begin{bmatrix}
0 & 0 & -2 \\
0 & 0 & 3 \\
2 & -3 & 0 
\end{bmatrix}
\]
is antisymmetric, but the matrices of \eqref{2} and \eqref{3} are not.
If $A$ is both symmetric and antisymmetric, then $A$ is the zero matrix.
To prove this, we must show that $a_{ij} = 0$ for all $i$ and $j$.
If $A$ is symmetric, then $a_{ij} = a_{ji}$.
If $A$ is also antisymmetric, then $a_{ji} = -a_{ij}$.
Combining these equalities, we find $a_{ij} = -a_{ij}$, which implies $a_{ij} = 0$, as desired.

\end{document}
