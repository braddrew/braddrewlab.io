\documentclass[letterpaper, 11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, enumitem, etoolbox, hyperref, microtype, mathtools, sectsty, systeme}
\usepackage[margin=2cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[mathcal]{eucal}
\usepackage[frenchstyle, largesmallcaps]{kpfonts}
%\setlength{\parindent}{0pt}

\allsectionsfont{\centering\fontseries{m}\scshape}

\theoremstyle{plain}
\newtheorem{thm}{Theorem}
\newtheorem{lemma}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{cor}[thm]{Corollary}
\theoremstyle{definition}
\newtheorem{defn}[thm]{Definition}
\newtheorem{rmk}[thm]{Remark}
\newtheorem{problem}{Problem}
\newtheorem*{ex}{Example}

\renewcommand{\thequestion}{\textsc{Problem} \arabic{question}}
\renewcommand{\thepartno}{\alph{partno}}
\renewcommand{\solutiontitle}{\noindent\textsc{Solution:}\enspace}

\firstpagefooter{}{}{}
\runningfooter{}{}{}

\newcommand{\im}{\operatorname{im}}

\begin{document}

\printanswers

\noindent
\LARGE
\textsc{Math} $225$ 
\hfill
\large
April $27$, $2016$ \\
\normalsize

\begin{center}
\Large
\textsc{Review of linear maps}
\normalsize
\end{center}

\vspace{\baselineskip}

\begin{questions}
\question
State the definition of each of the following:
\begin{parts}
\part
a linear map $f: V \to W$;
\part
the kernel of the linear map $f: V\to W$;
\part
the image of the linear maps $f: V \to W$;
\part
the matrix of $f$ relative to ordered bases $B$ and $C$ for $V$ an $W$, respectively;
\part
a one-to-one linear map;
\part
an onto linear map.
\end{parts}

\begin{solution}
\begin{enumerate}[topsep=0ex, itemsep=0ex, label=(\alph*)]
\item
a function such that $f(a(\mathbf{v}+\mathbf{w})) = af(\mathbf{v}) + af(\mathbf{w})$ for each $a \in \mathbf{R}$ and each $\mathbf{v}, \mathbf{w} \in V$;
\item
$\operatorname{ker}(f) \coloneqq \{\mathbf{v} \in V \mid f(\mathbf{v}) = \mathbf{0}\} \subseteq V$;
\item
$\operatorname{im}(f) \coloneqq \{f(\mathbf{v}) \mid \mathbf{v} \in V\} \subseteq W$;
\item
if $B = \langle \mathbf{v}_1, \ldots, \mathbf{v}_n\rangle$, then $[f]^C_B \coloneqq \begin{bmatrix} [f(\mathbf{v}_1)]_C & [f(\mathbf{v}_2]_C & \cdots & [f(\mathbf{v}_n)]_C \end{bmatrix}$;
\item
$\operatorname{ker}(f) = \{\mathbf{0}\}$;
\item
$\operatorname{im}(f) = W$.
\end{enumerate}
\end{solution}

\question
Which of the following are linear maps?
\begin{parts}
\part
$f: \mathbf{R}^7 \to \mathbf{R}^2$, $f(a,b,c,d,e,f,g) = (2c,0)$;
\part
$f: \mathbf{R} \to \mathbf{R}$, $f(a) = \mathrm{e}^a$;
\part
$f: \mathcal{P}_2(\mathbf{R}) \to \mathbf{R}^2$, $f(a+bx+cx^2) = (3a-b+2c, b-c)$;
\part
$f: \mathbf{R}^{1203} \to \mathbf{R}^3$, $f(\mathbf{v}) = \mathbf{0}$ for each $\mathbf{v}$.
\end{parts}

\begin{solution}
Only (b) fails to be linear.
\end{solution}

\question
Consider the linear map $f: \mathcal{P}_2(\mathbf{R}) \to \mathbf{R}^2$, $f(a+bx+cx^2) = (3a-b+2c, b-c)$.
\begin{parts}
\part
Find $\ker(f)$ and $\operatorname{im}(f)$.
Is $f$ one-to-one?
\part
Find bases for $\ker(f)$ and $\operatorname{im}(f)$.
Is $f$ onto?
\part
Extend these bases to bases for $\mathcal{P}_2(\mathbf{R})$ and $\mathbf{R}^2$, respectively.
\part
If $B = \langle 1, 1-x, 1+x-x^2\rangle$ and $C = \langle (1,0), (1,-1)\rangle$, find $[f]^C_B$.
\part
Find $[f(7+18x-13x^2)]_C$ using $[f]^C_B$.
\part
If $B'$ is the basis for $\mathcal{P}_2(\mathbf{R})$ you found in (c) with some chosen ordering, find $P_{B' \leftarrow B}$ and $P_{B \leftarrow B'}$.
\part
Find $[f]^C_{B'}$ using (f).
\part
If $g: \mathcal{P}_2(\mathbf{R}) \to \mathbf{R}$ is linear, then what can we say about $\operatorname{dim}(\ker(g))$?
\part
If $h: \mathcal{P}_2(\mathbf{R}) \to \mathbf{R}^3$ is linear and not onto, what can we say about $\operatorname{dim}(\ker(h))$?
\end{parts}

\begin{solution}
\begin{enumerate}[topsep=0ex, itemsep=0ex, label=(\alph*)]
\item
$
\systeme*{3a-b+2c = 0, b-c = 0}
\quad
\Rightarrow
\quad
\systeme*{c = -3a, b=c}
\quad
\Rightarrow
\quad
\boxed{\ker(f)
=
\{a-3ax-3ax^2\mid a \in \mathbf{R}\}
\,\,
\Rightarrow
\textup{not one-to-one}}
$
\\
$\boxed{\operatorname{im}(f) = \{(3a-b+2c,b-c\mid a,b,c\in \mathbf{R}\}}$
\item
$\ker(f) = \operatorname{span}\{1-3x-3x^2\}
\quad
\Rightarrow
\quad
\boxed{\{1-3x-3x^2\}\textup{ is a basis for }\ker(f)}
$
\\
Taking $a = 1$, $b=c=0$, $(3,0) \in \operatorname{im}(f)$.
Taking $b=1$, $a=c=0$, $(-1,1) \in \operatorname{im}(f)$.
As $(3,0)$ and $(-1,1)$ are not collinear and $\dim(\mathbf{R}^2) = 2$, $\operatorname{im}(f) = \mathbf{R}^2$, $\boxed{f\textup{ is onto}}$ and $\{(1,0),(0,1)\}$ (or $\{(3,0),(-1,1)\}$) is a basis for $\operatorname{im}(f)$.
\item
There's nothing to do for the basis for $\operatorname{im}(f)$.
Note that $\dim(\mathcal{P}_2(\mathbf{R}))=3$.
As $x \not \in \operatorname{span}\{1-3x-3x^2\}$ and $x^2 \not \in \operatorname{span}\{1-3x-3x^2,x\}$, $\{1-3x-3x^2,x,x^2\}$ is a basis for $\mathcal{P}_2(\mathbf{R})$.
\item
We have $f(1) = (3,0)$, $f(1-x) = (4,-1)$ and $f(1+x-x^2) = (0,2)$.
Find the coordinate vectors of these outputs relative to $C$:
\[
\begin{bmatrix}
1 & 1 & 3 & 4 & 0 \\
0 & -1 & 0 & -1 & 2
\end{bmatrix}
\rightarrow
\begin{bmatrix}
1 & 0 & 3 & 3 & 2 \\
0 & 1 & 0 & 1 & -2
\end{bmatrix}
\quad
\rightarrow
\quad
\boxed{
[f]^C_B
=
\begin{bmatrix}
3 & 3 & 2 \\
0 & 1 & -2
\end{bmatrix}}
\]
\item
\begin{align*}
&
\begin{bmatrix}
1 & 1 & 1 & 7 \\
0 & -1 & 1 & 18 \\
0 & 0 & -1 & -13
\end{bmatrix}
\to
\begin{bmatrix}
1 & 0 & 0 & -1 \\
0 & 1 & 0 & -5 \\
0 & 0 & 1 & 13
\end{bmatrix}
\quad
\Rightarrow
\quad
[7+18x-13x^2]_B = (-1,-5,13)
\\
&
[f(7+18x-13x^2)]_C
=
[f]^C_B[7+18x-13x^2]_B
=
\begin{bmatrix}
3 &3 & 2 \\\
0 & 1 & -2
\end{bmatrix}
\begin{bmatrix}
-1 \\ -5 \\ 13
\end{bmatrix}
=
\boxed{
(8,-31)
}
\end{align*}
\item
We fix the ordering $B' = \langle 1-3x-3x^2, x, x^2\rangle$.
\begin{align*}
&\begin{bmatrix}
1 & 1 & 1 & 1 & 0 & 0 \\
0 & -1 & 1 & -3 & 1 & 0 \\
0 & 0 & -1 & -3 & 0 & 1
\end{bmatrix}
\to
\begin{bmatrix}
1 & 0 & 0 & -8 & 1 & 2 \\
0 & 1 & 0 & 6 & -1 & -1 \\
0 & 0 & 1 & 3 & 0 & -1
\end{bmatrix}
\quad
\Rightarrow
\quad
\boxed{P_{B \leftarrow B'}
=
\begin{bmatrix}
-8 & 1 & 2 \\
6 & -1 & -1 \\
3 & 0 & -1
\end{bmatrix}
}
\\
&\begin{bmatrix}
1 & 0 & 0 & 1 & 1 & 1 \\
-3 & 1 & 0 & 0 & -1 & 1 \\
-3& 0 & 1 & 0 & 0 & -1
\end{bmatrix}
\to
\begin{bmatrix}
1 & 0 & 0 & 1 & 1 & 1 \\
0 & 1 & 0 & 3 & 2 & 4 \\
0 & 0 & 1 & 3 & 3 & 2   
\end{bmatrix}
\quad
\Rightarrow
\quad
\boxed{
P_{B' \leftarrow B}
=
\begin{bmatrix}
1 & 1 & 1 \\
3 & 2 & 4 \\
3 & 3 & 2 
\end{bmatrix}
}
\end{align*}
\item
$
[f]^C_{B'}
=
[f]^C_BP_{B \leftarrow B'}
=
\begin{bmatrix}
3 &3 & 2 \\\
0 & 1 & -2
\end{bmatrix}
\begin{bmatrix}
-8 & 1 & 2 \\
6 & -1 & -1 \\
3 & 0 & -1
\end{bmatrix}
=
\boxed{
\begin{bmatrix}
0 & 0 & 1\\
0 & -1 & 1
\end{bmatrix}
}
$
\item
$\dim(\ker(g)) + \dim(\operatorname{im}(g)) = \dim(\mathcal{P}_2(\mathbf{R})) = 3$, and $0 \leq \dim(\operatorname{im}(g)) \leq \dim(\mathbf{R}) = 1$, so $2 \leq \dim(\ker(g)) \leq 3$.
\item
$\dim(\ker(h))+ \dim(\operatorname{im}(h)) = \dim(\mathcal{P}_2(\mathbf{R})) = 3$ and $0 \leq \dim(\operatorname{im}(h)) < \dim(\mathbf{R}^3) = 3$, so $1 \leq \dim(\ker(h)) \leq 3$.
\end{enumerate}
\end{solution}
\end{questions}

\end{document}
