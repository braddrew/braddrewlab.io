\documentclass[letterpaper,11pt]{article}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, enumitem, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[frenchstyle, largesmallcaps, light]{kpfonts}

\theoremstyle{plain}
\newtheorem*{thm}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{convention}{Convention}

\theoremstyle{remark}
\newtheorem*{defn}{Definition}

\begin{document}

\LARGE
\textsc{Math} $225$ 
\hfill
\large
February $5$, $2016$ \\
\normalsize

\begin{center}
\Large
\textsc{Existence of determinant functions}
\normalsize
\end{center}

\begin{convention}
Throughout this note, we fix integers $k$ and $n$ such that $1 \leq k \leq n$.
\end{convention}

\begin{defn}
Let $A$ be an $n \times n$ matrix.
\begin{enumerate}[label=$(\roman*)$]
\item
If $n=1$, define $\det(A) \coloneqq a_{11}$ to be the single entry of $A$.
\item
If $1 \leq i, j \leq n$, then $A(i,j)$ denotes the $(n-1) \times (n-1)$ submatrix of $A$ obtained by deleting the $i$th row and $j$th column.
\item
Suppose we have defined $\det(B)$ for each $m \times m$ matrix with $m <n$.
Define the \emph{$ij$th  minor $M_{ij}^A$ of $A$} to be $M^A_{ij} \coloneqq \det(A(i,j))$ and define the \emph{$ij$th cofactor $C^A_{ij}$ of $A$} to be $C^A_{ij} \coloneqq (-1)^{i+j}M^A_{ij}$.
\item
If $n \geq 1$, define $\det(A)$ recursively as follows:
\[
\det(A)
\coloneqq
\sum_{i=1}^na_{ik}C^A_{ik}
\]
i.e., $\det(A)$ is the cofactor expansion of $A$ along the $k$th column.
\end{enumerate}
\end{defn}

\begin{lemma}
If $A$ and $B$ are $n \times n$ matrices and $B$ is obtained from $A$ by switching the $i$th row with the $j$th row for some $1 \leq i < j \leq n$, then $\det(A) = -\det(B)$.
\end{lemma}

\begin{proof}
We proceed by induction on $n$.
If $n=1$, then there is nothing to prove: $A$ does not have two distinct rows to swap.
Suppose that the claim is true for square matrices of size at most $(n-1) \times (n-1)$.

Without loss of generality, we may assume that $i = 1$ and $j > 1$.
To see this, note that, in general, switching the $i$th and $j$th rows is the same as first switching the first and $i$th rows, then the first and $j$th rows and finally the first and $i$th rows again.
If we know that switching the first row with any other multiplies the determinant by $-1$, then the general case, which is achieved by performing three such swaps, multiplies the determinant by $(-1)^3 = -1$.
So we henceforth assume $i = 1$ and $j > 1$.

For each $\ell$ between $1$ and $n$ such that $\ell \neq 1$ and $\ell \neq j$, we have 
\begin{equation}
\label{1}
a_{\ell k}C^A_{\ell k} = -b_{\ell k}C^B_{\ell k}
\end{equation}
by the inductive hypothesis, because $A(\ell, k)$ and $B(\ell, k)$ are obtained from one another by switching two rows.
We claim that we also have 
\begin{equation}
\label{2}
a_{jk}C^A_{j k} = -b_{1 k}C^B_{1 k} 
\quad\text{and}\quad
a_{1 k}C^A_{1 k} = -b_{j k}C^B_{j k}.
\end{equation}
By symmetry, it suffices to prove the first equality.
Note that $a_{jk} = b_{1k}$, since $B$ is obtained from $A$ by swapping the first and $k$th rows.
Also, $B(1, k)$ is obtained from $A(j, k)$ by moving the top row to the $j$th row and bumping each of the rows in between the first and the $j$th up one row.
Equivalently, this means we swap the first and second rows, then the second and third rows, \dots, then finally the $(j-2)$nd row with the $(j-1)$st row.
This makes a total of $j-2$ row swaps in an $(n-1) \times (n-1)$ matrix.
By the inductive hypothesis, these swaps multiply the determinant by $(-1)^{j-2} = (-1)^j$, so $M^A_{j k} = (-1)^jM^B_{1k}$.
Combining this with our previous observation, we have 
\[
a_{jk}C^A 
= 
a_{jk}(-1)^{j+k}M^A_{jk}
=
b_{1k}(-1)^{j+k}(-1)^jM^B_{1k}
=
b_{1k}(-1)^kM^B_{1k}
=
-b_{1k}(-1)^{k+1}M^B_{1k}
= 
-b_{1k}C^B_{1k}.
\]
Combining \eqref{1} and \eqref{2}, it follows that $\det(A) = \det(B)$.
\end{proof}

\begin{lemma}
If $A$ and $B$ are $n \times n$ matrices and $B$ is obtained from $A$ by multiplying the $\ell$th row by the scalar $c$, then $\det(B) = c \det(A)$.
\end{lemma}

\begin{proof}
We proceed by induction on $n$.
If $n = 1$, then $\det(A) = a_{11}$ and $\det(B) = ca_{11}$, as desired.
Suppose the claim is true for $(n-1) \times (n-1)$ matrices.
If $i \neq \ell$, then $a_{ik} = b_{ik}$ and the $(n-1) \times (n-1)$ matrix $B(i,k)$ is obtained from $A(i,k)$ by multiplying a row by $c$, so the inductive hypothesis implies that $C^B_{ik} = cC^A_{ik}$.
For $i = \ell$, then $b_{\ell k} = ca_{\ell k}$ and $B(\ell,k) = A(\ell,k)$, so $C^B_{\ell k} = C^A_{\ell k}$.
In either case, we find $b_{ik}C^B_{ik} = ca_{ik}C^A_{ik}$.
The claim follows.
\end{proof}

\begin{lemma}
Let $1 \leq \ell \leq n$ and let $A$, $B$ and $C$ be $n \times n$ matrices such that $a_{ij} = b_{ij} = c_{ij}$ for each $i \neq \ell$ and each $j$ and 
$
a_{\ell j} = b_{\ell j} + c_{\ell j}
$
for each $j$.
Then $\det(A) = \det(B) + \det(C)$.
\end{lemma}

\begin{proof}
We proceed by induction on $n$.
If $n = 1$, then  $\det(A) = a_{11} = b_{11} + c_{11} = \det(B) + \det(C)$.
Suppose the claim is true for $(n-1) \times (n-1)$ matrices.
We claim that 
\begin{equation}
\label{3}
a_{ik}C^A_{ik} = b_{ik}C^B_{ik} + c_{ik}C^C_{ik}
\end{equation}
for each $1 \leq i \leq k$.\footnote{If anybody actually bothers to read this, apologies for the terrible notation $C^C_{ik}$ for the $ik$th cofactor of the matrix $C$.}
If $i \neq \ell$, then $a_{ik} = b_{ik} = c_{ik}$ and the inductive hypothesis implies that $C^A_{ik} = C^B_{ik} + C^C_{ik}$, and \eqref{3} follows.
If $i = \ell$, then we have $a_{\ell k} = b_{\ell k} + c_{\ell k}$ and $A(\ell, k) = B(\ell, k) = C(\ell, k)$, so $C^A_{\ell k} = C^B_{\ell k} = C^C_{\ell k}$ and, again, \eqref{3} follows.
Adding up the equalities of \eqref{3} for each $1\leq i \leq n$ gives $\det(A) = \det(B) + \det(C)$.
\end{proof}

\begin{lemma}
We have $\det(I_n) = 1$.
\end{lemma}

\begin{proof}
We proceed by induction on $n$.
If $n = 1$, then $\det(I_1) = \det(1) = 1$.
Suppose $\det(I_{n-1}) = 1$.
Letting $\delta_{ij}$ denote the Kronecker delta symbol, we have
\[
\det(I_n)
=
\delta_{1k}C^{I_n}_{1k} + \delta_{2k}C^{I_n}_{2k} + \cdots + \delta_{nk}C^{I_n}_{nk}
=
C^{I_n}_{kk}
=
(-1)^{k+k}M^{I_n}_{kk}
\]
and $M^{I_n}_{kk}$ is the determinant of $I_n(k,k) = I_{n-1}$, which is $1$ by the inductive hypothesis.
\end{proof}

\begin{thm}
The assignment $A \mapsto \det(A)$ is an $n \times n$ determinant function.
\end{thm}

\begin{proof}
The four preceding lemmata establish each of the properties required of an $n \times n$ determinant function.
\end{proof}

\end{document}
