\documentclass[letterpaper,11pt]{article}
\usepackage{etoolbox, hyperref, microtype, mathtools}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle, largesmallcaps]{kpfonts}
\usepackage[T1]{fontenc}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0.25\baselineskip}

\newcommand{\im}{\operatorname{im}}

\begin{document}

%\printanswers

\LARGE
\textsc{Math} $225$ 
\hfill
\large
March $7$, $2016$ \\
\normalsize

\begin{center}
\Large
\textsc{Linear maps}
\normalsize
\end{center}

\textsc{Linear map:}
a function $f: V \to W$ between two vector spaces over $\mathbf{R}$ compatible with vector addition and scalar multiplication in the sense that $f(a(\mathbf{v}_1 + \mathbf{v}_2)) = af(\mathbf{v}_1) + af(\mathbf{v}_2)$ for each $a \in \mathbf{R}$, $\mathbf{v}_1,\mathbf{v}_2 \in V$.
The main example is the function $f: \mathbf{R}^n \to \mathbf{R}^m$ given by $f(\mathbf{x}) = A \mathbf{x}$, where $A$ is an $m \times n$ matrix.
In fact, \emph{every} linear map $f: \mathbf{R}^n \to \mathbf{R}^m$ is of this form for some such $A$:

\textsc{Matrix of a linear map:}
if $B = \langle \mathbf{v}_1, \ldots, \mathbf{v}_n\rangle$ (resp. $B' = \langle \mathbf{w}_1, \ldots, \mathbf{w}_m\rangle$) is an ordered basis for $V$ (resp. $W$) and let $f: V \to W$ be a linear map, then the \emph{matrix of $f$ relative to $B$ and $B'$} is the $m \times n$ matrix $[f]^{B'}_B = \begin{bmatrix}[f(\mathbf{v}_1)]_{B'} & \cdots & [f(\mathbf{v}_n)]_{B'} \end{bmatrix}$, and we have $[f]^{B'}_B[\mathbf{v}]_B = [f(\mathbf{v})]_{B'}$ for each $\mathbf{v} \in V$. 
This allows us to reduce any problem involving linear maps to a matrix problem by choosing ordered bases.

\textsc{Kernel} of a linear map $f: V \to W$:
the subspace $\ker(f) := \{\mathbf{v} \in V \mid f(\mathbf{v}) = \mathbf{0}\} \subseteq V$.
For $f: \mathbf{R}^n \to \mathbf{R}^m$, this is precisely the kernel of the matrix of $f$, i.e., the solution set of the corresponding homogeneous system of linear equations.

\textsc{Image (or range)} of a linear map $f: V \to W$:
the subspace $\im(f) := \{f(\mathbf{v}) \mid \mathbf{v} \in V\} \subseteq W$.

\textsc{Rank-Nullity Theorem:}
if $f: V \to W$ is a linear map and $\dim(V) < \infty$, then \
\[
\dim(V) = \dim(\ker(f)) + \dim(\im(f)).
\]

\textsc{Composite:}
the linear map $gf: U \to W$ obtained from the linear maps $f: U \to V$ and $g: V \to W$ by setting $(gf)(\mathbf{u}) = g(f(\mathbf{u}))$ is the \emph{composite of $f$ and $g$}.
If $B$ (resp. $B'$, resp. $B''$) is an ordered basis for $U$ (resp. $V$, resp. $W$), then $[gf]^{B''}_B = [g]^{B''}_{B'}[f]^{B'}_B$.

\textsc{One-to-one} linear map $f: V \to W$:
$f(\mathbf{v}_1) = f(\mathbf{v}_2)$ implies $\mathbf{v}_1 = \mathbf{v}_2$ or, equivalently, $\ker(f) = \{\mathbf{0}\}$.

\textsc{Onto} linear map $f: V \to W$:
for each $\mathbf{w} \in W$, there exists $\mathbf{v} \in V$ such that $\mathbf{w} = f(\mathbf{v})$, i.e., $\im(f) = W$.

\textsc{Inverse} of the linear map $f: V \to W$:
the unique linear map $f^{-1}: W \to V$ such that $f^{-1}(f(\mathbf{v})) = \mathbf{v}$ and $f(f^{-1}(\mathbf{w}))$ for each $\mathbf{v} \in V$ and $\mathbf{w} \in W$.
If $B$ (resp. $B'$) is an ordered basis for $V$ (resp. $W$), then $f$ is invertible if and only if the matrix $[f]^{B'}_B$ is invertible and, in that case, we have $[f^{-1}]^B_{B'} = ([f]^{B'}_B)^{-1}$.
 
\textsc{Characteristic polynomial} of $A \in M_{n \times n}(\mathbf{R})$:
the polynomial $p(\lambda) = \det(A - \lambda I_n)$ in the variable $\lambda$.

\textsc{Eigenvalue} of $A \in M_{n \times n}(\mathbf{R})$:
a scalar $\lambda \in \mathbf{R}$ such that, for some \emph{nonzero} $\mathbf{v} \in \mathbf{R}^n$, $A \mathbf{v} = \lambda \mathbf{v}$.
For example, if $\ker(A) \neq 0$, then $0$ is an eigenvalue of $A$: for each $\mathbf{0} \neq \mathbf{v} \in \ker(A)$, $A \mathbf{v} = \mathbf{0} = 0\mathbf{v}$.
If $A = \begin{bmatrix} 2 & -4 \\ -1 & -1 \end{bmatrix}$, then $-2$ is an eigenvalue of $A$, because $A \begin{bmatrix} 1 \\ 1 \end{bmatrix} = \begin{bmatrix} -2 \\ -2 \end{bmatrix}$.
The eigenvalues of $A$ are precisely the roots of the characteristic polynomial $\det(A - \lambda I_n)$, so finding them amounts to factoring this polynomial.

\textsc{Eigenvector} of $A \in M_{n \times n}(\mathbf{R})$:
a \emph{nonzero} vector $\mathbf{v} \in \mathbf{R}^n$ such that $A \mathbf{v} = \lambda \mathbf{v}$ for some scalar $\lambda \in \mathbf{R}$, which is necessarily an eigenvalue of $A$. 
To find the eigenvectors of $A$, find the roots of the characteristic polynomial, which give the eigenvalues $\lambda_1,\ldots,\lambda_k$ of $A$.
Then solve the homogeneous linear system $(A -\lambda_iI_n)\mathbf{v} = \mathbf{0}$ for each $1 \leq i \leq k$ to identify the eigenvectors.

\textsc{Eigenspace} of $A \in M_{n \times n}(\mathbf{R})$ corresponding to the eigenvalue $\lambda$:
the subspace $\{\mathbf{v} \in \mathbf{R}^n \mid A\mathbf{v} = \lambda \mathbf{v}\} \subseteq \mathbf{R}^n$.
\end{document}
