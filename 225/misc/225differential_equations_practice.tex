\documentclass[letterpaper, 11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, enumitem, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[margin=2cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[mathcal]{eucal}
\usepackage[frenchstyle, largesmallcaps]{kpfonts}
%\setlength{\parindent}{0pt}

\allsectionsfont{\centering\fontseries{m}\scshape}

\theoremstyle{plain}
\newtheorem{thm}{Theorem}
\newtheorem{lemma}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{cor}[thm]{Corollary}
\theoremstyle{definition}
\newtheorem{defn}[thm]{Definition}
\newtheorem{rmk}[thm]{Remark}
\newtheorem{problem}{Problem}
\newtheorem*{ex}{Example}

\renewcommand{\thequestion}{\textsc{Problem} \arabic{question}}
\renewcommand{\thepartno}{\alph{partno}}
\renewcommand{\solutiontitle}{\noindent\textsc{Solution:}\enspace}

\firstpagefooter{}{}{}
\runningfooter{}{}{}

\newcommand{\im}{\operatorname{im}}

\begin{document}

\printanswers

\noindent
\LARGE
\textsc{Math} $225$ 
\hfill
\large
April $25$, $2016$ \\
\normalsize

\begin{center}
\Large
\textsc{Review of differential equations and systems}
\normalsize
\end{center}

\noindent
On the final exam, the following table will be provided to you exactly as it appears here.

\begin{center}
\begin{tabular}[c]{|c|c|c|} 
\hline 
\shortstack{$cx^k\mathrm{e}^{ax}$ \\ \ } & \shortstack{$\mathrm{e}^{ax}(A_0 + \cdots + A_kx^k)$ \\ if $P(a) \neq 0$} & \shortstack{ $x^m\mathrm{e}^{ax}(A_0 + \cdots + A_kx^k)$ \\ if $P(a) = 0$ with multiplicity $m$} 
\\ 
\hline 
\shortstack{$c\mathrm{e}^{ax}\cos(bx)$ \\ or \\ $c\mathrm{e}^{ax}\sin(bx)$} & \shortstack{$\mathrm{e}^{ax}(A_0 \cos(bx) + B_0 \sin(bx))$ \\ if $P(a+bi) \neq 0$} & \shortstack{ $x^m\mathrm{e}^{ax}(A_0\cos(bx) + B_0\sin(bx))$ \\ if $P(a+bi) = 0$ with multiplicity $m$ } 
\\ 
\hline 
%\hline 
%& 
%$\mathrm{e}^{at} (\cos(bt)\operatorname{Re}(\mathbf{v}) - \sin(bt)\operatorname{Im}(\mathbf{v}))$ & $\mathrm{e}^{at} (\sin(bt)\operatorname{Re}(\mathbf{v}) + \cos(bt)\operatorname{Im}(\mathbf{v}))$ 
%\\ 
%\hline 
\end{tabular}
\end{center}

\vspace{\baselineskip}

\begin{questions}
\question
Find the general real-valued solution to the following linear differential equation:
\begin{equation}
\label{1}
y''' -9y''+31y' -39y = 39x
\end{equation}

\begin{solution}
\\
\textsc{Step $1$}:
Solve the complementary equation
\[
y''' -9y''+31y' -39y = 0.
\]
We must factor the auxiliary polynomial $P(r) = r^3-9r^2+31r-39$.
We hope this cubic admits an integer root, which must divide the constant term $39$.
The only integer divisors of $39$ are $\pm1, \pm3, \pm13, \pm39$.
A quick check shows that $P(\pm1) \neq 0$, but 
\[
P(3) 
= 
27 - 9\cdot 3^2 + 31\cdot 3 - 39
=
27-81 + 93-39 
=0.
\]
We deduce that $r-3$ divides $P(r)$.
By polynomial division, we find $P(r) = (r-3)(r^2-6r+13)$.
By the quadratic formula, the other roots are $3\pm2i$.
Each root of $P(r)$ has multiplicity $1$, so the general complex-valued solution of the complementary equation is
\[
z_{c} 
=
c_1\mathrm{e}^{3x} + c_2\mathrm{e}^{(3+2i)x} + c_3\mathrm{e}^{(3-2i)x}.
\]
Taking the real and imaginary parts of $\mathrm{e}^{(3+2i)x}$, we get two indpenendent real-valued solutions: $\mathrm{e}^{3x}\cos(2x)$ and $\mathrm{e}^{3x}\sin(2x)$, and the general real-valued solution is
\[
\boxed{
y_c
=
c_1\mathrm{e}^{3x} + c_2\mathrm{e}^{3x}\cos(2x) + c_3\mathrm{e}^{3x}\sin(2x)}
\]

\noindent
\textsc{Step $2$}:
Find a particular solution to \eqref{1}.
The above table of Ans\"atze tells us that we should guess a solution of the form $y_{p} = A_0+A_1x$, because $x = x^1\mathrm{e}^{0x}$ and $P(0) \neq 0$.
Now we compute the derivatives of $y_{p}$: $y_{p}'= A_1$, $y_{p}'' = 0$, $y_{p}''' = 0$. 
Plugging this into \eqref{1},
\[
0 - 0 + 31A_1 - 39(A_0 + A_1x) = 39x
\quad
\Longrightarrow
\quad
A_1 = -1, \, A_0 = -\frac{31}{39}
\quad
\Longrightarrow
\quad
\boxed{
y_{p}
=
-\frac{31}{39} -x}
\]

\noindent{Step $3$}:
Combine everything:
\[
\boxed{
y
=
y_c + y_p
=
c_1\mathrm{e}^{3x} + c_2\mathrm{e}^{3x}\cos(2x) + c_3\mathrm{e}^{3x}\sin(2x)
-\frac{31}{39} -x
}
\]
\end{solution}

\question
Find the general real-valued solution to the following linear differential equation:
\begin{equation}
\label{2}
y''' -9y''+31y' -39y = \mathrm{e}^{3x}
\end{equation}

\begin{solution}
\\
\noindent
\textsc{Step $1$}:
We already solved the complementary equation.

\noindent
\textsc{Step $2$}:
Find a particular solution to \eqref{2}.
Looking at the above table, we notice that $P(3) = 0$ with multiplicity $1$, so we guess $y_{p} = A_0x\mathrm{e}^{3x}$.
Now we compute its derivatives:
\begin{align*}
y_{p}'
&=
A_0\mathrm{e}^{3x}(1+3x) 
\\
y_{p}''
&=
A_0\mathrm{e}^{3x}(3(1+3x) + 3)
=
A_0\mathrm{e}^{3x}(6+9x)
\\
y_{p}'''
&=
A_0\mathrm{e}^{3x}(3(6+9x) + 9) 
=
A_0\mathrm{e}^{3x}(27 + 27x).
\end{align*}
Plugging this into \eqref{2},
\begin{align*}
\mathrm{e}^{3x}
&=
y_{p}''' - 9y_{p}'' +31y_{p}' - 39y_{p}
=
A_0\mathrm{e}^{3x}((27+27x) -9(6+9x)+31(1+3x)-39x)
\end{align*}
\[
A_0
=
\frac{1}{4}
\quad
\Longrightarrow
\quad
\boxed{y_{p}
=
\frac{x}{4}\mathrm{e}^{3x}}
\]

\noindent
\textsc{Step $3$}:
Combine everything:
\[
\boxed{
c_1\mathrm{e}^{3x} + c_2\mathrm{e}^{3x}\cos(2x) + c_3\mathrm{e}^{3x}\sin(2x)
+\frac{x}{4}\mathrm{e}^{3x}
}
\]
\end{solution}

\question
Find the general real-valued solution to the following linear differential equation:
\begin{equation}
\label{3}
y''' -9y''+31y' -39y = 39x +\mathrm{e}^{3x}
\end{equation}

\begin{solution}
\noindent
\textsc{Step $1$}:
We already solved the complementary equation.

\noindent
\textsc{Step $2$}:
Find a particular solution to \eqref{3}.
Recall that if $y_{p_1}$ is a particular solution to $Ly = F_1$ and $y_{p_2}$ is a particular solution to $Ly = F_2$, then, by superposition, $y_p = y_{p_1} + y_{p_2}$ is a particular solution to$Ly = F_1+F_2$.
Therefore, the sum of the particular solutions from the previous two problems is a particular solution to \eqref{3}.

\noindent
\textsc{Step $3$}:
Combine everything:
\[
\boxed{
y 
= 
y_c + y_{p_1} + y_{p_2}
=
c_1\mathrm{e}^{3x} + c_2\mathrm{e}^{3x}\cos(2x) + c_3\mathrm{e}^{3x}\sin(2x)
-\frac{31}{39} -x
+\frac{x}{4}\mathrm{e}^{3x}
}
\]
\end{solution}

\question
Consider the first-order homogeneous linear differential system $\mathbf{x}' = A\mathbf{x}$, where
\[
A
=
\begin{bmatrix}
3 & -5 \\
2 & 3
\end{bmatrix}.
\]
\begin{parts}
\part
Find the general complex-valued solution.

\begin{solution}
The characteristic polynomial of $A$ is $p(t) = t^2-6t+19$.
By the quadratic formula, the eigenvalues are $\lambda = 3 \pm i\sqrt{10}$.
We find that $(i\sqrt{5/2},1)$ is an eigenvector with eigenvalue $3 + i \sqrt{10}$.
Its conjugate $(-i\sqrt{5/2},1)$ must be an eigenvector for the conjugate eigenvalue $3-i\sqrt{10}$.
The general complex-valued solution is therefore
\[
\boxed{
\mathbf{z}(t)
=
c_1\mathrm{e}^{(3+i\sqrt{10})t}\begin{bmatrix} i\sqrt{5/2} \\ 1\end{bmatrix}
+
c_2\mathrm{e}^{(3-i\sqrt{10})t}\begin{bmatrix} -i\sqrt{5/2} \\ 1\end{bmatrix}
}
\]
\end{solution}

\part
Find the general real-valued solution.

\begin{solution}
We know that the real and imaginary parts of any complex valued solution are linearly independent real-valued solutions.
In particular,
\begin{align*}
\mathbf{x}_1(t)
&=
\operatorname{Re}\left(\mathrm{e}^{(3+i\sqrt{10})t}\begin{bmatrix} i\sqrt{5/2} \\ 1\end{bmatrix}\right)
\\
&=
\operatorname{Re}\left(\mathrm{e}^{3t}(\cos(\sqrt{10}t) + i \sin(\sqrt{10}t))\begin{bmatrix} i\sqrt{5/2} \\ 1\end{bmatrix}\right)
\\
&=
\operatorname{Re}\left(
\mathrm{e}^{3t}\begin{bmatrix}
i\sqrt{5/2}\cos(\sqrt{10}t) - \sqrt{5/2}\sin(\sqrt{10}t) \\
\cos(\sqrt{10}t) + i \sin(\sqrt{10}t)
\end{bmatrix}\right)
\\
&=
\boxed{\mathrm{e}^{3t}
\begin{bmatrix}
-\sqrt{5/2}\sin(\sqrt{10}t) \\
\cos(\sqrt{10}t)
\end{bmatrix}}
\\
\mathbf{x}_2(t)
&=
\operatorname{Im}\left(\mathrm{e}^{(3+i\sqrt{10})t}\begin{bmatrix} i\sqrt{5/2} \\ 1\end{bmatrix}\right)
=
\boxed{\mathrm{e}^{3t}
\begin{bmatrix}
\sqrt{5/2}\cos(\sqrt{10}t) \\
\sin(\sqrt{10}t)
\end{bmatrix}}
\end{align*}
are independent real-valued solutions.
The general real-valued solution is thus 
\[
\boxed{
\mathbf{x}(t)
=
c_1\mathbf{x}_1(t) + c_2\mathbf{x}_2(t)
}
\]
\end{solution}

\part
Solve the initial value problem $\mathbf{x}' = A\mathbf{x}$, $\mathbf{x}(0) = (1,-2)$.

\begin{solution}
In view of the general solution we found in the previous part,
\begin{align*}
\begin{bmatrix}
1 \\
-2
\end{bmatrix}
&=
\mathbf{x}(0)
=
c_1
\mathrm{e}^{3\cdot0}
\begin{bmatrix}
-\sqrt{5/2}\sin(\sqrt{10}\cdot0) \\
\cos(\sqrt{10}\cdot0)
\end{bmatrix}
+
c_2
\mathrm{e}^{3\cdot0}
\begin{bmatrix}
\sqrt{5/2}\cos(\sqrt{10}\cdot0) \\
\sin(\sqrt{10}\cdot0)
\end{bmatrix}
=
\begin{bmatrix}
c_2\sqrt{5/2} \\
c_1
\end{bmatrix},
\end{align*}
so $c_1 = -2$, $c_2 =\sqrt{2/5}$ and the solution is
\[
\boxed{
\mathbf{x}(t)
=
-2
\mathrm{e}^{3t}
\begin{bmatrix}
-\sqrt{5/2}\sin(\sqrt{10}t) \\
\cos(\sqrt{10}t)
\end{bmatrix}
+
\sqrt{2/5}
\mathrm{e}^{3t}
\begin{bmatrix}
\sqrt{5/2}\cos(\sqrt{10}t) \\
\sin(\sqrt{10}t)
\end{bmatrix}
}
\]
\end{solution}
\end{parts}

\end{questions}

\end{document}
