\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle, largesmallcaps, light]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\allsectionsfont{\fontseries{m}\scshape}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0.25\baselineskip}

\theoremstyle{remark}
\newcounter{thm}
%\numberwithin{thm}{section}
\newtheorem{problem}[thm]{Problem}
\newtheorem{definition}[thm]{\textsc{Definition}}
\newtheorem{example}[thm]{\textsc{Example}}
\newtheorem{exercise}[thm]{\textsc{Exercise}}
%\renewcommand{\thesubsection}{\arabic{section}.\Alph{subsection}}

\theoremstyle{plain}
\newtheorem{fact}[thm]{\fontseries{m}\textsc{Fact}}

%\runningheader{}{}{\oddeven{Name: \underline{\hspace{3in}}}{}}
\renewcommand{\thequestion}{\textsc{Problem  \arabic{question}}}
\renewcommand{\thepartno}{\arabic{partno}}
\renewcommand{\solutiontitle}{\noindent\textsc{Solution:}\enspace}

\newcommand{\dd}[1]{\ensuremath{\mathrm{d}#1}}
\newcommand{\ddt}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}t}}}
\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}
\newcommand{\ddu}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}u}}}
\newcommand{\DDh}[1]{\ensuremath{\frac{\partial#1}{\partial h}}}
\newcommand{\DDr}[1]{\ensuremath{\frac{\partial#1}{\partial r}}}
\newcommand{\DDx}[1]{\ensuremath{\frac{\partial#1}{\partial x}}}
\newcommand{\DDy}[1]{\ensuremath{\frac{\partial#1}{\partial y}}}
\newcommand{\DDxx}[1]{\ensuremath{\frac{\partial^2#1}{\partial x^2}}}
\newcommand{\DDyy}[1]{\ensuremath{\frac{\partial^2#1}{\partial y^2}}}
\newcommand{\DDxy}[1]{\ensuremath{\frac{\partial^2#1}{\partial x\partial y}}}
\newcommand{\DDyx}[1]{\ensuremath{\frac{\partial^2#1}{\partial y\partial x}}}
\newcommand{\dddx}[1]{\ensuremath{\frac{\mathrm{d}^2#1}{\mathrm{d}x^2}}}
\newcommand{\defn}[1]{\textbf{\emph{#1}}}

\begin{document}

%\printanswers

\LARGE
\textsc{Math} $225$ 
\hfill
\large
February $13$, $2016$ \\
\normalsize

\begin{center}
\Large
\textsc{Basic definitions pertaining to vector spaces}
\normalsize
\end{center}

\textsc{Vector space:}
a set (of \emph{vectors}) $V$ equipped with rules for vector addition and scalar multiplication satisfying a list of axioms forcing them to behave like matrix addition and scalar multiplication, e.g., $\mathbf{R}^n$.

\textsc{Subspace} of the vector space $V$:
a \emph{nonempty} subset $W \subseteq V$ stable under vector addition and scalar multiplication inside the vector space $V$, e.g., the kernel $\ker(A)$ of an $m \times n$ matrix, i.e., the subset of $\mathbf{R}^n$ consisting of the solutions of the linear system of equations $A\mathbf{x} = \mathbf{0}$.

\begin{example}
The set $\mathcal{F}(\mathbf{R},\mathbf{R})$ of all functions $f: \mathbf{R} \to \mathbf{R}$ is an $\mathbf{R}$-vector space and the subset $\mathbf{R}[x] \subseteq \mathcal{F}(\mathbf{R}, \mathbf{R})$ consisting of all the polynomial functions, i.e., all the functions of the form $f(x) = a_0 + a_1x + \cdots + a_nx^n$ for some $n \in \mathbf{Z}_{\geq0}$ and $a_0, \ldots, a_n \in \mathbf{R}$, is a subspace.
To prove this, we must check three things:
\begin{enumerate}[topsep=0ex, itemsep=0ex]
\item
\emph{$\mathbf{R}[x]$ is nonempty}: $f(x) = 0$ is a function $\mathbf{R} \to \mathbf{R}$, so it belongs to $\mathcal{F}(\mathbf{R}, \mathbf{R})$, and it's polynomial, so it belongs to $\mathbf{R}[x]$ (we could just as well have remarked that $f(x) = x$ belongs to $\mathbf{R}[x]$: the point is simply to show that $\mathbf{R}[x]$ contains \emph{at least} one element);
\item
\emph{$\mathbf{R}[x]$ is closed under vector addition}: if $f(x) = a_0 + a_1x + \cdots +a_mx^m$ and $g(x) = b_0+ b_1x + \cdots + b_nx^n$ are two polynomial functions with $m \leq n$, then their sum in $\mathcal{F}(\mathbf{R}, \mathbf{R})$ is polynomial, because
\[
(f+g)(x)
=
(a_0 +b_0) + (a_1 + b_1)x + \cdots + (a_m+b+_m)x^m + b_{m+1}x^{m+1} \cdots + b_nx^n,
\]
so $f+g \in \mathbf{R}[x]$;
\item
\emph{$\mathbf{R}[x]$ is closed under scalar multiplication}: if $c \in \mathbf{R}$ is any scalar and $f(x) = a_0 + a_1x+ \cdots + a_nx^n$ is any element of $\mathbf{R}[x]$, then $(c\cdot f)(x) = ca_0 + ca_1x + \cdots + ca_nx^n$ is polynomial, so $c\cdot f \in \mathbf{R}[x]$.
\end{enumerate}
We could also have checked the last two properties in one fell swoop by showing that, for each $c \in \mathbf{R}$ and each $f$ and $g$ in $\mathbf{R}[x]$, the vector $c\cdot(f+g)$ of $\mathcal{F}(\mathbf{R}, \mathbf{R})$ actually belongs to $\mathbf{R}[x]$.
\end{example}

\textsc{Spanning set} of $\{\mathbf{v}_1, \ldots, \mathbf{v}_k\} \subseteq V$:
the smallest subspace of $V$ containing the vectors $\mathbf{v}_1$, \ldots, $\mathbf{v}_k$ or, equivalently, the subset of $V$ consisting of all linear combinations of $\mathbf{v}_1$, \ldots, $\mathbf{v}_k$.

\begin{example}
The span of $\{(3,0,-1),(0,-2,1)\} \subseteq \mathbf{R}^3$ is the subspace
\[
\{
a(3,0,-1) + b(0,-2,1) \mid a, b \in \mathbf{R}
\}
=
\{
(3a,-2b,-a+b) \mid a,b \in \mathbf{R}
\}.
\]
This is the plane through the origin, $(3,0,-1)$ and $(0,-2,1)$.
\end{example}

\textsc{Linear dependence:}
a set of vectors $\{\mathbf{v}_1,\ldots,\mathbf{v}_k\} \subseteq V$ is linearly dependent if there exist scalars $c_1,\ldots,c_k \in \mathbf{F}$ \emph{not all zero} such that $c_1\mathbf{v}_1 + \cdots + c_k\mathbf{v}_k = \mathbf{0}$, and it's linearly independent if there don't.

\begin{example}
\begin{enumerate}[topsep=0ex, itemsep=0ex]
\item
Any set containing $\mathbf{0}$ is linearly dependent.
\item
The set $\{\mathbf{v}_1, \ldots, \mathbf{v}_k\}$ is linearly dependent if and only if, for some $1 \leq i \leq k$, the vector $\mathbf{v}_i$ is a linear combination of $\mathbf{v}_1,\ldots,\mathbf{v}_{i-1},\mathbf{v}_{i+1},\ldots,\mathbf{v}_k$.
\item
The set $\{\mathbf{v}, \mathbf{w}\}$ is linearly dependent if and only if $\mathbf{v}$ and $\mathbf{w}$ lie on the same line through the origin.
\item
The set $\{\mathbf{u}, \mathbf{v}, \mathbf{w}\}$ is linearly dependent if and only if $\mathbf{u}$, $\mathbf{v}$ and $\mathbf{w}$ lie on the same plane through the origin.
\item
The set $\{\mathbf{v}_1, \ldots, \mathbf{v}_k\} \subseteq \mathbf{R}^n$ is linearly dependent if and only if the homogeneous linear system $[\mathbf{v}_1 \cdots \mathbf{v}_k]\mathbf{x} = \mathbf{0}$ has a nontrivial solution, so we can always test for linear dependence in $\mathbf{R}^n$ by solving systems of linear equations, e.g., by Gaussian elimination.
\item
By the previous example, we can deduce that, if $n < k$, then $\{\mathbf{v}_1, \ldots, \mathbf{v}_k\} \subseteq \mathbf{R}^n$ is linearly dependent, because a homogeneous linear system of $n$ equations in $k$ variables always has infinitely many solutions when $n < k$. 
\end{enumerate}
\end{example}

\end{document}

\textsc{Basis} of a vector space:
a linearly independent set $\{\mathbf{v}_1, \ldots, \mathbf{v}_k\} \subseteq V$ that spans $V$.

\textsc{Dimension} of a vector space:
the number of vectors contained in a basis for $V$.

\textsc{Fact:}
If $\dim(V) = n$ and $S = \{\mathbf{v}_1, \ldots, \mathbf{v}_n\}$ is a set of $n$ vectors in $V$, then the following are equivalent:
\begin{enumerate}[topsep=0ex]
\item
$S$ is a basis for $V$;
\item
$S$ spans $V$;
\item
$S$ is linearly independent.
\end{enumerate}

\clearpage

\textsc{Method for finding a basis:}
\begin{enumerate}[topsep=0ex]
\item
Choose any nonzero vector $\mathbf{v}_1 \in V$.
\item
If $\{\mathbf{v}_1\}$ spans $V$, then it's basis already.
If not, choose any vector $\mathbf{v}_2 \in V$ not contained in the span of $\mathbf{v}_1$.
\item
If $\{\mathbf{v}_1, \mathbf{v}_2\}$ spans $V$, then it is a basis.
If not, choose any vector $\mathbf{v}_3 \in V$ not contained in the span of $\mathbf{v}_1$ and $\mathbf{v}_2$.
Repeat as needed.
\end{enumerate}

\textsc{Fact:}
If $\{\mathbf{v}_1, \ldots, \mathbf{v}_n\}$ is a basis for $V$, then each $\mathbf{v} \in V$ can be written as a \emph{unique} linear combination of $\mathbf{v}_1$, \ldots, $\mathbf{v}_n$.

\textsc{Coordinate vector} with respect to an ordered basis:
if $B = \{\mathbf{v}_1, \ldots, \mathbf{v}_n\}$ is an \emph{ordered} basis for $V$ and $\mathbf{v} \in V$, then, by the previous fact we can write $\mathbf{v}$ uniquely in the form
\[
\mathbf{v} 
=
c_1\mathbf{v}_1 + \cdots + c_n \mathbf{v}_n
\]
for some $c_1, \ldots, c_n \in \mathbf{F}$, and the \emph{coordinate vector} $[\mathbf{v}]_B$ of $\mathbf{V}$ relative to $B$ is the column vector $[\mathbf{v}]_B = (c_1, \ldots, c_n)$.

\begin{example}
Consider the ordered basis $B = \{1+x, 2+3x, 5+x+x^2\}$ of the vector space of polynomials of degree $\leq 2$ and the vector $5+7x-3x^2$.
To determine the coordinate vector $[5+7x-3x^2]_B$, we first write $5+7x-3x^2$ as a linear combination of our basis vectors:
\begin{align*}
5+7x-3x^2
&=
a(1+x) + b(2+3x) + c(5+x+x^2)
\\
5+7x - 3x^2
&=
(a+2b+5c) + (a + 3b + c)x + cx^2
\end{align*}
which means $5 = a + 2b + 5c$, $7 = a+3b+c$ and $-3 = c$.
Sovling this linear system gives $a = 40$ $b = -10$, $c = -3$, i.e.
\[
5+7x - 3x^2
=
40(1+x) -10(2+3x) -3(5+x+x^2).
\]
This gives $[5+7x-3x^2]_B = (40,-10,-3)$.
\end{example}

\textsc{Change of basis matrix} from $B$ to $B'$:
if $B = \{\mathbf{v}_1, \ldots, \mathbf{v}_n\}$ and $B' = \{\mathbf{w}_1, \ldots, \mathbf{w}_n\}$ are ordered bases for $V$, then the change of basis matrix from $B$ to $B'$ is the $n \times n$ matrix 
$
P_{B' \leftarrow B}
=
\Big[ [\mathbf{v}_1]_{B'} \cdots [\mathbf{v}_n]_{B'} \Big].
$

\textsc{Facts:}
\begin{enumerate}[topsep=0ex]
\item
For each $\mathbf{v},\mathbf{w} \in V$, $a \in \mathbf{F}$, 
$[a(\mathbf{v} + \mathbf{w})]_B = a[\mathbf{v}]_B + a[\mathbf{w}]_B$.
\item For each $\mathbf{v} \in V$, $[\mathbf{v}]_{B'} = P_{B' \leftarrow B}[\mathbf{v}]_B$.
\item
The matrix $P_{B' \leftarrow B}$ is invertible and in fact $P_{B \leftarrow B'}$ is its inverse.
\item
If $B''$ is a third ordered basis for $V$, then
$
P_{B'' \leftarrow B} 
=
P_{B'' \leftarrow B}P_{B' \leftarrow B}.
$
\end{enumerate}


