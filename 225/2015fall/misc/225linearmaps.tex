\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle, largesmallcaps, light]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\allsectionsfont{\fontseries{m}\scshape}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0.25\baselineskip}

\theoremstyle{remark}
\newcounter{thm}
%\numberwithin{thm}{section}
\newtheorem{problem}[thm]{Problem}
\newtheorem{definition}[thm]{\textsc{Definition}}
\newtheorem{example}[thm]{\textsc{Example}}
\newtheorem{exercise}[thm]{\textsc{Exercise}}
%\renewcommand{\thesubsection}{\arabic{section}.\Alph{subsection}}

\theoremstyle{plain}
\newtheorem{fact}[thm]{\fontseries{m}\textsc{Fact}}

%\runningheader{}{}{\oddeven{Name: \underline{\hspace{3in}}}{}}
\renewcommand{\thequestion}{\textsc{Problem  \arabic{question}}}
\renewcommand{\thepartno}{\arabic{partno}}
\renewcommand{\solutiontitle}{\noindent\textsc{Solution:}\enspace}

\newcommand{\dd}[1]{\ensuremath{\mathrm{d}#1}}
\newcommand{\ddt}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}t}}}
\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}
\newcommand{\ddu}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}u}}}
\newcommand{\DDh}[1]{\ensuremath{\frac{\partial#1}{\partial h}}}
\newcommand{\DDr}[1]{\ensuremath{\frac{\partial#1}{\partial r}}}
\newcommand{\DDx}[1]{\ensuremath{\frac{\partial#1}{\partial x}}}
\newcommand{\DDy}[1]{\ensuremath{\frac{\partial#1}{\partial y}}}
\newcommand{\DDxx}[1]{\ensuremath{\frac{\partial^2#1}{\partial x^2}}}
\newcommand{\DDyy}[1]{\ensuremath{\frac{\partial^2#1}{\partial y^2}}}
\newcommand{\DDxy}[1]{\ensuremath{\frac{\partial^2#1}{\partial x\partial y}}}
\newcommand{\DDyx}[1]{\ensuremath{\frac{\partial^2#1}{\partial y\partial x}}}
\newcommand{\dddx}[1]{\ensuremath{\frac{\mathrm{d}^2#1}{\mathrm{d}x^2}}}
\newcommand{\defn}[1]{\textbf{\emph{#1}}}
\newcommand{\im}{\operatorname{im}}

\begin{document}

%\printanswers

\LARGE
\textsc{Math} $225$ 
\hfill
\large
October $25$, $2015$ \\
\normalsize

\begin{center}
\Large
\textsc{Linear maps}
\normalsize
\end{center}

\textsc{Linear map:}
a function $f: V \to W$ between two $\mathbf{F}$-vector spaces compatible with vector addition and scalar multiplication in the sense that $f(a(\mathbf{v}_1 + \mathbf{v}_2)) = af(\mathbf{v}_1) + af(\mathbf{v}_2)$ for each $a \in \mathbf{F}$, $\mathbf{v}_1,\mathbf{v}_2 \in V$.
The main example is the function $f: \mathbf{R}^n \to \mathbf{R}^m$ given by $f(\mathbf{x}) = A \mathbf{x}$, where $A$ is an $m \times n$ matrix.
In fact, \emph{every} linear map $f: \mathbf{R}^n \to \mathbf{R}^m$ is of this form for some such $A$:

\textsc{Matrix of a linear map:}
if $f: \mathbf{R}^n \to \mathbf{R}^m$ linear, then its matrix is the $m \times n$ matrix $A = \begin{bmatrix}f(\mathbf{e}_1 & \cdots & f(\mathbf{e}_n) \end{bmatrix}$, where $\{\mathbf{e}_1, \ldots, \mathbf{e}_n\}$ is the standard basis of $\mathbf{R}^n$, and we have $f(\mathbf{x}) = A \mathbf{x}$ for each $\mathbf{x} \in \mathbf{R}^n$.

\textsc{Kernel} of a linear map $f: V \to W$:
the subspace $\ker(f) := \{\mathbf{v} \in V \mid f(\mathbf{v}) = \mathbf{0}\} \subseteq V$.
For $f: \mathbf{R}^n \to \mathbf{R}^m$, this is precisely the kernel of the matrix of $f$, i.e., the solution set of the corresponding homogeneous system of linear equations.

\textsc{Image (or range)} of a linear map $f: V \to W$:
the subspace $\im(f) := \{f(\mathbf{v}) \mid \mathbf{v} \in V\} \subseteq W$.

\textsc{Rank-Nullity Theorem:}
if $f: V \to W$ is a linear map and $\dim(V) < \infty$, then \
\[
\dim(V) = \dim(\ker(f)) + \dim(\im(f)).
\]

\textsc{Composite:}
the linear map $gf: U \to W$ obtained from the linear maps $f: U \to V$ and $g: V \to W$ by setting $(gf)(\mathbf{u}) = g(f(\mathbf{u}))$.
For $f: \mathbf{R}^n \to \mathbf{R}^m$ and $g: \mathbf{R}^m \to \mathbf{R}^k$ given by matrices $A$ and $B$, respectively, the matrix of $gf$ is the product $BA$.

\textsc{One-to-one} linear map $f: V \to W$:
$f(\mathbf{v}_1) = f(\mathbf{v}_2)$ implies $\mathbf{v}_1 = \mathbf{v}_2$ or, equivalently, $\ker(f) = \{\mathbf{0}\}$.

\textsc{Onto} linear map $f: V \to W$:
for each $\mathbf{w} \in W$, there exists $\mathbf{v} \in V$ such that $\mathbf{w} = f(\mathbf{v})$, i.e., $\im(f) = W$.

\textsc{Inverse} of the linear map $f: V \to W$:
the unique linear map $f^{-1}: W \to V$ such that $f^{-1}(f(\mathbf{v})) = \mathbf{v}$ and $f(f^{-1}(\mathbf{w}))$ for each $\mathbf{v} \in V$ and $\mathbf{w} \in W$.
If $f: \mathbf{R}^n \to \mathbf{R}^m$ is given by the matrix $A$, then $f^{-1}$ is given by $A^{-1}$.

\textsc{Characteristic polynomial} of $A \in M_{n \times n}(\mathbf{F})$:
the polynomial $p(\lambda) = \det(A - \lambda I_n)$ in the variable $\lambda$.

\textsc{Eigenvalue} of $A \in M_{n \times n}(\mathbf{F})$:
a scalar $\lambda \in \mathbf{F}$ such that, for some \emph{nonzero} $\mathbf{v} \in \mathbf{F}^n$, $A \mathbf{v} = \lambda \mathbf{v}$.
For example, if $\ker(A) \neq 0$, then $0$ is an eigenvalue of $A$: for each $\mathbf{0} \neq \mathbf{v} \in \ker(A)$, $A \mathbf{v} = \mathbf{0} = 0\mathbf{v}$.
If $A = \begin{bmatrix} 2 & -4 \\ -1 & -1 \end{bmatrix}$, then $-2$ is an eigenvalue of $A$, because $A \begin{bmatrix} 1 \\ 1 \end{bmatrix} = \begin{bmatrix} -2 \\ -2 \end{bmatrix}$.
The eigenvalues of $A$ are precisely the roots of the characteristic polynomial $\det(A - \lambda I_n)$, so finding them amounts to factoring this polynomial.

\textsc{Eigenvector} of $A \in M_{n \times n}(\mathbf{F})$:
a \emph{nonzero} vector $\mathbf{v} \in \mathbf{F}^n$ such that $A \mathbf{v} = \lambda \mathbf{v}$ for some scalar $\lambda \in \mathbf{F}$, which is necessarily an eigenvalue of $A$. 
To find the eigenvectors of $A$, find the roots of the characteristic polynomial, which give the eigenvalues $\lambda_1,\ldots,\lambda_k$ of $A$.
Then solve the homogeneous linear system $(A -\lambda_iI_n)\mathbf{v} = \mathbf{0}$ for each $1 \leq i \leq k$ to identify the eigenvectors.

\textsc{Eigenspace} of $A \in M_{n \times n}(\mathbf{F})$ corresponding to the eigenvalue $\lambda$:
the subspace $\{\mathbf{v} \in \mathbf{F}^n \mid A\mathbf{v} = \lambda \mathbf{v}\} \subseteq \mathbf{F}^n$.

\end{document}
