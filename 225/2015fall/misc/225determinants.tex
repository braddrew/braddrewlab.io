\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle, largesmallcaps, light]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\allsectionsfont{\fontseries{m}\scshape}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0.25\baselineskip}

\theoremstyle{remark}
\newcounter{thm}
%\numberwithin{thm}{section}
\newtheorem{problem}[thm]{Problem}
\newtheorem{definition}[thm]{\textsc{Definition}}
\newtheorem{example}[thm]{\textsc{Example}}
\newtheorem{exercise}[thm]{\textsc{Exercise}}
%\renewcommand{\thesubsection}{\arabic{section}.\Alph{subsection}}

\theoremstyle{plain}
\newtheorem{fact}[thm]{\fontseries{m}\textsc{Fact}}

%\runningheader{}{}{\oddeven{Name: \underline{\hspace{3in}}}{}}
\renewcommand{\thequestion}{\textsc{Problem  \arabic{question}}}
\renewcommand{\thepartno}{\arabic{partno}}
\renewcommand{\solutiontitle}{\noindent\textsc{Solution:}\enspace}

\newcommand{\dd}[1]{\ensuremath{\mathrm{d}#1}}
\newcommand{\ddt}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}t}}}
\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}
\newcommand{\ddu}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}u}}}
\newcommand{\DDh}[1]{\ensuremath{\frac{\partial#1}{\partial h}}}
\newcommand{\DDr}[1]{\ensuremath{\frac{\partial#1}{\partial r}}}
\newcommand{\DDx}[1]{\ensuremath{\frac{\partial#1}{\partial x}}}
\newcommand{\DDy}[1]{\ensuremath{\frac{\partial#1}{\partial y}}}
\newcommand{\DDxx}[1]{\ensuremath{\frac{\partial^2#1}{\partial x^2}}}
\newcommand{\DDyy}[1]{\ensuremath{\frac{\partial^2#1}{\partial y^2}}}
\newcommand{\DDxy}[1]{\ensuremath{\frac{\partial^2#1}{\partial x\partial y}}}
\newcommand{\DDyx}[1]{\ensuremath{\frac{\partial^2#1}{\partial y\partial x}}}
\newcommand{\dddx}[1]{\ensuremath{\frac{\mathrm{d}^2#1}{\mathrm{d}x^2}}}
\newcommand{\defn}[1]{\textbf{\emph{#1}}}

\begin{document}

%\printanswers

\LARGE
\textsc{Math} $225$ 
\hfill
\large
September $18$, $2015$ \\
\normalsize

\begin{center}
\Large
\textsc{Properties of determinants}
\normalsize
\end{center}

\textsc{Linearity in each row:}
Let $1 \leq i \leq n$, let $\mathbf{a}_1$, \ldots, $\mathbf{a}_{i-1}$, $\mathbf{a}_{i+1}$, \ldots, $\mathbf{a}_n$, $\mathbf{x}$ and $\mathbf{y}$ be row vectors of length $n$ and let $a, b \in \mathbf{R}$.
Then
\[
\det \left(
\begin{bmatrix}
\mathbf{a}_1 \\
\vdots \\
\mathbf{a}_{i-1} \\
a\mathbf{x} + b\mathbf{y} \\
\mathbf{a}_{i+1} \\
\vdots \\
\mathbf{a}_n
\end{bmatrix}
\right)
=\
a \det \left(
\begin{bmatrix}
\mathbf{a}_1 \\
\vdots \\
\mathbf{a}_{i-1} \\
\mathbf{x}\\
\mathbf{a}_{i+1} \\
\vdots \\
\mathbf{a}_n
\end{bmatrix}
\right)
+
b \det \left(
\begin{bmatrix}
\mathbf{a}_1 \\
\vdots \\
\mathbf{a}_{i-1} \\
\mathbf{y} \\
\mathbf{a}_{i+1} \\
\vdots \\
\mathbf{a}_n
\end{bmatrix}
\right).
\]
In particular, letting $\mathbf{x} = \mathbf{0} = \mathbf{y}$ and $a = 1 = b$, this implies that the determinant of any matrix with a row of zeros is zero.

\begin{example}
\[
\det \left(
\begin{bmatrix}
1 & -7 & 2 & -1 \\
2 & 3 & -3 & 2 \\
4 & 22 & -21 & 14 \\
0 & -17 & 6 & 8
\end{bmatrix}
\right)
=
2 \det \left(
\begin{bmatrix}
1 & -7 & 2 & -1 \\
2 & 3 & -3 & 2 \\
2 & 2 & -3 & 1 \\
0 & -17 & 6 & 8
\end{bmatrix}
\right)
+
3 \det \left(
\begin{bmatrix}
1 & -7 & 2 & -1 \\
2 & 3 & -3 & 2 \\
0 & 6 & -5 & 4 \\
0 & -17 & 6 & 8
\end{bmatrix}
\right)
\]
\end{example}

\textsc{Linearity in each column:}
Let $1 \leq j \leq n$, let $\mathbf{a}_1$, \ldots, $\mathbf{a}_{j-1}$, $\mathbf{a}_{j+1}$, \ldots, $\mathbf{a}_n$, $\mathbf{x}$ and $\mathbf{y}$ be column vectors of length $n$ and let $a, b \in \mathbf{R}$.
Then
\begin{align*}
\det & \left(
\begin{bmatrix}
\mathbf{a}_1 &
\cdots & 
\mathbf{a}_{j-1} &
a\mathbf{x} + b\mathbf{y} &
\mathbf{a}_{j+1} &
\cdots &
\mathbf{a}_n
\end{bmatrix}
\right)
\\
=
& a \det  \left(
\begin{bmatrix}
\mathbf{a}_1 &
\cdots & 
\mathbf{a}_{j-1} &
\mathbf{x} &
\mathbf{a}_{j+1} &
\cdots &
\mathbf{a}_n
\end{bmatrix}
\right)
+
b \det \left(
\begin{bmatrix}
\mathbf{a}_1 &
\cdots & 
\mathbf{a}_{j-1} &
\mathbf{y} &
\mathbf{a}_{j+1} &
\cdots &
\mathbf{a}_n
\end{bmatrix}
\right).
\end{align*}
In particular, letting $\mathbf{x} = \mathbf{0} = \mathbf{y}$ and $a  = 1 = b$, this implies that the determinant of any matrix with a column of zeros is zero.

\begin{example}
Taking the transposes of the matrices of the previous example, we have an example of linearity in a column.
\end{example}

\textsc{Permuting two rows:}
Let $\mathbf{a}_1$, \ldots, $\mathbf{a}_n$ be row vector of length $n$ and let $1 \leq i < j \leq n$.
Then 
\[
\det \left(
\begin{bmatrix}
\mathbf{a}_1 \\
\vdots \\
\mathbf{a}_i \\
\vdots \\
\mathbf{a}_j \\
\vdots \\
\mathbf{a}_n
\end{bmatrix}
\right)
=
- \det \left(
\begin{bmatrix}
\mathbf{a}_1 \\
\vdots \\
\mathbf{a}_j \\
\vdots \\
\mathbf{a}_i \\
\vdots \\
\mathbf{a}_n
\end{bmatrix}
\right).
\]
In particular, the determinant of any matrix with two equal rows is zero.

\textsc{Permuting two columns:}
Let $\mathbf{a}_1$, \ldots, $\mathbf{a}_n$ be column vector of length $n$ and let $1 \leq i < j \leq n$.
Then 
\[
\det \left(
\begin{bmatrix}
\mathbf{a}_1 &
\cdots &
\mathbf{a}_i &
\cdots &
\mathbf{a}_j &
\cdots &
\mathbf{a}_n
\end{bmatrix}
\right)
=
- \det \left(
\begin{bmatrix}
\mathbf{a}_1 &
\cdots &
\mathbf{a}_j &
\cdots &
\mathbf{a}_i &
\cdots &
\mathbf{a}_n
\end{bmatrix}
\right).
\]
In particular, the determinant of any matrix with two equal columns is zero.

\textsc{Compatibility with elementary row operations:}
Suppose $A$ and $B$ are $n \times n$ matrices and $B$ is obtained from $A$ by:
\begin{enumerate}[topsep=0ex, itemsep=0ex, label=(\alph*)]
\item
switching the order of two rows (resp. switching the order of two columns), then $\det(B) = -\det(A)$;
\item
multiplying the $i$th row (resp. the $j$th column) of $A$ by $c \in \mathbf{R}$, then $\det(B) = c \det(A)$;
\item
adding $c$ times the $j$th row (resp. $j$th column) to the $i$th row (resp. $i$th column) for some $c \in \mathbf{R}$, then $\det(A) = \det(B)$.
\end{enumerate}
In particular, if $B = cA$ for some $c \in \mathbf{R}$, then $\det(B) = c^n\det(A)$ by (b).

\textsc{Invertibility:}
If $A$ is $n \times n$, then $A$ is invertible if and only if $\det(A) \neq 0$.

\textsc{Multiplicativity:}
If $A$ and $B$ are $n \times n$, then $\det(AB) = \det(A)\det(B)$.

\textsc{Triangular matrices:}
If $A$ is an upper or lower triangular $n \times n$ matrix, then $\det(A) = a_{11}a_{22} \cdots a_{nn}$.

\textsc{Transposes:}
If $A$ is any $n \times n$ matrix, then $\det(A) = \det(A^T)$.

\textsc{Inverses:}
If $A$ is an invertible $n \times n$ matrix, then $\det(A^{-1}) = \displaystyle \frac{1}{\det(A)}$.

\textsc{Cofactor expansion:}
Let $A$ be $n \times n$.
For any $1 \leq i \leq n$ (resp., $1 \leq j \leq n$), we have
\[
\det(A)
=
\sum_{k=1}^na_{ik}C_{ik} 
=
\sum_{k=1}^n(-1)^{i+k}a_{ik}M_{ik}
\quad
\left(
\text{resp.,}
\quad
\det(A) 
=
\sum_{k=1}a_{kj}C_{kj}
=
\sum_{k=1}(-1)^{k+j}a_{kj}M_{kj},
\right)
\]
where $C_{ij}$ denotes the $ij$th cofactor and $M_{ij}$ denotes the $ij$th minor of $A$.

\textsc{$2 \times 2$ determinants}:
\[
\det \left(
\begin{bmatrix}
a & b \\
c & d
\end{bmatrix}
\right)
=
ad-bc.
\]
\end{document}
