\documentclass[leqno, letterpaper, 11pt]{exam}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, systeme}
\usepackage[top=2.5cm, bottom=3cm, left=2.5cm, right=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle, largesmallcaps, light]{kpfonts}
\usepackage[normalem]{ulem}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newcounter{thm}
\newtheorem{problem}[thm]{Problem}

\renewcommand{\thequestion}{\textsc{Problem} \arabic{question}}
\renewcommand{\thepartno}{\alph{partno}}
\renewcommand{\solutiontitle}{\textsc{Solution:}\enspace}

\firstpagefooter{}{Page \thepage\ of \numpages}{}
\runningfooter{}{Page \thepage\ of \numpages}{}

\begin{document}

\printanswers

\LARGE
\textsc{Math} $225$ 
\Large
Homework $5$ 
\hfill
\large
Due February $18$, $2016$
\normalsize

\vspace{\baselineskip}

\emph{Show all your work, justify each response and write legibly and comprehensibly to receive full credit.} 

\vspace{.5\baselineskip}

\emph{Throughout this assignment, whenever you need to invoke the axioms for vector spaces to justify a claim, indicate precisely which axiom you are invoking in each step, either by name, e.g., ``by the existence of additive inverses'', ``by associativity of vector addition'', or by the numbering convention used in the lecture notes, e.g., ``by \textup{(V5)}'', ``by \textup{(V3)}''.}

\begin{questions}
\question
Let $V$ be a vector space over $\mathbf{R}$, let $\mathbf{v}, \mathbf{w} \in V$ and let $a \in \mathbf{R}$.

\begin{parts}
\part
Prove that if $a \neq 0$ and $a\cdot(\mathbf{v} + \mathbf{w}) = \mathbf{0}$, then $\mathbf{w} = -\mathbf{v}$.

\part
Prove that $(-\mathbf{v}) + (-\mathbf{w})$ is the additive inverse of $\mathbf{v} + \mathbf{w}$.
\end{parts}

\question
Let $V$ be a vector space over $\mathbf{R}$ and let $W \subseteq V$ be a subset.
Prove that $W$ is a subspace of $V$ if and only if $W$ is a vector space over $\mathbf{R}$ with respect to the operations of vector addition and scalar multiplication in $V$.
Check all the axioms!

\question
Which of the following are vector spaces over $\mathbf{R}$?
Prove each answer.
It may be helpful in some cases to use what you know about subspaces.

\begin{parts}
\part
the rational numbers $\mathbf{Q} = \big\{\frac{p}{q} \mid p,q\in \mathbf{Z}, q\neq0\big\}$ with the usual operations of addition and multiplication

\part
the set $\mathbf{R}[x] = \{a_nx^n + a_{n-1}x^{n-1} + \cdots + a_1x + a_0 \mid a_i \in \mathbf{R}\}$ of all polynomials in the variable $x$ with real coefficients with the usual operations of addition and scalar multiplication, i.e., for $m \leq n$,
\begin{align*}
(a_nx^n + \cdots + a_1x + a_0) & + (b_mx^m + \cdots b_1x + b_0) 
\\
&:=
a_nx^n + \cdots + a_{m+1}x^{m+1} + (a_m + b_m)x^m + \cdots + (a_1+ b_1)x + (a_0 + b_0)
\\
c \cdot(a_nx^n + \cdots + a_1x + a_0)
&:=
ca_nx^n + \cdots + ca_1x + ca_0
\end{align*}

\part
the set $D_n(\mathbf{R})$ of all $n \times n$ diagonal marices with matrix addition and scalar multiplication

\part
the set $T_n(\mathbf{R})$ of all upper triangular $n \times n$ matrices with matrix addition and scalar multiplication

\part
the set $\operatorname{GL}_n(\mathbf{R})$ of all $n \times n$ invertible matrices with matrix addition and scalar multiplication
\end{parts}

\question
In the following examples, determine whether the subset $W \subseteq V$ is a subspace and justify your response:
\begin{parts}
\part
$V = \mathcal{F}(\mathbf{R}, \mathbf{R})$ is the vector space of all functions from $\mathbf{R}$ to $\mathbf{R}$ and $W = \{f: \mathbf{R} \to \mathbf{R} \mid f(3) = -2\}$ is the subset consisting of those functions $f: \mathbf{R} \to \mathbf{R}$ such that $f(3) = -2$;

\part
$V = \mathcal{F}(\mathbf{R}, \mathbf{R})$ and $W = \{f: \mathbf{R} \to \mathbf{R} \mid f(3) = 0\}$.

\part
$V = \mathbf{R}^2$ and $W \subseteq V$ is the subset consisting of those vectors $(a,b)$ such that $a^3 + b^5 = 0$.
[Hint: think about closure under scalar multiplication.]
\end{parts}

\question
Recall that the complex numbers are those of the form $a+bi$ with $i = \sqrt{-1}$ and $a,b \in \mathbf{R}$.
Vector spaces \emph{over the complex numbers $\mathbf{C}$} are defined by the same axioms as vector spaces over $\mathbf{R}$, except that scalars and scalar multiplication are taken from the set of all complex numbers rather than that of real numbers. 
Show that the set 
\[
\Big\{ \begin{bmatrix}0 & a \\ b & 0\end{bmatrix} \mid a,b\in \mathbf{C},\, a+b = 0\Big\}
\]
with scalar multiplication of matrices and matrix addition forms a vector space over $\mathbf{C}$.

\question
Determine the kernels of the following matrices.
[Compute $\ker(A)$ as an $\mathbf{R}$-vector space and $\ker(B)$ as a $\mathbf{C}$-vector space.]

\begin{parts}
\part
$
A
=
\begin{bmatrix}
1 & -2 & 1 \\
4 & -7 & -2 \\
-1 & 3 & 4
\end{bmatrix}
$

\part
$B 
=
\begin{bmatrix}
1 & i & -2 \\
3 & 4i & -5 \\
-4 & -6i & 6 
\end{bmatrix}
$

\end{parts}

\renewcommand{\thefootnote}{\fnsymbol{footnote}}

\question\footnote{This problem will not be graded, but will reappear on Homework 6.}
Show that $\mathbf{v}_1 = (-1,3,2)$, $\mathbf{v}_2 = (1, -2, 1)$ and $\mathbf{v}_3 = (2,1,1)$ span $\mathbf{R}^3$ and express $(6, -1,7)$ as a linear combination of $\mathbf{v}_1$, $\mathbf{v}_2$ and $\mathbf{v}_3$.

\question\footnote{This problem will not be graded, but will reappear on Homework 6.}
Determine the spans of the following sets of vectors in the given vector spaces over $\mathbf{R}$.
For parts (a) and (b), describe the span geometrically.

\begin{parts}
\part
$\{(6,-2), (-2,2/3), (3,-1)\}$ in $\mathbf{R}^2$

\part
$\{(1,2,-1), (-2,-4,2)\}$ in $\mathbf{R}^3$

\part
$\{2x^3+1, x-3, x^2+2, x + 3\}$ in $\mathbf{R}[x]$
[Hint: it's the subspace of all polynomials of degree $\leq 3$.]

\part
the matrices $\begin{bmatrix} 1 & 0 \\ 0 & -1 \end{bmatrix}$, $\begin{bmatrix} 0 & 1 \\ 0 & 1 \end{bmatrix}$ and $\begin{bmatrix} 0 & -3 \\ 0 & 0 \end{bmatrix}$ in $M_{2 \times 2}(\mathbf{R})$
[Hint: show that it contains $\begin{bmatrix} 1 & 0 \\ 0 & 0 \end{bmatrix}$, $\begin{bmatrix} 0 & 1 \\ 0 & 0 \end{bmatrix}$ and $\begin{bmatrix} 0 & 0 \\ 0 & 1 \end{bmatrix}$.]
\end{parts}

\question\footnote{This problem will not be graded, but will reappear on Homework 6.}
Determine the spans of the following pairs of vectors and describe them geometrically:

\begin{parts}
\part
$\mathbf{v}_1 = (1, -1, 2)$, $\mathbf{v}_2 = (2,-1,3)$
\part
$\mathbf{w}_1 = (1,2,-1)$, $\mathbf{w}_2 = (-5,-10,5)$
\end{parts}

\question
Consider the real vector space $V = \mathbf{R}^2$ and let $W \subseteq V$ be any subspace.
If $\mathbf{w} \in W$ is any \emph{nonzero} vector in $W$ and $\mathbf{v} \in V$ is any vector in $V$ such that $\mathbf{v}$ lies on the line passing through $\mathbf{w}$ and the origin $\mathbf{0}$, prove that $\mathbf{v} \in W$.
\end{questions}

\end{document}
