\documentclass[leqno, letterpaper, 11pt]{exam}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, systeme}
\usepackage[top=2.5cm, bottom=3cm, left=2.5cm, right=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle, largesmallcaps]{kpfonts}
\usepackage[normalem]{ulem}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newcounter{thm}
\newtheorem{problem}[thm]{Problem}

\renewcommand{\thequestion}{\textsc{Problem} \arabic{question}}
\renewcommand{\thepartno}{\alph{partno}}
\renewcommand{\solutiontitle}{\textsc{Solution:}\enspace}

\firstpagefooter{}{Page \thepage\ of \numpages}{}
\runningfooter{}{Page \thepage\ of \numpages}{}

\newcommand{\im}{\operatorname{im}}
\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}

\begin{document}

\printanswers

\LARGE
\textsc{Math} $225$ 
\Large
Homework $8$ 
\hfill
\large
Due March $24$, $2016$
\normalsize

\vspace{\baselineskip}

\emph{Show all your work, justify each response and write legibly and comprehensibly to receive full credit.} 

\vspace{.5\baselineskip}

\begin{questions}
\question
Let $f: \mathbf{R}^3 \to \mathbf{R}^4$ be a linear map such that $f(0,-1,4) = (2,5,-2,1)$, $f(0,3,3) = (-1,0,0,5)$ and $f(4,4,-1) = (-3,1,1,3)$.
\begin{parts}
\part Find the matrix of $f$ with respect to the standard bases.
\part Find the matrix of $f$ with respect to the ordered bases $B = \langle (0,-1,4), (0,2,7), (4,1,-4)\rangle$ and $B' = \langle (1,0,1,0), (0,1,0,1), (1,-1,0,0), (1,0,0,-1) \rangle$.
\end{parts}

\question
Prove that the kernel and the image of a linear map $f: V \to W$ are subspaces of $V$ and $W$, respectively.

\question
Compute $\ker(f)$, $\im(f)$ and show that the rank-nullity formula $\dim(V) = \dim(\ker(f)) + \dim(\im(f))$ holds for each of the following linear maps $f: V \to W$:
\begin{parts}
\part
$f: \mathbf{R}^3 \to \mathbf{R}^3$ given by the matrix
$A = \begin{bmatrix} 1 & -2 & 1 \\ 2 & -3 & -1 \\ 5 & -8 & -1 \end{bmatrix}$
\part
$f: \mathbf{R}^3 \to \mathbf{R}^2$ given by the matrix
$A = \begin{bmatrix} 1 & 3 & 2 \\ 2 & 6 & 5 \end{bmatrix}$
\end{parts}

\question
Consider the linear map $f: \mathcal{P}_2(\mathbf{R}) \to \mathcal{P}_2(\mathbf{R})$ given by 
\[
f(ax^2 + bx + c)
=
ax^2 + (a+2b+c)x + (3a-2b-c).
\]
\begin{parts}
\part
Find $\ker(f)$ and $\dim(\ker(f))$.
\part
Find $\im(f)$ and $\dim(\im(f))$.
\end{parts}

\question
Consider the linear map $\ddx{}: \mathbf{R}[x] \to \mathbf{R}[x]$ sending each polynomial $P(x)$ to its first derivative $P'(x)$.
For example, $\ddx{}[3-x^2+5x^3] = -2x + 15x^2$.
Find $\ker(\ddx{})$ and $\im(\ddx{})$.
Does the rank-nullity theorem apply?

\question
Determine if the following linear maps $f$ are one-to-one or onto.
\begin{parts}
\part
$f: \mathcal{P}_1(\mathbf{R}) \to \mathcal{P}_1(\mathbf{R})$ given by $f(ax+b) = (2b-a)x + (b+a)$
\part
$f: \mathbf{R}^3 \to \mathbf{R}^3$ given by $f(1,0,0) = (1,0,3)$, $f(0,1,0) = (1,1,5)$ and $f(0,-1,1) = (2,1,-6)$
\end{parts}

\question
Determine the matrix for the linear map $f: \mathcal{P}_2(\mathbf{R}) \to \mathcal{P}_3(\mathbf{R})$ given by $f(ax^2+bx+c) = (x+1)(ax^2+bx+c)$ with respect to the following ordered bases:
\begin{parts}
\part
$B = \langle 1,x,x^2\rangle$ and $C = \langle1,x,x^2,x^3\rangle$
\part
$B = \langle 1,x-1,(x-1)^2\rangle$ and $C = \langle 1,x-1,(x-1)^2,(x-1)^3\rangle$
\end{parts}

\clearpage

\question
In this problem, you will prove the rank-nullity theorem in several steps.
Let $f: V \to W$ be a linear map and suppose $\dim(V) = n$.
Choose a basis $B = \{\mathbf{v}_1, \ldots, \mathbf{v}_k\}$ for $\ker(f)$.
We can extend $B$ to a basis $B' = \{\mathbf{v}_1, \ldots, \mathbf{v}_k, \mathbf{v}_{k+1}, \ldots, \mathbf{v}_n\}$ for $V$.
Consider $C = \{f(\mathbf{v}_{k+1}), \ldots, f(\mathbf{v}_n)\} \subseteq W$.
\begin{parts}
\part
Prove the rank-nullity theorem in the special case in which $k = n$.
\part
For the remainder of the problem, suppose $k < n$.
For each $\mathbf{w} \in \im(f)$, there exists $\mathbf{v} \in V$ such that $f(\mathbf{v}) = \mathbf{w}$.
Show that $\mathbf{w} \in \operatorname{span}(C)$, and therefore $\operatorname{span}(C) = \im(f)$.
[Hint: Write $\mathbf{v} = a_1 \mathbf{v}_1 + \cdots + a_n \mathbf{v}_n$.
What can we say about $f(\mathbf{v}) = f(a_1 \mathbf{v}_1 + \cdots + a_n \mathbf{v}_n)$?]
\part
Suppose $b_{k+1}f(\mathbf{v}_{k+1}) + \cdots + b_nf(\mathbf{v}_n) = \mathbf{0}$ for some $b_{k+1}, \ldots, b_n \in \mathbf{R}$ and let $\mathbf{v} \coloneqq b_{k+1}\mathbf{v}_{k+1} + \cdots + b_n\mathbf{v}_n$.
Show that $\mathbf{v} \in \ker(f)$.
\part
With the notation of (c), show that there exist $b_1, \ldots, b_k \in \mathbf{R}$ such that $b_1\mathbf{v}_1 + \cdots + b_k\mathbf{v}_k = \mathbf{v} = b_{k+1}\mathbf{v}_{k+1} + \cdots + b_n\mathbf{v}_n$. 
[Hint: What is $B$?]
\part
With the notation of (d), conclude that $b_1 = \cdots = b_n = 0$.
\part
Conclude that $C$ is linearly independent, hence a basis for $\im(f)$. 
\part
Show that $\dim(\ker(f)) + \dim(\im(f)) = n$.
\end{parts}
\end{questions}

\end{document}
