\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newcounter{thm}
\newtheorem{problem}[thm]{Problem}

\renewcommand{\thequestion}{\textbf{Problem \arabic{question}}}
\renewcommand{\thepartno}{\alph{partno}}

\firstpagefooter{}{Page \thepage\ of \numpages}{}
\runningfooter{}{Page \thepage\ of \numpages}{}

\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}
\newcommand{\ddt}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}t}}}

\begin{document}

%\printanswers

\LARGE
\textsc{Math} $118$ 
\Large
Homework $6$ 
\hfill
\large
Due March $3$, $2015$
\normalsize

\vspace{\baselineskip}

\emph{Show all your work, justify each response and write legibly and comprehensibly in order to receive full credit.}

\begin{questions}
\question
A cylindrical container with no top has a surface area of $3\pi$ square meters.
What base radius and height result in the largest possible volume for such a cylindrical container?

\begin{solution}
Let $r$ denote the radius of the base of the container and $h$ the height.
The surface area is the area of the base plus the area of the side.
The base is a circle of radius $r$, so its area is $\pi r^2$.
The side is a curled-up rectangle: the height is $h$ and the width is the circumference of the base, $2\pi r$, so the area is $2\pi r h$.
Thus, the surface area is $A = 3\pi = \pi r^2 + 2\pi rh$, from which we can deduce that $h = \displaystyle \frac{3-r^2}{2r}$ and thus eliminate one of the variables in the question.

The volume of the container is the area of the base times the height, i.e. $V(r) = \pi r^2 h = \displaystyle \pi r^2 \left(\frac{3-r^2}{2r}\right) = \frac{3\pi r - \pi r^3}{2}$.
We're looking for the absolute maximum of $V(r)$:
\[
0
=
V'(r) 
=
\frac{3\pi - 3\pi r^2}{2}
\Rightarrow
r
=
\pm1
\]
and $V'(r)$ has no discontinuities.
Also, $r = -1$ doesn't make sense: the base can't have a negative radius, so $r=1$ gives us our critical point.
Let's confirm that this is an absolute maximum:
\[
V''(r)
=
3\pi
<0,
\]
so $V$ is concave-down and {\color{red} $r=1$ meter} must be an absolute maximum.
When $r=1$, we have $h = \displaystyle 1$ meter.
\end{solution}

\question
Sketch the graph of a function satisfying all of the following properties simultaneously:
\begin{enumerate}[label=(\alph*), itemsep=0ex, topsep=0ex]
\item $f'(x) >0$ when $x<1$
\item $f'(x) <0$ when $x>1$ 
\item $f''(x) >0$ when $x<1$ and when $x>1$
\item $f'(1)$ is undefined.
\end{enumerate}

\begin{solution}
There are several possibilities here, but the essential features of the graph are the following: the function is increasing and concave-up on $(-\infty,1)$, decreasing and concave-up on $(1,\infty)$ and there's a vertical asymptote or a sharp point at $x=1$.
So it could look like the following:
\begin{center}
\includegraphics[scale=.5]{118hw6graphic1.png}
\end{center}
\end{solution}

\question
Sketch the graphs of the following functions (by plotting several points and connecting the dots---later, when we've discussed derivatives of exponential functions, we can be more scientific by thinking about increasing/decreasing behavior and concavity):
\begin{parts}
\part $f(x) = 2^x$
\part $g(x) = \displaystyle \left(\frac{1}{5}\right)^x$
\end{parts}

\begin{solution}
\begin{center}
\includegraphics[scale=.3]{118hw6graphic2.png}
\includegraphics[scale=.3]{118hw6graphic3.png}
\end{center}
\end{solution}

\question
Evaluate the following expressions:
\begin{parts}
\part $(-128)^{3/7}$

\begin{solution}
\[
(-128)^{3/7}
=
(-2^7)^{3/7}
=
(-1)^{3/7}(2^7)^{3/7}
=
(-1)(2^3)
=
{\color{red} -8}
\]
\end{solution}

\part $\displaystyle \left(\frac{27}{64}\right)^{2/3}\left(\frac{64}{25}\right)^{3/2}$

\begin{solution}
\[
\left(\frac{27}{64}\right)^{2/3}\left(\frac{64}{25}\right)^{3/2}
=
\left(\frac{3^3}{2^6}\right)^{2/3}\left(\frac{2^6}{5^2}\right)^{3/2}
=
\left(\frac{3^2}{2^{12/3}}\right)\left(\frac{2^{18/2}}{5^3}\right)
=
\frac{3^2\cdot 2^5}{5^3}
=
{\color{red} \frac{288}{125}}
\]
\end{solution}

\part $\displaystyle \frac{(4^{1.4})(4^{5.3})}{4^{2.7}}$

\begin{solution}
\[
\frac{(4^{1.4})(4^{5.3})}{4^{2.7}}
=
\frac{4^{1.4+5.3}}{4^{2.7}}
=
4^{1.4+5.3-2.7} 
=
4^{4}
=
{\color{red} 256}
\]
\end{solution}
\end{parts}

\question
Simplify the following expressions:
\begin{parts}
\part $\displaystyle \left(\frac{x^3y^{-2}}{z^4}\right)^{1/6}$

\begin{solution}
\[
\left(\frac{x^3y^{-2}}{z^4}\right)^{1/6}
=
(x^3y^{-2}z^{-4})^{1/6}
=
{\color{red} x^{1/2}y^{-1/3}z^{-2/3}}
\]
\end{solution}

\part $\displaystyle \frac{(x+2y)^0}{(x^3y^4)^{1/12}}$

\begin{solution}
\[
\frac{(x+2y)^0}{(x^3y^4)^{1/12}}
=
\frac{1}{(x^3y^4)^{1/12}}
=
(x^3y^4)^{-1/12}
=
{\color{red} x^{-1/4}y^{-1/3}}
\]
\end{solution}
\end{parts}

\question
Solve the following equations for $x$:
\begin{parts}
\part $10^{x^2-1} = 1000$

\begin{solution}
\[
10^{x^2-1} = 10^3
\Rightarrow
x^2-1 = 3
\Rightarrow
x^2=4
\Rightarrow
{\color{red} x=\pm2}
\]
\end{solution}

\part $\displaystyle \left(\frac{1}{9}\right)^{1-3x^2} = 3^{4x}$

\begin{solution}
\[
(3^{-2})^{1-3x^2} = 3^{4x}
\Rightarrow
3^{-2+6x^2} = 3^{4x}
\Rightarrow
-2+6x^2 = 4x
\Rightarrow
3x^2-2x-1 = 0
\Rightarrow
{\color{red} x=1, -\frac{1}{3}}
\]
\end{solution}

\end{parts}

\question
A principal of $\$7000$ is deposited in an account at an annual interest rate of $10\%$.
What is the balance on the account $10$ years later if the interest is compounded:
\begin{parts}
\part annually

\begin{solution}
${\color{red} 7000(1+0.1)^{10}} 
\approx 
18156.20$ dollars
\end{solution}

\part daily (ignoring leap years)

\begin{solution}
$\displaystyle {\color{red} 7000\left(1+\frac{0.1}{365}\right)^{365(10)}} 
\approx
19025.37$ dollars
\end{solution}

\part continuously

\begin{solution}
$7000\mathrm{e}^{0.1(10)}  
=
{\color{red} 7000\mathrm{e}}
\approx
19027.97$ dollars
\end{solution}

\end{parts}


\question
Rewrite the following expressions in terms of $\log_3(2)$ and $\log_3(5)$:
\begin{parts}
\part $\log_3(2.5)$

\begin{solution}
$\log_3(5/2) 
= 
{\color{red} \log_3(5) - \log_3(2)}$
\end{solution}

\part $\displaystyle \log_3\left(\frac{64}{125}\right)$

\begin{solution}
$\displaystyle \log_3\left(\frac{2^6}{5^3}\right)
=
\log_3(2^6) - \log_3(5^3)
=
{\color{red} 6\log_3(2) - 3\log_3(5)}$
\end{solution}

\end{parts}

\question
Simplify the following expressions:
\begin{parts}
\part $\displaystyle \log_2\left(\frac{1}{x}+\frac{1}{x^5}\right)$

\begin{solution}
$\displaystyle \log_2\left(\frac{x^4+1}{x^5}\right)
=
\log_2(x^4+1)-\log_2(x^5)
=
{\color{red}\log_2(x^4+1) - 5\log_2(x)}$
\end{solution}

\part $\displaystyle \ln\left(\frac{\sqrt[4]{x+1}}{(x+1)^5\sqrt{1-x^2}}\right)$

\begin{solution}
\begin{align*} 
\ln(\sqrt[4]{x+1}) - \ln\left((x+1)^5\sqrt{1-x^2}\right)
&=
\frac{1}{4}\ln(x+1) - \ln\left((x+1)^5\right) - \ln(\sqrt{1-x^2})
\\
&=
0.25\ln(x+1) - 5\ln(x+1) - 0.5\ln((1-x)(x+1))
\\
&=
{\color{red}-5.25\ln(x+1) - 0.5\ln(1-x)}
\end{align*}
\end{solution}

\end{parts}

\question
Solve for $x$:
\begin{parts}
\part $3^{2x-1} = 17$

\begin{solution}
$\displaystyle 2x-1 = \log_3(17)
\Rightarrow
{\color{red}x = \frac{1}{2}(\log_3(17)+1)}$
\end{solution}

\part $\displaystyle 5 = 3\ln(x) - \frac{1}{2}\ln(x)$

\begin{solution}
$\displaystyle 5 = 2.5\ln(x)
\Rightarrow
2 = \ln(x)
\Rightarrow
{\color{red} x = \mathrm{e}^2}$
\end{solution}

\end{parts}

\question
A drug is injected into a patient's bloodstream.
After $t$ seconds, the concentration of the drug is $C(t)$ grams per cubic centimeter, where
\[
C(t)
=
0.1(1+3\mathrm{e}^{-0.03t}).
\]
\begin{parts}
\part What is the concentration after $10$ seconds?

\begin{solution}
$\displaystyle C(10) 
=
0.1(1+3\mathrm{e}^{-0.03(10)}
=
{\color{red} 0.1(1+3\mathrm{e}^{-0.3})}
\approx
0.322245$
\end{solution}

\part How long does it take for the drug concentration to reach $0.12$ grams per cubic centimeter?

\begin{solution}
$\displaystyle
0.12 = 0.1(1+3\mathrm{e}^{-0.03t})
\Rightarrow
1.2 = 1 + 3\mathrm{e}^{-0.03t}
\Rightarrow
{\color{red} t = \frac{1}{-0.03}\ln\left(\frac{0.2}{3}\right)}
\approx
90.2683
$
seconds
\end{solution}
\end{parts}

\end{questions}
\end{document}
