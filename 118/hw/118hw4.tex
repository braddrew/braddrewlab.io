\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newcounter{thm}
\newtheorem{problem}[thm]{Problem}

\renewcommand{\thequestion}{\textbf{Problem \arabic{question}}}
\renewcommand{\thepartno}{\alph{partno}}

\firstpagefooter{}{Page \thepage\ of \numpages}{}
\runningfooter{}{Page \thepage\ of \numpages}{}

\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}
\newcommand{\ddt}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}t}}}

\begin{document}

%\printanswers

\LARGE
\textsc{Math} $118$ 
\Large
Homework $4$ 
\hfill
\large
Due February $10$, $2015$
\normalsize

\vspace{\baselineskip}

\emph{Show all your work, justify each response and write legibly and comprehensibly in order to receive full credit.}

\begin{questions}

\question
Find $\ddx{y}$ for the following functions using the Chain Rule:
\begin{parts}
\part $\displaystyle y = \frac{1}{u^2}$, where $u = 3x^2-1$

\begin{solution}
\begin{align*}
\ddx{y}
=
\frac{\mathrm{d}y}{\mathrm{d}u}\ddx{u}
&=
(-2u^{-3})(6x)
=
(-2(3x^2-1)^{-3})(6x)
=
{\color{red} \frac{-12x}{(3x^2-1)^{3}}}
\end{align*}
\end{solution}

\part $\displaystyle y=u^3-u$, where $\displaystyle u=\frac{-2}{\sqrt{x}}$

\begin{solution}
\begin{align*}
\ddx{y}
=
\frac{\mathrm{d}y}{\mathrm{d}u}\ddx{u}
&=
(3u^2-1)(-2(-0.5x^{-1.5}))
=
\left(3\left(\frac{-2}{\sqrt{x}}\right)^2 -1\right)x^{-1.5}
=
{\color{red} 12x^{-2.5}-x^{-1.5}}
\end{align*}
\end{solution}

\end{parts}

\question Find $f'$ using the Chain Rule:
\begin{parts}
\part $\displaystyle f(x) = \sqrt{1-\frac{1}{3x}}$

\begin{solution}
Let $g(x) = \sqrt{x}$ and $\displaystyle u(x)=1-\frac{1}{3x}$.
Then $g'(x) = 0.5x^{-0.5}$, $u'(x) = 3^{-1}x^{-2}$ and
\begin{align*}
f(x)
&=
g(u(x))
\\
f'(x)
&=
g'(u(x))u'(x)
=
0.5(1-(3x)^{-1})^{-0.5}3^{-1}x^{-2}
=
{\color{red} \frac{1}{6x^2\sqrt{1-\frac{1}{3x}}}}
\end{align*}
\end{solution}

\part $\displaystyle f(x) = \frac{(1-2x)^3}{(3x+1)^2}$

\begin{solution}
Let $g(x) = 1-2x$ and $h(x) = 3x+1$.
Then $f(x) = \frac{g(x)^3}{h(x)^2}$.
By the Chain Rule, the derivative of the numerator is $-6(1-2x)^2$ and that of the denominator is $6(3x+1)$.
Now, using the Quotient Rule, we have
\begin{align*}
f'(x)
&=
\frac{(3x+1)^2(-6)(1-2x)^2 - (1-2x)^3(6(3x+1))}{(3x+1)^4}
\\
&=
\frac{-6(3x+1)(1-2x)^2 - 6(1-2x)^3}{(3x+1)^3}
\\
& =
\frac{-6(1-2x)^2((3x+1) +1-2x)}{(3x+1)^3}
=
{\color{red} \frac{-6(1-2x)^2(x+2)}{(3x+1)^3}}
\end{align*}
\end{solution}
\end{parts}

\question
Hamlet releases an R\&B album entitled \textit{Sea of troubles} under the Motown Records label.
If the record company invests $t$ thousand dollars in advertising for the album, they will sell ${6\sqrt{t+3}+23}$ thousand copies of the album in the first year.
According to his contract, Hamlet will receive a total of $\displaystyle 1.5\sqrt[5]{x}$ thousand dollars in royalties for the first year of album sales, where $x$ is the number of thousands of copies sold in that first year.
\begin{parts}
\part At what rate are Hamlet's royalty earnings changing with respect to the amount Motown Records invests in advertising when they invest $13$ thousand dollars?
Give an approximate answer, rounded to the nearest cent.

\begin{solution}
Let $S(t) = 6\sqrt{t+3}+23=6(t+3)^{0.5}+23$ and $R(x) = 1.5\sqrt[5]{x}=1.5x^{0.2}$.
Then Hamlet's royalty earnings as a function of the number of thousands of dollars invested in advertising are given by $f(t) = R(S(t)) = 1.5(6(t+3)^{0.5}+23)^{0.2}$.
Using the Chain Rule,
\begin{align*}
R'(x)
&=
1.5(0.2x^{-0.8})
=
0.3x^{-0.8}
\\
S'(t)
&=
6(0.5)(t+3)^{-0.5}(1)
=
3(t+3)^{-0.5}
\\
f'(t)
&=
R'(S(t))S'(t)
\\
&=
0.3(6(t+3)^{0.5}+23)^{-0.8}3(t+3)^{-0.5}
\\
&=
0.9(6(t+3)^{0.5}+23)^{-0.8}(t+3)^{-0.5}
\\
f'(13)
&=
0.9(6(13+3)^{0.5}+23)^{-0.8}(13+3)^{-0.5}
\\
&\approx
0.01034
\end{align*}
So Hamlet's royalties are increasing at a rate of about {\color{red}$0.01034$ thousand dollars (that is, $\$10.34$) per thousand dollars of spending on advertising} when the record company invests $13$ thousand dollars.
\end{solution}

\part At what percentage rate will Hamlet's royalty earnings be changing if the record company spends $13$ thousand dollars on advertising? 
Round your answer to the nearest percent.

\begin{solution}
The percentage rate of change is 
\begin{align*}
\frac{100f'(13)}{f(13)}
&=
\frac{100(0.9(6(13+3)^{0.5}+23)^{-0.8}(13+3)^{-0.5})}{1.5(6(13+3)^{0.5}+23)^{0.2}}
\\
&\approx
{\color{red} 3\%\text{ per thousand dollars of spending}}
\end{align*}
\end{solution}

\end{parts}

\question
The cost of producing $x$ thousand copies of Hamlet's second album, \textit{What dreams may come}, is $\displaystyle \frac{1}{127}x^2 + 13$ thousand dollars and the price at which each of these $x$ thousand copies will be sold is $\displaystyle p(x) = \frac{5(2x+12)}{x+3}$ dollars.
\begin{parts}
\part Find the marginal cost and the marginal revenue.

\begin{solution}
The marginal cost is just the value of the first derivative of the cost function $\displaystyle C(x) = \frac{1}{127}x^2 + 13$:
\[
C'(x)
=
\frac{1}{127}(2x)
=
{\color{red} \frac{2x}{127}}.
\]
The marginal revenue is the first derivative of the revenue function, given by the number of units sold times the price of each unit: $\displaystyle R(x) = xp(x) = \frac{5(2x^2 + 12x)}{x+3}$ thousand dollars:
\[
R'(x)
=
5\frac{(4x+12)(x+3) - (2x^2+12x)(1)}{(x+3)^2}
=
{\color{red} \frac{5(2x^2+12x+36)}{(x+3)^2}}.
\]
\end{solution}

\part Estimate the cost of producing the fourth batch of $1000$ copies of the album using the marginal cost, rounded to the nearest cent.

\begin{solution}
The cost of producing the fourth batch of $1000$ copies is the change in cost as $x$ increases from $3$ to $4$, which we can estimate using the marginal cost:
\[
C'(3)
=
\frac{6}{127}\text{ thousand dollars}
\approx 
{\color{red} \$47.24}.
\]
\end{solution}

\part Find the actual cost of producing the fourth batch of $1000$ copies of the album, rounded to the nearest cent.

\begin{solution}
The actual cost is
\[
C(4) - C(3)
=
\frac{16-9}{127}
=
\frac{7}{127} \text{ thousand dollars}
\approx 
{\color{red} \$55.12}.
\]
\end{solution}

\part Estimate the revenue from the sale of the fourth batch of $1000$ copies of the album using the marginal revenue, rounded to the nearest cent.

\begin{solution}
\[
R'(3)
=
\frac{5(2(9) + 12(3) + 36)}{(3+3)^2}
=
\frac{450}{36}
=
{\color{red} 12.5\text{ thousand dollars} = \$12500.00}
\]
\end{solution}

\part Find the actual revenue from the sale of the fourth batch of $1000$ copies of the album, rounded to the nearest cent.

\begin{solution}
\begin{align*}
R(4) - R(3)
&=
\frac{5(2(16) + 12(4))}{4+3} - \frac{5(2(9) + 12(3))}{3+3}
=
5\left(\frac{80}{7} - \frac{54}{6}\right)
\\
&\approx
{\color{red} 12.14286\text{ thousand dollars}
=
\$12142.86}.
\end{align*}
\end{solution}
\end{parts}

\question
Consider the equation $\displaystyle xy-3y = x^2$.
Find $\ddx{y}$ in two different ways: first, by implicit differentiation, and again by differentiating an explicit formula for $y$.
Compare your results and show they are compatible.

\begin{solution}
Implicit differentiation:
\begin{align*}
xy - 3y 
&=
x^2
\\
\ddx{}[xy - 3y]
&=
\ddx{}[x^2]
&&
\text{differentiate both sides}
\\
\ddx{x}y + x\ddx{y} -3 \ddx{y}
&=
2x
&&
\text{product rule}
\\
y + x\ddx{y} - 3\ddx{y}
&=
2x
\\
y + (x-3)\ddx{y}
&=
2x
&&
\text{solve for $\ddx{y}$}
\\
\ddx{y}
&=
{\color{red} \frac{2x-y}{x-3}}
\end{align*}
Explicit formula for $y$:
\begin{align*}
xy - 3y
&= 
x^2
\\
(x-3)y
&=
x^2
\\
y
&=
\frac{x^2}{x-3}
\\
\ddx{y}
&=
\frac{(2x)(x-3) - (x^2)(1)}{(x-3)^2}
\\
\ddx{y}
&=
{\color{red} \frac{x^2-6x}{(x-3)^2}}
\end{align*}
Comparing our results, since $\displaystyle y = \frac{x^2}{x-3}$, our implicit differentiation result gives
\[
\ddx{y}
=
\frac{2x-y}{x-3}
=
\frac{2x-\frac{x^2}{x-3}}{x-3}
=
\frac{2x}{x-3} - \frac{x^2}{(x-3)^2}
=
\frac{x^2-6x}{(x-3)^2},
\]
which does indeed agree with the expression we obtained by solving for $y$ first.
\end{solution}

\clearpage

\question
Find $\ddx{y}$ by implicit differentiation:
\begin{parts}
\part $y^2 + 2xy^2-5x-7 = 0$

\begin{solution}
\begin{align*}
0
&=
\ddx{}[y^2 + 2xy^2 - 5x - 7]
\\
0
&=
2y\ddx{y} + 2\left(\ddx{x}y^2 + x\left(2y\ddx{y}\right)\right) -5
\\
0
&=
2y\ddx{y} + 2y^2 + 4xy\ddx{y} -5
\\
0
&=
(2y+4xy)\ddx{y} + 2y^2 - 5
\\
\ddx{y}
&=
{\color{red} \frac{5-2y^2}{2y+4xy}}
\end{align*}
\end{solution}

\part $(x-2y)^2 = y$

\begin{solution}
\begin{align*}
\ddx{y}
&=
\ddx{}[(x-2y)^2]
\\
\ddx{y}
&=
2(x-2y)\ddx{}[x-2y]
\\
\ddx{y}
&=
2(x-2y)\left(1-2\ddx{y}\right)
\\
\ddx{y}
&=
2(x-2y)-4(x-2y)\ddx{y}
\\
(1+4(x-2y))\ddx{y}
&=
2(x-2y)
\\
\ddx{y}
&=
\frac{2(x-2y)}{1+4(x-2y)}
=
{\color{red} \frac{2x-4y}{4x-8y+1}}
\end{align*}
\end{solution}

\end{parts}

\question
Find the equation of the line tangent to the given curve at the specified point:
\begin{parts}
\part $x^2-y^3=2x$; $(1,-1)$

\begin{solution}
\begin{align*}
\ddx{}[x^2-y^3]
&=
\ddx{}[2x]
\\
2x - 3y^2\ddx{y}
&=
2
\\
\ddx{y}
&=
\frac{2x-2}{3y^2}
\end{align*}
Thus, the slope at $(1,-1)$ is $\displaystyle \frac{2(1)-2}{3(-1)^2} = 0$ and the equation of the tangent is {\color{red} $y = -1$}.
\end{solution}

\part $x^2y^3 - 2xy = x+ 6y- 1$; $(1,0)$

\begin{solution}
\begin{align*}
\ddx{}[x^2y^3 - 2xy]
&=
\ddx{}[x+6y-1]
\\
2xy^3 + 3x^2y^2\ddx{y} - 2y -2x\ddx{y}
&=
1 + 6\ddx{y}
\\
2xy^3 -2y -1 + (3x^2y^2-2x)\ddx{y}
&=
6\ddx{y}
\\
2xy^3 -2y -1 
&=
(6-3x^2y^2+2x)\ddx{y}
\\
\ddx{y}
&=
\frac{2xy^3-2y-1}{6-3x^2y^2+2x}
\end{align*}
Thus, the slope at $(1,0)$ is $\displaystyle \frac{0-0-1}{6-0+2}= \frac{-1}{8}$ and the equation of the tangent is {\color{red} $y = \frac{-1}{8}(x-1)$}.
\end{solution}
\end{parts}

\question
After a misguided rebranding effort, Hamlet releases a dubstep album unfortunately entitled \textit{Beetles o'er his bass}.
If each copy of the album sells for $p$ dollars, then there will be demand for $x$ thousand copies, where ${x^2+3px + p^2 = 500}$.
How fast is the demand $x$ changing with respect to time when the price is set at $\$10$ per copy and is decreasing at a rate of $\$0.20$ per month?

\begin{solution}
Letting $t$ represent the time in months,
\begin{align*}
\ddt{}[x^2+3px+p^2]
&=
\ddt{}[500]
\\
2x\ddt{x} + 3\ddt{p}x+3p\ddt{x} + 2p\ddt{p}
&=
0
\end{align*}
If the price is set at $\$10$ per copy, then 
\[
x^2 + 3(10)x + (10)^2 = 500
\Rightarrow
x^2 + 30x - 400 = (x+40)(x-10) = 0,
\]
meaning there's demand for either $10$ thousand or $-40$ thousand copies.
Let's give Hamlet the benefit of the doubt and say that the demand is for a positive number of copies: $x=10$.
Now, if the price is changing at a rate of $\ddt{p}=-0.20$, then we have
\begin{align*}
0
&=
2x\ddt{x} + 3\ddt{p}x+3p\ddt{x} + 2p\ddt{p}
\\
0
&=
2(10)\ddt{x} + 3(-0.2)(10) + 3(10)\ddt{x} + 2(10)(-0.2)
\\
0
&=
20\ddt{x} -6 + 30\ddt{x}-4
\\
0
&=
50\ddt{x} - 10 
\\
\ddt{x}
&=
0.2
\end{align*}
and the demand increases at a rate of {\color{red} $0.2$ thousand (that is, $200$) copies per month}.
\end{solution}

\end{questions}

\end{document}
