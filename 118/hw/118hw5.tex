\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newcounter{thm}
\newtheorem{problem}[thm]{Problem}

\renewcommand{\thequestion}{\textbf{Problem \arabic{question}}}
\renewcommand{\thepartno}{\alph{partno}}

\firstpagefooter{}{Page \thepage\ of \numpages}{}
\runningfooter{}{Page \thepage\ of \numpages}{}

\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}
\newcommand{\ddt}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}t}}}

\begin{document}

%\printanswers

\LARGE
\textsc{Math} $118$ 
\Large
Homework $5$ 
\hfill
\large
Due February $24$, $2015$
\normalsize

\vspace{\baselineskip}

\emph{Show all your work, justify each response and write legibly and comprehensibly in order to receive full credit.}

\begin{questions}
\question
A factory employs $x$ skilled laborers and $y$ unskilled laborers to produce widgets.
The number of widgets produced each month is 
\[
Q
=
2x^3+3x^2y^2 + (1+y)^3.
\]
If there are currently $x=30$ skilled laborers and $y=20$ unskilled laborers employed at the factory, estimate what change should be made in the number $y$ of unskilled laborers to maintain the current production level $Q$ if $2$ skilled laborers go on strike.

\begin{solution}
Start by finding the value of $Q$ when $(x,y) = (30,20)$:
\[
Q
=
2(30)^2 + 3(30)^2(20)^2 + (1+20)^3
=
1091061.
\]
Now, ``estimate'' means we should use linear approximation, which will require knowing how to find $\ddx{y}$, since we are given the change is in $x$ and asked to estimate the corresponding change in $y$.
For this, we differentiate implicitly:
\begin{align*}
\ddx{}[Q]
&=
2x^3+3x^2y^2 + (1+y)^3
\\
0
&=
6x^2 + 3\left(2xy^2 + 2x^2y\ddx{y}\right) + 3(1+y)^2\ddx{y}
\\
\ddx{y}
&=
\frac{-2(x^2+xy^2)}{2x^2y+(1+y)^2}
\\
\ddx{y}\Big|_{\substack{x=30\\y=20}}
&=
\frac{-2((30)^2+(30)(20)^2)}{2(30)^2(20) + (1+20)^2}
=
\frac{-25800}{36441}
\end{align*}
Linear approximation tells us that 
\[
\Delta y 
\approx 
\left(\ddx{y}\Big|_{\substack{x=30\\y=20}}\right)\Delta x
=
{\color{red} \frac{-25800}{36441}(-2)
\approx
1.416}
\]
In other words, if the number $x$ of skilled workers decreases by $2$ from $30$ to $28$, then we must increase the number $y$ of unskilled workers by approximately $1.416$ to $21.416$ in order to maintain the approximately the same production level $Q = 1091061$.
\end{solution}

\question
Consider the function $f(x) = \displaystyle \sqrt[3]{x} + \frac{1}{\sqrt[3]{x}}$.
\begin{parts}
\part Find the critical points of $f$, identify any local extrema and find the intervals on which $f$ is increasing or decreasing.

\begin{solution}
\begin{align*}
0
=
f'(x)
&=
\frac{1}{3}(x^{-\frac{2}{3}} - x^{-\frac{4}{3}})
\\
x^{-\frac{2}{3}}
&=
x^{-\frac{4}{3}}
\\
x^{\frac{2}{3}}
&=
1
\\
x
&=
\pm1
\end{align*}
and $f$ and $f'$ are both discontinuous at $x=0$.
\begin{center}
\begin{tabular}{c|c|c}
interval & $f'$ & $f$ \\
\hline
$(-\infty,-1)$ & $<0$ & increasing \\
\hline
$(-1,0)$ & $<0$ & decreasing \\
\hline
$(0,1)$ & $<0$ & decreasing \\
\hline
$(1,\infty)$ & $>0$ & increasing
\end{tabular}
\end{center}
Hence, $f$ has critical points at $x=\pm1$, $x=-1$ is a local maximum and $x=1$ is a local minimum.
\end{solution}

\part Find the inflection points of $f$ and the intervals on which the graph of $f$ is concave-up or concave-down.

\begin{solution}
\begin{align*}
0
=
f''(x)
&=
\frac{1}{9}(-2x^{-\frac{5}{3}} + 4x^{-\frac{7}{3}})
\\
x^{-\frac{5}{3}}
&=
2x^{-\frac{7}{3}}
\\
x^{\frac{2}{3}}
&=
2
\\
x
&=
\pm\sqrt{8}
\end{align*}
and both $f$ and $f'$ are discontinuous at $x=0$.
\begin{center}
\begin{tabular}{c|c|c}
interval & $f''$ & $f$ \\
\hline
$(-\infty,-\sqrt{8})$ & $>0$ & concave-up
\\
\hline
$(-\sqrt{8},0)$ & $<0$ & concave-down
\\
\hline
$(0,\sqrt{8})$ & $>0$ & concave-up
\\
\hline
$(\sqrt{8},\infty)$ & $<0$ & concave-down
\end{tabular}
\end{center}
Thus, there are inflection points at {\color{red} $x=\pm\sqrt{8}$}.
\end{solution}

\part Sketch the graph of $f$.

\begin{solution}
Before we sketch the graph, we should take a second to think about any possible asymptotes.
You can check that there are no horizontal asymptotes and there is a vertical asymptote at $x=0$. 
\begin{center}
\includegraphics[scale=.5]{118hw5graphic1.png}
\end{center}
\end{solution}
\end{parts}

\question
Find the absolute maximum and absolute minimum, if they exist, for each of the following functions on the specified interval:
\begin{parts}
\part $f(x) = \displaystyle x^5-5x^4+1$, $0\leq x\leq 5$

\begin{solution}
\[
0
=
f'(x)
=
5x^4 - 20x^3
\Rightarrow
x = 0, 4 
\]
and $f'$ has no discontinuities.
Since $f$ has exactly one critical point on the open interval $(0,5)$, we can use the second derivative test for absolute extrema:
\[
f''(4)
=
20(4)^3 - 60(4)^2
>0
\]
Thus, $f$ is concave-up on $[0,5]$ with a critical point at $x=4$ and a horizontal tangent at this critical point, from which we deduce that $f$ has an {\color{red} absolute minimum at $x=4$}. 
Also, $f(0) = 1 = f(5)$, so $f$ has {\color{red} absolute maxima at $x=0$ and $x=5$}.
\end{solution}

\part $f(u) = \displaystyle 2u-\frac{32}{u}$, $u>0$

\begin{solution}
\[
f'(u)
=
2+\frac{32}{u^2}
\]
Note that $f'(u) >0$ for each $u>0$ and $f'$ has no discontinuities on $(0,\infty)$.
In particular, $f$ is increasing on $(0,\infty)$.
Since $\lim_{u \to 0^+}f(u) = -\infty$ and $\lim_{u\to+\infty}f(u) = +\infty$, $f$ has {\color{red} no absolute extrema on $(0,\infty)$}.
\end{solution}

\end{parts}

\question 
The Groucho Marx Club for people who refuse to join any club that would have them as members had $f(t) = \displaystyle t^4-50t^2+125$ members $t$ years after its founding in $2005$.
\begin{parts}
\part Between $2006$ and $2009$, when was the membership the largest? the smallest?

\begin{solution}
\[
0
=
f'(t) 
=
4t^3-100t
\Rightarrow
t
=
0,
\pm5
\]
and $f'$ has no discontinuities.
We're considering the interval $[1,4]$, so we conclude that $f$ has no critical points on this interval.
Since $f'(1)<0$, it follow that $f$ is decreasing on the interval $[1,4]$.
Therefore, the absolute maximum on this interval must occur at the initial point $t=1$, i.e. {\color{red} in the year $2006$}.
Similarly, the absolute minimum must occur at the final point in the interval, $t=4$ or {\color{red} in the year $2009$}.
\end{solution}

\part How many members were there during this same period when the membership was largest and smallest?

\begin{solution}
We have $f(1) = {\color{red} 76}$ and $f(4) = {\color{red} -419}$.
If you aren't asleep, then you should have noticed that the latter value is an absurd number of members for a club to have, but no more absurd than the premise of a club for people who refuse to join clubs.
\end{solution}
\end{parts}

\question
Find two numbers $x$ and $y$ whose sum is $6$ such that $xy-y^2$ is as large as possible.
Justify your answer.

\begin{solution}
If $x+y = 6$, then $y = 6-x$, so we can eliminate this second variable by substitution and the question is to find the absolute maximum of $ xy - y^2 = x(6-x) - (6-x)^2 = f(x)$.
\begin{align*}
f(x)
&=
-2x^2+18x-36
\\
0
=
f'(x) 
&=
-4x+18
\\
x
&=
4.5
\\
f''(x)
&=
-4
\end{align*}
It's important to note that $f'$ has no discontinuities.
Since $f''(x) <0$ for all $x$, $f$ is concave-down, which tells us that the critical point $x=4.5$ must be the absolute maximum.
The answer is therefore {\color{red} $x=4.5$ and $y=6-4.5 = 1.5$}.
\end{solution}

\question
A factory receives an order to produce closed boxes with square bases with a volume of $250$ cubic meters.
The material used to construct the top and bottom of the box costs $\$2$ per square meter and the material for the sides costs $\$1$ per square meter.
What is the minimal cost of producing one such box?

\begin{solution}
Let $b$ denote the length in meters of one of the sides of the square base and $h$ the height of the box.
The volume is then $250 = b^2h$, so $h = \displaystyle \frac{250}{b^2}$.
The area of the top and bottom combined is $2b^2$ and the area of all four sides combined is $4bh = \displaystyle \frac{1000}{b}$.
It follows that the cost of producing a box as a function of the length $b$ is
\[
C(b)
=
2(2b^2) + 1\frac{1000}{b}
=
4b^2+\frac{1000}{b} \text{ dollars}.
\]
The question is to find the absolute minimum of $C(b)$.
Since it doesn't make sense to have $b\leq0$, we're considering the interval $(0,\infty)$.
\[
0
=
C'(b)
=
8b-\frac{1000}{b^2}
\Rightarrow
b 
=
5
\]
Since $C''(b) = 8 + \displaystyle \frac{2000}{b^3}$ is positive on $(0,\infty)$, $C$ is concave-up on $(0,\infty)$ and so {\color{red} $C(5) = 300$ dollars} is the minimal cost.
\end{solution}
\end{questions}

\end{document}
