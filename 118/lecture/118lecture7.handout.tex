\documentclass[letterpaper,11pt]{article}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newcounter{thm}
\newtheorem{defn}[thm]{Definition}
\newtheorem{problem}[thm]{Problem}
\newtheorem{example}[thm]{Example}
\theoremstyle{plain}
\newtheorem{fact}[thm]{Fact}

\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}
\newcommand{\dfn}[1]{\textbf{\emph{#1}}}

\begin{document}

\LARGE
\textsc{Math} $118$ 
\hfill
\large
January $28$, $2015$ \\
Derivatives 
\hfill
In-class handout
\normalsize

%\vspace{\baselineskip}

\begin{defn}
Let $f:A \to \mathbf{R}$ be a function whose domain $A$ is some subset of the real numbers.
\begin{enumerate}[label=(\alph*)]
\item A \dfn{secant line of $f$} is a line passing through (at least) two distinct points on the graph of $f$.
The \dfn{difference quotient of $f$} is the expression
\[
\frac{f(x+h)-f(x)}{(x+h)-x} = \frac{f(x+h)-f(x)}{h}.
\]
It is just the \emph{slope of the secant line} of $f$ through $(x,f(x))$ and $(x+h, f(x+h))$.
\item A \dfn{tangent line to $f$ at $x=c$} is a line passing through the point $(c,f(c))$ ``just touching'' the graph of $f$.
One could think of it as a secant line of $f$ through the point $(c,f(c))$ and some infinitely close second point $(c_0,f(c_0))$, where, by ``infinitely close'', we mean that the distance $|c-c_0|$ from $c$ to $c_0$ is small to the point of being negligle.
\item The \dfn{derivative of $f$ at $x=c$} is the slope of the tangent line to $f$ at $x=c$.
Thinking of the tangent line at $x=c$ as a secant line through $(c,f(c))$ and some infinitely close second point $(c_0,f(c_0))$, the derivative at $x=c$ becomes the limit of slopes of the secant lines as $c_0\to c$:
\[
\lim_{c_0\to c}\frac{f(c_0) - f(c)}{c_0-c}.
\]
Letting $h = c_0-c$, we can rewrite this as the limit of the difference quotients as $h\to0$:
\[
\lim_{h\to 0}\frac{f(c+h)-f(c)}{h}.
\]
We denote the derivative of $f$ at $x=c$ by $f'(c)$ or by $\ddx{f}\Big|_{x=c}$.
The derivative of $f$ at $x=c$ tells us the \emph{instantaneous} rate of change in $y$ with respect to $x$ near $x=c$, i.e. how small changes in $x$ affect $f(x)$ for $x$ close to $c$.
We say that $f$ is \dfn{differentiable at $x=c$} when the derivative of $f$ at $x=c$ is defined.
\end{enumerate}
\end{defn}

\begin{example}
Consider the function $f(x) = x^2-8x+1$.
The curve in the first image below is the graph of $f$, the steeper two lines are secant lines---one through $(5,f(5))$ and $(7,f(7))$ and the other through $(5,f(5))$ and $(10,f(10))$---and the shallowest line is the tangent to $f$ at $x=5$.
The second image illustrates the notion that the tangent is like a secant line through to extremely close points: this is a close-up of the graph of $f$, the secant through $(5,f(5))$ and $(5.1,f(5.1))$ (the steeper line) and the tangent at $(5,f(5))$ (the shallower line).
\begin{center}
\includegraphics[scale=.4]{118lecture7handoutgraphic1.png}
\includegraphics[scale=.4]{118lecture7handoutgraphic2.png}
\end{center}
\end{example}

\begin{fact}
Let $f$ be a function.
\begin{enumerate}[label=(\alph*)]
\item If $f'(c)>0$, then $f$ is increasing close to $x=c$.
This means that, for any $c_0$ and $c_1$ sufficiently close to $c$, if $c_0<c_1$, then $f(c_0) <f(c_1)$.
\item Similarly, if $f'(c)<0$, then $f$ is decreasing close to $x=c$, that is, if $c_0$ and $c_1$ are sufficiently close to $c$ and $c_0<c_1$, then $f(c_0)>f(c_1)$.
\end{enumerate}
\end{fact}

Indeed, looking at the second image from Example 2, notice how the graph of the tangent line and the graph of $f$ are virtually indistinguishable close to $x=5$. 
This means that, if the tangent line at $x=c$ is the graph of an increasing (decreasing) function, then so is the graph of $f$ close to $x=c$, and linear functions are increasing (decreasing) if they have positive (negative) slope.
The slope of the tangent line is just $f'(c)$, so this explains the above facts. 

\begin{fact}
If $f$ is a function differentiable at $x=c$, then $f$ is also continuous at $x=c$.
\end{fact}
The idea behind this is that, if the limit of the difference quotient exists as $h\to0$, then the numerator $[f(c+h)-f(c)]$ must also exist and equal $0$, which means in particular that $f(c)$ is defined.
Moreover, the limit $\lim_{h\to0}f(c+h)=\lim_{x\to c}f(x)$ must also exist, and it must equal $f(c)$ since $\lim_{h\to 0}[f(c+h)-f(c)] =\left[\lim_{h\to0}f(c+h)\right]-f(c) =0$.

\begin{fact}
Let $f$ and $g$ be functions and $c\in\mathbf{R}$.
We have the following useful properties of derivatives:
\begin{enumerate}[label=(\alph*)]
\item $\ddx{[c]} = 0$ (e.g., $f(x) = c= -7$,  $\ddx{f} =f'(x) = 0$);
\item for any exponent $n$, $\ddx{[x^n]} = nx^{n-1}$ (e.g., $f(x) = x^{-2.4}$, $\ddx{f} = f'(x) = -2.4x^{-3.4}$);
\item $\ddx{[cf]} = c\ddx{f}$ (e.g., $f(x) = x^5$, $\ddx{[-2f]} = (-2f)'(x) = -2(f'(x)) = -2(5x^4) = -10x^4$);
\item $\ddx{[f+g]} = \ddx{f} + \ddx{g}$ (e.g., $f(x) = 3x$, $g(x) = x^2$, $\ddx{[f+g]} = (f+g)'(x) = 3 + 2x$).
\end{enumerate}
\end{fact}

\begin{fact}
If $f$ is differentiable at $x=c$, then the the tangent line to $f$ at $x=c$ is given by the equation $y-f(c) = f'(c)(x-c)$ in point-slope form.
\end{fact}
Indeed, $f'(c)$ is the slope of the tangent line essentially by definition, and---also by definition---the tangent line passes through the point $(c,f(c))$ on the graph of $f$.
For instance, if $f(x) = 3x^2-1$, then $f'(x) = 3(2x)=6x$ and so the tangent line to  at $x=-2$ has the equation 
\[
y-f(-2) = f'(-2)(x-(-2)) 
\Rightarrow
y-(3(-2)^2-1) = (6(-2))(x-(-2))
\Rightarrow
\boxed{y-11 = -12(x+2).}
\]

\begin{defn}
Let $f$ be a function of the independent variable $x$.
\begin{enumerate}[label=(\alph*)]
\item The \dfn{relative rate of chage of $f(x)$ with respect to $x$} is the ratio $\displaystyle \frac{f'(x)}{f(x)}$.
\item The \dfn{percentage rate of change of $f(x)$ with respect to $x$} is the ratio $\displaystyle \frac{100 f'(x)}{f(x)}$.
\end{enumerate}
\end{defn}

\begin{example}
If the GDP of a country is $f(t) = t^2 +6t +100$ billion dollars $t$ years after the year $2000$, then the derivative of $f$ is $f'(t) = 2t+6$ and the relative rate of change of the GDP with respect to time in $2010$ is
\[
\frac{f'(2010-2000)}{f(2010-2000)}
=
\frac{f'(10)}{f(10)}
=
\frac{2(10)+6}{10^2+6(10)+100}
=
\frac{26}{260}
=
0.1\text{ billion dollars per year}
\]
and the percentage rate of change is $\displaystyle \frac{100f'(10)}{f(10)} = 10\%$ per year.
\end{example}

\end{document}
