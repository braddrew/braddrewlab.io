\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newcounter{thm}
\newtheorem{problem}[thm]{Problem}

%\runningheader{}{}{\oddeven{Name: \underline{\hspace{3in}}}{}}
\renewcommand{\thequestion}{\textbf{Problem \arabic{question}}}
\renewcommand{\thepartno}{\alph{partno}}

\newcommand{\ddt}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}t}}}
\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}
\newcommand{\ddu}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}u}}}
\newcommand{\dddx}[1]{\ensuremath{\frac{\mathrm{d}^2#1}{\mathrm{d}x^2}}}
\newcommand{\dfn}[1]{\textbf{\emph{#1}}}

\begin{document}

\printanswers

\LARGE
\textsc{Math} $118$ 
\hfill
\large
February $6$, $2015$ \\
Implicit differentiation
\hfill
In-class handout
\normalsize

\vspace{\baselineskip}

\begin{questions}

\question
Consider the equation $\displaystyle x+ y^{-1} = 5$.
Find $\ddx{y}$ in two ways: first, by implicit differentiation, and again by differentiating an explicit formula for $y$.
Compare your results and show they are compatible.

\begin{solution}
Implicit differentiation:
\begin{align*}
\ddx{}[x+y^{-1}]
&=
\ddx{}[5]
\\
1+(-1)y^{-2}\ddx{y}
&=
0
\\
-y^{-2}\ddx{y}
&=
-1
\\
\ddx{y}
&=
\frac{1}{y^{-2}}
=
{\color{red} y^2}
\end{align*}
Explicit formula for $y$:
\begin{align*}
x+\frac{1}{y}
&=
5
\\
\frac{1}{y}
&=
-x+5
\\
y
&=
\frac{1}{-x+5}
\\
\ddx{y}
&=
\ddx{}[(-x+5)^{-1}]
=
-(-x+5)^{-2}(-1)
=
{\color{red} (-x+5)^{-2}}
\end{align*}
Comparison:
plugging our explicit formula for $y$ into our first expression for $\ddx{y}$ found by implicit differentiation, we have
\[
\ddx{y}
=
y^2
=
\left(\frac{1}{-x+5}\right)^2
=
(-x+5)^{-2}.
\]
This is indeed equal to the expression for $\ddx{y}$ obtained by differentiating our explicit formula for $y$.
\end{solution}

\question
Consider the curve described by the equation $(x^2+2y)^3 = 2xy^2 + 64$.
Find $\ddx{y}$ by implicit differentiation and use it to find an equation for the tangent to the curve at the point $(0,2)$.

\begin{solution}
\begin{align*}
\ddx{}[(x^2+2y)^3]
&=
\ddx{}[2xy^2+64]
\\
3(x^2+2y)^2\left(2x+2\ddx{y}\right)
&=
2\left(y^2 + 2xy\ddx{y}\right)
\\
6x(x^2+2y)^2 + 6(x^2+2y)^2\ddx{y}
&=
2y^2 + 4xy\ddx{y}
\\
(6(x^2+2y)^2 - 4xy)\ddx{y}
&=
2y^2 - 6x(x^2+2y)^2
\\
\ddx{y}
&=
{\color{red} \frac{2y^2-6x(x^2+2y)^2}{6(x^2+2y)^2-4xy}}
\end{align*}
First, observe that $(0,2)$ is really a point on the curve, i.e. a point of the form $(x,y)$ satisfying the relation $(x^2+2y)^3 = 2xy^2+64$, because $(0^2+2(2))^3 = 64 = 2(0)(2)^2 + 64$, so it's not absurd to ask what the tangent at that point is.
To find the slope of this tangent at $(0,2)$, we evaluate $\ddx{y}$ at $x=0$ and $y=2$:
\[
\ddx{y}\Big|_{x=0, y=2}
=
\frac{2(2)^2 - 6(0)(0^2+2(2))^2}{6(0^2+2(2))^2-4(0)(2)}
=
\frac{8}{6(16)}
=
\frac{1}{12}.
\]
Now that we know the slope and that the tangent passes through the point $(0,2)$, the point-slope for of its equation is ${\color{red} y-2 =\displaystyle \frac{1}{12}x}$.
\end{solution}

\question
An oil tanker capsizes, releasing oil into the ocean in a circular pattern.
Several hours after the tanker capsizes, the circular oil slick has a radius of $51$ meters and this radius is increasing at a rate of $12$ meters per hour.
At what rate is the area of the oil slick changing?

\begin{solution}
Since the oil slick is circular, its area is $A = \pi r^2$, where $r$ is the length of the radius.
Implicitly differentiating this formula, we have $\ddt{A} = \ddt{}[\pi r^2] = 2 \pi r\ddt{r}$.
Here, $\ddt{r}$ is the rate at which the radius is changing.
Letting $r = 51$ meters and $\ddt{r} = 12$ meters per hour, we find that $\ddt{A} = 2\pi(51)(12) = {\color{red}1224\pi}$ square meters per hour.
\end{solution}

\question
Suppose there is a demand for $x$ units of a certain commodity at a price of $p$ thousand dollars per unit and that $x$ and $p$ satisfy the relation $x^2-60px-300p^2= 1000$.
If there's a demand for $10$ units and this demand is changing at a rate of $1$ unit per month, how fast is the price changing with respect to time?

\begin{solution}
First, let's determine the value of $p$ based on $x=10$ units:
\[
(10)^2-60(10)p - 300p^2 = 1000
\Rightarrow
-300(p^2+2p + 3) = 0
\Rightarrow
(p+3)(p-1) = 0
\]
so $p=-3$ or $p=1$ thousand dollars.
We wouldn't be wise to sell anything for $-3$ thousand dollars, so let's go ahead and say $p=1$.
Now, we implicitly differentiate with respect to time:
\begin{align*}
\ddt{}[x^2-60px - 300p^2]
&=
\ddt{}[1000]
\\
2x\ddt{x} -60\left(\ddt{p}x + p\ddt{x}\right) -300\left(2p\ddt{p}\right)
&=
0
\end{align*}
Plugging in $x=10$ units, $\ddt{x} = 1$ unit per month and $p = 1$ thousand dollars, we find
\begin{align*}
0 
&=
2(10)(1) - 60\left(\ddt{p}(10) + (1)(1)\right) - 600(1)\ddt{p}
\\
0
&=
20 - 600\ddt{p} - 60 -600 \ddt{p}
\\
40
&=
-1200\ddt{p}
\\
\ddt{p}
&=
{\color{red} -\frac{1}{30}\text{ thousand dollars per month}}
\approx -33.33\text{ dollars per month}
\end{align*}
\end{solution}

\end{questions}
\end{document}
