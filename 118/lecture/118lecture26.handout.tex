\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle, largesmallcaps, light]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0.25\baselineskip}

\theoremstyle{remark}
\newcounter{thm}
%\numberwithin{thm}{section}
\newtheorem{problem}[thm]{Problem}
\newtheorem{definition}[thm]{Definition}
\newtheorem{example}[thm]{Example}
\newtheorem{exercise}[thm]{Exercise}
\renewcommand{\thesubsection}{\arabic{section}.\Alph{subsection}}

\theoremstyle{plain}
\newtheorem{fact}[thm]{Fact}

%\runningheader{}{}{\oddeven{Name: \underline{\hspace{3in}}}{}}
\renewcommand{\thequestion}{\textsc{Problem  \arabic{question}}}
\renewcommand{\thepartno}{\alph{partno}}
\renewcommand{\solutiontitle}{\noindent\textsc{Solution:}\enspace}

\newcommand{\dd}[1]{\ensuremath{\mathrm{d}#1}}
\newcommand{\ddt}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}t}}}
\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}
\newcommand{\ddu}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}u}}}
\newcommand{\dddx}[1]{\ensuremath{\frac{\mathrm{d}^2#1}{\mathrm{d}x^2}}}
\newcommand{\defn}[1]{\textbf{\emph{#1}}}

\begin{document}

%\printanswers

\LARGE
\textsc{Math} $118$ 
\hfill
\large
March $25$, $2015$ \\
Integration by parts
\hfill
In-class handout
\normalsize

\begin{questions}

\question
Evaluate the following integrals:

\begin{parts}
\part $\int x^3\ln(x)\D{x}$

\begin{solution}
Let $u = \ln(x)$ and $\D{v} = x^2\D{x}$.
Then $\D{u} = \frac{\D{x}}{x}$ and $v = \int x^3\D{x} = \frac{1}{4}x^4$.
\begin{align*}
\int x^3\ln(x)\D{x}
&=
\int u \D{v}
=
uv - \int v\D{u}
\\
&=
\frac{1}{4}x^4\ln(x) - \int \frac{1}{4}x^4\frac{\D{x}}{x}
\\
&=
\frac{1}{4}x^4\ln(x) - \frac{1}{4}\int x^3\D{x}
\\
&=
\frac{1}{4}x^4\ln(x) - \frac{1}{16}x^4 + C
\end{align*}
\end{solution}

\part $\int x\sqrt{x+1}\D{x}$
[Bonus: evaluate this integral by parts \emph{and} by substitution and compare your results.]

\begin{solution}
Let $u = x$ and $\D{v} = \sqrt{x+1}\D{x}$, so that $\D{u} = \D{x}$ and $v = \int\D{v} = \int \sqrt{x+1}\D{x}$.
Making the substitution $w=x+1$ and $\D{w} = \D{x}$, we have $v = \int \sqrt{w}\D{w} = \frac{2}{3}w^{3/2} = \frac{2}{3}(x+1)^{3/2}$.
\begin{align*}
\int x\sqrt{x+1}\D{x}
&=
uv - \int v\D{u}
=
\frac{2}{3}x(x+1)^{3/2} - \int \frac{2}{3}(x+1)^{3/2}\D{x}
\\
&=
\frac{2}{3}x(x+1)^{3/2} - \frac{2}{3}\int w^{3/2}\D{w}
\\
&=
\frac{2}{3}x(x+1)^{3/2} - \frac{2}{3}\left(\frac{2}{5}w^{5/2}\right) + C
\\
&=
\frac{2}{3}x(x+1)^{3/2} - \frac{2}{3}\left(\frac{2}{5}(x+1)^{5/2}\right) + C
\end{align*}

Alternatively, we could have made the subsitution $w= x+1$ right at the beginning and found 
\begin{align*}
\int x\sqrt{x+1}\D{x} 
&=
\int (w-1)\sqrt{w}\D{w}
=
\int (w^{3/2} - w^{1/2})\D{w}
\\
&=
\frac{2}{5}w^{5/2} - \frac{2}{3}w^{3/2} + C
=
\frac{2}{5}(x+1)^{5/2} - \frac{2}{3}(x+1)^{3/2} + C
\end{align*}

We can compare the two results as follows:
\begin{align*}
\frac{2}{3}x(x+1)^{3/2} - \frac{2}{3}\left(\frac{2}{5}(x+1)^{5/2}\right)
&=
\frac{2}{3}(x+1)^{3/2} \left(x - \frac{2}{5}(x+1)\right)
=
\frac{2}{15}(x+1)^{3/2}(3x-2)
\\
\frac{2}{5}(x+1)^{5/2} - \frac{2}{3}(x+1)^{3/2}
&=
\frac{2}{15}(x+1)^{3/2}(3(x+1) - 5)
=
\frac{2}{15}(x+1)^{3/2}(3x -2)
\end{align*}
\end{solution}

\part $\int \ln(x)\D{x}$

\begin{solution}
Let $u = \ln(x)$ and $\D{v} = (1)\D{x}$, so $\D{u} = \frac{\D{x}}{x}$ and $v = \int \D{v} = \int \D{x} = x$, giving
\begin{align*}
\int \ln(x)\D{x}
&=
uv - \int v\D{u}
=
x\ln(x) - \int x\frac{\D{x}}{x}
=
x\ln(x) - \int \D{x}
=
x\ln(x) - x +C
\end{align*}
\end{solution}

\part $\int (x^2+2x+1)\mathrm{e}^{7x}\D{x}$

\begin{solution}
Let $u = x^2 + 2x +1$ and $\D{v} = \mathrm{e}^{7x}\D{x}$, so $\D{u} = (2x + 2)\D{x}$ and $v = \int \mathrm{e}^{7x}\D{x} = \frac{1}{7}\mathrm{e}^{7x}$, giving
\begin{align*}
\int (x^2+2x+1)\mathrm{e}^{7x}\D{x}
&=
\frac{1}{7}(x^2+2x+1)\mathrm{e}^{7x} - \int \frac{1}{7}\mathrm{e}^{7x}(2x+2)\D{x}
\\
&=
\frac{1}{7}(x^2+2x+1)\mathrm{e}^{7x} - \frac{2}{7} \int (x+1)\mathrm{e}^{7x}\D{x}
\end{align*}
We have thus reduced the problem to the slightly easier one of finding $\int (x+1)\mathrm{e}^{7x}\D{x}$, which we can accomplish by another integration by parts.
Let $s = x+1$ and $\D{t} = \mathrm{e}^{7x}\D{x}$, so $\D{s} = \D{x}$ and $t = \frac{1}{7}\mathrm{e}^{7x}$, giving
\begin{align*}
\int (x+1)\mathrm{e}^{7x}\D{x}
&=
st - \int t\D{s}
=
\frac{1}{7}(x+1)\mathrm{e}^{7x} - \int \frac{1}{7}\mathrm{e}^{7x}\D{x}
\\
&=
\frac{1}{7}(x+1)\mathrm{e}^{7x} - \frac{1}{49}\mathrm{e}^{7x}
\end{align*}
Plugging this back into our previous result, we find
\[
\int (x^2+2x+1)\mathrm{e}^{7x}\D{x}
=
\frac{1}{7}(x^2+2x+1)\mathrm{e}^{7x} - \frac{2}{7} \left(\frac{1}{7}(x+1)\mathrm{e}^{7x} - \frac{1}{49}\mathrm{e}^{7x}\right) + C,
\]
which I won't bother to simplify.
\end{solution}

\part $\int \frac{\ln(x)}{x^2}\D{x}$

\begin{solution}
Let $u = \ln(x)$ and $\D{v} = x^{-2}\D{x}$, so $\D{u} = \frac{\D{x}}{x}$ and $v = \int x^{-2}\D{x} = -x^{-1}$, giving
\[
\int \frac{\ln(x)}{x^2}\D{x}
=
-\frac{\ln(x)}{x} - \int -\frac{\D{x}}{x^2}
=
-\frac{\ln(x)}{x} - \frac{1}{x} + C
\]
\end{solution}

\part $\int \frac{x}{\mathrm{e}^{x}}\D{x}$

\begin{solution}
Begin by rewriting the integrand as $x\mathrm{e}^{-x}$.
Let $u = x$ and $\D{v} = \mathrm{e}^{-x}\D{x}$, so $\D{u} = \D{x}$ and $v = \int \mathrm{e}^{-x}\D{x} = -\mathrm{e}^{-x}$, giving
\[
\int \frac{x}{\mathrm{e}^x}\D{x}
=
-x\mathrm{e}^{-x} - \int -\mathrm{e}^{-x}\D{x}
=
-x\mathrm{e}^{-x}-\mathrm{e}^{-x}+C
\]
\end{solution}

\part $\int \ln(x)^2\D{x}$

\begin{solution}
Let $u = \ln(x)$ and $\D{v} = \ln(x)\D{v}$, so $\D{u} = \frac{\D{x}}{x}$ and $v = \int\ln(x)\D{x}$.
We have already computed $\int \ln(x)\D{x}$ above and found it to be $x\ln(x) - x$, so we have
\begin{align*}
\int \ln(x)^2\D{x}
&=
\ln(x)(x\ln(x)-x) - \int (x\ln(x)-x)\frac{\D{x}}{x}
\\
&=
\ln(x)(x\ln(x)-x) - \int(\ln(x)-1)\D{x}
\\
&=
\ln(x)(x\ln(x)-x) - \int \ln(x)\D{x} - \int \D{x}
\\
&=
\ln(x)(x\ln(x)-x) - (x\ln(x) - x) - x + C
\\
&=
x\ln(x)^2 - 2x\ln(x) - 2x + C
\end{align*}
\end{solution}

\end{parts}
\end{questions}

\end{document}
