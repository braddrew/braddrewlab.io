\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newcounter{thm}
\newtheorem{problem}[thm]{Problem}
\newtheorem{definition}[thm]{Definition}
\newtheorem{example}[thm]{Example}
\newtheorem{exercise}[thm]{Exercise}

\theoremstyle{plain}
\newtheorem{fact}[thm]{Fact}

%\runningheader{}{}{\oddeven{Name: \underline{\hspace{3in}}}{}}
\renewcommand{\thequestion}{\textbf{Problem \arabic{question}}}
\renewcommand{\thepartno}{\alph{partno}}

\newcommand{\ddt}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}t}}}
\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}
\newcommand{\ddu}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}u}}}
\newcommand{\dddx}[1]{\ensuremath{\frac{\mathrm{d}^2#1}{\mathrm{d}x^2}}}
\newcommand{\defn}[1]{\textbf{\emph{#1}}}

\begin{document}

\printanswers

\LARGE
\textsc{Math} $118$ 
\hfill
\large
February $11$, $2015$ \\
Local extrema, concavity and curve sketching
\hfill
In-class handout
\normalsize

\vspace{\baselineskip}

\begin{questions}
\question
Find the intervals on which $f$ is increasing or decreasing:
\begin{parts}
\part $f(x) = \displaystyle \sqrt{6-x-x^2}$

\begin{solution}
First derivative test:
\[
0 
=
f'(x)
=
\frac{1}{2}(6-x-x^2)^{-\frac{1}{2}}(-1-2x)
=
\frac{-2x-1}{2\sqrt{(3+x)(2-x)}}
\Rightarrow
0
=
-2x-1
\Rightarrow
x
=
-\frac{1}{2}
\]
Moreover, $f$ and $f'$ are both undefined for $x<-3$ and $x>2$, since $\sqrt{(3+x)(2-x)}$ does not make sense for such $x$-values.
\begin{center}
\begin{tabular}{c|c|c}
interval & $f'$ & $f$ \\
\hline
$(-3,-0.5)$ & $>0$ & increasing \\
\hline
$(-0.5,2)$ & $<0$ & decreasing
\end{tabular}
\end{center}
\end{solution}

\part $f(x) = \displaystyle \frac{x^2}{x-1}$

\begin{solution}
\[
0 
=
f'(x) 
=
\frac{2x(x-1) - x^2}{(x-1)^2}
=
\frac{x(x-2)}{(x-1)^2}
\Rightarrow
0
=
x(x-2)
\Rightarrow
x=0,2
\]
Also, $f'$ is discontinuous at $x=1$.
\begin{center}
\begin{tabular}{c|c|c}
interval & $f'$ & $f$ \\
\hline
$(-\infty,0)$ & $>0$ & increasing \\
\hline 
$(0,1)$ & $<0$ & decreasing \\
\hline
$(1,2)$ & $<0$ & decreasing \\
\hline
$(2,\infty)$ & $>0$ & increasing
\end{tabular}
\end{center}
\end{solution}
\end{parts}

\question
Find the critical points of $f(x) = \displaystyle 4- \frac{2}{x}+\frac{3}{x^2}$.

\begin{solution}
\[
0
=
f'(x)
=
\frac{2}{x^2} -\frac{6}{x^3}
\Rightarrow
\frac{2}{x^2}
=
\frac{6}{x^3}
\Rightarrow
6x^2
=
2x^3
\Rightarrow
x=3
\]
and neither $f$ nor $f'$ is defined at $x=0$, so {\color{red}$x=3$} is the only critical point of $f$.
\end{solution}

\question
Find the inflection points of $f$ and the intervals on which its graph is concave-up or concave-down:
\begin{parts}
\part $f(x) = x(x+3)^2$

\begin{solution}
Second derivative test:
\begin{align*}
f'(x)
&=
(x+3)^2 + 2x(x+3)
=
(x+3)(x+3+2x)
=
3(x+1)(x+3)
\\
f''(x)
&=
3(x+3) + 3(x+1)
=
6(x+2)
\end{align*}
Solving $0=f''(x)$ gives $x=-2$ and $f''$ has no discontinuities.
It remains to check that the concavity of $f$ does change at $x=-2$:
\begin{center}
\begin{tabular}{c|c|c}
interval & $f''$ & $f$ \\
\hline
$(-\infty,-2)$ & $<0$ & concave-down \\
\hline
$(-2,\infty)$ & $>0$ & concave-up
\end{tabular}
\end{center}
Hence, {\color{red}$x=-2$} is the only inflection point of $f$.
\end{solution}

\part $f(s) = 3s^5-15s^4+11s-17$

\begin{solution}
\begin{align*}
f'(s)
&=
15s^4 - 60s^3 + 11
\\
f''(s)
&=
60s^3-180s^2
\end{align*}
Solving $0=f''(s)$ gives $s=0,3$ and $f''$ has no discontinuities.
\begin{center}
\begin{tabular}{c|c|c}
interval & $f''$ & $f$ \\
\hline
$(-\infty,0)$ & $<0$ & concave-down \\
\hline
$(0,3)$ & $<0$ & concave-down \\
\hline
$(3,\infty)$ & $>0$ & concave-up
\end{tabular}
\end{center}
Hence, the concavity changes at $s=3$ but not at $s=0$, so {\color{red} $s=3$} is the only inflection point.
\end{solution}
\end{parts}

\question
For each of the following functions given in parts (a) and (b) below:
\begin{enumerate}[label=(\arabic*), itemsep=0ex]
\item find the domain of $f$;
\item find the intervals on which $f$ is positive or negative;
\item find the vertical and horizontal asymptotes of $f$;
\item compute the first and second derivatives of $f$;
\item find the intervals on which $f$ is increasing or decreasing;
\item find the critical points of $f$ and indicate whether each is a local maximum, local minimum or neither;
\item find the intervals on which $f$ is concave-up or concave-down;
\item find the inflections points of $f$;
\item sketch the graph of $f$ based on the above information.
\end{enumerate}

\vspace{.5\baselineskip}

\begin{parts}
\part $f(x) = x^2(x-5)^2$

\begin{solution}
\begin{enumerate}[label=(\arabic*)]
\item The domain is $\mathbf{R}=(-\infty,\infty)$.

\item The zeros of $f(x)$ are $x=0,5$.
Testing values from each interval, we find:
\begin{center}
\begin{tabular}{c|c}
interval & $f$ \\
\hline
$(-\infty,0)$ & $>0$ \\
\hline
$(0,5)$ & $>0$ \\
\hline
$(5,\infty)$ & $>0$
\end{tabular}
\end{center}
This is the general method that always works, but we also could have been a bit more perspicacious and noted immediately that $x^2(x-5)^2$ is always nonnegative.

\item As $x\to \pm\infty$, $f(x) \to +\infty$, so there are no horizontal asymptotes.
Since the domain of $f$ is $\mathbf{R}$, $f$ has no vertical asymptotes.
As a general rule, polynomial functions like $f$ have no vertical or horizontal asymptotes, but rational functions can have either or both.

\item 
\begin{align*}
f'(x)
&=
2x(x-5)^2+2x^2(x-5)
=
(x-5)(4x^2-10x)
=
{\color{red} 2x(x-5)(2x-5)}
\\
f''(x)
&=
(4x^2-10x) + (x-5)(8x-10)
=
{\color{red} 12x^2-60x+50}
\end{align*}

\item $0=f'(x) \Rightarrow x=0,2.5,5$
\begin{center}
\begin{tabular}{c|c|c}
interval & $f'$ & $f$ \\
\hline
$(-\infty,0)$ & $<0$ & decreasing \\
\hline 
$(0,2.5)$ & $>0$ & increasing \\
\hline
$(2.5,5)$ & $<0$ & decreasing \\
\hline
$(5,\infty)$ & $>0$ & increasing
\end{tabular}
\end{center}

\item From the previous part, we know that the critical points are {\color{red} $x=0$, $x=2.5$ and $x=5$}.
Also, based on the table from the previous part, $x=0$ is a local minimum, $x=2.5$ is a local maximum and $x=5$ is a local minimum.

\item By the quadratic formula, the solutions of $0=f''(x)$ are $\displaystyle x=\frac{15\pm5\sqrt{3}}{6}$.
\begin{center}
\begin{tabular}{c|c|c}
interval & $f''$ & $f$ \\
\hline
$(-\infty, (15-5\sqrt{3})/6)$ & $>0$ & concave-up \\
\hline
$((15-5\sqrt{3})/6, (15+5\sqrt{3})/6)$ & $<0$ & concave-down \\
\hline
$((15+5\sqrt{3})/6,\infty)$ & $>0$ & concave-up
\end{tabular}
\end{center}

\item By the previous part, we know that the inflection points are {\color{red} $x=\displaystyle \frac{15\pm5\sqrt{3}}{6}$}.

\item 
\begin{center}
\includegraphics[scale=.5]{118lecture13handoutgraphic1.png}
\end{center}
\end{enumerate}
\end{solution}

\part $f(x) = \displaystyle \frac{x+1}{(x-1)^2}$

\begin{solution}
\begin{enumerate}[label=(\arabic*)]
\item $(-\infty,1)\cup(1,\infty)$

\item $0=f(x) \Rightarrow x=-1$ and $f$ is discontinuous at $x=1$
\begin{center}
\begin{tabular}{c|c}
interval & $f$ \\
\hline
$(-\infty,-1)$ & $<0$ \\
\hline
$(-1,1)$ & $>0$ \\
\hline
$(1,\infty)$ & $>0$
\end{tabular}
\end{center}

\item As $\lim_{x\to \infty}f(x) = 0$, there's a horizontal asymptote at $y=0$.
As $\lim_{x\to 1^-}f(x) = +\infty = \lim_{x\to 1^+}f(x)$, there's a vertical asymptote at $x=1$.

\item 
\begin{align*}
f'(x)
&=
\frac{(x-1)^2-2(x+1)(x-1)}{(x-1)^4}
=
\frac{-(x+3)}{(x-1)^3}
\\
f''(x) 
&=
\frac{-(x-1)^3+3(x+3)(x-1)^2}{(x-1)^6}
=
\frac{2(x+5)}{(x-1)^4}
\end{align*}

\item $0=f'(x) \Rightarrow x=-3$ and $f'$ is discontinuous at $x=1$
\begin{center}
\begin{tabular}{c|c|c}
interval & $f'$ & $f$ \\
\hline
$(-\infty,-3)$ & $<0$ & decreasing \\
\hline
$(-3,1)$ & $>0$ & increasing \\
\hline 
$(1,\infty)$ & $<0$ & decreasing
\end{tabular}
\end{center}

\item Looking at the previous part and noting that $x=1$ doesn't belong to the domain of $f$, we find that {\color{red}$x=-3$} is the only critical point and it's a local minimum.

\item $0=f''(x) \Rightarrow x=-5$ and $f''$ is discontinuous at $x=1$
\begin{center}
\begin{tabular}{c|c|c}
interval & $f''$ & $f$ \\
\hline
$(-\infty,-5)$ & $<0$ & concave-down \\
\hline 
$(-5,1)$ & $>0$ & concave-up \\
\hline
$(1,\infty)$ & $>0$ & concave-up
\end{tabular}
\end{center}

\item By the previous part, the only inflection point is {\color{red}$x=-5$}.

\item 
\begin{center}
\includegraphics[scale=.5]{118lecture13handoutgraphic2.png}
\end{center}
\end{enumerate}

\end{solution}

\end{parts}
\end{questions}

\end{document}
