\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle, largesmallcaps, light]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0.25\baselineskip}

\theoremstyle{remark}
\newcounter{thm}
%\numberwithin{thm}{section}
\newtheorem{problem}[thm]{Problem}
\newtheorem{definition}[thm]{Definition}
\newtheorem{example}[thm]{Example}
\newtheorem{exercise}[thm]{Exercise}
\renewcommand{\thesubsection}{\arabic{section}.\Alph{subsection}}

\theoremstyle{plain}
\newtheorem{fact}[thm]{Fact}

%\runningheader{}{}{\oddeven{Name: \underline{\hspace{3in}}}{}}
\renewcommand{\thequestion}{\textsc{Problem  \arabic{question}}}
\renewcommand{\thepartno}{\alph{partno}}
\renewcommand{\solutiontitle}{\noindent\textsc{Solution:}\enspace}

\newcommand{\dd}[1]{\ensuremath{\mathrm{d}#1}}
\newcommand{\ddt}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}t}}}
\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}
\newcommand{\ddu}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}u}}}
\newcommand{\dddx}[1]{\ensuremath{\frac{\mathrm{d}^2#1}{\mathrm{d}x^2}}}
\newcommand{\defn}[1]{\textbf{\emph{#1}}}

\begin{document}

\printanswers

\LARGE
\textsc{Math} $118$ 
\hfill
\large
March $23$, $2015$ \\
Integration by parts
\hfill
In-class handout
\normalsize

\vspace{\baselineskip}

Integration by parts exploits the Product Rule for differentiation to simplify the computation of certain antiderivatives.
Given two differentiable functions $u$ and $v$, we have
\begin{align*}
\ddx{}[u(x)v(x)]
&=
u(x)\ddx{v} + v(x)\ddx{u}
\\
\int \ddx{}[u(x)v(x)]\D{x}
&=
\int \left[u(x)\ddx{v} + v(x) \ddx{u}\right]\D{x}
&&
\text{integrate both sides}
\\
uv
&=
\int u(x)\D{v} + \int v(x)\D{u},
\end{align*}
where, as usual, $\D{u} = u'(x)\D{x}$ and $\D{v} = v'(x)\D{x}$.

Fine, but, in the immortal words of Miles Davis, so what?
Well, suppose we're given some integral $\int f(x)\D{x}$ and we can rewrite it as $\int f(x) \D{x} = \int u\D{v}$ for some $u$ and $\D{v}$.
In that case, we look at the above equation and we realize that we can compute $\int f(x)\D{x}$ as follows:
\[
\int f(x)\D{x}
=
\int u\D{v}
=
uv - \int v\D{u}.
\]
This may be a lateral move, but if we chose $u$ and $\D{v}$ wisely, then $\int v \D{u}$ might be easier to compute than $\int u\D{v} = \int f(x)\D{x}$.

\fbox{\fbox{\parbox{6.35in}{
\begin{center}
\large
\textsc{Steps for integrating by parts}
\normalsize
\end{center}
To evaluate $\int f(x)\D{x}$ using integration by parts:
\begin{enumerate}[label=(\arabic*), topsep=0.2\baselineskip, itemsep=0ex]
\item Choose a function $u(x)$ and a differential $\D{v} = v'(x)\D{x}$ such that $f(x) \D{x} = u(x)\D{v}$. 
If possible, choose $u$ such that $\D{u}$ is simpler than $u$ and $\D{v}$ such that $\int \D{v} = \int v'(x)\D{x}$ is easy to compute.
\item Compute $\D{u}$ and $v = \int \D{v}$.
\item Compute $\int v\D{u}$.
\item Plug everything into the formula: \qquad
$\displaystyle
\boxed{
\int f(x)\D{x} 
=
\int u\D{v} 
=
uv - \int v\D{u}
}
$
\end{enumerate}
}}}

Observe that we can safely ignore the constants of integration \emph{until Step (4)}, when we must include one.

\begin{example}
\label{2}
Compute $\int x\mathrm{e}^{6x}\D{x}$.

\begin{solution}
There's no obvious substitution to be made here, so we need to integrate by parts.
\begin{enumerate}[label=(\arabic*), topsep=0ex, itemsep=0ex]
\item If we let $u(x) = x$, then $\D{u} = \D{x}$ is certainly simpler than $u$.
If we let $\D{v} = \mathrm{e}^{6x}\D{x}$, then $v = \int \D{v} = \int \mathrm{e}^{6x}\D{x}$ is rather easy to compute.
\item $\D{u} = \D{x}$ and $v = \int \D{v} = \frac{1}{6}\mathrm{e}^{6x}$.
\item $\int v \D{u} 
= 
\int \frac{1}{6}\mathrm{e}^{6x}\D{x}
=
\frac{1}{36}\mathrm{e}^{6x}$
\item $\int x\mathrm{e}^{6x}\D{x}
=
{\color{red} \frac{1}{6}x\mathrm{e}^{6x} - \frac{1}{36}\mathrm{e}^{6x} +C}$
\end{enumerate}
\end{solution}
\end{example}

Sometimes, one needs to integrate by parts more than once to compute a given integral, i.e. sometimes computing $\int v\D{u}$ in Step (3) requires \emph{another} integration by parts.


\begin{example}
\label{3}
Compute $\int (x^2+3) \sqrt{2x-1}\D{x}$.

\begin{solution}
\begin{enumerate}[label=(\arabic*), topsep=0ex, itemsep=0ex]
\item Let $u = x^2+3$ and $\D{v} = \sqrt{2x-1}\D{x}$, since the derivative of $\sqrt{2x-1}$ is not simpler than $\sqrt{2x-1}$, but $\D{v}$ \emph{is} something we can integrate.
\item We have $\D{u} = 2x\D{x}$ and $v = \int \sqrt{2x-1}\D{x} = \int \frac{1}{2}\sqrt{w}\D{w} = \frac{1}{3}w^{3/2} = \frac{1}{3}(2x-1)^{3/2}$, where we have used the substitution $w= 2x-1$.
\item $\int v\D{u} 
=
\int \frac{1}{3}(2x-1)^{3/2}(2x)\D{x}
=
\frac{2}{3}\int x(2x-1)^{3/2}\D{x}
.
$
In order to evaluate this, we must again integrate by parts---but note that this integral \emph{is} less complicated than the original one.
\begin{enumerate}[label=(\roman*), topsep=0ex, itemsep=0ex]
\item Let $s = x$ and $\D{t} = (2x-1)^{3/2}\D{x}$.
\item $\D{s} = \D{x}$ and $t = \int \D{t} = \int (2x-1)^{3/2}\D{x} = \frac{1}{5}(2x-1)^{5/2}$, again using the substitution $w = 2x-1$.
\item $\int t\D{s} = \int \frac{1}{5}(2x-1)^{5/2}\D{x} = \frac{1}{35}(2x-1)^{7/2}$, again by substitution with $w=2x-1$.
\item $\int x(2x-1)^{3/2}\D{x} 
=
st - \int t\D{s}
=
\frac{1}{5}x(2x-1)^{5/2} - \frac{1}{35}x(2x-1)^{7/2}$,
\end{enumerate}
so $\int v\D{u} = \frac{2}{3}\left(\frac{1}{5}x(2x-1)^{5/2} - \frac{1}{35}x(2x-1)^{7/2} \right)$.
\item $\int (x^2+3)\sqrt{2x-1}\D{x}
=
{\color{red} \frac{1}{3}(x^2+3)(2x-1)^{3/2} - \frac{2}{3}\left(\frac{1}{5}x(2x-1)^{5/2} - \frac{1}{35}x(2x-1)^{7/2} \right) +C}
$
\end{enumerate}
\end{solution}
\end{example}

\begin{example}
\label{1}
Compute $\int x^2\mathrm{e}^x\D{x}$.

\begin{solution}
\begin{enumerate}[label=(\arabic*), topsep=0ex, itemsep=0ex]
\item Let $u = x^2$ and $\D{v} = \mathrm{e}^{x}\D{x}$, since the derivative of $\mathrm{e}^x$ is no simpler than $\mathrm{e}^x$, whereas the derivative of $x^2$ is simpler than $x^2$.
\item $\D{u} = 2x\D{x}$ and $v = \int \mathrm{e}^x\D{x} = \mathrm{e}^x$.
\item $\int v \D{u} = \int \mathrm{e}^x2x\D{x}$ is simpler than the original integral, but it's still not something we can compute by one of our basic rules for integration or substitution, so we need to integrate it by another integration by parts:
\begin{enumerate}[label=(\roman*), topsep=0ex, itemsep=0ex]
\item Let $s = 2x$ and $\D{t} = \mathrm{e}^x\D{x}$.
\item $\D{s} = 2\D{x}$ and $t = \int \mathrm{e}^x\D{x} = \mathrm{e}^x$.
\item $\int t\D{s} = \int 2\mathrm{e}^x\D{x} = 2\mathrm{e}^x$.
\item $\int 2x\mathrm{e}^x\D{x} = 2x\mathrm{e}^x - \int 2\mathrm{e}^x\D{x} = 2x\mathrm{e}^x - 2\mathrm{e}^x$
\end{enumerate}
so $\int v\D{u} = 2x\mathrm{e}^x - 2\mathrm{e}^x$.
\item $\int x^2\mathrm{e}^x = { \color{red} x^2\mathrm{e}^x - 2x\mathrm{e}^x + 2\mathrm{e}^x}$
\end{enumerate}
\end{solution}
\end{example}

\begin{questions}

\question
Evaluate the following integrals:

\begin{parts}
\part $\int x^3\ln(x)\D{x}$

\part $\int x\sqrt{x+1}\D{x}$
[Bonus: evaluate this integral by parts \emph{and} by substitution and compare your results.]

\part $\int \ln(x)\D{x}$
\end{parts}
\end{questions}

\end{document}
