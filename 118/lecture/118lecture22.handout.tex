\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newcounter{thm}
\numberwithin{thm}{section}
\newtheorem{problem}[thm]{Problem}
\newtheorem{definition}[thm]{Definition}
\newtheorem{example}[thm]{Example}
\newtheorem{exercise}[thm]{Exercise}
\renewcommand{\thesubsection}{\arabic{section}.\Alph{subsection}}

\theoremstyle{plain}
\newtheorem{fact}[thm]{Fact}

%\runningheader{}{}{\oddeven{Name: \underline{\hspace{3in}}}{}}
\renewcommand{\thequestion}{\textbf{Problem  \arabic{section}.\arabic{question}}}
\renewcommand{\thepartno}{\alph{partno}}

\newcommand{\dd}[1]{\ensuremath{\mathrm{d}#1}}
\newcommand{\ddt}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}t}}}
\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}
\newcommand{\ddu}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}u}}}
\newcommand{\dddx}[1]{\ensuremath{\frac{\mathrm{d}^2#1}{\mathrm{d}x^2}}}
\newcommand{\defn}[1]{\textbf{\emph{#1}}}

\begin{document}

\printanswers

\LARGE
\textsc{Math} $118$ 
\hfill
\large
March $9$, $2015$ \\
Integral calculus
\hfill
In-class handout
\normalsize

%\vspace{\baselineskip}

\section{Indefinite integrals}


\begin{definition}
\label{1.1}
Let $f$ and $F$ be two functions.
We say that $F$ is an \defn{antiderivative of $f$} if $F'(x) = f(x)$ for each $x$ in the domain of $f(x)$.
\end{definition}

\begin{example}
\label{1.2}
\
\begin{enumerate}[label=(\alph*), itemsep=0ex, topsep=0ex]
\item If $f(x) = 3x^2 + 2x + 1$, then $F(x) = x^3 + x^2 + x +3$ is an antiderivative of $f(x)$: $F'(x) = 3x^2 +2x + 1 = f(x)$.
Note that $G(x) = x^3 + x^2 + x -201$ is \emph{also} an antiderivative of $f$, so \emph{antiderivatives are not unique!}
\item If $f(x) = \sqrt{x}$ and $F(x) = \displaystyle \frac{2}{3}x^{3/2} -1$, then $F(x)$ is an antiderivative of $f$: $F'(x) = \displaystyle \frac{2}{3}\left(\frac{3}{2}x^{1/2}\right) = \sqrt{x}$.
\end{enumerate}
\end{example}

\begin{fact}
\label{1.3}
If $F$ is an antiderivative of $f$ and $C$ is a constant, then $G(x) = F(x) +C$ is another antiderivative of $f$.
Moreover, if $f$ is continuous, then \emph{every} antiderivative of $f$ is of the form $G(x) = F(x) +C$ for some constant $C$.
As a consequence, if $f$ is continuous, then, once we have found one antiderivative, we know what all the other antiderivatives are.
\end{fact}

Indeed, if $F$ is an antiderivative of $f$ and $C$ is a constant, then $\ddx{}[F(x) +C] = \ddx{F(x)} + 0 = F'(x) = f(x)$ and so $G(x) = F(x) +C$ is another antiderivative.
For the second assertion, suppose $F$ and $G$ are two antiderivatives of $f$.
Then $F'(x) = f(x) = G'(x)$, so $F'(x) - G'(x) = \ddx{}[F(x)-G(x)]= 0$, which means that the function $F(x) - G(x)$ has a horizontal tangent at every point in its domain, so its graph is a horizontal line and $F(x) - G(x)$ is a constant function with some value $C$.

\begin{definition}
\label{1.4}
Let $f$ be continuous.
The \defn{indefinite integral of $f$} is the family of all antiderivatives of $f$.
We denote it by $\int f(x) \dd{x}$.
\end{definition}

By \ref{1.3}, if we can identify one antiderivative $F$ of $f$, then $\int f(x)\dd{x}$ is the family of all functions of the form $F(x) +C$, where $C$ ranges over all real numbers.
In this situation, we often write 
\[
\boxed{\int f(x) \dd{x} = F(x) +C}
\]
with the tacit implication that $C$ is an indeterminate constant. 

We will discuss later how integrals can be interpreted as a sort of infinite summation of the areas of rectangles of infinitesimal width $\dd{x}$ and height $f(x)$.
This is the origin of the strange-looking \defn{integral symbol} ``$\int$'': it's an elongated ``S'', for ``sum''.
In the notation $\int f(x) \dd{x} = F(x) +C$, $f(x)$ is the \defn{integrand}, $C$ is the \defn{constant of integration} and the differential $\dd{x}$ specifies the \defn{variable of integration}.

\begin{fact}
\label{1.5}
\
\begin{enumerate}[label= \textup{(\alph*)}, itemsep=0ex, topsep=0ex]
\item If $F$ is differentiable, then $\int F'(x)\dd{x} = F(x) +C$.
\item We can always check whether $F(x)+C$ is the indefinite integral of $f$ by differentiating $F$: if $F'(x) = f(x)$, then $\int f(x)\dd{x} = F(x) +C$; otherwise, $F(x)+C$ is \emph{not} the indefinite integral of $f$.
\end{enumerate}
\end{fact}

\begin{fact}
\label{1.6}
Here are some useful rules for finding indefinite integrals:
\begin{enumerate}[label=\textup{(\alph*)}, itemsep=0ex, topsep=0ex]
\item Constant Rule: $\displaystyle \int k\dd{x} = kx +C$ for constant $k$;
\item General Constant Rule: $\displaystyle \int kf(x) \dd{x} = k\int f(x) \dd{x}$ for constant $k$;
\item Sum Rule: $\displaystyle \int [f(x)+g(x)]\dd{x} = \int f(x)\dd{x} + \int g(x)\dd{x}$;
\item Power Rule: $\displaystyle \int x^n\dd{x} = \frac{x^{n+1}}{n+1}+C$ for $n \neq -1$;
\item Exponential Rule: $\displaystyle \int \mathrm{e}^{kx}\dd{x} = \frac{1}{k}\mathrm{e}^{kx} + C$;
\item Logarithmic Rule: $\displaystyle \int \frac{\dd{x}}{x} = \int x^{-1}\dd{x} = \ln(|x|) +C$.
\end{enumerate}
\end{fact}

You can easily check that all of these facts are true by differentiating and using fact \ref{1.5}(b).
The only slightly obscure part is the absolue value in \ref{1.6}(f).
Recall that $\ln(x)$ is only defined for $x>0$, but $f(x) = \frac{1}{x}$ is defined for each $x\neq0$, as is $\ln(|x|)$.
If $x<0$, then $|x| = -x$ and so $\ddx{}[\ln(|x|) +C] = \ddx{}[\ln(-x) +C] = -\frac{1}{-x} = \frac{1}{x}$.
If $x>0$, then $|x| = x$ and so $\ddx{}[\ln(|x|) +C] = \ddx{}[\ln(x) + C] = \frac{1}{x}$.
So $\ln(|x|)$ is an antiderivative for $\frac{1}{x}$ on $(-\infty,0)\cup (0,\infty)$.

\begin{example}
\label{1.7}
\
\begin{enumerate}[label=(\alph*), itemsep=0ex, topsep=0ex]
\item $\int (-5)\dd{x} = -5x +C$
\item $\int 7\sqrt{x^2-2x-8}\dd{x} = 7 \int \sqrt{x^2-2x-8}\dd{x}$
\item $\int \left[2x^4 - \frac{3}{x^5}\right]\dd{x} = \int 2x^4\dd{x} + \int\frac{-3\dd{x}}{x^5} = \left(2\left(\frac{x^5}{5}\right)+C_0\right) - \left(3 \left(\frac{x^{-4}}{-4}\right) +C_1\right)$ 
\item $\int x^{-7.53}\dd{x} = \frac{x^{-6.53}}{-6.53}+C$
\item $\int \mathrm{e}^{\frac{2x}{3}}\dd{x} = \frac{3}{2}\mathrm{e}^{\frac{2x}{3}}+C$
\end{enumerate}
\end{example}

\begin{definition}
\label{1.8}
As we have mentioned, antiderivatives are not unique: infinitely many distinct functions can have the same derivative $f(x)$.
However, if we start from the function $f(x)$ and impose some initial conditions on the antiderivative, then there will be at most one antiderivative satisfying these conditions.
We call this type of problem an \defn{initial value problem}.
Examples of initial value problems are provided in the following exercises.
\end{definition}

\subsection{Exercises}

\begin{questions}
\question 
Compute the following indefinite integrals:
\begin{parts}
\part $\displaystyle \int \mathrm{e}^{\pi}\dd{x}$

\begin{solution}
Since $\mathrm{e}^{\pi}$ is just a constant, we have $\int \mathrm{e}^{\pi}\dd{x} = {\color{red} \mathrm{e}^{\pi}x+C}$.
\end{solution}

\part $\displaystyle \int (5x^4 + 4x^5)\dd{x}$

\begin{solution}
\[
\int 5x^4\dd{x} + \int 4x^5\dd{x}
=
5\int x^4\dd{x} + 4\int x^5\dd{x}
=
\left(5\frac{x^5}{5} + C_0\right) + \left(4\frac{x^6}{6} + C_1\right)
=
{\color{red}x^5 + \frac{2}{3}x^6 +C}
\]
We have replaced the sum $C_0 +C_1$ of the two constants of integration by the single constant $C$.

Sanity check: $\ddx{}[x^5 + (2/3)x^6+C] = 5x^4 + 4x^5$.
\end{solution}

\part $\displaystyle \int \frac{5x^{1/3}}{2x^{2/3}}\dd{x}$

\begin{solution}
\[
\frac{5}{2} \int \frac{x^{1/3}}{x^{2/3}}\dd{x}
=
\frac{5}{2} \int x^{-1/3}\dd{x}
=
\frac{5}{2} \left(\frac{3x^{2/3}}{2}\right) +C
=
{\color{red} \frac{15}{4}x^{2/3} +C}
\]
Sanity check: $\ddx{}[(15/4)x^{2/3}+C] = (30/12)x^{-1/3} = \frac{5x^{1/3}}{2x^{2/3}}$
\end{solution}

\part $\displaystyle \int \frac{x^7 - 3x^5 +\pi x^4 - x^2 +21}{\sqrt[5]{x}}\dd{x}$

\begin{solution}
\begin{align*}
\int \left[x^{7-0.2} \right. &- \left.3x^{5-0.2} + \pi x^{4-0.2} - x^{2-0.2} +21x^{-0.2}\right]\dd{x}
\\
&=
{\color{red} \frac{x^{7.8}}{7.8} - \frac{3x^{5.8}}{5.8} + \frac{\pi x^{4.8}}{4.8} - \frac{x^{2.8}}{2.8} +\frac{21 x^{0.8}}{0.8} +C}
\end{align*}
\end{solution}

\part $\displaystyle \int (x^2 -1)^2\dd{x}$

\begin{solution}
\[
\int (x^4 -2x^2 +1)\dd{x}
=
{\color{red}\frac{x^5}{5} -\frac{2x^3}{3} + x +C}
\]
\end{solution}
\end{parts}

\question
The slope of a curve $y = f(x)$ passing through $(1,3)$ is given by $f'(x) = x^3 - \frac{2}{x^2}+2$ at each point.
Find $f$ using this information.

\begin{solution}
We know that $f$ is an antiderivative of $f'(x)$, so note that
\[
\int f'(x) \dd{x}
=
\int \left(x^3 - \frac{2}{x^2} + 2\right)\dd{x}
=
\int x^3\dd{x} -2 \int x^{-2}\dd{x} + \int 2\dd{x}
=
\frac{x^4}{4} + 2x^{-1} + 2x +C.
\]
It remains to find $C$.
For this, we use the extra bit of information that $y = f(x)$ passes through $(1,3)$, so
\[
3 
=
f(1) 
=
\frac{(1)^4}{4} + 2(1)^{-1} + 2(1) + C
=
4.25 +C
\Rightarrow
C = 3 - 4.25 =  -1.25.
\]
Thus, $f(x) = \frac{x^4}{4} + 2x^{-1} + 2x -1.25$.

\end{solution}

\question 
Solve the following initial value problem: $f'(x) = \displaystyle \frac{2}{x} - \mathrm{e}^{2x-2}$ and $f(1) = 3$.

\begin{solution}
First, find the indefinite integral of $f'$:
\[
\int\left(\frac{2}{x} - \mathrm{e}^{2x-2}\right)\dd{x}
=
2\int \frac{\dd{x}}{x} - \mathrm{e}^2\int \mathrm{e}^{2x}\dd{x}
=
2\ln(|x|) - \mathrm{e^{-2}}\left(\frac{\mathrm{e}^{2x}}{2}\right)+C
=
2\ln(|x|) - \frac{1}{2}\mathrm{e}^{2x-2}+C
\]
Now, we know that
\[
3 
=
f(1) 
=
2\ln(|1|) - \frac{1}{2}\mathrm{e}^{2(1)-2}+C
=
0 - \frac{1}{2} +C,
\]
so $C = 3.5$ and {\color{red} $\displaystyle f(x) = 2\ln(|x|) - \frac{1}{2}\mathrm{e}^{2x-2} + 3.5$}.
\end{solution}
\end{questions}

\section{Integration by substitution}

The process of integration is inverse to that of differentiation.
By the same token, integration by substitution is inverse to the process of applying the Chain Rule for differentiation.

\begin{example}
\label{2.1}
If we want to compute $\int 4x\sqrt{2x^2-1}\dd{x}$, we must first recognize that the factor $4x$ is actually the derivative of the argument of the square root, $2x^2-1$.
So we let $u = 2x^2 -1$. 
Making this change of variables also affects the differential $\dd{x}$:
\[
\ddx{u} 
=
\ddx{}[2x^2-1]
=
4x
\Rightarrow
\dd{u}
=
4x\dd{x}.
\]
Thus, we can rewrite our integral as follows:
\[
\int 4x\sqrt{2x^2-1}\dd{x}
=
\int (\sqrt{2x^2-1}) (4x\dd{x})
=
\int \sqrt{u}\dd{u}.
\]
This is now an indefinite integral with respect to $u$ that we learned how to compute in the previous section: $\int u^{1/2}\dd{u} = \frac{2}{3}u^{3/2} +C$, so
\[
\int 4x\sqrt{2x^2-1}\dd{x}
=
\int \sqrt{u}\dd{u}
=
\frac{2}{3}u^{3/2}\dd{u}+C
=
\frac{2}{3}(2x^2-1)^{3/2}+C.
\]
Sanity check: $\displaystyle \ddx{}\left[\frac{2}{3}(2x^2-1)^{3/2}+C\right]
=
\frac{2}{3}\left(\frac{3}{2}(2x^2-1)^{1/2}(4x)\right)
=
4x\sqrt{2x^2-1}$.
\end{example}

The basic principle at work here is the following:

\begin{fact}[Integration by substitution]
\label{2.2}
Let $G$ and $u$ be differentiable functions.
Then
\[
\boxed{
\int G'(u(x))u'(x)\dd{x} 
=
G(u(x)) +C.
}
\]
Indeed, $\ddx{}[G(u(x))+C] = G'(u(x))u'(x)$ by the Chain Rule.
Letting $u = u(x)$ and $\dd{u} = u'(x)\dd{x}$, we can rewrite the above equality as
\[
\boxed{
\int G'(u)\dd{u} = G(u) +C = G(u(x)) +C.
}
\]
\end{fact}

\large
\textbf{Steps for integrating by substitution:}
\normalsize
Suppose we are computing an indefinite integral $\int f(x)\dd{x}$ with respect to the variable $x$.
\begin{enumerate}[label=\textbf{\arabic*.}]
\item Choose $u= u(x)$ in a way that simplifies the integrand $f(x)$.
\item Compute the differential $\dd{u} = u'(x)\dd{x}$.
\item Express the original integral in terms of $u$ and $\dd{u}$ so that $x$ and $\dd{x}$ no longer appear in the new expression.
We should now have $\int f(x)\dd{x} = \int g(u)\dd{u}$, where $g(u)$ is some function of $u$.
If we have chosen $u$ wisely, $g(u)$ will be easier to integrate than $f(x)$.
\item If we can compute $\int g(u)\dd{u} = G(u) +C$, then do so.
If not, go back to Step $1$ and choose a better $u$.
Sometimes there is no way to solve an integral by substitution.
\item Replace $u$ by $u(x)$ in $G(u)$ to obtain an antiderivative $G(u(x))$ for $f(x)$:
\[
\int f(x)\dd{x} = G(u(x)) +C.
\]
\end{enumerate}

\large
\textbf{Suggestions for choosing $u(x)$:}
\normalsize
\begin{enumerate}[label=(\alph*), itemsep=0ex, topsep=0ex]
\item Try to choose $u$ in such a way that $u'(x)$ is part of the integrand $f(x)$.
For instance, in Example \ref{2.1}, we chose $u(x) = 2x^2-1$ in part because $u'(x) = 4x$ appeared in the integrand $4x\sqrt{2x^2-1}$.
\item Consider taking $u$ to be the part of the integrand that makes the integral difficult to compute, such as the denominator of a fraction, the argument of a radical or the exponent of an exponential function.
\item Be persistent and be ready to try again a different substitution if your first choice doesn't work out.
\end{enumerate}

\begin{example}
\label{2.3}
Consider $\displaystyle \int \frac{6x^2-4}{x^3-2x+5}\dd{x}$.
This would certainly be easier if the denominator weren't so messy, so let's consider $u(x) = x^3 - 2x +5$.
In this case, $\dd{u} = u'(x) \dd{x} = (3x^2-2) \dd{x}$.
Observe that the numerator is actually $2u'(x)$, so we can rewrite the integral as
\[
\int \frac{6x^2-4}{x^3-2x+5}\dd{x}
=
\int \frac{2u'(x)}{u(x)}\dd{x}
=
\int \frac{2}{u}\dd{u}.
\]
Now, this is certainly much easier to compute.
We actually wrote down a rule for specifically this kind of integral in the last section: $\int 2u^{-1}\dd{u} = 2\ln(|u|) + C$.
Replacing $u$ by $u(x)$, we have
\[
\int \frac{6x^2-4}{x^3-2x+5}\dd{x}
=
2\ln(|x^3-2x+5|) +C.
\]
Sanity check:
$\displaystyle
\ddx{}[2\ln(|x^3-2x+5|)+C]
=
\frac{2(3x^2-2)}{x^3-2x+5}.
$
\end{example}

\begin{example}
\label{2.4}
Let's try to find $\displaystyle \int \frac{x^3}{x^2+1}\dd{x}$.
If we let $u(x) = x^2+1$ to simplify the entire denominator, then we'll run into trouble when we try to rewrite the whole fraction in terms of $u$.
So, instead, let's try $u(x) = x^2$, which gives $\dd{u} = 2x\dd{x}$.
We have
\[
\int \frac{x^3}{x^2+1}\dd{x} 
=
\int \frac{x^2}{x^2+1}x\dd{x}
=
\int \frac{u}{u+1}\cdot\frac{1}{2}\dd{u}.
\]
That looks slightly better, but, unfortunately, it's still not clear how to integrate $\frac{u}{u+1}$.
Well, let's try another substitution: $v(u) = u+1$ and $\dd{v} = \dd{u}$.
\[
\frac{1}{2}\int \frac{u}{u+1}\dd{u}
=
\frac{1}{2}\int \frac{v-1}{v}\dd{v}
=
\frac{1}{2}\int \frac{v}{v}\dd{v} - \frac{1}{2}\int \frac{1}{v}\dd{v}
=
\frac{1}{2}v - \frac{1}{2}\ln(|v|) +C.
\]
Now, we just have to undo all of our substitutions:
\[
\frac{1}{2}(v - \ln(|v|)) + C
=
\frac{1}{2}(u+1 - \ln(|u+1|)) +C
=
\frac{1}{2}(x^2+1 - \ln(|x^2+1|) +C.
\]
Sanity check:
\[
\ddx{}[\frac{1}{2}(x^2+1 - \ln(|x^2+1|)+C]
=
\frac{1}{2}\left(2x - \frac{2x}{x^2+1}\right)
=
\frac{x(x^2+1) - x}{x^2+1}
=
\frac{x^3}{x^2+1}.
\]
\end{example}

\end{document}
