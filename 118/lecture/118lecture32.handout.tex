\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle, largesmallcaps, light]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\allsectionsfont{\fontseries{m}\scshape}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0.25\baselineskip}

\theoremstyle{remark}
\newcounter{thm}
%\numberwithin{thm}{section}
\newtheorem{problem}[thm]{Problem}
\newtheorem{definition}[thm]{\textsc{Definition}}
\newtheorem{example}[thm]{\textsc{Example}}
\newtheorem{exercise}[thm]{\textsc{Exercise}}
%\renewcommand{\thesubsection}{\arabic{section}.\Alph{subsection}}

\theoremstyle{plain}
\newtheorem{fact}[thm]{\fontseries{m}\textsc{Fact}}

%\runningheader{}{}{\oddeven{Name: \underline{\hspace{3in}}}{}}
\renewcommand{\thequestion}{\textsc{Problem  \arabic{question}}}
\renewcommand{\thepartno}{\arabic{partno}}
\renewcommand{\solutiontitle}{\noindent\textsc{Solution:}\enspace}

\newcommand{\dd}[1]{\ensuremath{\mathrm{d}#1}}
\newcommand{\ddt}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}t}}}
\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}
\newcommand{\ddu}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}u}}}
\newcommand{\DDh}[1]{\ensuremath{\frac{\partial#1}{\partial h}}}
\newcommand{\DDr}[1]{\ensuremath{\frac{\partial#1}{\partial r}}}
\newcommand{\DDx}[1]{\ensuremath{\frac{\partial#1}{\partial x}}}
\newcommand{\DDy}[1]{\ensuremath{\frac{\partial#1}{\partial y}}}
\newcommand{\DDxx}[1]{\ensuremath{\frac{\partial^2#1}{\partial x^2}}}
\newcommand{\DDyy}[1]{\ensuremath{\frac{\partial^2#1}{\partial y^2}}}
\newcommand{\DDxy}[1]{\ensuremath{\frac{\partial^2#1}{\partial x\partial y}}}
\newcommand{\DDyx}[1]{\ensuremath{\frac{\partial^2#1}{\partial y\partial x}}}
\newcommand{\dddx}[1]{\ensuremath{\frac{\mathrm{d}^2#1}{\mathrm{d}x^2}}}
\newcommand{\defn}[1]{\textbf{\emph{#1}}}

\begin{document}

%\printanswers

\LARGE
\textsc{Math} $118$ 
\hfill
\large
April $10$, $2015$ \\
Linear approximation; two-variable optimization 
\hfill
In-class handout
\normalsize

\section*{Linear approximation}

If $f$ is a function of two variables, $\Delta x$ is a small change in $x$ and $\Delta y$ is a small change in $y$, then we can approximate the corresponding change in $f$ by
\[
\boxed{
\Delta f
\approx
\DDx{f} \Delta x + \DDy{f}\Delta y 
}
\]


\begin{exercise}
The productivity of a factory is $Q(x,y) = 60x^{1/2}y^{1/3}$ units per day, where $x$ is the capital investment (in thousands of dollars) and $y$ is the size of the work force (in worker-hours).
If the current capital investment is $\$ 900,000$ and the work force is $1000$ worker-hours, estimate the affect on the productivity if the capital investment is increased by $\$1000$ and the work force is increased by $2$ worker-hours.
\end{exercise}

\begin{proof}[\textsc{Solution:}]
\begin{align*}
\left.\DDx{f}\right|_{\substack{x=900\\y=1000}}
&=
\left.60\left(\frac{1}{2}x^{-1/2}\right)y^{1/3}\right|_{\substack{x=900\\y=1000}}
=
\left.30x^{-1/2}y^{1/3}\right|_{\substack{x=900\\y=1000}}
=
30(1/30)(10) 
=
10
\\
\left.\DDy{f}\right|_{\substack{x=900\\y=1000}}
&=
\left.60x^{1/2}\left(\frac{1}{3}y^{-2/3}\right)\right|_{\substack{x=900\\y=1000}}
=
\left.20x^{1/2}y^{-2/3}\right|_{\substack{x=900\\y=1000}}
=
20(30)(1/100) 
=
6
\end{align*}
Since $\Delta x = 1$ and $\Delta y = 2$, we have
\[
\Delta Q
\approx
(10)(1) + (6)(2)
=
22 \mbox{ units}
\qedhere\]
\end{proof}

\begin{exercise}
The surface area of a cylinder of radius $r$ and height $h$ is $S = 2\pi r^2 + 2\pi rh$.
Approximate the change in surface area that would result from increasing the radius from $3$ to $4$ centimeters while fixing the height at $12$ centimeters.
\end{exercise}

\begin{proof}[\textsc{Solution:}]
\begin{align*}
\left.\DDr{S}\right|_{\substack{r=3\\h=12}}
&=
4\pi r + 2\pi h\Big|_{\substack{r=3\\h=12}}
=
4\pi (3) + 2\pi (12) 
=
36\pi
\\
\left.\DDh{S}\right|_{\substack{r=3\\h=12}}
&=
2\pi r\Big|_{\substack{r=3\\h=12}}
=
2\pi(3)
=
6\pi
\end{align*}
Since $\Delta r = 1$ and $\Delta h = 0$, we have
\[
\Delta S
\approx
(36\pi)(1) + (6\pi)(0)
=
36\pi
\qedhere
\]
\end{proof}

\clearpage

\section*{Optimization}

\begin{definition}
The point $(a,b)$ is a \emph{relative (or local) maximum} of $f$ if $f(a,b) \geq f(x,y)$ for each $(x,y)$ in a sufficiently small circular disk centered at $(a,b)$.
Similarly, $(a,b)$ is a \emph{relative (or local) minimum} of $f$ if $f(a,b) \leq f(x,y)$ for each $(x,y)$ in a sufficiently small circular disk centered at $(a,b)$.
\end{definition}

\begin{definition}
A point $(a,b)$ in the domain of $f$ such that $f_x(a,b)$ and $f_y(a,b)$ both exist is a \emph{critical point} of $f$ if $f_x(a,b) = 0$ and $f_y(a,b) = 0$.
\end{definition}

Analogously to the critical point $x=0$ of $f(x) = x^3$ which is not a local extremum, two-variable functions $f(x,y)$ may have critical points that are neither relative minima nor relative maxima.
For example, the point $(0,0)$ is a critical point of $f(x,y) = y^2 - x^2$, but it isn't a relative extremum---it's what one calls a ``saddle point''.


\begin{exercise}
Find all critical points of $f(x,y) = x^2 + y^2$.
\end{exercise}

\begin{proof}[\textsc{Solution:}]
Just as with single-variable functions, we find the first (partial) derivative(s) and solve for zero:
\[
0
=
\DDx{f}
=
2x
\quad
\Rightarrow
\quad
x
=
0,
\qquad
0
=
\DDy{f}
=
2y
\quad
\Rightarrow
\quad
y
=
0
\]
So the only critical point is $(0,0)$.
\end{proof}

\begin{fact}
[Second partials test for relative extrema]
Let $f(x,y)$ be a function of $x$ and $y$ such that $f_x$, $f_y$, $f_{xx}$, $f_{yy}$, $f_{xy}$ and $f_{yx}$ exist. 
Let 
\[
D(x,y)
=
f_{xx}(x,y)f_{yy}(x,y) - f_{xy}(x,y)^2.
\]
If $(a,b)$ is a critical point of $f$ such that $f_x(a,b) = 0$ and $f_y(a,b) =0$, then:
\begin{enumerate}
\item if $D(a,b)<0$, then $(a,b)$ is a \emph{saddle point} of $f$;
\item if $D(a,b)>0$ and $f_{xx}(a,b) > 0$, then $(a,b)$ is a relative minimum of $f$;
\item if $D(a,b)>0$ and $f_{xx}(a,b) < 0$, then $(a,b)$ is a relative maximum of $f$.
\end{enumerate}
If $D(a,b)=0$, then the test is inconclusive and $(a,b)$ could be a relative extremum or a saddle point of $f$.
\end{fact}

\begin{center}
\begin{tabular}{|c|c|c|}
\hline
Sign of $D(a,b)$ & Sign of $f_{xx}(a,b)$ & $(a,b)$ \\
\hline
$+$ & $+$ & relative min \\
\hline
$+$ & $-$ & relative max \\
\hline
$-$ & & saddle point \\
\hline
\end{tabular}
\end{center}

\begin{questions}

\question
Find all critical points of the following functions and determine whether each is a relative minimum, relative maximum or saddle point:

\begin{parts}
\part $f(x,y) = x^2 + y^2$
\part $f(x,y) = 12x - x^3 - 4y^2$
\part $f(x,y) = x^3 - y^3 + 6xy$
\end{parts}

\begin{solution}
\begin{enumerate}[itemsep=0ex, label=(\arabic*)]
\item The process is as follows: first, find the critical points, then use the second partials test to determine their nature.
We already found the critical point of $f(x,y) = x^2+y^2$ in the last example: $(0,0)$.
The second-order partial derivatives are
\[
\DDxx{f} = 2,
\quad
\DDyy{f} = 2,
\quad
\DDxy{f} = 0,
\quad
\DDyx{f} = 0,
\]
and $D(x,y) = (2)(2) - (0)^2$, so $D(0,0) = 4 >0$.
Since $f_{xx}(0,0) = 2 >0$, $(0,0)$ is a relative minimum.

\item The first-order partials are
\[
\DDx{f}
=
12 - 3x^2,
\quad
\DDy{f}
=
-8y,
\]
and the equations $0 = 12-3x^2$ and $0 = -8y$ have solutions $x = \pm2$ and $y = 0$, so the critical points are $(2,0)$ and $(-2,0)$.
Now we must determine the nature of each of these critical points:
\[
\DDxx{f}
=
-6x,
\quad
\DDyy{f}
=
-8,
\quad
\DDxy{f}
=
0,
\quad
\DDyx{f}
=
0,
\]
and $D(x,y) = (-6x)(-8) - (0)^2$, so $D(2,0) = 96>0$ and $D(-2,0) = -96 <0$.
Since $f_{xx}(2,0) = -12 <0$ and $f_{xx}(-2,0) = 12 >0$, the second partials test therefore tells us that $(2,0)$ is a relative maximum and $(-2,0)$ is a saddle point.

\item 
\[
0
=
\DDx{f}
=
3x^2 + 6y
\quad
\Rightarrow
\quad
y = -\frac{1}{2}x^2,
\quad
0
=
\DDy{f}
=
-3y^2 + 6x
\quad
\Rightarrow
\quad
2x 
=
y^2
\]
Substituting $y = -\frac{1}{2}x^2$ into $2x = y^2$, we find
\[
2x 
=
\left(-\frac{1}{2}x^2\right)^2
=
\frac{x^4}{4}
\quad
\Rightarrow
\quad
0 
=
\frac{x^4}{4} - 2x
\quad
\Rightarrow
\quad
0
=
x^4 - 8x
=
x(x^3-8)
\]
Note that $2^3 - 8 = 0$ and, by polynomial division, $x^3 - 8 = (x-2)(2x^2 + 4x + 8)$.
Since $2x^2 + 4x + 8$ has no real roots, the only solutions of $0 = x(x^3 - 8)$ are thus $x=0$ and $y = 2$.
Since $y = -\frac{1}{2}x^2$, $x=0$ implies $y = 0$ and $x=2$ implies $y = -2$.
Thus, the critical points of $f$ are $(0,0)$ and $(2,-2)$.
It remains to determine their nature:
\[
\DDxx{f}
=
6x,
\quad
\DDyy{f}
=
-6y,
\quad
\DDxy{f}
=
6,
\quad
\DDyx{f}
=
6,
\]
so $D(x,y) = (6x)(-6y) - (6)^2 = -36xy - 36$.
Thus, $D(0,0) = -36 <0$, so $(0,0)$ is a saddle point.
Also, $D(2,-2) = -36(2)(-2) - 36 >0$ and $f_{xx}(2,-2) = 6(2) > 0$, so $(2,-2)$ is a relative minimum.
\end{enumerate}
\end{solution}

\end{questions}

\end{document}
