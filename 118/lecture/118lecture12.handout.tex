\documentclass[letterpaper,11pt]{exam}
\usepackage[leqno]{amsmath}
\usepackage{amsthm, color, etoolbox, hyperref, microtype, mathtools, sectsty}
\usepackage[top=1.5cm, bottom=2cm, margin=2.5cm]{geometry}
\usepackage[inline]{enumitem}
\usepackage[frenchstyle]{kpfonts}
\usepackage[mathcal]{eucal}
\usepackage[T1]{fontenc}
\usepackage[all]{xy}
\setlength{\parindent}{0pt}

\theoremstyle{definition}
\newcounter{thm}
\newtheorem{problem}[thm]{Problem}
\newtheorem{definition}[thm]{Definition}
\newtheorem{example}[thm]{Example}
\newtheorem{exercise}[thm]{Exercise}

\theoremstyle{plain}
\newtheorem{fact}[thm]{Fact}

%\runningheader{}{}{\oddeven{Name: \underline{\hspace{3in}}}{}}
\renewcommand{\thequestion}{\textbf{Problem \arabic{question}}}
\renewcommand{\thepartno}{\alph{partno}}

\newcommand{\ddt}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}t}}}
\newcommand{\ddx}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}x}}}
\newcommand{\ddu}[1]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}u}}}
\newcommand{\dddx}[1]{\ensuremath{\frac{\mathrm{d}^2#1}{\mathrm{d}x^2}}}
\newcommand{\defn}[1]{\textbf{\emph{#1}}}

\begin{document}

\printanswers

\LARGE
\textsc{Math} $118$ 
\hfill
\large
February $9$, $2015$ \\
Applications of the derivative
\hfill
In-class handout
\normalsize

\vspace{\baselineskip}

\begin{definition}
If $f$ is a function defined on an interval $I$, then we say $f$ is:
\begin{enumerate}[label=(\alph*), itemsep=0ex, topsep=0.5ex]
\item \defn{increasing on $I$} if $f(x_2) >f(x_1)$ for each $x_1 <x_2$ in $I$;
\item \defn{decreasing on $I$} if $f(x_2) < f(x_1)$ for each $x_1 < x_2$ in $I$.
\end{enumerate}
\end{definition}

\begin{example}
\label{2}
The following examples demonstrate that it is essential to specify the interval $I$ in consideration when discussing whether $f$ is increasing or decreasing:
\begin{enumerate}[label=(\alph*), itemsep=0ex, topsep=0.5ex]
\item $f(x) = -3x+2$ is decreasing on $I = \mathbf{R}$:
whenever $a < b$, we have $f(a) = -3a+2 > -3b+2 = f(b)$;
\item $f(x) = x^2$ is decreasing on $(-\infty,0)$, increasing on $(0,\infty)$ and neither on $\mathbf{R}$;
\item $f(x) = x^3$ is increasing on $\mathbf{R}$.
\end{enumerate}
Graph the above functions and give a graphical interpretation of the definition of increasing and decreasing functions.
\end{example}

\begin{fact}[First derivative test for increasing/decreasing intervals]
Given a differentiable function $f$, we determine the intervals on which it is increasing or decreasing as follows:
\begin{enumerate}[label=\textup{(\arabic*)}, itemsep=0ex, topsep=0.5ex]
\item find the zeros and discontinuities of $f'$;
\item use the these values to break the real number line into intervals;
\item for each of these intervals, choose a value $x=c$: if $f'(c) >0$, then $f$ is increasing on this interval; if $f'(c) <0$, then $f$ is decreasing on this interval.
\end{enumerate}
\end{fact}

\begin{example}
\
\begin{enumerate}[label=(\alph*), itemsep=0ex, topsep=0.5ex]
\item Use this test to check that what we said in Example \ref{2} was true.
\item If $f(x) = 3x^3-5x^2+1$, then we proceed as follows:
\begin{enumerate}[label=\textup{(\arabic*)}, itemsep=0ex, topsep=0.5ex]
\item $0 = f'(x) = 9x^2-10x \Rightarrow 9x^2=10x \Rightarrow x =0\text{ or }\frac{10}{9}$;
\item these values break the real number line into the intervals $(-\infty,0)$, $(0,10/9)$ and $(10/9,\infty)$;
\item we have $-1 \in (-\infty,0)$, $1\in(0,10/9)$ and $2\in(10/9,\infty)$, and $f'(-1)>0$, $f'(1) <0$ and $f'(2)>0$, so $f$ is increasing on $(-\infty,0)$, decreasing on $(0,10/9)$ and increasing on $(10/9,\infty)$.
\end{enumerate}
\item We can also perform the second and third steps by making a table.
If $f(x) = x^4 - 8x^2 + 1$, then $0= f'(x) = 4x^3-16x \Rightarrow 4x^3 = 16x \Rightarrow x=0,\pm2$ and we have the following table:
\begin{center}
\begin{tabular}{c|c|c}
interval & $f'(x)$ & $f$ \\
\hline
$(-\infty,-2)$ & $<0$ & decreasing \\
\hline
$(-2,0)$ & $>0$ & increasing \\
\hline
$(0,2)$ & $<0$ & decreasing \\
\hline
$(2,\infty)$ & $>0$ & increasing
\end{tabular}
\end{center}
\end{enumerate}
Graph the above functions and convince yourself that this process is a valid means of determining where a function is increasing or decreasing by thinking about the meaning of the first derivative.
\end{example}

\begin{definition}
Let $f$ be a function and $c$ an element of the domain of $f$. 
We say that $x=c$ is:
\begin{enumerate}[label=(\alph*), itemsep=0ex, topsep=0.5ex]
\item a \defn{critical point of $f$} if $f'(c)=0$ or $f'(c)$ is undefined;
\item a \defn{local maximum} (\defn{local minimum}) \defn{of $f$} if there is an interval $(a,b)$ containing $c$ such that $f(c) \geq f(x)$ ($f(c) \leq f(x)$) for each $x\in (a,b)$.
\end{enumerate}
\end{definition}

\begin{example}
Graph the following functions, give a graphical description of critical points and local extrema and convince your self that all local minima and local maxima are critical points:
\begin{enumerate}[label=(\alph*), itemsep=0ex, topsep=0.5ex]
\item $f(x) = (x+1)^2+2$ has a local minimum at $x=-1$;
\item $f(x) = 5-|x|$ has a local maximum at $x=0$;
\item $f(x) = (x-2)^3$ has a critical point at $x=2$ which is not a local extremum;
\item $f(x) = \sqrt[3]{x}$ has a critical point at $x=0$ which is not a local extremum.
\end{enumerate}
\end{example}

\begin{fact}[First derivative test for local extrema]
Let $x=c$ be a critical point of $f$.
\begin{enumerate}[label=\textup{(\alph*)}, itemsep=0ex, topsep=0.5ex]
\item If $f'(x) >0$ to the left of $c$ and $f'(x) <0$ to the right of $c$, then $x=c$ is a local maximum.
\item If $f'(x) <0$ to the left of $c$ and $f'(x) >0$ to the right of $c$, then $x=c$ is a local minimum.
\item If $f'(x)$ has the same sign to the left and to the right of $c$, then $x=c$ is not a local extremum.
\end{enumerate}
\end{fact}

\begin{example}
\
\begin{enumerate}[label=(\alph*), itemsep=0ex, topsep=0.5ex]
\item Use this test to verify that what we said in the previous example was true.
\item If $f(x) = 3x^4 - 8x^3 + 6x^2+2$, then the critical points are the solutions of 
\[
0 
= 
f'(x) 
=
12x^3 - 24x^2+12x
=
12(x^3-2x^2+x)
=
12x(x-1)^2
\Rightarrow 
x=0,1
\]
\begin{center}
\begin{tabular}{c|c|c}
interval & $f'(x)$ & $f$ \\
\hline
$(-\infty,0)$ & $<0$ & decreasing \\
\hline
$(0,1)$ & $>0$ & increasing \\
\hline
$(1,\infty)$ & $>0$ & increasing
\end{tabular}
\end{center}
This table tells us that $x=0$ is a local minimum and $x=1$ is not a local extremum.
\end{enumerate}
\end{example}

\begin{definition}
If $f$ is differentiable on $(a,b)$, then we say that the graph of $f$ is:
\begin{enumerate}[label=(\alph*), itemsep=0ex, topsep=0.5ex]
\item \defn{concave-up on $(a,b)$} if $f'$ is increasing on $(a,b)$;
\item \defn{concave-down on $(a,b)$} if $f'$ is decreasing on $(a,b)$.
\end{enumerate}
\end{definition}

\begin{fact}
If $f'$ is also differentiable on $(a,b)$, then, by the first derivative test for increasing/decreasing intervals applied to $f'$, $f$ is concave-up (concave-down) on $(a,b)$ if and only if $f''(x)>0$ ($f''(x)<0$) for each $x\in (a,b)$.
\end{fact}

\begin{example}
\label{11}
Graph each of the following functions and find a graphical interpretation of the definition of concave-up and concave-down:
\begin{enumerate}[label=(\alph*), itemsep=0ex, topsep=0.5ex]
\item $f(x) = x^2$ is concave-up on $\mathbf{R}$;
\item $f(x) = -x^2$ is concave-down on $\mathbf{R}$;
\item $f(x) = x^3$ is concave-down on $(-\infty,0)$ and concave-up on $(0,\infty)$;
\item $f(x) = \sqrt[3]{x}$ is concave-up on $(-\infty,0)$ and concave-down on $(0,\infty)$.
\end{enumerate}
\end{example}

\begin{fact}[Second derivative test for concavity]
Given a function $f$, we can determine the intervals on which it is concave-up or concave-down as follows:
\begin{enumerate}[label=\textup{(\arabic*)}, itemsep=0ex, topsep=0.5ex]
\item find the zeros and discontinuities of $f''$;
\item use these values to break the real number line into intervals;
\item for each of these intervals, choose a value $x=c$: if $f''(c) >0$, then $f$ is concave-up on this interval; if $f''(c) <0$, then $f$ is concave-down on this interval.
\end{enumerate}
\end{fact}

\begin{example}
\label{13}
\
\begin{enumerate}[label=(\alph*), itemsep=0ex, topsep=0.5ex]
\item Use this test to verify that what we said in Example \ref{11} was true.
\item If $f(x) = 3x^3-5x^2+1$, then $f'(x) = 9x^2-10x$ and the zeros of $f''$ are the solutions of 
\[
0 
=
f''(x)
=
18x-10
\Rightarrow 
x
=
\frac{5}{9}
\]
\begin{center}
\begin{tabular}{c|c|c}
interval & $f''(x)$ & $f$ \\
\hline
$(-\infty, 5/9)$ & $<0$ & concave-down \\
\hline
$(5/9, \infty)$ & $>0$ & concave-up
\end{tabular}
\end{center}
\end{enumerate}
\end{example}

\begin{definition}
An \defn{inflection point of $f$} is a point $x=c$ in the domain of $f$ at which the concavity of the graph changes or, equivalently, at which the sign of $f''(x)$ changes.
In particular, if $x=c$ is an inflection point of $f$, then $f''(c)=0$ or $f''(c)$ is undefined---but it could very well happen that $f''(c)=0$ or $f''(c)$ is undefined and the concavity doesn't change, i.e. $x=c$ isn't an inflection point.
\end{definition}

\begin{example}
\
\begin{enumerate}[label=(\alph*), itemsep=0ex, topsep=0.5ex]
\item In part (b) of Example \ref{13}, we showed that $x=\frac{5}{9}$ is an inflection point of $f(x) = 3x^3-5x^2+1$.
\item If $f(x) = x^4$, then $f'(x) = 4x^3$ and $f''(x)= 12x^2$.
The value $x=0$ is a zero of $f''$, but we have
\begin{center}
\begin{tabular}{c|c|c}
interval & $f''(x)$ & $f$ \\
\hline
$(-\infty,0)$ & $>0$ & concave-up \\
\hline
$(0,\infty)$ & $>0$ & concave-up
\end{tabular}
\end{center}
and so $x=0$ is \emph{not} an inflection point: the concavity doesn't change at $x=0$.
\item If $f(x) = x^3+1$, then $f'(x) = 3x^2$, $f''(x) = 6x$ and the only zero of $f''$ is $x=0$.
In this case, we have
\begin{center}
\begin{tabular}{c|c|c}
interval & $f''(x)$ & $f$ \\
\hline
$(-\infty,0)$ & $<0$ & concave-down \\
\hline
$(0,\infty)$ & $>0$ & concave-up
\end{tabular}
and so $x=0$ is an inflection point.
\end{center}
\end{enumerate}
\end{example}

\begin{fact}[Second derivative test for local extrema]
If $f''(x)$ exists for each $x\in (a,b)$ and $f'(c) = 0$ for some $c\in (a,b)$, then:
\begin{enumerate}[label=\textup{(\alph*)}, itemsep=0ex, topsep=0.5ex]
\item if $f''(c)>0$, then $x=c$ is a local minimum;
\item if $f''(c) <0$, then $x=c$ is a local maximum;
\item if $f''(c) = 0$ or $f''(c)$ is undefined, then the test is inconclusive and $x=c$ may or may not be a local extremum of some sort.
\end{enumerate}
\end{fact}

\begin{example}
Let $f(x) = x^5-5x$, so $f'(x) = 5x^4-5$ and $f''(x) = 20x^3$.
The critical points of $f$ are the solutions of $0=f'(x) = 5x^4-5$, i.e. $x=\pm1$.
We have $f''(-1) <0$, so $x=-1$ is a local maximum.
Similarly, $f''(1) >0$, so $x=1$ is a local minimum.
\end{example}

\begin{definition}
Let $f$ be a function and $b,c\in \mathbf{R}$.
\begin{enumerate}[label=(\alph*), itemsep=0ex, topsep=0.5ex]
\item The line $x=c$ is a \defn{vertical asymptote of $f$} if any of the following holds:
\[
\lim_{x\to c^+}f(x) = +\infty,
\quad
\lim_{x\to c^+}f(x) = -\infty,
\quad
\lim_{x\to c^-}f(x) = +\infty
\quad
\text{or}
\quad
\lim_{x\to c^-}f(x) = -\infty.
\]
Note in particular that, if $f$ has a vertical asymptote at $x=c$, then $f$ is not continuous at $x=c$.
\item The line $y=b$ is a \defn{horizontal asymptote of $f$} if either of the following holds:
\[
\lim_{x\to +\infty}f(x) = b
\quad
\text{or}
\quad
\lim_{x\to-\infty}f(x) = b.
\]
\end{enumerate}
\end{definition}

\begin{example}
The function $f(x) = \frac{1}{x-2} + 3$ has a vertical asymptote at $x=2$ and a horizontal asymptote at $y=3$.
Graph this function and give a graphical interpretation of the definition of horizontal and vertical asymptotes.
\end{example}

\begin{center}
\underline{\hspace{6in}}
\end{center}

\begin{questions}
\question
Consider the function $f(x) = \displaystyle \frac{x^3}{x^2-4}$.
\begin{parts}
\part Find the domain of $f$.

\begin{solution}
Solving $0=x^2-4$, we see that the denominator is zero when $x=\pm2$, so $f$ is defined at all real numbers except $2$ and $-2$.
\end{solution}

\part Compute the first and second derivatives of $f$.

\begin{solution}
\[
f'(x)
=
\frac{x^2(x^2-12)}{(x^2-4)^2},
\qquad
f''(x)
=
\frac{8x(x^2+12)}{(x^2-4)^3}
\]
\end{solution}

\part Find the intervals on which $f$ is positive or negative.

\begin{solution}
\[
0
=
f(x)
\Rightarrow
0
=
x^3
\Rightarrow
x
=
0
\]
Also, $f$ is discontinuous at $x=\pm2$.
\begin{center}
\begin{tabular}{c|c}
interval & $f$ \\
\hline
$(-\infty,-2)$ & $<0$ \\
\hline
$(-2,0)$ & $>0$ \\
\hline
$(0,2)$ & $<0$ \\
\hline
$(2,\infty)$ & $>0$
\end{tabular}
\end{center}
\end{solution}

\part Find all vertical and horizontal asymptotes of $f$.

\begin{solution}
We have $\lim_{x\to +\infty}f(x) = +\infty$ and $\lim_{x\to-\infty}f(x) = -\infty$, so there are no horizontal asymptotes.
We expect there to be vertical asymptotes at points where $f$ is undefined and, indeed, we find
$\lim_{x\to 2^-}f(x) = -infty$, $\lim_{x\to 2^+}f(x) = +\infty$, $\lim_{x\to -2^-}f(x) = -\infty$ and $\lim_{x\to -2^+}f(x) = +\infty$, so we have vertical asymptotes at {\color{red} $x=-2$ and $x=2$}.
\end{solution}

\part Find the intervals on which $f$ is increasing or decreasing.

\begin{solution}
\[
0
=
f'(x) 
\Rightarrow
0 = x^2(x^2-12)
\Rightarrow
x=0,\pm\sqrt{12}
\]
Also, $f'$ is discontinuous at $x=\pm2$.
\begin{center}
\begin{tabular}{c|c|c}
interval & $f'$ & $f$ \\
\hline
$(-\infty,-\sqrt{12})$ & $>0$ & increasing \\
\hline
$(-\sqrt{12},-2)$ & $<0$ & decreasing \\
\hline
$(-2,0)$ & $<0$ & decreasing \\
\hline
$(0,2)$ & $<0$ & decreasing \\
\hline
$(2,\sqrt{12})$ & $<0$ & decreasing \\
\hline
$(\sqrt{12},\infty)$ & $>0$ & increasing
\end{tabular}
\end{center}
\end{solution}

\part Find the $x$- and $y$-coordinates of each critical point of $f$ and indicate whether each such point is a local maximum, a local minimum or neither.

\begin{solution}
By the previous part, the critical points are 
\[
{\color{red} (-\sqrt{12},f(-\sqrt{12}))
= 
\left(-\sqrt{12},\frac{-(\sqrt{12})^3}{8}\right),
\quad
(0,f(0)) 
=
(0,0),
\quad
(\sqrt{12},f(\sqrt{12}))
=
\left(\sqrt{12},\frac{(\sqrt{12})^3}{8}\right)
}
\]
Also, looking at the table from the previous part, we see that $x=-\sqrt{12}$ is a local maximum, $x=\sqrt{12}$ is a local minimum and $x=0$ is neither.
\end{solution}

\part Find the intervals on which $f$ is concave-up or concave-down.

\begin{solution}
\[
0
=
f''(x)
\Rightarrow
0
=
8x(x^2+12)
\Rightarrow 
x
=
0
\]
since $x^2+12$ is never equal to $0$.
Also, $f''$ is discontinuous at $x=\pm2$.
\begin{center}
\begin{tabular}{c|c|c}
interval & $f''$ & $f$ \\
\hline
$(-\infty,-2)$ & $<0$ & concave-down \\
\hline
$(-2,0)$ & $>0$ & concave-up \\
\hline
$(0,2)$ & $<0$ & concave-down \\
\hline 
$(2,\infty)$ & $<0$ & concave-up
\end{tabular}
\end{center}
\end{solution}

\part Find the $x$- and $y$-coordinates of each inflection point of $f$.

\begin{solution}
By the previous part, since $x=-2$ and $x=2$ are not part of the domain of $f$, the only possible inflection point is $x=0$.
Since the concavity of $f$ does in fact change at $x=0$, it is an inflection point, and the corresponding $y$-coordinate is $f(0) = 0$.
\end{solution}

\part Sketch the graph of $f$ using the information collected in the previous parts of the problem.

\begin{solution}
\begin{center}
\includegraphics[scale=0.5]{118lecture12handoutgraphic1.png}w
\end{center}
\end{solution}

\end{parts}
\end{questions}

\end{document}
